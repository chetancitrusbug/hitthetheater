<div class="row">
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('showTitle') ? ' has-error' : ''); ?>">
            <?php echo Form::label('showTitle', '* Name: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::text('showTitle', null, ['class' => 'form-control input-sm', 'required' => 'required']); ?>

                <?php echo $errors->first('showTitle', '<p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('category') ? ' has-error' : ''); ?>">
            <?php echo Form::label('category', '* Category:', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::select('category[]', $category, isset($showCategory) ? $showCategory : [], ['id' => 'facilities','class' => 'form-control
                tags' ,'multiple' => 'multiple']); ?> <?php echo $errors->first('category', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('venue') ? ' has-error' : ''); ?>">
            <?php echo Form::label('venue', '* Venue For Show: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::select('venue',$venue,isset($show->venueCode) ? $show->venueCode : '',['class' => 'form-control tags' ]); ?> <?php echo $errors->first('venue', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
    <?php if(isset($show)): ?>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('showLink') ? ' has-error' : ''); ?>">
            <?php echo Form::label('showLink', 'Show Link: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
               <?php echo Form::text('showLink', $show->showLink, ['class' => 'form-control input-sm', 'disabled' =>'disabled']); ?>

            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('location') ? ' has-error' : ''); ?>">
            <?php echo Form::label('location', '* Location For Show: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::select('location',$locationcodes,isset($show->location) ? $show->location : '',['class' => 'form-control tags' ]); ?> <?php echo $errors->first('location', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('price_form') ? ' has-error' : ''); ?>">
            <label class="col-sm-3 control-label">* Price From : </label>
            <div class="col-sm-12">
                <input type="text" name="price_form" data-slider-value="[10,90]" data-slider-step="1" data-slider-max="500" data-slider-min="10"
                    value="<?php echo isset($show->priceFrom) ? $show->priceFrom : '' ;?>" class="form-control">            <?php echo $errors->first('price_form', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group <?php echo e($errors->has('price_to') ? ' has-error' : ''); ?>">
            <label class="col-sm-3 control-label">* Price To : </label>
            <div class="col-sm-12">
                <input type="text" name="price_to" data-slider-value="[10,90]" data-slider-step="1" data-slider-max="500" data-slider-min="10"
                    value="<?php echo isset($show->priceTo) ? $show->priceTo : '' ;?>" class="form-control"> <?php echo $errors->first('price_to', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('banner') ? ' has-error' : ''); ?>">
            <?php echo Form::label('banner', '* Banner For Show: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::file('banner',['class' => 'input-sm'] ); ?> <?php echo $errors->first('banner', '
                <p class="help-block">:message</p>'); ?>

                 <?php if(isset($banner->banner) && $banner->banner): ?>
                <a href=<?php echo e($banner->banner); ?> target="_blank" >
                <img src=<?php echo e($banner->banner); ?> alt="image" width="100">
                </a> <?php endif; ?>
            </div>

        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group<?php echo e($errors->has('poster') ? ' has-error' : ''); ?>">
            <?php echo Form::label('rectangle-poster', '* Rectangle Poster For Show: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">

                <?php echo Form::file('rectangle-poster',['class' => 'input-sm'] ); ?> <?php echo $errors->first('rectangle-poster', '
                <p class="help-block">:message</p>'); ?>


                <?php if(isset($rectanglePoster->poster) && $rectanglePoster->poster): ?>
                <a href=<?php echo e($rectanglePoster->poster); ?> target="_blank" >
                <img src=<?php echo e($rectanglePoster->poster); ?> alt="image" width="100">
                </a>
                <?php endif; ?>
            </div>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('bookingStarts') ? ' has-error' : ''); ?>">
            <?php echo Form::label('bookingStarts', '* Booking Starts:', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
            <input type="date" id="bookingStarts" class="form-control" name="bookingStarts" data-toggle="tooltip" data-trigger="hover" data-placement="top" data-title="Booking Starts" value="<?php echo e(isset($show->bookingStarts) ? $show->bookingStarts : ''); ?>">
                <?php echo $errors->first('bookingStarts', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('bookingEnds') ? ' has-error' : ''); ?>">
            <?php echo Form::label('bookingEnds', '* Booking Ends:', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <input type="date" id="bookingEnds" class="form-control" name="bookingEnds" data-toggle="tooltip" data-trigger="hover"
                    data-placement="top" data-title="Booking Ends" value="<?php echo e(isset($show->bookingEnds) ? $show->bookingEnds : ''); ?>">
            <?php echo $errors->first('bookingEnds', '<p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('metaTitle') ? ' has-error' : ''); ?>">
            <?php echo Form::label('metaTitle', '* Meta Title: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::text('metaTitle', isset($showMetadata->meta_title) ? $showMetadata->meta_title : '', ['class' => 'form-control input-sm']); ?> <?php echo $errors->first('metaTitle', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>

    <?php if(isset($show)): ?>
    <div class="col-md-5">
        <div class="form-group<?php echo e($errors->has('offer_ids') ? ' has-error' : ''); ?>">
            <?php echo Form::label('offer_ids', 'Offer:', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                
                <?php echo Form::select('offer_ids[]', $offer, isset($showoffer) ? $showoffer : [], ['id' => 'offer_ids','class' => 'form-control tags offer_id_selectbox' ,'multiple' => 'multiple']); ?> <?php echo $errors->first('offer_ids', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>


    <div class="col-md-1">
        <button type="button" class="plusoffers btn">+</button>
        <button type="button" class="minusoffers btn">-</button>
        </div>
    <?php endif; ?>

</div>
<?php if(isset($showsingleOfferSelection) && count($showsingleOfferSelection) > 0): ?>
<div class="row">

    <div class="col-md-12">
        <div class="form-group<?php echo e($errors->has('offer_id') ? ' has-error' : ''); ?>">
            <?php echo Form::label('offer_id', '* Single Offer for List: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::select('offer_id', $showsingleOfferSelection, null , ['class' => 'form-control
                tags' ]); ?> <?php echo $errors->first('offer_id', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<div class="row">

    <div class="col-md-12">
        <div class="form-group<?php echo e($errors->has('description') ? ' has-error' : ''); ?>">
            <?php echo Form::label('description', '* Description: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::textarea('description',isset($show->description) ? $show->description : '', ['class' => 'form-control input-sm',
                'id'=>'description','required' => 'required']); ?> <?php echo $errors->first('description', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group<?php echo e($errors->has('summary') ? ' has-error' : ''); ?>">
            <?php echo Form::label('summary', '* Summary: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::textarea('summary', isset($show->summary) ? $show->summary : '' , ['class' => 'form-control input-sm', 'id'=>'summary','required'
                => 'required']); ?> <?php echo $errors->first('summary', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group<?php echo e($errors->has('metaDescription') ? ' has-error' : ''); ?>">
            <?php echo Form::label('metaDescription', '* Meta Description: ', ['class' => 'col-sm-3 control-label']); ?>

            <div class="col-sm-12">
                <?php echo Form::textarea('metaDescription',isset($showMetadata->meta_description) ? $showMetadata->meta_description : '', ['class' => 'form-control input-sm',
                'id'=>'metaDescription','required' => 'required']); ?> <?php echo $errors->first('metaDescription', '
                <p class="help-block">:message</p>'); ?>

            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('js'); ?>
    <script>
        $(".tags").select2({tags: true, width: '100%'});
        $('#description').summernote({
            height: 250
        });
        $('#metaDescription').summernote({ height: 250 });
        $('#summary').summernote({
            height: 250
        });
    </Script>
<?php $__env->stopPush(); ?>

