<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border"
    role="navigation" data-menu="menu-wrapper">
    <!-- Horizontal menu content-->
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include ../../../includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li  class="nav-item <?php echo e(($route == 'dashboard')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/dashboard')); ?>"><i class="ft-home"></i><span>Dashboard</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'users')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/users')); ?>"><i class="ft-users"></i><span>Users</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'location')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/location')); ?>"><i class="ft-navigation"></i><span>Location</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'category')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/category')); ?>"><i class="ft-tag"></i><span>Category</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'venue')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/venue')); ?>"><i class="ft-map-pin"></i><span>Venue</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'show')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/show')); ?>"><i class="ft-film"></i><span>Show</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'setting')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/setting')); ?>"><i class="ft-settings"></i><span>Setting</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'cms_setting')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/cms_setting')); ?>"><i class="ft-book"></i><span>CMS Setting</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'contactus')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/contactus')); ?>"><i class="ft-clipboard"></i><span>Contact Us</span></a>
            </li>

            <li  class="nav-item <?php echo e(($route == 'banner')?'active':''); ?>"><a class="dropdown-toggle nav-link"   href="<?php echo e(url('admin/banner')); ?>"><i class="ft-maximize"></i><span>Banner</span></a>
            </li>

        </ul>
    </div>
    <!-- /horizontal menu content-->
</div>