<?php $__env->startSection('title',"Edit Show"); ?>

<?php $__env->startSection('content'); ?>
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo e(url('admin/dashboard')); ?>">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="<?php echo e(url('admin/show')); ?>">show</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"><?php echo e($moduleName); ?></h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="col-md-6">
                            <div class="form-group col-sm-6">
                                <a href="<?php echo e(url('/admin/show')); ?>" title="Back">
                                    <button class="btn btn-space btn-warning">Back</button>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
           <?php echo Form::model($show, ['method' => 'PATCH','url' => ['/admin/show', $show->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]); ?>

                <?php echo $__env->make('admin.show.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="col-md-6">
                <div class="form-group col-sm-4">
                    <?php echo Form::submit('Update', ['class' => 'btn btn-space btn-primary']); ?>

                </div>
                </div>
            <?php echo Form::close(); ?>

        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>


        <div class="modal fade" id="offeraddmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Add Offer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="add_offer_form" name="add_offer_form">
                                        <?php if(isset($show)): ?>

                                        <input name="showLink" id="showLink" type="hidden" value="<?php echo e($show->showLink); ?>"> <?php echo e(csrf_field()); ?>

                                        <?php endif; ?>

                                        <span class="errmsgsoffer"></span>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer ID: </label>
                                                        <div class="col-sm-12">
                                                            <input type="number" name="offer_id" id="add_offer_id" class="form-control">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Name: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="title" id="add_title" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Start Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="start_date" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer End Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="end_date" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer valid: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="valid" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer type: </label>
                                                        <div class="col-sm-12">
                                                            <select name="type" class="form-control">
                                                                    <option value="single_facevalue">Single Facevalue</option>
                                                                    <option value="multiple_facevalue">Multiple Facevalue</option>
                                                                    <option value="no_booking_multiple_facevalue">No Booking Multiple Facevalue</option>
                                                                </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer facevalue: </label>
                                                        <div class="col-sm-12">
                                                            <input type="number" name="facevalue" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer price: </label>
                                                        <div class="col-sm-12">
                                                            <input type="number" name="price" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer description: </label>
                                                        <div class="col-sm-12">
                                                            <textarea name="description" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="offerminusmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Delete Offer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="minus_offer_form" name="minus_offer_form">

                                        <span class="errmsgsoffer"></span>

                                        <div class="form-group prepend-top">
                                            <div class="row offer_div">

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('js'); ?>
<script>
    $(".tags").select2({tags: true, width: '100%'});
        $('#description').summernote({
            height: 250
        });
        $('#metaDescription').summernote({ height: 250 });
        $('#summary').summernote({
            height: 250
        });

        $(document).ready(function (e) {
           listdrow();
        });
        $(document).on('click', '.plusoffers', function (e) {
            $("#add_offer_form")[0].reset();
            $('.errmsgsoffer').html('');
            $('#offeraddmodel').modal('show');
        });

        $(document).on('click', '.minusoffers', function (e) {
            listdrow();
            $('#offerminusmodel').modal('show');
            $("#minus_offer_form")[0].reset();
            $('.errmsgsoffer').html('');
        });


        function listdrow() {
            $('.offer_div').html('');
            $('.offer_id_selectbox').html('');
            var show_link = '<?php echo e($show->showLink); ?>';
            var url = '<?php echo e(url("/admin/offerlist/")); ?>/'+show_link;
            var method = "get";
            $.ajax({
                type: method,
                url: url,
                beforeSend: function () {
                },
                success: function (formdata)
                {
                    console.log(formdata);
                    if (formdata.offersStr != '') {
                        $('.offer_id_selectbox').append(formdata.offersStr);
                        $('.minusoffers').show();
                    }
                    else
                    {
                        $('.minusoffers').hide();
                    }

                    if (formdata.offerDeletedStr != '') {
                        $('.offer_div').append(formdata.offerDeletedStr);
                    }
                    else{
                        $('.minusoffers').hide();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $('#add_offer_form').validate(
            {
                rules: {
                    title: { required: true },
                    type: { required: true },
                    valid: { required: true },
                    start_date: { required: true },
                    end_date: { required: true },
                    facevalue: { required: true },
                    price: { required: true },
                    description: { required: true },
                    offer_id: { required: true }
                },
                messages: {
                    title: { required:  'Please enter title', },
                    type: { required: 'Please Select type', },
                    valid: { required: 'Please enter valid', },
                    start_date: { required: 'Please enter start date', },
                    end_date: { required: 'Please enter end date', },
                    facevalue: { required: 'Please enter facevalue', },
                    price: { required: 'Please enter price', },
                    description: { required: 'Please enter description', },
                    offer_id: { required: 'Please enter offer id', }

                },
                errorPlacement: function(error, element)
                {
                    error.insertAfter( element );
                },
                submitHandler: function (form) {
                    var addformdata  =$('#add_offer_form').serialize();
                    var url = '<?php echo e(url('/admin/offeradd')); ?>';
                    var method = "post";
                    add_offer_id =$('#add_offer_id').val();
                    title = $('#add_title').val();
                    $.ajax({
                        type: method,
                        url: url,
                        data: $('#add_offer_form').serialize(),
                        beforeSend: function () {
                        },
                        success: function (formdata)
                        {

                            if(formdata.code == '200')
                            {
                                $('#offeraddmodel').modal('hide');
                                $('.minusoffers').show();
                                listdrow();
                            }
                            else
                                $('.errmsgsoffer').html(formdata.message);
                        },
                        error: function (error) {
                            $('#offeraddmodel').modal('hide');
                        // toastr.error(data.message);
                        }

                    });
                    return false;

                }
        });

        $(document).on('click', '.offer_btn', function (e) {

            // var offer_btn_div = $(".offer_div").length;

            var offer_id = $(this).data('id');
            var div = $(this).parent('div');
            var url = '<?php echo e(url("/admin/offerdelete/")); ?>/'+offer_id;
            var method = "get";
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: method,
                        url: url,
                        beforeSend: function () {
                        },
                        success: function (formdata)
                        {

                            if(formdata.code == '200')
                            {
                                listdrow();
                                $('#offerminusmodel').modal('hide');
                                swal({ title: 'Success!', type: 'success' });

                                //$(".offer_id_selectbox").append('<option value="'+add_offer_id+'">'+title+'</option>');
                            }
                            else
                                swal("Cancel", formdata.message, "error");
                        },
                        error: function (error) {
                            $('#offerminusmodel').modal('hide');
                        }

                    });
                } else {
                    swal("Cancel", "", "error");
                }
            });

        });
</Script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>