<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <link rel="shortcut icon" href="<?php echo e(asset('frontend/images/logo.png')); ?>">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo $__env->yieldContent('title'); ?> <?php echo e(config('app.name')); ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('frontend/images/logo.png')); ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
            crossorigin="anonymous">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/vendors.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/switchery.min.css')); ?>">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')); ?>"
    />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/app.css')); ?>">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/horizontal-menu.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/palette-gradient.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/switch.css')); ?>">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/style.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/css/datatables.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/lib/select2/select2.css')); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('backend/lib/summernote/summernote.css')); ?>" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />
    
    <style type="text/css">
        .error{
            color: #ff0000
        }
    </style>

    <?php echo $__env->yieldPushContent('css'); ?>
</head>

<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover" data-menu="horizontal-menu" data-col="2-columns">
    <!-- fixed-top-->
    
    <?php echo $__env->make('include.admin.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <!-- Horizontal navigation-->
    <?php echo $__env->make('include.admin.top-nav-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- Horizontal navigation-->
    <div class="app-content content">
        <div class="content-wrapper">
                <?php echo $__env->make('include.admin.alerts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo $__env->yieldContent('content'); ?>
        </div>
    </div>
    
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="#" >Hit The Theater </a>, All rights reserved. </span>
        </p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="<?php echo e(asset('backend/js/vendors.min.js')); ?>" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="<?php echo e(asset('backend/js/jquery.sticky.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('backend/js/jquery.sparkline.min.js')); ?>"></script>
    <!-- END PAGE VENDOR JS-->

    <script src="<?php echo e(asset('backend/js/bootstrap-checkbox.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/js/switchery.min.js')); ?>" type="text/javascript"></script>


    <!-- BEGIN STACK JS-->

    <script src="<?php echo e(asset('backend/js/app-menu.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/js/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/js/customizer.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/js/dev.js')); ?>" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="<?php echo e(asset('backend/js/breadcrumbs-with-stats.js')); ?>"></script>
    <!-- END PAGE LEVEL JS-->
    <script src="<?php echo e(asset('backend/js/datatables.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/js/switch.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/lib/select2/select2.js')); ?>" type="text/javascript">
    </script>
    <script src="<?php echo e(asset('backend/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/lib/summernote/summernote.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('backend/lib/summernote/summernote-ext-beagle.js')); ?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){



            //initialize the javascript
            // App.init();
            // App.dashboard();
            setTimeout(function(){
                jQuery('.alert-dismissible').hide();
            }, 10000);

            $(".datetimepicker").datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                componentIcon: '.mdi.mdi-calendar',
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });


        });
    </script>
    <?php echo $__env->yieldPushContent('js'); ?>
</body>

</html>


