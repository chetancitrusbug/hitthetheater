<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempVenueFacilities extends Model
{
    protected $table = 'tmp_venue_facilities';

    protected $fillable = [
         'venueId','facilitieId','venueCode'
    ];
}
