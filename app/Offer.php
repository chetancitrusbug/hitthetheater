<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    //

    protected $table = 'offers';

    protected $fillable = [
         'title','type', 'valid','start_date','end_date','offer_id','exclusive','facevalue','price','description','showLink','created_by'
    ];

}
