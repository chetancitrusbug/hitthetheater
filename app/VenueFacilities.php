<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueFacilities extends Model
{
    protected $table = 'venue_facilities';

    protected $fillable = [
         'venueId','facilitieId','venueCode'
    ];
}
