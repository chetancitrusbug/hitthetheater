<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempCategory extends Model
{
    protected $table = 'tmp_category';

    protected $fillable = [
         'categoryTitle', 'status'
    ];
}
