<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowLog extends Model
{
   
    protected $table = 'show_log';

    protected $fillable = [
        'code', 'key', 'value'
    ];
   
}
