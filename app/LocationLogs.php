<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationLogs extends Model
{
    //
    protected $table = 'location_logs';

    protected $fillable = [
         'code','key','value'
    ];
}
