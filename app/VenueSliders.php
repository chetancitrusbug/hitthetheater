<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueSliders extends Model
{
    protected $table = 'venue_sliders';

    protected $fillable = [
         'venueId','image','type','title','width','height','venueCode'
    ];

}
