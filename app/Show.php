<?php

namespace App;
ini_set('max_execution_time', 300);
use Illuminate\Database\Eloquent\Model;
use App\Show_Images;
use App\ShowOffers;
use App\Offer;

class Show extends Model
{
    protected $table = 'show';

    protected $fillable = [
        'showTitle', 'indexName', 'showLink', 'bookingStarts', 'bookingEnds', 'isEvent', 'summary', 'description', 'priceFrom', 'priceTo', 'onSaleDate', 'runningTime', 'ageRestriction', 'limitedStock', 'whitelabelURI', 'importantInfo', 'bannerBackgroundColour', 'showType','venueCode','popular','showStatus','createdby','updatedby','orderby','offer_id','youtube_link','iframe_display','slug','bookingOpen'
    ];

    protected $appends =['rectangle_image','square_image'];


    public function Category()
    {
        return $this->belongsToMany('App\Show','show_category','show_id','category_id');
    }

     public function venue()
    {
        return $this->belongsTo('App\Venue','venueCode','venueCode');
    }

     public function imageShow()
    {
        return $this->hasMany('App\Show_Images','show_id','id');
    }

    public function showImages()
    {
        return $this->hasMany('App\Show_Images','showLink','showLink');
    }



    public function getRectangleImageAttribute()
    {
        $show_images=Show_Images::where('showLink',$this->showLink)->where('type','rectangle-poster')->orderBy('id','desc')->first();
        //dd($show_images);
        if($show_images != null)
        {


            $path=public_path().'/storage/show'.$show_images->url;

            if(file_exists($path))
            {
                $path=url('').'/storage/show'.$show_images->url;
                return $path;
            }
            elseif(@fopen($show_images->url,'r'))
            {
                return $show_images->url;

            }
            else
            {
                return url('').'\frontend\images\no_image.png';
            }

        }
        else
        {

            return url('').'\frontend\images\no_image.png';
        }
    }

    public function getSquareImageAttribute()
    {
        $show_images=Show_Images::where('showLink',$this->showLink)->where('type','square-poster')->orderBy('id','desc')->first();
        //dd($show_images);
        if($show_images != null)
        {

                return $show_images->url;

        }

                return '';
    }

}
