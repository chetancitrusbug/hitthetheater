<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempShow extends Model
{
    protected $table = 'tmp_show';

    protected $fillable = [
        'showTitle', 'indexName', 'showLink', 'bookingStarts', 'bookingEnds', 'isEvent', 'summary', 'description', 'priceFrom', 'priceTo', 'onSaleDate', 'runningTime', 'ageRestriction', 'limitedStock', 'whitelabelURI', 'importantInfo', 'bannerBackgroundColour', 'showType','resource_image','venueCode','categories'
    ];
}
