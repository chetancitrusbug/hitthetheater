<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

class AuthenticateClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /* if(!(auth()->check()) && !$request->has('login')){
            return redirect(url(request()->path()).'?login');
        } */
        return $next($request);
    }
}
