<?php

namespace App\Http\Middleware;
use App\Category;
use App\CmsSetting;
use App\Setting;
use Closure;

class Frontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headercategory=CmsSetting::first();
        $footer_display = Setting::where('key', 'footer_display')->first();

        if(isset($footer_display) && $footer_display->value != null)
        {
            $footer_display = $footer_display->value;
        }
        else {
            $footer_display = 0;
        }

        $headercategory = explode('|',$headercategory->category_for_header);
        $headercategory = Category::whereIn('id',$headercategory)->where('status',1)->get();
        // dd($headercategory);
        if (count($headercategory) > 0) {
           $headercategory->toArray();
        }
        else {
            $headercategory = [];
        }
        view()->composer('*', function ($view) use ($headercategory,$footer_display) {
            $view->with(compact('headercategory','footer_display'));
        });
        return $next($request);
    }
}
