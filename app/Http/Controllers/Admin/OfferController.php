<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Offer;
use App\ShowOffers;
use App\Show;


class OfferController extends Controller
{

    public function offeradd(Request $request)
    {
         $rules = array(
            'start_date' => 'required|date|after:today',
            'end_date' => 'required|date|after:start_date',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            return response()->json(['message' => $messages, 'code' => 400], 200);
        }

        $requestData = $request->all();
        $requestData['created_by'] = 1;

        $existOffer = Offer::where('offer_id', $request->offer_id)->first();

        if ($existOffer != null) {
            return response()->json(['message' => 'Offer already exist Please enter another offer id', 'code' => 400], 200);
        }

        $Offer = Offer::create($requestData);

        $data = array();
        $data['showLink'] = $request->showLink;
        $data['offer_id']=$Offer->offer_id;
        ShowOffers::create($data);
        $message = 'Offer added!';
        $offers = Offer::where('showLink', $request->showLink)->pluck('title', 'offer_id');

        return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function offerupdate(Request $request)
    {
        $rules = array(
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            return response()->json(['message' => $messages, 'code' => 400], 200);
        }

        $requestData = $request->all();

        $existOffer = Offer::where('offer_id', $request->offer_id)->first();

        if ($existOffer == null) {
            return response()->json(['message' => 'Offer not exist', 'code' => 400], 200);
        }

        $existOffer = $existOffer->update($requestData);
        $message = 'Offer updated!';
        $offers = Offer::where('showLink', $request->showLink)->pluck('title', 'offer_id');
        return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function offerdelete(Request $request,$offer_id)
    {

        $existOffer = Offer::where('offer_id', $offer_id)->first();
        if ($existOffer != null) {
            $showoffer = ShowOffers::where('offer_id', $offer_id)->first();
            if ($showoffer != null) {
                $showoffer->delete();
            }
            $existOffer->delete();
        }

        $message = 'Offer deleted!';


        return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function offerdetail(Request $request,$offer_id)
    {
        $code = 200;
        $message = '';
        $data = null;

        $existOffer = ShowOffers::join('offers','offers.offer_id','show_offers.offer_id')->where('show_offers.offer_id', $offer_id)->first();
        if ($existOffer != null) {
            $data = $existOffer;
        }
        else {
            $message = 'Offer not exist!';
            $code = 400;
        }
        return response()->json(['data'=>$data,'message' => $message,'code'=>$code], 200);
    }

    public function offerlist(Request $request,$location,$show_link)
    {
        $offerDeleted = Offer::where('showLink', $location.'/'.$show_link)->get();
        $show = Show::where('showLink', $location . '/' . $show_link)->first();
        $showoffer = ShowOffers::where('showLink', $show->showLink)->pluck('offer_id');

        if ($showoffer != null) {
           $showoffer = $showoffer->toArray();
        }


        $offerDeletedStr = '';
        $offersStr = '';
        $offerSingleStr = '<option value"  >Select Offer</option>';
        foreach ($offerDeleted as $key => $item)
        {
           $offerDeletedStr .= '<div class="col-md-12 offer_btn_div">
                <span>'.$item->title ." - #". $item->offer_id.'</span>
                <button type="button" data-id="'.$item->offer_id.'" class="btn offer_btn" > <span aria-hidden="true">&times;</span> </button>
                 <button type="button" data-id="'.$item->offer_id.'" class="btn edit_offer_btn" > <i class="ft-edit"></i> </button>
            </div>';

            $selected = (isset($showoffer) && (in_array($item->offer_id , $showoffer))) ? 'selected' : '';
            $offersStr .= '<option value="'.$item->offer_id.'" '.$selected.' >'.$item->title .'</option>';

            $selected = (isset($show->offer_id) && ($show->offer_id == $item->offer_id)) ? 'selected' : '';
            $offerSingleStr .= '<option value="'.$item->offer_id.'" '.$selected.' >'.$item->title .'</option>';
        }


        return response()->json(['offerDeletedStr'=>$offerDeletedStr,'offersStr'=>$offersStr ,'offerSingleStr'=>$offerSingleStr], 200);
    }
}
