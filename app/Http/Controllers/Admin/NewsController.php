<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Session;
use File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'news');
        view()->share('moduleName','News');
    }

    public function index(Request $request)
    {

        return view('admin.news.index');
    }

    public function datatable(Request $request)
    {
        if($request->has('isFuture')){
            $news = News::where('isFuture',$request->isFuture)->orderby('orderby', 'desc')->get();
        }
        else {
            $news = News::orderby('id', 'desc')->get();
        }

        return datatables()->of($news)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.news.create');
    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $rule = [
            'title' => 'required|unique:news',
            'description' => 'required',
            'slug' => 'required|unique:news',
            'publishDate' => 'required|date',
            'featuredImage' => 'required|mimes:jpg,jpeg,png|max:1000',
        ];
        $requestData = $request->all();

        $this->validate($request, $rule);

        $orderbymaxvalue = News::max('orderby');
        $data['slug'] = $this->slugCreate($request->slug);

        $data['title'] = $request->title;
        $data['publishDate'] = date('Y-m-d',strtotime($request->publishDate));
        $data['description'] = $request->description;
        $data['isFuture'] = isset($request->isFuture)?1:0;
        if ($data['isFuture'] == 1) {
            $data['orderby'] = $orderbymaxvalue+1;
        }

        $path = storage_path('app/public');
        $destinationPath = $path. '/news/';
        if($request->hasFile('featuredImage')){
            $featuredImage = $request->file('featuredImage');
            $fileName = $this->getImageName($featuredImage);
            $featuredImage->move($destinationPath . '/' , $fileName);
            $data['featuredImage'] =  '/' . $fileName;
        }
        else{
            $data['featuredImage'] = '';
        }

        $news = News::create($data);

        Session::flash('flash_success', 'News added!');
        return redirect('admin/news');
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$news = News::where('id',$id)->first();
        if ($news) {
            return view('admin.news.edit', compact('news'));
        } else {
            Session::flash('flash_warning', 'News is not exist!');
            return redirect('admin/news');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rule = [
            'title' => 'required|unique:news,title,'.$id,
            'description' => 'required',
            'slug' => 'required|unique:news,slug,'.$id,
            'publishDate' => 'required|date',
            'featuredImage' => 'sometimes|mimes:jpg,jpeg,png|max:1000',
        ];
        $requestData = $request->all();

        $this->validate($request, $rule);



        $news = News::where('id',$id)->first();

        $requestData['status'] = isset($request->status)?1:0;
        $requestData['publishDate'] = date('Y-m-d',strtotime($request->publishDate));
        $requestData['isFuture'] = isset($request->isFuture)?1:0;
        $requestData['title'] = $request->title;
        $requestData['description'] = $request->description;

        $path = storage_path('app/public');
        $destinationPath = $path. '/news/';
        if($request->hasFile('featuredImage')){
            $featuredImage = $request->file('featuredImage');
            $fileName = $this->getImageName($featuredImage);
            $featuredImage->move($destinationPath . '/' , $fileName);
            $requestData['featuredImage'] =  '/' . $fileName;
        }

        $news->update($requestData);

        Session::flash('flash_success', 'News updated!');
        return redirect('admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $news = News::find($id);
        $news->delete();
        if($request->has('from_index')){
            $message = "news Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'News deleted!');

            return redirect('admin/news');
        }

    }
    public function orderbyStatus($id,$orderbyvalue)
    {

        $news = News::find($id);
        if ($news == null) {
            return response()->json(['message' => 'News not Found!'],400);
        }
        $newsOrderbyValue = $news->orderby;
        $changeOrderbyValue = $orderbyvalue;

        if ($newsOrderbyValue == $changeOrderbyValue) {
            return response()->json(['message' => 'News Update'],200);
        }
        else {
            $orderbynews = News::where('orderby', $changeOrderbyValue)->first();
            if ($orderbynews == null) {
                $news->orderby = $orderbyvalue;
                $news->save();
            }
            else {
                $orderbynews->orderby = $newsOrderbyValue;
                $orderbynews->save();

                $news->orderby = $orderbyvalue;
                $news->save();
            }
             return response()->json(['message' => 'News Update'],200);
        }
    }
}
