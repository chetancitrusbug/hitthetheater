<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CmsSetting;
use App\Category;
use Session;

class CmsSettingController extends Controller
{
    //

    public function __construct(){
        view()->share('route', 'cms_setting');
        view()->share('moduleName','Cms-Setting');
    }

    public function index(Request $request)
    {
        $cms_setting=CmsSetting::first();
        $category_for_header = Category::where('status', '1')->pluck('categoryTitle', 'id');
        $cms_setting->category_for_header = explode('|',$cms_setting->category_for_header);
        // dd($cms_setting);
        return view('admin.cms_setting.index',compact('cms_setting','category_for_header'));
    }

    public function post_cms_setting(Request $request)
    {
        $rule = [
            'category_for_header' => 'required|array|min:5|max:5'
        ];

        $this->validate($request, $rule,[]);

        $category = implode('|',$request->category_for_header);
        // dd($category);
        $cms_setting=CmsSetting::first();
        if($cms_setting != null)
        {
            $cms_setting->about_us=$request->about_us;
            $cms_setting->contact_us_email=$request->email;
            $cms_setting->contact_us_phone=$request->contact_us_phone;
            $cms_setting->contact_us_address=$request->contact_us_address;
            $cms_setting->contact_us_email_tamplate=$request->contact_us_email_tamplate;
            $cms_setting->contact_us=$request->contact_us_details;
            $cms_setting->banner=$request->banner;
            $cms_setting->faq =$request->faq;
            $cms_setting->category_for_header=$category;
            $cms_setting->termandcondition =$request->termandcondition;
            $cms_setting->privacypolicy =$request->privacypolicy;
            $cms_setting->cookiepolicy =$request->cookiepolicy;
            $cms_setting->facebook_link =$request->facebook_link;
            $cms_setting->twitter_link =$request->twitter_link;
            $cms_setting->youtube_link =$request->youtube_link;
            $cms_setting->instagram_link =$request->instagram_link;
            $cms_setting->copy_right_content =$request->copy_right_content;
            $cms_setting->about_us_display = ($request->about_us_display == 'on') ? 1 : 0;
            $cms_setting->faq_display = ($request->faq_display == 'on') ? 1 : 0;
            $cms_setting->contact_us_display = ($request->contact_us_display == 'on') ? 1 : 0;
            $cms_setting->termandcondition_display = ($request->termandcondition_display == 'on') ? 1 : 0;
            $cms_setting->privacypolicy_display = ($request->privacypolicy_display == 'on') ? 1 : 0;
            $cms_setting->cookiepolicy_display = ($request->cookiepolicy_display == 'on') ? 1 : 0;
            $cms_setting->contact_us_address_display = ($request->contact_us_address_display == 'on') ? 1 : 0;
            $cms_setting->facebook_link_display = ($request->facebook_link_display == 'on') ? 1 : 0;
            $cms_setting->twitter_link_display = ($request->twitter_link_display == 'on') ? 1 : 0;
            $cms_setting->youtube_link_display = ($request->youtube_link_display == 'on') ? 1 : 0;
            $cms_setting->instagram_link_display = ($request->instagram_link_display == 'on') ? 1 : 0;
            $cms_setting->copy_right_content_display = ($request->copy_right_content_display == 'on') ? 1 : 0;
            $cms_setting->trust_factor_display = ($request->trust_factor_display == 'on') ? 1 : 0;
            $cms_setting->trust_factor =$request->trust_factor;
            $cms_setting->save();
        }
        else
        {
            $cms_setting=new CmsSetting();
            $cms_setting->about_us=$request->about_us;
            $cms_setting->contact_us_email=$request->email;
            $cms_setting->contact_us_phone=$request->contact_us_phone;
            $cms_setting->contact_us_address=$request->contact_us_address;
            $cms_setting->contact_us_email_tamplate=$request->contact_us_email_tamplate;
            $cms_setting->contact_us=$request->contact_us_details;
            $cms_setting->banner=$request->banner;
            $cms_setting->faq = $request->faq;
            $cms_setting->termandcondition = $request->termandcondition;
            $cms_setting->privacypolicy = $request->privacypolicy;
            $cms_setting->cookiepolicy = $request->cookiepolicy;
            $cms_setting->facebook_link =$request->facebook_link;
            $cms_setting->twitter_link =$request->twitter_link;
            $cms_setting->youtube_link =$request->youtube_link;
            $cms_setting->instagram_link =$request->instagram_link;
            $cms_setting->copy_right_content =$request->copy_right_content;
            $cms_setting->about_us_display = ($request->about_us_display == 'on') ? 1 : 0;
            $cms_setting->faq_display = ($request->faq_display == 'on') ? 1 : 0;
            $cms_setting->contact_us_display = ($request->contact_us_display == 'on') ? 1 : 0;
            $cms_setting->termandcondition_display = ($request->termandcondition_display == 'on') ? 1 : 0;
            $cms_setting->privacypolicy_display = ($request->privacypolicy_display == 'on') ? 1 : 0;
            $cms_setting->cookiepolicy_display = ($request->cookiepolicy_display == 'on') ? 1 : 0;
            $cms_setting->contact_us_address_display = ($request->contact_us_address_display == 'on') ? 1 : 0;
            $cms_setting->facebook_link_display = ($request->facebook_link_display == 'on') ? 1 : 0;
            $cms_setting->twitter_link_display = ($request->twitter_link_display == 'on') ? 1 : 0;
            $cms_setting->youtube_link_display = ($request->youtube_link_display == 'on') ? 1 : 0;
            $cms_setting->instagram_link_display = ($request->instagram_link_display == 'on') ? 1 : 0;
            $cms_setting->copy_right_content_display = ($request->copy_right_content_display == 'on') ? 1 : 0;
            $cms_setting->trust_factor_display = ($request->trust_factor_display == 'on') ? 1 : 0;
            $cms_setting->trust_factor =$request->trust_factor;
            $cms_setting->save();
        }

        $path = storage_path('app/public');
        $destinationPath = $path. '/cms/';
        if ($request->hasFile('logo')) {
            $image = $request->file('logo');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' , $fileName);
            $cms_setting->logo =  '/' .$fileName;
            $cms_setting->save();
        }

        Session::flash('flash_success', 'CMS Setting Update');
        return redirect('admin/cms_setting');
    }
}
