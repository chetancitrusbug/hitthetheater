<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\VenueCategories;
use App\VenueCategory;
use App\ShowCategory;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'category');
        view()->share('moduleName','Category');
    }

    public function index(Request $request)
    {

        return view('admin.category.index');
    }

    public function datatable(Request $request)
    {
        $category = Category::orderby('id','desc')->get();
        return datatables()->of($category)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'categoryTitle' => 'required|unique:categories|max:40',
            'slug' => ['required', \Illuminate\Validation\Rule::unique('categories', 'slug')]
        ];

        $this->validate($request, $rule);
        $data['categoryTitle'] = $request->categoryTitle;
        $data['slug'] = $this->slugCreate($request->slug);
        $category = Category::create($data);

        Session::flash('flash_success', 'Category added!');
        return redirect('admin/category');
    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$category = Category::where('id',$id)->first();
        if ($category) {
            return view('admin.category.edit', compact('category'));
        } else {
            Session::flash('flash_warning', 'Category is not exist!');
            return redirect('admin/category');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'categoryTitle' => 'required|max:40',
            'slug' => 'required|unique:categories,slug,'.$id,
        ]);

        $requestData = $request->all();

        $category = Category::where('id',$id)->first();

        $requestData['status'] = isset($requestData['status'])?1:0;
        $requestData['slug'] = $this->slugCreate($request->slug);
        $category->update($requestData);

        Session::flash('flash_success', 'Category updated!');
        return redirect('admin/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $category = Category::find($id);
        if ($category != null) {
            $venueExistcategory = ShowCategory::where('category_id', $id)->get();
            if (count($venueExistcategory) > 0 ) {
                    return response()->json(['message' => 'Already used in another show.you can not delete this category.'], 200);
                }
            $category->delete();
        }
        if($request->has('from_index')){
            $message = "category Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Category deleted!');

            return redirect('admin/category');
        }

    }


    public function categoryadd(Request $request)
    {
        $requestData = $request->all();
        $requestData['status'] = 1;

        $category = VenueCategories::where('categoryTitle', $requestData['categoryTitle'])->first();
        if ($category == null) {
            $category = VenueCategories::create($requestData);
        }
        else {
            return response()->json(['message' => 'Already added', 'code' => 400], 200);
        }

        $data = array();
        $data['venueCode'] = $request->venueCode;
        $data['category_id'] = $category->id;
        VenueCategory::create($data);

        $message = 'Category added!';

        return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function categoryupdate(Request $request)
    {
        $requestData = $request->all();
        $requestData['status'] = 1;


        $category = VenueCategories::where('id', $requestData['category_id'])->first();

        if ($category == null) {
            return response()->json(['message' => 'Category not exist', 'code' => 400], 200);
        }

        $category = $category->update($requestData);

        $message = 'Category updated!';

       return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function categorydelete(Request $request,$category_id,$venueCode)
    {

        $existcategory = VenueCategories::where('id', $category_id)->first();

        if ($existcategory != null) {
            $venueExistcategory = VenueCategory::where('category_id', $category_id)->where('venueCode', '!=', $venueCode)->get();

            if (count($venueExistcategory) > 0 ) {
                return response()->json(['message' => 'Already used in another venue.you can not delete this category.','code'=>400], 200);
            }
            $existcategory->delete();
            $category = VenueCategory::where('category_id', $category_id)->first();
            if ($category != null) {
                $category->delete();
            }
        }
        $message = 'Category deleted!';
        return response()->json(['message' => $message,'code'=>200], 200);
    }

    public function categorydetail(Request $request,$category_id)
    {
        $code = 200;
        $message = '';
        $data = null;

        $existcategory = VenueCategories::where('id', $category_id)->first();
        if ($existcategory != null) {
            $data = $existcategory;
        }
        else {
            $message = 'Category not exist!';
            $code = 400;
        }
        return response()->json(['data'=>$data,'message' => $message,'code'=>$code], 200);
    }

    public function categorylist(Request $request,$venueCode)
    {
        $allCategory = VenueCategories::where('status', 1)->get();
        $venueCategory = VenueCategory::select('venue_categories.*','venue_category.*','venue_category.id as main_cat_id')->join('venue_categories','venue_categories.id','venue_category.category_id')->where('venue_category.venueCode', $venueCode)->get();


        $categoryDeletedStr = '';
        $categoryStr = '';
        $ids= array();

        foreach ($venueCategory as $key => $item)
        {
            $ids[] = $item->category_id;
            $categoryDeletedStr .= '<div class="col-md-12 category_btn_div">
                <span>' . $item->categoryTitle . " - #" . $item->category_id . '</span>
                <button type="button" data-id="' . $item->category_id . '" class="btn category_btn" > <span aria-hidden="true">&times;</span> </button>
                 <button type="button" data-id="' . $item->category_id . '" class="btn edit_category_btn" > <i class="ft-edit"></i> </button>
            </div>';
        }

        foreach ($allCategory as $key => $item)
        {
            $selected = (isset($ids) && (in_array($item->id , $ids))) ? 'selected' : '';
            $categoryStr .= '<option value="'.$item->id.'" '.$selected.' >'.$item->categoryTitle .'</option>';
        }


        return response()->json(['categoryDeletedStr'=>$categoryDeletedStr,'categoryStr'=>$categoryStr], 200);
    }
}
