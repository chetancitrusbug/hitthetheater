<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use App\LocationLogs;
use Session;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'location');
        view()->share('moduleName','Location');
    }

    public function index(Request $request)
    {

        return view('admin.locations.index');
    }

    public function datatable(Request $request)
    {
        $locations = Location::get();
        return datatables()->of($locations)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'locationTitle' => 'required|unique:locations|max:40',
            'locationCode' => 'required|unique:locations',
        ];

        $this->validate($request, $rule);
        $data['locationTitle'] = $request->locationTitle;
        $data['locationCode'] = $request->locationCode;
        $data['createdby'] = 1;

        $location = Location::create($data);

        Session::flash('flash_success', 'Location added!');
        return redirect('admin/location');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$location = Location::where('id',$id)->first();
        if ($location) {
            return view('admin.locations.edit', compact('location'));
        } else {
            Session::flash('flash_warning', 'Location is not exist!');
            return redirect('admin/location');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'locationTitle' => 'required|unique:locations|max:40',
        ]);

        $requestData = $request->all();

        $location = Location::where('id',$id)->first();

        $requestData['status'] = isset($requestData['status'])?1:0;
        $requestData['updatedby'] = 1;
        if($location->locationTitle != $request->locationTitle)
        {
            $locationlogdata = LocationLogs::where('code',$location->locationCode)->where('key', 'locationTitle')->where('value', $requestData['locationTitle'])->first();
            if($locationlogdata == null)
            {
                $data['key'] = "locationTitle";
                $data['value'] = $requestData['locationTitle'];
                $data['code'] = $location->locationCode;
                $location_logs = LocationLogs::create($data);
            }
        }
        if($location->status != $requestData['status'])
        {
            $locationlogdata = LocationLogs::where('code',$location->locationCode)->where('key', 'status')->where('value', $requestData['status'])->first();
            if($locationlogdata == null)
            {
                $data['key'] = "status";
                $data['value'] = $requestData['status'];
                $data['code'] = $location->locationCode;
                $location_logs = LocationLogs::create($data);
            }
        }

        $location->update($requestData);

        Session::flash('flash_success', 'Location updated!');
        return redirect('admin/location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $location = Location::find($id);
        $location->delete();
        if($request->has('from_index')){
            $message = "Location Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Location deleted!');

            return redirect('admin/location');
        }

    }
}
