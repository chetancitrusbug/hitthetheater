<?php

namespace App\Http\Controllers\Admin;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Location;
use App\LocationLogs;
use App\VenueLog;
use App\ImportLogs;
use App\TempLocation;
use App\TempOldLocation;
use App\TempVenue;
use App\TempOldVenue;
use App\TempOldVenueTransportInfo;
use App\TempOldVenueMetadata;
use App\TempOldVenueFacilities;
use App\TempVenueFacilities;
use App\TempVenueSliders;
use App\TempVenueTransportInfo;
use App\TempVenueMetadata;
use App\TempCategory;
use App\Facilities;
use App\VenueFacilities;
use App\VenueSliders;
use App\VenueTransportInfo;
use App\VenueMetaData;
use App\Transport;
use App\TempShow;
use App\TempOffers;
use App\Offer;
use App\Show;
use App\Show_Images;
use App\Schedule;
use App\Category;
use App\Venue;
use Session;
use App\ShowMetadata;
use App\ShowLog;
use App\ShowCategory;
use App\Banner;
use App\TempOldShow;
use App\TempOldOffers;
use App\TempOldShowCategory;
use App\TempOldShowMetadata;
use App\TempOldSchedule;
use App\TempSchedule;
use App\TempShowMetadata;
use App\TempShowCategory;
use App\VenueCategories;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use Mail;
use Response;
use File;
class DashboardController extends Controller
{
    protected $authorization;
    protected $usernameSetting;
    protected $passwordSetting;
    protected $liveUrlSetting;

    public function __construct(){
        view()->share('route', 'dashboard');
        view()->share('moduleName','Dashboard');
        $this->authorization = Setting::where('key', 'authorization')->first();
        $this->usernameSetting = Setting::where('key', 'username')->first();
        $this->passwordSetting = Setting::where('key', 'password')->first();
        $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
        //view()->share('authorization', $this->authorization);
        ini_set('max_execution_time', 3000);

    }

    public function index()
    {



        $latestLocation = ImportLogs::where('import_type', 'location')->latest()->first();
        $latestCategory = ImportLogs::where('import_type', 'category')->latest()->first();
        $latestVenue = ImportLogs::where('import_type','venue')->latest()->first();
        $latestShow = ImportLogs::where('import_type', 'show')->latest()->first();
        $locationLogs = LocationLogs::all()->count();
        $venueLogs = VenueLog::all()->count();
        $showLogs = ShowLog::all()->count();

        return view('admin.dashboard.index',compact('latestLocation','latestCategory','latestVenue','latestShow','locationLogs','venueLogs','showLogs'));
    }

    public function showincrement()
    {

        $shows = Show::select('show.*')
            ->orderby('show.id', 'desc')->get();
        foreach ($shows as $key => $value) {
            $show = Show::find($value->id);
            $show->orderby = $key+1;
            $show->save();
        }

        echo '1';exit;
    }

    public function venueslug()
    {
        $venues = Venue::select('venues.venueTitle','venues.id')
            ->orderby('venues.id', 'desc')->get();
        foreach ($venues as $key => $value) {
            $string = str_replace(' ', '-', rtrim($value->venueTitle));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
            $string = preg_replace('/--+/', '', $string);
            $string = strtolower($string);

            $venue = Venue::find($value->id);
            $venue->slug = $string;
            $venue->save();
        }

        echo '1';exit;
    }

    public function categoryslug()
    {
        $category = Category::select('categoryTitle','id')
            ->orderby('id', 'desc')->get();
        foreach ($category as $key => $value) {
            $string = str_replace(' ', '-', rtrim($value->categoryTitle));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
            $string = preg_replace('/--+/', '', $string);
            $string = strtolower($string);

            $cate = Category::find($value->id);
            $cate->slug = $string;
            $cate->save();
        }

        $category = VenueCategories::select('categoryTitle','id')
            ->orderby('id', 'desc')->get();
        foreach ($category as $key => $value) {
            $string = str_replace(' ', '-', rtrim($value->categoryTitle));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
            $string = preg_replace('/--+/', '', $string);
            $string = strtolower($string);

            $cate = VenueCategories::find($value->id);
            $cate->slug = $string;
            $cate->save();
        }

        echo '1';exit;
    }

    public function showslug()
    {
        $shows = Show::select('show.showTitle','show.id')
            ->orderby('show.id', 'desc')->get();
        foreach ($shows as $key => $value) {
            $string = str_replace(' ', '-', rtrim($value->showTitle));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
            $string = preg_replace('/--+/', '', $string);
            $string = strtolower($string);
            $show = Show::find($value->id);
            $show->slug = $string;
            $show->save();
        }
        echo '1';exit;
    }

    public function bannerincrement()
    {

        $banners = Banner::select('*')
            ->orderby('id', 'desc')->get();
        foreach ($banners as $key => $value) {
            $banner = Banner::find($value->id);
            $banner->orderby = $key+1;
            $banner->save();
        }

        echo '1';exit;
    }

    public function venueincrement()
    {
        $venues = Venue::select('venues.*')
            ->orderby('venues.id', 'desc')->get();
        foreach ($venues as $key => $value) {
            $venue = Venue::find($value->id);
            $venue->orderby = $key+1;
            $venue->save();
        }

        echo '1';exit;
    }


    public function getLocation()
    {
        $tempLocation = TempLocation::all();

        if($tempLocation)
        {
            $html = view('admin.dashboard.locationList', compact('tempLocation'))->render();
            $message = 'Listed Successfully';
            return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
        }
         else {
                $message = 'No have Location';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }

    public function importLocation()
    {

        $tempLocations = TempLocation::all();
        $tempOldLocations = TempOldLocation::all();

        $newArray = array();
        $oldArray = array();
        $codes = array();
        foreach ($tempLocations as $key => $value) {

            $location = Location::where('locationCode', $value->locationCode)->first();
            $tempOldLocation = TempOldLocation::where('locationCode', $value->locationCode)->first();
            if($location == null)
            {
                $newArray[$value->locationCode] = $value;
                $newArray[$value->locationCode]['existrecord'] = 0;
                $newArray[$value->locationCode]['updatedArr'] = 0;
                $codes[] = $value->locationCode;
            }
            else if(isset($tempOldLocation->locationTitle) && isset($value->locationTitle) && $tempOldLocation->locationTitle != $value->locationTitle)
            {
                $newArray[$value->locationCode] = $value;
                $newArray[$value->locationCode]['existrecord'] = 1;
                $newArray[$value->locationCode]['updatedArr'] = 1;
                $codes[] = $value->locationCode;
            }
        }
        foreach ($tempOldLocations as $key => $value) {
            $location = Location::where('locationCode', $value->locationCode)->first();
            $tempLocation = TempLocation::where('locationCode', $value->locationCode)->first();
            if($location == null)
            {
                $oldArray[$value->locationCode] = $value;
                $oldArray[$value->locationCode]['existrecord'] = 0;
                $oldArray[$value->locationCode]['updatedArr'] = 0;
                $codes[] = $value->locationCode;
            }
            else if(isset($tempLocation->locationTitle) && isset($value->locationTitle) && $tempLocation->locationTitle != $value->locationTitle)
            {
                $oldArray[$value->locationCode] = $value;
                $oldArray[$value->locationCode]['existrecord'] = 1;
                $oldArray[$value->locationCode]['updatedArr'] = 1;
                $codes[] = $value->locationCode;
            }
        }

        $tempLocationCodes = TempLocation::pluck('locationCode');
        $tempOldLocationCodes = TempOldLocation::pluck('locationCode');
        if ($tempLocationCodes != null && $tempOldLocationCodes != null) {
            // $merge = array_merge($tempLocationCodes->toArray(), $tempOldLocationCodes->toArray());
            // dd(array_count_values($merge));
            $locationCodes = array_unique($codes);
        }
        else {
            $locationCodes = [];
        }
        if(count($locationCodes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import location.');

            return view('admin.dashboard.locationconflictList', compact('locationCodes','newArray','oldArray'));

        }
        else {
            Session::flash('flash_success', 'No new or updated records found!');
            return redirect('admin/dashboard');
        }
    }

    public function solveConflictLocation(Request $request)
    {
        $data = $request->except('_token');

        if(isset($data['data']) && count($data['data']) > 0)
        {
            foreach ($data['data'] as $key => $value) {
                $keyArray = explode('-',$key);
                $updateaArray = array();
                $location = Location::where('locationCode', $keyArray[0])->first();

                if($location != null)
                {
                    $updateaArray[$keyArray[1]] = $value;
                    $location->update($updateaArray);
                    // $location = LocationLogs::where('code', $keyArray[0])->where('key', $keyArray[1])->delete();
                }
                else {
                    $updateaArray[$keyArray[1]] = $value;
                    $updateaArray['locationCode'] = $keyArray[0];
                    $location = Location::create($updateaArray);
                }
            }

            ImportLogs::create(['import_type' => 'location']);
            Session::flash('flash_success', 'Location import successfully!');
        }

        return redirect('admin/dashboard');
    }

    public function getCategory()
    {

        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');

        $URL = $this->liveUrlSetting->value.'content/location/';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);
        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);

            TempCategory::truncate();
            $data = array();
            foreach ($array['location'] as $key => $value) {
                $data[$key]['locationTitle'] = $value['title'];
                $data[$key]['locationCode'] = $value['@attributes']['id'];
            }
            $location = TempCategory::insert($data);

            $tempCategory = TempCategory::all();

            if($tempCategory != null)
            {
                $html = view('admin.dashboard.categoryList', compact('tempCategory'))->render();
                $message = 'Listed Successfully';
                return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
            }
            else {
                $message = 'No have Location';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
        }
        else {
                $message = 'Unauthorization';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }

    public function importCategory()
    {
        ImportLogs::create(['import_type' => 'location']);
        Location::truncate();
        $sql = "INSERT INTO locations (locationTitle, locationCode, status)
        SELECT locationTitle, locationCode, status FROM tmp_location;";
        DB::statement($sql);
        Session::flash('flash_success', 'Location added!');
        return redirect('admin/dashboard');

    }

    public function getVenue()
    {
        $tempVenue = TempVenue::with('location')->get();
        $flag = 0;
        if($tempVenue != null)
        {
            $html = view('admin.dashboard.venueList', compact('tempVenue','flag'))->render();
            $message = 'Listed Successfully';
            return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
        }
        else {
            $message = 'No have Venue';
            return response()->json(['message' => $message, 'code' => 400], 200);
        }
    }

    public function importVenues()
    {

        // use App\TempOldVenue;
        // use App\TempOldVenueTransportInfo;
        // use App\TempOldVenueMetadata;
        // use App\TempOldVenueFacilities;
        // use App\TempVenueFacilities;
        // use App\TempVenueSliders;
        // use App\TempVenueTransportInfo;
        // use App\TempVenueMetadata;

        $tempVenues = TempVenue::all()->pluck('venueCode');
        $tempOldVenues = TempOldVenue::all()->pluck('venueCode');

        if ($tempVenues != null && $tempOldVenues != null) {
            $venues = array_unique(array_merge($tempVenues->toArray(), $tempOldVenues->toArray()));
        }
        else {
            $venues = [];
        }

        $newArray = array();
        $oldArray = array();
        $fields = array('venueTitle','venueDescription','locationCode','postcode','latitude','longitude','addressLine1','addressLine2');

        foreach ($venues as $key => $value) {
            // if ($key > 1) {

            $venueCode = $value;

            //new Array
            $currentRec = TempVenue::where('venueCode', $venueCode)->first();
            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->venueCode;

                $tempOld = TempOldVenue::where('venueCode', $currentRec->venueCode)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $newArray[$currentRec->venueCode] = $value->toArray();
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                $tabledata = Venue::where('venueCode', $currentRec->venueCode)->first();

                if($tabledata == null)
                {
                    $newArray[$currentRec->venueCode] = $value->toArray();
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey ) {

                        // if ($currentRec->venueCode == '21') {
                        //     dd(($currentRec[$fieldKey]));
                        // }
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $newArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $newArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $newArray[$currentRec->venueCode][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                // $newArray[$currentRec->venueCode]['facilities'] = '';
                // $newArray[$currentRec->venueCode]['transportinfo'] = '';
                // $newArray[$currentRec->venueCode]['metadata'] = '';
                $tempFacilitiesIds = TempVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempFacilitiesIds != null) {
                    $tempFacilitiesIds = $tempFacilitiesIds->toArray();
                }
                $tempOldFacilitiesIds = TempOldVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_old_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempOldFacilitiesIds != null) {
                    $tempOldFacilitiesIds = $tempOldFacilitiesIds->toArray();
                }

                if ($tempFacilitiesIds != null && $tempOldFacilitiesIds != null) {
                    $facilitiesIds = array_diff($tempFacilitiesIds, $tempOldFacilitiesIds);
                }
                else {
                    $facilitiesIds = [];
                }


                if (count($tempFacilitiesIds) > 0) {
                    $newArray[$currentRec->venueCode]['facilities'] = '';
                    $facilities = Facilities::whereIn('id', $tempFacilitiesIds)->get();
                    foreach ($facilities as $facilitiesKey => $facilitiesValue) {
                        if (isset($facilitiesValue->facilitieTitle) && $facilitiesValue->facilitieTitle != null) {
                            $changeColor = (!in_array($facilitiesValue->id,$tempOldFacilitiesIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->venueCode]['facilities'] .= '<p class="'.$changeColor.'">'.$facilitiesValue->facilitieTitle.'</p>';
                            if ($changeColor != '') {
                               $newArray[$currentRec->venueCode]['updatedArr'][] = 'facilities';
                            }
                        }
                    }
                }

                $transport = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->get();

                $tempTransportIds = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempTransportIds != null) {
                    $tempTransportIds = $tempTransportIds->toArray();
                }
                $tempOldTransportIds = TempOldVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_old_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempOldTransportIds != null) {
                    $tempOldTransportIds = $tempOldTransportIds->toArray();
                }

                if ($tempTransportIds != null && $tempOldTransportIds != null) {
                    $transportIds = array_diff($tempTransportIds, $tempOldTransportIds);
                }
                else {
                    $transportIds = [];
                }


                if (count($tempTransportIds) > 0) {
                    $newArray[$currentRec->venueCode]['transportinfo'] = '';
                    $transport = Transport::whereIn('id', $tempTransportIds)->get();
                    foreach ($transport as $transportKey => $transportValue) {
                        if (isset($transportValue->transportTitle) && $transportValue->transportTitle != null) {
                            $changeColor = (!in_array($transportValue->id,$tempOldTransportIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->venueCode]['transportinfo'] .= '<p class="'.$changeColor.'">'.$transportValue->transportTitle.'</p>';
                            if ($changeColor != '') {
                            $newArray[$currentRec->venueCode]['updatedArr'][] = 'transportinfo';
                            }
                        }
                    }
                }

                $tempMetadata = TempVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                $tempOldmetadata = TempOldVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();

                if($tempMetadata != null)
                {
                    $newArray[$currentRec->venueCode]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_title) && $tempMetadata->meta_title != null) {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->venueCode]['metadata'] .= (isset($tempMetadata->meta_title)) ? '<p><b>Title -</b> '.$tempMetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_description) && $tempMetadata->meta_description != null) {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->venueCode]['metadata'] .= (isset($tempMetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempMetadata->meta_description : '';
                    }
                }
                if (isset($newArray[$currentRec->venueCode]['updatedArr']) && count($newArray[$currentRec->venueCode]['updatedArr']) > 0) {
                    $newArray[$currentRec->venueCode]['updatedArr'] = array_unique($newArray[$currentRec->venueCode]['updatedArr']);
                    if($tabledata == null)
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $newArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else if (isset($newArray[$currentRec->venueCode]['existrecord'])) {
                    if($tabledata == null)
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $newArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else {
                    unset($newArray[$currentRec->venueCode]);
                    unset($venues[$key]);
                }
                // dd($newArray);
            }

            //old Array
            $currentRec = TempOldVenue::where('venueCode', $venueCode)->first();
            if($currentRec != null)
            {
                $value = $currentRec;
                $codes[] = $currentRec->venueCode;

                $tempOld = TempVenue::where('venueCode', $currentRec->venueCode)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $oldArray[$currentRec->venueCode] = $value->toArray();
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                $tabledata = Venue::where('venueCode', $currentRec->venueCode)->first();

                if($tabledata == null)
                {
                    $oldArray[$currentRec->venueCode] = $value->toArray();
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey ) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $oldArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $oldArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $oldArray[$currentRec->venueCode][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                // $oldArray[$currentRec->venueCode]['facilities'] = '';
                // $oldArray[$currentRec->venueCode]['transportinfo'] = '';
                // $oldArray[$currentRec->venueCode]['metadata'] = '';
                $tempFacilitiesIds = TempVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempFacilitiesIds != null) {
                    $tempFacilitiesIds = $tempFacilitiesIds->toArray();
                }
                $tempOldFacilitiesIds = TempOldVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_old_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempOldFacilitiesIds != null) {
                    $tempOldFacilitiesIds = $tempOldFacilitiesIds->toArray();
                }

                if ($tempFacilitiesIds != null && $tempOldFacilitiesIds != null) {
                    $facilitiesIds = array_diff($tempOldFacilitiesIds, $tempFacilitiesIds);
                }
                else {
                    $facilitiesIds = [];
                }


                if (count($tempOldFacilitiesIds) > 0) {
                    $oldArray[$currentRec->venueCode]['facilities'] = '';
                    $facilities = Facilities::whereIn('id', $tempOldFacilitiesIds)->get();
                    foreach ($facilities as $facilitiesKey => $facilitiesValue) {
                        if (isset($facilitiesValue->facilitieTitle) && $facilitiesValue->facilitieTitle != null) {
                            $changeColor = (!in_array($facilitiesValue->id,$tempFacilitiesIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->venueCode]['facilities'] .= '<p class="'.$changeColor.'">'.$facilitiesValue->facilitieTitle.'</p>';
                            if ($changeColor != '') {
                               $oldArray[$currentRec->venueCode]['updatedArr'][] = 'facilities';
                            }
                        }
                    }
                }

                $transport = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->get();

                $tempTransportIds = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempTransportIds != null) {
                    $tempTransportIds = $tempTransportIds->toArray();
                }
                $tempOldTransportIds = TempOldVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_old_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempOldTransportIds != null) {
                    $tempOldTransportIds = $tempOldTransportIds->toArray();
                }

                if ($tempTransportIds != null && $tempOldTransportIds != null) {
                    $transportIds = array_diff($tempOldTransportIds, $tempTransportIds);
                }
                else {
                    $transportIds = [];
                }


                if (count($tempOldTransportIds) > 0) {
                    $oldArray[$currentRec->venueCode]['transportinfo'] = '';
                    $transport = Transport::whereIn('id', $tempOldTransportIds)->get();
                    foreach ($transport as $transportKey => $transportValue) {
                        if (isset($transportValue->transportTitle) && $transportValue->transportTitle != null) {
                            $changeColor = (!in_array($transportValue->id,$tempTransportIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->venueCode]['transportinfo'] .= '<p >'.$transportValue->transportTitle.'</p>';
                            if ($changeColor != '') {
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = 'transportinfo';
                            }
                        }
                    }
                }

                $tempMetadata = TempVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                $tempOldmetadata = TempOldVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                // dd($tempOldmetadata);
                if($tempOldmetadata != null)
                {
                    $oldArray[$currentRec->venueCode]['metadata'] = '';
                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_title) && $tempOldmetadata->meta_title != null) {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->venueCode]['metadata'] .= (isset($tempOldmetadata->meta_title)) ? '<p><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>' : '';
                    }

                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_description) && $tempOldmetadata->meta_description != null) {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->venueCode]['metadata'] .= (isset($tempOldmetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempOldmetadata->meta_description : '';
                    }
                }
                if (isset($oldArray[$currentRec->venueCode]['updatedArr']) && count($oldArray[$currentRec->venueCode]['updatedArr']) > 0) {
                    $oldArray[$currentRec->venueCode]['updatedArr'] = array_unique($oldArray[$currentRec->venueCode]['updatedArr']);
                    if($tabledata == null)
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $oldArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else if (isset($oldArray[$currentRec->venueCode]['existrecord'])) {
                    if($tabledata == null)
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $oldArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else {
                    unset($oldArray[$currentRec->venueCode]);
                    unset($venues[$key]);
                }
            }
            // dd($oldArray);


        // }
        }


        $codes = $venues;
        $fields = array('venueTitle'=>'Venue Title','venueDescription'=>'Venue Description','locationCode'=>'Location Code','postcode'=>'Post code','latitude'=>'Latitude','longitude'=>'Longitude','addressLine1'=>'Address Line 1','addressLine2'=>'Address Line 2','facilities'=>'Facilities','transportinfo'=>'Transport info','metadata'=>'Meta data');

        // dd($newArray);
        if(count($codes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import venue.');
            return view('admin.dashboard.venueconflictList', compact('codes','newArray','oldArray','fields'));
        }
        else {
            Session::flash('flash_success', 'No new or updated records found!');
            return redirect('admin/dashboard');
        }

    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    public function solveConflictVenue(Request $request)
    {

        $data = $request->except('_token');

        if(isset($data['data']) && count($data['data']) > 0)
        {
            foreach ($data['data'] as $key => $value) {
                // dd($value);
                $orderbymaxvalue = Venue::max('orderby');

                $tabledata = Venue::where('venueCode', $key)->first();

                if($tabledata != null)
                {

                        $updateaArray = array();
                        if(isset($value['venueTitle'])  && $value['venueTitle'] != null)
                        {
                            $updateaArray['venueTitle'] = $value['venueTitle'];
                        }
                        if(isset($value['venueDescription']) && $value['venueDescription'] != null)
                        {
                            $updateaArray['venueDescription'] = $value['venueDescription'];
                        }
                        if(isset($value['locationCode']) && $value['locationCode'] != null)
                        {
                            $updateaArray['locationCode'] = $value['locationCode'];
                        }
                        if(isset($value['postcode']) && $value['postcode'] != null)
                        {
                            $updateaArray['postcode'] = $value['postcode'];
                        }
                        if(isset($value['latitude']) && $value['latitude'] != null)
                        {
                            $updateaArray['latitude'] = $value['latitude'];
                        }
                        if(isset($value['longitude']) && $value['longitude'] != null)
                        {
                            $updateaArray['longitude'] = $value['longitude'];
                        }
                        if(isset($value['addressLine1']) && $value['addressLine1'] != null)
                        {
                            $updateaArray['addressLine1'] = $value['addressLine1'];
                        }
                        if(isset($value['addressLine2']) && $value['addressLine2'] != null)
                        {
                            $updateaArray['addressLine2'] = $value['addressLine2'];
                        }

                        if(isset($tabledata->orderby) && $tabledata->orderby == 0)
                        {
                            $updateaArray['orderby'] = $orderbymaxvalue+1;
                        }

                        if(count($updateaArray) > 0)
                        {
                            $tabledata->update($updateaArray);
                        }

                        if(isset($value['facilities']) && $value['facilities'] != null ) {
                            $tablename = (isset($value['facilities']) && $value['facilities'] == 'new') ? 'tmp_venue_facilities' : 'tmp_old_venue_facilities';
                            VenueFacilities::where('venueCode', $key)->delete();
                            $sql = "INSERT INTO `venue_facilities` ( venueId, facilitieId, venueCode) SELECT venueId, facilitieId, venueCode FROM $tablename WHERE venueCode='".$key."';";
                            DB::statement($sql);
                        }

                        if(isset($value['transportinfo']) && $value['transportinfo'] != null ) {
                            VenueTransportInfo::where('venueCode', $key)->delete();
                            $tablename = (isset($value['transportinfo']) && $value['transportinfo'] == 'new') ? 'tmp_venue_transport_info' : '	tmp_old_venue_transport_info';
                            $sql = "INSERT INTO `venue_transport_info` ( venueId, transportId, transportValue ,venueCode) SELECT venueId, transportId, transportValue ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                            DB::statement($sql);
                        }

                        if(isset($value['metadata']) && $value['metadata'] != null ) {
                            VenueMetaData::where('venueCode', $key)->delete();
                            $tablename = (isset($value['metadata']) && $value['metadata'] == 'new') ? 'tmp_venue_metadata' : 'tmp_old_venue_metadata';
                            $sql = "INSERT INTO `venue_meta_data` ( venue_id, meta_title, meta_description ,venueCode) SELECT venue_id, meta_title, meta_description ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                            DB::statement($sql);
                        }

                }
                else {
                    if (isset($value['allrecords']) && $value['allrecords'] == 'new') {
                        $tablename = 'tmp_venue';
                        $sql = "INSERT INTO `venues` ( locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude, venueCode, addressLine1, addressLine2, locationCode ) SELECT locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude, venueCode, addressLine1, addressLine2, locationCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);
                        $tabledata = Venue::where('venueCode', $key)->first();

                        $updata['slug'] = $this->slugCreate($tabledata->venueTitle);
                        $updata['orderby'] = $orderbymaxvalue+1;
                        $tabledata->update($updata);
                        $tablename = 'tmp_venue_facilities';
                        VenueFacilities::where('venueCode', $key)->delete();
                        $sql = "INSERT INTO `venue_facilities` ( venueId, facilitieId, venueCode) SELECT venueId, facilitieId, venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);

                        VenueTransportInfo::where('venueCode', $key)->delete();
                        $tablename = 'tmp_venue_transport_info';
                        $sql = "INSERT INTO `venue_transport_info` ( venueId, transportId, transportValue ,venueCode) SELECT venueId, transportId, transportValue ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);

                        VenueMetaData::where('venueCode', $key)->delete();
                        $tablename = 'tmp_venue_metadata';
                        $sql = "INSERT INTO `venue_meta_data` ( venue_id, meta_title, meta_description ,venueCode) SELECT venue_id, meta_title, meta_description ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);
                    }
                    else if (isset($value['allrecords']) && $value['allrecords'] == 'old') {
                        $tablename = 'tmp_old_venue';
                        $sql = "INSERT INTO `venues` ( locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude, venueCode, addressLine1, addressLine2, locationCode ) SELECT locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude, venueCode, addressLine1, addressLine2, locationCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);
                        $tabledata = Venue::where('venueCode', $key)->first();
                        $updata['slug'] = $this->slugCreate($tabledata->venueTitle);
                        $updata['orderby'] = $orderbymaxvalue+1;
                        $tabledata->update($updata);

                        $tablename = 'tmp_old_venue_facilities';
                        VenueFacilities::where('venueCode', $key)->delete();
                        $sql = "INSERT INTO `venue_facilities` ( venueId, facilitieId, venueCode) SELECT venueId, facilitieId, venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);

                        VenueTransportInfo::where('venueCode', $key)->delete();
                        $tablename = 'tmp_old_venue_transport_info';
                        $sql = "INSERT INTO `venue_transport_info` ( venueId, transportId, transportValue ,venueCode) SELECT venueId, transportId, transportValue ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);

                        VenueMetaData::where('venueCode', $key)->delete();
                        $tablename = 'tmp_old_venue_metadata';
                        $sql = "INSERT INTO `venue_meta_data` ( venue_id, meta_title, meta_description ,venueCode) SELECT venue_id, meta_title, meta_description ,venueCode FROM $tablename WHERE venueCode='".$key."';";
                        DB::statement($sql);
                    }
                }
            }

            ImportLogs::create(['import_type' => 'venue']);
            Session::flash('flash_success', 'Venue import successfully!');
        }

        return redirect('admin/dashboard');
    }

    public function getShow()
    {
        $tempShow = TempShow::all();

        if($tempShow != null)
        {
            $html = view('admin.dashboard.showList', compact('tempShow'))->render();
            $message = 'Listed Successfully';
            return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
        }
        else {
            $message = 'No have Show';
            return response()->json(['message' => $message, 'code' => 400], 200);
        }
    }

    public function importShow()
    {
        $recCode = array();
        $tempShows = TempShow::all()->pluck('showLink');
        $tempOldShows = TempOldShow::all()->pluck('showLink');

        if ($tempShows != null && $tempOldShows != null) {
            $shows = array_unique(array_merge($tempShows->toArray(), $tempOldShows->toArray()));
        }
        else {
            $shows = [];
        }


        $fields = array('showTitle'=>'Show Title','summary'=>'Summary','description'=>'Description', 'priceFrom'=>'Price From','priceTo'=>'Price To','runningTime'=>'Running Time','ageRestriction'=>'Age Restriction','importantInfo'=>'ImportantInfo','venueCode'=>'Venue Code','bannerBackgroundColour'=>'Banner Background Colour');

        $newArray = array();
        $oldArray = array();
        foreach ($shows as $key => $value) {

            $showLink = $value;
            //new Array

            $currentRec = TempShow::where('showLink',$showLink)->first();

            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->showLink;

                $tempOld = TempOldShow::where('showLink', $currentRec->showLink)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $newArray[$currentRec->showLink] = $value->toArray();
                    $newArray[$currentRec->showLink]['existrecord'] = 0;
                }
                $tabledata = Show::where('showLink',$currentRec->showLink)->first();

                if($tabledata == null)
                {
                    $newArray[$currentRec->showLink] = $value->toArray();
                    $newArray[$currentRec->showLink]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey => $fieldValue) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $newArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $newArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $newArray[$currentRec->showLink][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                $tempMetadata = TempShowMetadata::where('showLink',$currentRec->showLink)->first();
                $tempOldmetadata = TempOldShowMetadata::where('showLink',$currentRec->showLink)->first();

                if($tempMetadata != null)
                {
                    $newArray[$currentRec->showLink]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_title) && $tempMetadata->meta_title != null) {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->showLink]['metadata'] .= (isset($tempMetadata->meta_title)) ? '<p><b>Title -</b> '.$tempMetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_description) && $tempMetadata->meta_description != null) {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->showLink]['metadata'] .= (isset($tempMetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempMetadata->meta_description : '';
                    }
                }

                $tempOffers = TempOffers::where('showLink', $currentRec->showLink)->get();
                $offerFields = array('title'=>'Title','type'=>'Type','valid'=>'Valid','start_date'=>'Start date','end_date'=>'End date', 'exclusive'=>'Exclusive','facevalue'=>'Facevalue','price'=>'Price','description'=>'Description');
                if ($tempOffers != null) {
                    $newArray[$currentRec->showLink]['offers'] = '';
                    foreach ($tempOffers as $offersKey => $offersValue) {
                        // $offersValue = $offersValue->toArray();

                        $tempOldOffer = TempOldOffers::where('offer_id', $offersValue['offer_id'])->first();
                        foreach ($offerFields as $offerFieldKey => $offerFieldValue) {
                            if(isset($tempOldOffer[$offerFieldKey]) && isset($offersValue[$offerFieldKey]) && $tempOldOffer[$offerFieldKey] != $offersValue[$offerFieldKey])
                            {
                                $newArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else if (!isset($tempOldOffer[$offerFieldKey]) && $offersValue[$offerFieldKey] != null) {
                                $newArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else {
                                $newArray[$currentRec->showLink]['offers'] .= (isset($offersValue[$offerFieldKey])) ? '<p><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>' : '';
                            }


                        }
                    }
                }


                $tempCategoryIds = TempShowCategory::leftJoin('categories','tmp_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempCategoryIds != null) {
                    $tempCategoryIds = $tempCategoryIds->toArray();
                }
                $tempOldCategoryIds = TempOldShowCategory::leftJoin('categories','tmp_old_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempOldCategoryIds != null) {
                    $tempOldCategoryIds = $tempOldCategoryIds->toArray();
                }

                if ($tempCategoryIds != null && $tempOldCategoryIds != null) {
                    $categoryIds = array_diff($tempCategoryIds, $tempOldCategoryIds);
                }
                else {
                    $categoryIds = [];
                }

                if (count($tempCategoryIds) > 0) {
                    $newArray[$currentRec->showLink]['category'] = '';
                    $category = Category::whereIn('id', $tempCategoryIds)->get();
                    foreach ($category as $categoryKey => $categoryValue) {
                        if (isset($categoryValue->categoryTitle) && $categoryValue->categoryTitle != null) {
                            $changeColor = (!in_array($categoryValue->id,$tempOldCategoryIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->showLink]['category'] .= '<p class="'.$changeColor.'">'.$categoryValue->categoryTitle.'</p>';
                            if ($changeColor != '') {
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'category';
                            }
                        }
                    }
                }

                $tempSchedule = TempSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();
                $tempoldSchedule = TempOldSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();

                $tempoldSchedule = $this->scheduleArrReturn($tempoldSchedule);
                $tempSchedule = $this->scheduleArrReturn($tempSchedule);
                if ($tempSchedule != null) {
                   $newArray[$currentRec->showLink]['schedule'] = '';
                    foreach ($tempSchedule as $key => $value) {
                        if (count($tempSchedule[$key]) > 0) {
                            if (isset($tempoldSchedule[$key]) && count($tempSchedule[$key]) == count($tempoldSchedule[$key])) {
                                $diff = array_diff($tempSchedule[$key], $tempoldSchedule[$key]);
                                if (count($diff) > 0) {
                                    $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // foreach ($diff as $valueKey) {
                                    //     $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    //     $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // }
                                }
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    // $changeColor = (in_array($valueKey,$diff)) ? 'changecolor' : '';
                                    $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';

                                }

                            }
                            else {
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                }
                            }
                        }
                    }
                }
                // if ($newArray[$currentRec->showLink]['schedule'] == '') {
                //     if (in_array('schedule',$newArray[$showLink]['updatedArr'])) {
                //         if (count($newArray[$showLink]['updatedArr']) == 1) {
                //             unset($newArray[$currentRec->showLink]);
                //             unset($shows[$key]);
                //         }
                //     }
                // }
                    // dd($newArray);
                if (isset($newArray[$showLink]) && isset($newArray[$showLink]['updatedArr']) && count($newArray[$showLink]['updatedArr']) > 0) {
                    $newArray[$showLink]['updatedArr'] = array_unique($newArray[$showLink]['updatedArr']);
                    if($tabledata == null)
                    $newArray[$showLink]['existrecord'] = 0;
                    else
                    $newArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else if (isset($newArray[$showLink]['existrecord'])) {
                    if($tabledata == null)
                    $newArray[$showLink]['existrecord'] = 0;
                    else
                    $newArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else {
                    unset($newArray[$showLink]);
                }
                // dd($newArray);
            }

            // Old Array
            $currentRec = TempOldShow::where('showLink',$showLink)->first();

            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->showLink;

                $tempOld = TempShow::where('showLink', $currentRec->showLink)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $oldArray[$currentRec->showLink] = $value->toArray();
                    $oldArray[$currentRec->showLink]['existrecord'] = 0;
                }
                $tabledata = Show::where('showLink',$currentRec->showLink)->first();

                if($tabledata == null)
                {
                    $oldArray[$currentRec->showLink] = $value->toArray();
                    $oldArray[$currentRec->showLink]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey => $fieldValue) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $oldArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $oldArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $oldArray[$currentRec->showLink][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }

                $tempMetadata = TempShowMetadata::where('showLink',$currentRec->showLink)->first();
                $tempOldmetadata = TempOldShowMetadata::where('showLink',$currentRec->showLink)->first();

                if($tempOldmetadata != null)
                {
                    $oldArray[$currentRec->showLink]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_title) && $tempOldmetadata->meta_title != null) {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->showLink]['metadata'] .= (isset($tempOldmetadata->meta_title)) ? '<p><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_description) && $tempOldmetadata->meta_description != null) {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->showLink]['metadata'] .= (isset($tempOldmetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempOldmetadata->meta_description : '';
                    }
                }

                $tempOffers = TempOldOffers::where('showLink', $currentRec->showLink)->get();
                $offerFields = array('title'=>'Title','type'=>'Type','valid'=>'Valid','start_date'=>'Start date','end_date'=>'End date', 'exclusive'=>'Exclusive','facevalue'=>'Facevalue','price'=>'Price','description'=>'Description');
                if ($tempOffers != null) {

                    $oldArray[$currentRec->showLink]['offers'] = '';
                    foreach ($tempOffers as $offersKey => $offersValue) {
                        $offersValue = $offersValue->toArray();
                        $tempOldOffer = TempOffers::where('offer_id', $offersValue['offer_id'])->first();
                        foreach ($offerFields as $offerFieldKey => $offerFieldValue) {
                            if(isset($tempOldOffer[$offerFieldKey]) && isset($offersValue[$offerFieldKey]) && $tempOldOffer[$offerFieldKey] != $offersValue[$offerFieldKey])
                            {
                                $oldArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else if (!isset($tempOldOffer[$offerFieldKey]) && $offersValue[$offerFieldKey] != null) {
                                $oldArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else {
                                $oldArray[$currentRec->showLink]['offers'] .= (isset($offersValue[$offerFieldKey])) ? '<p><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>' : '';
                            }

                        }
                    }
                }

                $tempCategoryIds = TempShowCategory::leftJoin('categories','tmp_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempCategoryIds != null) {
                    $tempCategoryIds = $tempCategoryIds->toArray();
                }
                $tempOldCategoryIds = TempOldShowCategory::leftJoin('categories','tmp_old_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempOldCategoryIds != null) {
                    $tempOldCategoryIds = $tempOldCategoryIds->toArray();
                }

                if ($tempCategoryIds != null && $tempOldCategoryIds != null) {
                    $categoryIds = array_diff($tempOldCategoryIds, $tempCategoryIds);
                }
                else {
                    $categoryIds = [];
                }

                if (count($tempOldCategoryIds) > 0) {
                    $oldArray[$currentRec->showLink]['category'] = '';
                    $category = Category::whereIn('id', $tempOldCategoryIds)->get();
                    foreach ($category as $categoryKey => $categoryValue) {
                        if (isset($categoryValue->categoryTitle) && $categoryValue->categoryTitle != null) {
                            $changeColor = (!in_array($categoryValue->id,$tempCategoryIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->showLink]['category'] .= '<p class="'.$changeColor.'">'.$categoryValue->categoryTitle.'</p>';
                            if ($changeColor != '') {
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'category';
                            }
                        }
                    }
                }

                $tempSchedule = TempOldSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();
                $tempoldSchedule = TempSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();

                $tempoldSchedule = $this->scheduleArrReturn($tempoldSchedule);
                $tempSchedule = $this->scheduleArrReturn($tempSchedule);
                if ($tempSchedule != null) {
                   $oldArray[$currentRec->showLink]['schedule'] = '';
                    foreach ($tempSchedule as $key => $value) {
                        if (count($tempSchedule[$key]) > 0) {
                            if (isset($tempoldSchedule[$key]) && count($tempSchedule[$key]) == count($tempoldSchedule[$key])) {
                                $diff = array_diff($tempSchedule[$key], $tempoldSchedule[$key]);
                                if (count($diff) > 0) {
                                    $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // foreach ($diff as $valueKey) {
                                    //     $oldArray[$currentRec->showLink]['schedule'] .= '<p class="changecolor">'.$key.'-'.$valueKey.'</p>';
                                    //     $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // }
                                }
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $oldArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                }
                            }
                            else {
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $oldArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                }
                            }
                        }
                    }
                }


                if ($oldArray[$currentRec->showLink]['schedule'] == '') {
                    unset($oldArray[$currentRec->showLink]['schedule']);
                }

                if (isset($oldArray[$showLink]) && isset($oldArray[$showLink]['updatedArr'])  && count($oldArray[$showLink]['updatedArr']) > 0) {
                    $oldArray[$showLink]['updatedArr'] = array_unique($oldArray[$showLink]['updatedArr']);
                    if($tabledata == null)
                    $oldArray[$showLink]['existrecord'] = 0;
                    else
                    $oldArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else if (isset($oldArray[$showLink]['existrecord'])) {
                    if($tabledata == null)
                    $oldArray[$showLink]['existrecord'] = 0;
                    else
                    $oldArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else {
                    unset($oldArray[$showLink]);
                }

            }
        }
        // dd($oldArray);

        $codes = array_unique($recCode);

        $fields = array('showTitle'=>'Show Title','summary'=>'Summary','description'=>'Description', 'priceFrom'=>'Price From','priceTo'=>'Price To','runningTime'=>'Running Time','ageRestriction'=>'Age Restriction','importantInfo'=>'ImportantInfo','venueCode'=>'Venue Code','bannerBackgroundColour'=>'Banner Background Colour','schedule'=>'Schedule','offers'=>'Offers','metadata'=>'Metadata','category'=>'Category');
        // dd($newArray);
        if(count($codes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import show.');
            return view('admin.dashboard.showconflictList', compact('codes','newArray','oldArray','fields'));
        }
        else {
            Session::flash('flash_success', 'No new or updated records found!');
            return redirect('admin/dashboard');
        }


    }

    public function scheduleArrReturn($schedules)
    {
        $newschedule = array('Mon'=>array(),'Tue'=>array(),'Wed'=>array(),'Thu'=>array(),'Fri'=>array(),'Sat'=>array(),'Sun'=>array());
            foreach ($schedules as $key => $val)
            {
                switch ($val->day_id) {
                    case 'Mon':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Tue':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Wed':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Thu':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Fri':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sat':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sun':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;

                    default:
                        break;
                }
            }
        return $newschedule;
    }

    public function solveConflictShow(Request $request)
    {
        $data = $request->except('_token');

        if(isset($data['data']) && count($data['data']) > 0)
        {

            foreach ($data['data'] as $key => $value) {

                $orderbymaxvalue = Show::max('orderby');

                $updateaArray = array();
                $tabledata = Show::where('showLink', $key)->first();
                if($tabledata != null)
                {
                    $updateaArray = $value;

                    if(isset($tabledata->orderby) && $tabledata->orderby == 0)
                    {
                        $updateaArray['orderby'] = $orderbymaxvalue+1;
                    }

                    $tabledata = $tabledata->update($updateaArray);

                    if(isset($value['schedule']) && $value['schedule'] != null ) {
                        $tablename = (isset($value['schedule']) && $value['schedule'] == 'new') ? 'temp_schedule' : 'temp_old_schedule';
                        Schedule::where('showLink', $key)->delete();
                        $sql = "INSERT INTO `schedule` (show_id, day_id,time,showLink) SELECT show_id, day_id,time,showLink FROM $tablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                    }

                    if(isset($value['category']) && $value['category'] != null ) {
                        ShowCategory::where('showLink', $key)->delete();
                        $tablename = (isset($value['category']) && $value['category'] == 'new') ? 'tmp_show_category' : 'tmp_old_show_category';
                        $sql = "INSERT INTO show_category (show_id, category_id,showLink) SELECT show_id, category_id,showLink  FROM $tablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                    }

                    if(isset($value['metadata']) && $value['metadata'] != null ) {
                        ShowMetaData::where('showLink', $key)->delete();
                        $tablename = (isset($value['metadata']) && $value['metadata'] == 'new') ? 'tmp_show_meta_data' : 'tmp_old_show_meta_data';
                        $sql = "INSERT INTO show_meta_data (show_id, meta_title, meta_description,showLink) SELECT show_id, meta_title, meta_description,showLink  FROM $tablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                    }

                    if(isset($value['offers']) && $value['offers'] != null ) {
                        Offer::where('showLink', $key)->delete();
                        $tablename = (isset($value['offers']) && $value['offers'] == 'new') ? 'tmp_offers' : 'tmp_old_offers';
                        $sql = "INSERT INTO `offers` ( title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink) SELECT title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink FROM $tablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                    }


                }
                else {

                    if (isset($value['allrecords']) && $value['allrecords'] == 'new') {
                        $showtablename = 'tmp_show';
                        $sql = "INSERT INTO `show` ( showTitle, indexName, showLink, bookingStarts,bookingEnds,  isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status) SELECT showTitle, indexName, showLink, bookingStarts,bookingEnds,  isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status FROM $showtablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                        $tabledata = Show::where('showLink', $key)->first();

                        $updata['slug'] = $this->slugCreate($tabledata->showTitle);
                        $updata['orderby'] = $orderbymaxvalue+1;
                        $tabledata->update($updata);
                        // dd($tabledata);
                        //if(isset($value['schedule']) && $value['schedule'] != null ) {
                            $tablename = 'temp_schedule';
                            Schedule::where('showLink', $key)->delete();
                            $sql = "INSERT INTO `schedule` (show_id, day_id,time,showLink) SELECT show_id, day_id,time,showLink FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['category']) && $value['category'] != null ) {
                            ShowCategory::where('showLink', $key)->delete();
                            $tablename = 'tmp_show_category';
                            $sql = "INSERT INTO show_category (show_id, category_id,showLink) SELECT show_id, category_id,showLink  FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['metadata']) && $value['metadata'] != null ) {
                            ShowMetaData::where('showLink', $key)->delete();
                            $tablename = 'tmp_show_meta_data';
                            $sql = "INSERT INTO show_meta_data (show_id, meta_title, meta_description,showLink) SELECT show_id, meta_title, meta_description,showLink  FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['offers']) && $value['offers'] != null ) {
                            Offer::where('showLink', $key)->delete();
                            $tablename = 'tmp_offers';
                            $sql = "INSERT INTO `offers` ( title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink) SELECT title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}
                    }
                    else if (isset($value['allrecords']) && $value['allrecords'] == 'old') {
                        $showtablename = 'tmp_old_show';
                        $sql = "INSERT INTO `show` ( showTitle, indexName, showLink, bookingStarts,bookingEnds,  isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status) SELECT showTitle, indexName, showLink, bookingStarts,bookingEnds,  isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status FROM $showtablename WHERE showLink='".$key."';";
                        DB::statement($sql);
                        $tabledata = Show::where('showLink', $key)->first();

                        $updata['slug'] = $this->slugCreate($tabledata->showTitle);
                        $updata['orderby'] = $orderbymaxvalue+1;
                        $tabledata->update($updata);

                        //if(isset($value['schedule']) && $value['schedule'] != null ) {
                            $tablename = 'temp_old_schedule';
                            Schedule::where('showLink', $key)->delete();
                            $sql = "INSERT INTO `schedule` (show_id, day_id,time,showLink) SELECT show_id, day_id,time,showLink FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['category']) && $value['category'] != null ) {
                            ShowCategory::where('showLink', $key)->delete();
                            $tablename = 'tmp_old_show_category';
                            $sql = "INSERT INTO show_category (show_id, category_id,showLink) SELECT show_id, category_id,showLink  FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['metadata']) && $value['metadata'] != null ) {
                            ShowMetaData::where('showLink', $key)->delete();
                            $tablename = 'tmp_old_show_meta_data';
                            $sql = "INSERT INTO show_meta_data (show_id, meta_title, meta_description,showLink) SELECT show_id, meta_title, meta_description,showLink  FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}

                        //if(isset($value['offers']) && $value['offers'] != null ) {
                            Offer::where('showLink', $key)->delete();
                            $tablename = 'tmp_old_offers';
                            $sql = "INSERT INTO `offers` ( title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink) SELECT title, type, valid, start_date, end_date, exclusive, facevalue, price, description, created_at, updated_at, offer_id, showLink FROM $tablename WHERE showLink='".$key."';";
                            DB::statement($sql);
                        //}
                    }
                }
            }

            ImportLogs::create(['import_type' => 'show']);
            Session::flash('flash_success', 'Show import successfully!');

        }
        return redirect('admin/dashboard');
    }

    public function schedule_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;
        $URL = $this->liveUrlSetting->value . 'content/show/'.$showlink."/schedule";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }

    }

    private function show_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    private function show_rating($showlink)
    {

        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        //CURLOPT_URL => 'https://api.entstix.com/api/v1/xlive/content/show/LDN/1014/quote',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else
        {

            $show_rating_xml = simplexml_load_string($response);
            $show_rating_array = json_decode(json_encode($show_rating_xml), true);
            if(is_array($show_rating_array['quotes']))
            {
                $show_rating=$show_rating_array['quotes']['quote'];
                $rating=0;

                foreach($show_rating as $value)
                {

                    if(isset($value['starRating']) && is_integer($value['starRating']))
                    {
                        $rating=$rating+$value['starRating'];
                    }
                }

                $avg_rating = $rating / count($show_rating);

            }
            else
            {
                return 0;
            }

        }
    }

}
