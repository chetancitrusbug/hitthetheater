<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Show;
use App\Venue;
use App\Offer;
use App\Location;
use App\Category;
use App\Facilities;
use App\Show_Images;
use App\ShowMetadata;
use App\Transport;
use App\ShowLog;
use App\ShowCategory;
use App\Schedule;
use Session;
use App\ShowOffers;
use Illuminate\Support\Facades\Input;


class ShowController extends Controller
{
    //
    public function __construct(){
        view()->share('route', 'show');
        view()->share('moduleName','Show');
        $facilities = Facilities::where('status', '1')->pluck('facilitieTitle', 'id')->prepend('Select Facility', '');
        $transport = Transport::where('status', '1')->pluck('transportTitle', 'id')->prepend('Select Transport', '');
        $location = Location::where('status', '1')->pluck('locationTitle', 'id')->prepend('Select Location', '');
        $category = Category::where('status', '1')->pluck('categoryTitle', 'id');
        $venueslist = Venue::where('status', '1')->pluck('venueTitle', 'venueCode')->prepend('Select Venue', '');
        $locationcodes = Location::where('status', '1')->pluck('locationTitle', 'locationCode')->prepend('Select Location', '');
        view()->share('locationcodes', $locationcodes);
        view()->share('facilities', $facilities);
        view()->share('transport', $transport);
        view()->share('category', $category);
        view()->share('location', $location);
        view()->share('venueslist', $venueslist);
        ini_set('upload_max_filesize', 20000);
        ini_set('post_max_size', 20000);
    }
    public function index(Request $request)
    {
        return view('admin.show.index');
    }
    public function datatable(Request $request)
    {
        $show = Show::select('id','showTitle','bookingStarts','bookingEnds','orderby','status','popular','showLink')->orderby('orderby', 'desc')->get();
        $show->each(function ($s) {
            $s->setAppends([]);
        });

        return datatables()->of($show)->make(true) ;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
		
		
        return view('admin.show.create');
    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        // dd($request->all());
        \Validator::extend('greater_than', function($attribute, $value, $parameters) { $other = Input::get($parameters[0]);

            return isset($other) and intval($value) > intval($other);
        });

        $rule = [
            'showTitle' => 'required|unique:show|max:40',
            'summary' => 'required',
            'description' => 'required',
            'ageRestriction' => 'required',
            'runningTime' => 'required',
            'venue' => 'required',
            'bookingStarts' => 'required|date|after:today',
            'bookingEnds' => 'required|date|after:bookingStarts',
            'bookingOpen' => 'required|date',
            'category' => 'required|array|min:1',
            'location' => 'required',
            'banner' => 'required|mimes:jpg,jpeg,png|max:2000',
            'rectangle-poster' => 'required|mimes:jpg,jpeg,png|max:2000',
            'metaTitle' => 'required',
            'metaDescription' => 'required',
            'price_form' => 'required|numeric',
            'price_to' => 'required|numeric|greater_than:price_form',
            'slug' => ['required', \Illuminate\Validation\Rule::unique('show', 'slug')]
        ];

        $this->validate($request, $rule,[
            'showTitle.required' => 'Title is required.',
            'location.required' => 'Location is required.',
            'runningTime.required' => 'Running Time is required.',
            'summary.required' => 'Summary is required.',
            'description.required' => 'Description is required',
            'ageRestriction.required' => 'Age Restriction is required',
            'venue.required' => 'Venue is required',
            'bookingStarts.required' => 'Booking Start Date is reuqired',
            'bookingStarts.date' => 'Booking Start Date is have to be date',
            'bookingEnds.required' => 'Booking End Date is reuqired',
            'bookingOpen.required' => 'Booking Open Date is reuqired',
            'bookingOpen.date' => 'Booking Open Date is have to be date',
            'bookingEnds.date' => 'Booking End Date is have to be date',
            'category.required' => 'Category is required',
            'category.array' => 'Category is required',
            'category.min' => 'Please select atleast on category',
            'banner.required' => 'Please Upload Banner',
            'rectangle-poster.required' => 'Please Upload Poster',
            'price_to.required' => 'Please enter price',
            'price_form.required' => 'Please enter price',
            'price_to.greater_than' => 'Please enter price from grater than price to',
        ]);
            $scheduleDaysArray = $request->scheduleDays;

        $orderbymaxvalue = Show::max('orderby');
            $showImages = $request->showImages;

        $showlink = $request->location.'/'.rand(0000,9999);
        $data['showTitle'] = $request->showTitle;
        $data['indexName'] = $request->showTitle;
        $data['showLink'] = $showlink;
        $data['bookingStarts']  = date('Y-m-d',strtotime($request->bookingStarts));
        $data['bookingEnds'] = date('Y-m-d',strtotime($request->bookingEnds));
        $data['bookingOpen'] = date('Y-m-d',strtotime($request->bookingOpen));
        $data['summary'] = $request->summary;
        $data['ageRestriction'] = $request->ageRestriction;
        $data['runningTime'] = $request->runningTime;
        $data['description'] = $request->description;
        $data['priceFrom'] = $request->price_form;
        $data['priceTo'] = $request->price_to;
        $data['slug'] = $this->slugCreate($request->slug);
        $data['status'] = 1;
        $data['venueCode'] = $request->venue;
        $data['showStatus'] = 'custom';
        $data['orderby'] = $orderbymaxvalue+1;
        $data['createdby'] = 1;
        if ($request->iframe_display != ''){
            $data['iframe_display'] = $request->iframe_display;
            $data['youtube_link'] = $this->convertYoutube($request->youtube_link);
        }
        $show = Show::create($data);


        if ($scheduleDaysArray != null) {
            // Schedule::where('showLink', $show->showLink)->delete();

            foreach ($scheduleDaysArray as $dayKey => $dayValue) {

                foreach ($dayValue as $key => $value) {
                    if ($value != null) {
                        $data = array();
                        $data['showLink'] = $show->showLink;
                        $data['show_id'] = $show->id;
                        $data['day_id'] = $dayKey;
                        $data['time'] = $value;
                        $res = Schedule::create($data);
                    }
                }
            }
        }

        //$show->Category()->attach($request->category);
        if ($request->category != null) {
            $cateArr =  $request->category;

            foreach ($cateArr as $key => $value) {
                $category=Category::where('id',$value)->first();

                if($category != null)
                {
                    $data = array();
                    $data['show_id']=$show->id;
                    $data['showLink'] = $show->showLink;
                    $data['category_id']=$category->id;
                    $data = ShowCategory::create($data);
                }
            }
        }
        $path = storage_path('app/public');
        $destinationPath = $path. '/show/';
        if ($showImages != null) {
            foreach ($showImages as $key => $value) {
                $image = $value;
                $fileType = substr($image->getMimeType(), 0, 5);
                $fileName = $this->getImageName($image);
                $image->move($destinationPath . '/' . str_replace(' ','_',$request->showTitle) . '/' , $fileName);
                $galleryimage = array();
                $galleryimage['show_id'] = $show->id;
                $galleryimage['showLink'] = $show->showLink;
                $galleryimage['url'] =  '/' . str_replace(' ','_',$request->showTitle). '/' .$fileName;
                if ($fileType == 'video') {
                    $galleryimage['type'] ='video';
                    $galleryimage['height'] ='';
                    $galleryimage['width'] ='';
                }
                else {
                    $galleryimage['type'] ='production';
                    $galleryimage['height'] ='387';
                    $galleryimage['width'] ='580';
                }

                $galleryimage['created_by'] = 1;
                $galleryimage['title'] = $request->showTitle;
                $show_data=Show_Images::insert($galleryimage);

            }

        }




        // if (!\File::exists($destinationPath)) {
        //     \File::makeDirectory($destinationPath);
        // }
        if($request->hasFile('banner')){
            $image = $request->file('banner');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' . str_replace(' ','_',$request->showTitle) . '/' , $fileName);
            $showBanner['show_id'] = $show->id;
            $showBanner['showLink'] = $show->showLink;
            $showBanner['url'] =  '/' . str_replace(' ','_',$request->showTitle) . '/' .$fileName;
            $showBanner['type'] ='banner';
            $showBanner['title'] = $request->showTitle;
            $show_data = Show_Images::insert($showBanner);
        }
        if ($request->hasFile('rectangle-poster')) {
            $image = $request->file('rectangle-poster');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath. '/'.str_replace(' ','_',$request->showTitle).'/' , $fileName);
            $rectanglePoster['show_id'] = $show->id;
            $rectanglePoster['showLink'] = $show->showLink;
            $rectanglePoster['url'] = '/' . str_replace(' ','_',$request->showTitle) . '/' . $fileName;
            $rectanglePoster['type'] = 'rectangle-poster';
            $rectanglePoster['title'] = $request->showTitle;
            $show_data = Show_Images::insert($rectanglePoster);
        }

        $meta['show_id'] = $show->id;
        $meta['showLink'] = $show->showLink;
        $meta['meta_title'] = $request->metaTitle;
        $meta['meta_description'] = $request->metaDescription;
        $showMetaData = ShowMetadata::insert($meta);

        Session::flash('flash_success', 'Show added!');
        return redirect('admin/show');
    }

    public function edit($id)
    {

		$show = Show::with(['showImages' => function($query)
                {
                    $query->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->orderBy('orderBy','asc');
                }])->where('id',$id)->first();
        if ($show) {
            $showLink = $show->showLink;

            $showImages = Show_Images::where('showLink', $show->showLink)->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->where('orderBy',0)->get();

            if(count($showImages) > 0){
                foreach ($showImages as $key => $value) {
                    $orderbymaxvalue = Show_Images::where('showLink', $show->showLink)->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->max('orderby');
                    $value->orderBy = $orderbymaxvalue + 1;
                    $value->save();
                }
            }

            $show = Show::with(['showImages' => function($query) use($showLink)
                {
                    $query->where('showLink', $showLink)->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->orderBy('orderBy','asc');
                }])->where('id',$id)->first();
            // dd($show->showImages);

            $showCategory = ShowCategory::where('showLink', $show->showLink)->pluck('category_id');
            $showMetadata = ShowMetadata::where('showLink', $show->showLink)->first();
            $offer = Offer::where('showLink', $show->showLink)->pluck('title','offer_id');
            $showoffer = ShowOffers::where('showLink', $show->showLink)->pluck('offer_id');
            $offerDeleted = Offer::where('showLink', $show->showLink)->where('created_by',1)->pluck('title','offer_id');
            $banner = Show_Images::where('showLink', $show->showLink)->where('type', 'banner')->orderBy('id','desc')->first();
            $rectanglePoster = Show_Images::where('showLink', $show->showLink)->where('type', 'rectangle-poster')->orderBy('id','desc')->first();
            $schedules = Schedule::where('showLink', $show->showLink)->orderby('time','ASC')->get();

            $newschedule = array('Mon'=>array(),'Tue'=>array(),'Wed'=>array(),'Thu'=>array(),'Fri'=>array(),'Sat'=>array(),'Sun'=>array());
            foreach ($schedules as $key => $val)
            {
                switch ($val->day_id) {
                    case 'Mon':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Tue':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Wed':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Thu':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Fri':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sat':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sun':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;

                    default:
                        break;
                }


            }

            if ($showoffer != null) {
               $showoffer = $showoffer->toArray();
            }

            $showsingleOfferSelection = ShowOffers::join('offers','offers.offer_id','show_offers.offer_id')->where('show_offers.showLink', $show->showLink)->pluck('offers.title', 'offers.offer_id')->prepend('Select Offer' ,'');

            // dd($show);



            return view('admin.show.edit', compact('show','showCategory','offer','showMetadata','banner','rectanglePoster','offerDeleted','showoffer','showsingleOfferSelection','newschedule'));
        } else {
            Session::flash('flash_warning', 'show is not exist!');
            return redirect('admin/show');
        }

    }


    function convertYoutube($string) {
        return preg_replace(
            "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
            "//www.youtube.com/embed/$2",
            $string
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        \Validator::extend('greater_than', function($attribute, $value, $parameters) { $other = Input::get($parameters[0]);

            return isset($other) and intval($value) > intval($other);
        });
        // dd($request->all());
        $rule = [
            'showTitle' => 'required|max:40',
            'summary' => 'required',
            'description' => 'required',
            'venue' => 'required',
            'bookingStarts' => 'required|date',
            'bookingEnds' => 'required|date|after:bookingStarts',
            'bookingOpen' => 'required|date',
            'category' => 'required|array|min:1',
            'metaDescription' => 'required',
            'ageRestriction' => 'required',
            'runningTime' => 'required',
            'price_form' => 'required|numeric',
            'banner' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'rectangle-poster' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'price_to' => 'required|numeric|greater_than:price_form',
            'slug' => 'required|unique:show,slug,'.$id,
        ];

        $this->validate($request, $rule,[
            'showTitle.required' => 'Title is required.',
            'location.required' => 'Location is required.',
            'summary.required' => 'Summary is required.',
            'description.required' => 'Description is required',
            'ageRestriction.required' => 'Age Restriction is required',
            'runningTime.required' => 'running Time is required',
            'venue.required' => 'Venue is required',
            'bookingStarts.required' => 'Booking Start Date is reuqired',
            'bookingStarts.date' => 'Booking Start Date is have to be date',
            'bookingEnds.required' => 'Booking End Date is reuqired',
            'bookingEnds.date' => 'Booking End Date is have to be date',
            'bookingOpen.required' => 'Booking Open Date is reuqired',
            'bookingOpen.date' => 'Booking Open Date is have to be date',
            'category.required' => 'Category is required',
            'category.array' => 'Category is required',
            'category.min' => 'Please select atleast on category',
            'banner.required' => 'Please Upload Banner',
            'rectangle-poster.required' => 'Please Upload Poster',
            'price_to.required' => 'Please enter price',
            'price_form.required' => 'Please enter price',
            'price_to.greater_than' => 'Please enter price from grater than price to',
        ]);

        $requestData = $request->all();
        $offerArr = $request->offer_ids;
        $scheduleDaysArray = $request->scheduleDays;

        $showImages = $request->showImages;

        unset($requestData['_method'],$requestData['offer_ids']);
        unset($requestData['_token']);
        $show = Show::where('id', $id)->first();

        $requestData['updatedby'] = 1;
        $requestData['ageRestriction'] = $request->ageRestriction;
        $requestData['priceFrom'] = $request->price_form;
        $requestData['priceTo'] = $request->price_to;
        $requestData['venueCode'] = $request->venue;
        $requestData['slug'] = $this->slugCreate($request->slug);
        $requestData['bookingStarts'] = date('Y-m-d', strtotime($requestData['bookingStarts']));
        $requestData['bookingEnds'] = date('Y-m-d', strtotime($requestData['bookingEnds']));
        $requestData['bookingOpen'] = date('Y-m-d',strtotime($requestData['bookingOpen']));
        $showLog = ShowLog::where('code',$show->showLink)->get()->pluck('value' ,'key')->toArray() ;

        if ($request->iframe_display == 'on'){
            $requestData['iframe_display'] = 1;
            $requestData['youtube_link'] = $this->convertYoutube($request->youtube_link);
        }
        else {
            $requestData['iframe_display'] = 0;
            $requestData['youtube_link'] = '';
        }

        if ($scheduleDaysArray != null) {
            Schedule::where('showLink', $show->showLink)->delete();

            foreach ($scheduleDaysArray as $dayKey => $dayValue) {

                foreach ($dayValue as $key => $value) {
                    if ($value != null) {
                        $data = array();
                        $data['showLink'] = $show->showLink;
                        $data['show_id'] = $show->id;
                        $data['day_id'] = $dayKey;
                        $data['time'] = $value;
                        $res = Schedule::create($data);
                    }
                }
            }
        }

        if ($request->metaTitle != null || $request->metaDescription != null) {
            $meatarec = ShowMetadata::where('showLink', $show->showLink)->first();
            if ($meatarec != null) {
                $meatarec->delete();
            }
            $meta['show_id'] = $show->id;
            $meta['showLink'] = $show->showLink;
            $meta['meta_title'] = $request->metaTitle;
            $meta['meta_description'] = $request->metaDescription;
            $showMetaData = ShowMetadata::insert($meta);
        }

        if (isset($request->showImagesData) && count($request->showImagesData) > 0) {
            foreach ($request->showImagesData as $showImagesDataKey => $showImagesDataValue) {
                $requestDataImage = array();
                $requestDataImage['alt'] = $showImagesDataValue['alt'];
                $requestDataImage['title'] = $showImagesDataValue['title'];
                $requestDataImage['orderBy'] = $showImagesDataValue['orderBy'];
                $glryImage = Show_Images::where('id',$showImagesDataValue['id'])->first();
                // $glryImage = $glryImage->update($requestDataImage);
                if ($glryImage != null) {
                    $showImageOrderbyValue = $glryImage->orderBy;

                    $temporderby = Show_Images::where('showLink', $show->showLink)->where('orderBy', $showImagesDataValue['orderBy'])->first();
                    // echo '<pre>'; print_r($temporderby);
                    if ($temporderby == null) {
                        $requestDataImage['orderBy'] = $showImagesDataValue['orderBy'];
                        $glryImage = $glryImage->update($requestDataImage);
                    }
                    else {
                        $tempdata['orderBy'] = $showImageOrderbyValue;
                        $temporderby = $temporderby->update($tempdata);

                        $requestDataImage['orderBy'] = $showImagesDataValue['orderBy'];
                        $glryImage = $glryImage->update($requestDataImage);
                    }
                }
            }
        }

        // $showImagesdata = Show_Images::where('showLink', $show->showLink)->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->get();
        // dd($showImagesdata);
        $path = storage_path('app/public');
        $destinationPath = $path. '/show/';

        if ($showImages != null) {
            foreach ($showImages as $key => $value) {
                $image = $value;
                $fileType = substr($image->getMimeType(), 0, 5);
                $fileName = $this->getImageName($image);
                $image->move($destinationPath . '/' . str_replace(' ','_',$request->showTitle) . '/' , $fileName);
                $galleryimage = array();
                $galleryimage['show_id'] = $show->id;
                $galleryimage['showLink'] = $show->showLink;
                $galleryimage['url'] =  '/' . str_replace(' ','_',$request->showTitle). '/' .$fileName;
                if ($fileType == 'video') {
                    $galleryimage['type'] ='video';
                    $galleryimage['height'] ='';
                    $galleryimage['width'] ='';
                }
                else {
                    $galleryimage['type'] ='production';
                    $galleryimage['height'] ='387';
                    $galleryimage['width'] ='580';
                }

                $galleryimage['created_by'] = 1;
                $galleryimage['title'] = $request->showTitle;
                $show_data=Show_Images::insert($galleryimage);
            }

        }

        if ($offerArr != null) {
            ShowOffers::where('showLink', $show->showLink)->delete();

            foreach ($offerArr as $key => $value) {
                $ofr =Offer::where('offer_id',$value)->first();

                if($ofr != null)
                {
                    $data = array();
                    $data['showLink'] = $show->showLink;
                    $data['offer_id']=$value;
                    ShowOffers::create($data);
                }
            }
        }




        if($request->hasFile('banner')){
            $image = $request->file('banner');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' . str_replace(' ','_',$request->showTitle) . '/' , $fileName);
            $showBanner['show_id'] = $show->id;
            $showBanner['showLink'] = $show->showLink;
            $showBanner['url'] =  '/' . str_replace(' ','_',$request->showTitle). '/' .$fileName;
            $showBanner['type'] ='banner';
            $showBanner['title'] = $request->showTitle;
            $show_data = Show_Images::insert($showBanner);
        }
        if ($request->hasFile('rectangle-poster')) {
            $image = $request->file('rectangle-poster');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath. '/'.str_replace(' ','_',$request->showTitle).'/' , $fileName);
            $rectanglePoster['show_id'] = $show->id;
            $rectanglePoster['showLink'] = $show->showLink;
            $rectanglePoster['url'] = '/' . str_replace(' ','_',$request->showTitle) . '/' . $fileName;
            $rectanglePoster['type'] = 'rectangle-poster';
            $rectanglePoster['title'] = $request->showTitle;
            $rectanglePoster['alt'] = $request->showTitle;
            $show_data = Show_Images::insert($rectanglePoster);
        }

        if ($request->category != null) {
            ShowCategory::where('showLink', $show->showLink)->delete();

            $cateArr =  $request->category;

            foreach ($cateArr as $key => $value) {
                $category=Category::where('id',$value)->first();

                if($category != null)
                {
                    $data = array();
                    $data['show_id']=$show->id;
                    $data['showLink'] = $show->showLink;
                    $data['category_id']=$category->id;
                    $data = ShowCategory::create($data);
                }
            }
        }

        // if ($request->file('banner')) {
        //     $banner = Show_Images::where('showLink', $show->showLink)->where('type', 'banner')->first();

        //     $image = $request->file('banner');
        //     $fileName = 'banner_'.time() . '.' . $image->getClientOriginalExtension();
        //     $image->move($destinationPath . '/' . $request->showTitle . '/' , $fileName);
        //     $showBanner['show_id'] = $show->id;
        //     $showBanner['showLink'] = $show->showLink;
        //     $showBanner['url'] =  '/' . $request->showTitle . '/' .$fileName;
        //     $showBanner['type'] ='banner';
        //     $showBanner['title'] = $request->showTitle;

        //     if ($banner == null) {
        //         $banner = Show_Images::insert($showBanner);
        //     }
        //     else {
        //         $banner = $banner->save($showBanner);
        //     }
        // }

        // if ($request->file('poster')) {
        //     $rectanglePoster = Show_Images::where('showLink', $show->showLink)->where('type', 'rectangle-poster')->first();

        //     $image = $request->file('poster');
        //     $fileName = 'rectangle-poster_'.time() . '.' . $image->getClientOriginalExtension();
        //     $image->move($destinationPath . '/' . $request->showTitle . '/' , $fileName);
        //     $showBanner['show_id'] = $show->id;
        //     $showBanner['showLink'] = $show->showLink;
        //     $showBanner['url'] =  '/' . $request->showTitle . '/' .$fileName;
        //     $showBanner['type'] ='rectangle-poster';
        //     $showBanner['title'] = $request->showTitle;

        //     if ($rectanglePoster == null) {
        //         $rectanglePoster = Show_Images::insert($showBanner);
        //     }
        //     else {
        //         $rectanglePoster = $rectanglePoster->save($showBanner);
        //     }
        // }

        foreach($requestData as $key => $value){
            if($key == 'showTitle' || $key == 'bookingStarts' || $key == 'bookingEnds' || $key == 'status' || $key == 'summary' || $key == 'description'){
                if(!($value === $show->$key)){
                    if(array_key_exists($key,$showLog)){
                        $showLogData = array( 'key' => $key, 'value' => $value);
                        $showLogDataInsert = ShowLog::where('code',$show->showLink)->where('key',$key)->update(['value' => $value]);
                    }else{
                        $showLogData = array('code'=> $show->showLink,'key'=> $key,'value'=>$value);
                        $showLogDataInsert = ShowLog::create($showLogData);
                    }
                }
            }
        }
        $show->update($requestData);

        Session::flash('flash_success', 'Show updated!');
        return redirect('admin/show');
    }

    public function destroy(Request $request,$id)
    {
        $show = Show::find($id);
        $show->delete();
        if($request->has('from_index')){
            $message = "Show Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Show deleted!');

            return redirect('admin/show');
        }

    }

    public function orderbyStatus($id,$orderbyvalue)
    {

        $show = Show::find($id);
        if ($show == null) {
            return response()->json(['message' => 'Show not Found!'],400);
        }
        $showOrderbyValue = $show->orderby;
        $changeOrderbyValue = $orderbyvalue;

        if ($showOrderbyValue == $changeOrderbyValue) {
            return response()->json(['message' => 'Show Update'],200);
        }
        else {
            $orderbyShow = Show::where('show.orderby', $changeOrderbyValue)->first();
            if ($orderbyShow == null) {
                $show->orderby = $orderbyvalue;
                $show->save();
            }
            else {
                $orderbyShow->orderby = $showOrderbyValue;
                $orderbyShow->save();

                $show->orderby = $orderbyvalue;
                $show->save();
            }
             return response()->json(['message' => 'Show Update'],200);
        }
    }

    public function showImageRemove($showLocationCode,$showId,$id)
    {
        $showImages = Show_Images::find($id);
        if ($showImages == null) {
            return response()->json(['message' => 'Image not Found!'],400);
        }
        $showImages->delete();

        $showImagesHtml ='';
        return response()->json(['showImagesHtml'=>$showImagesHtml,'message' => 'Image Deleted'],200);

    }

    public function showImageDetail($imgId)
    {
        $showImages = Show_Images::find($imgId);
        if ($showImages == null) {
            return response()->json(['message' => 'Image not Found!','code' => 400],200);
        }
        return response()->json(['data'=>$showImages,'message' => '','code' => 200],200);

    }

    public function showImageupdate(Request $request)
    {
        $rules = array(
            'alt' => 'required',
            'orderBy' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
            return response()->json(['message' => $messages, 'code' => 400], 200);
        }

        $requestData = $request->all();
        $showImage = Show_Images::find($request->img_id);

        if ($showImage == null) {
            return response()->json(['message' => 'Offer not exist', 'code' => 400], 200);
        }

        $showImageOrderbyValue = $showImage->orderBy;

        $orderby = Show_Images::where('orderBy', $request->orderBy)->first();
        if ($orderby == null) {
            $showImage = $showImage->update($requestData);
        }
        else {
            $data['orderBy'] = $showImageOrderbyValue;
            $orderby = $orderby->update($data);

            $requestData['orderBy'] = $request->orderBy;
            $showImage = $showImage->update($requestData);
        }


        $message = 'Image updated!';

        $showImagesHtml ='';

        return response()->json(['showImagesHtml'=>$showImagesHtml,'message' => $message,'code'=>200], 200);
    }

    public function showImageList($showLocationCode,$showId)
    {
        $showImagesHtml = '';

        $showImagesUrls = Show_Images::where('showLink',$showLocationCode.'/'.$showId)->whereIn('type', ['production', 'video'])->whereIn('width', ['580', ''])->whereIn('height', ['387', ''])->orderBy('orderBy','asc')->get();
        $count = 0;
        foreach($showImagesUrls as $showImage)
        {
            if(isset($showImage->url) && $showImage->url != null)
            {
                $path=public_path().'/storage/show'.$showImage->url;

                if(file_exists($path))
                {
                    $path=  url('').'/storage/show'.$showImage->url;
                    $img =  $path;
                }
                elseif($showImage->created_by == 0)
                {
                    $img = $showImage->url;
                }
                if(isset($img) && $img != null)
                {

                    $showImagesHtml .= '<li><input type="hidden" name="showImagesData['.$count.'][id]" value="' . $showImage->id . '" class="form-control">';
                    if($showImage->type == 'video')
                    {
                        $showImagesHtml .= '
                        <div class="col-md-2">
                            <div class="form-group glry-form-group">
                                <video controls title="' .$showImage->title.'" alt="' .$showImage->alt.'" order="' .$showImage->orderBy.'">
                                    <source src="' . $img . '" type="video/mp4">
                                </video>
                            </div>
                        </div>
                        ';
                    }
                    else
                    {
                        $showImagesHtml .= '<div class="col-md-2">
                            <div class="form-group glry-form-group">
                                <img src="' . $img . '" title="' .$showImage->title.'" alt="' .$showImage->alt.'" order="' .$showImage->orderBy.'">
                            </div>
                        </div>';
                    }

                    $showImagesHtml .= '<div class="col-md-3">
                            <div class="form-group glry-form-group">
                                <label class="col-sm-12 control-label">* Title: </label>
                                <div class="col-sm-12">
                                    <input type="text" name="showImagesData['.$count.'][title]" id="title" value="'.$showImage->title.'" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group glry-form-group">
                                <label class="col-sm-12 control-label">* Alter: </label>
                                <div class="col-sm-12">
                                    <input type="text" name="showImagesData['.$count.'][alt]" id="alt" value="'.$showImage->alt.'" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group glry-form-group">
                                <label class="col-sm-12 control-label">* Orderby: </label>
                                <div class="col-sm-12">
                                    <input type="number" name="showImagesData['.$count.'][orderBy]" id="orderBy" value="'.$showImage->orderBy.'" class="form-control">
                                </div>
                            </div>
                        </div>
                        <button type="button"  class="btn imgRemove"  data-url="' . url('/admin/show/showImageRemove') . '/' . $showImage->showLink . '" data-imgid="' . $showImage->id . '"  ><i class="fa fa-times"></i></button></li>';
                        $count++;
                }
            }
        }
        return response()->json(['data'=>$showImagesHtml,'message' => '','code' => 200],200);

    }

}
