<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\VenueCategories;
use App\VenueCategory;
use Session;

class VenueCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'venuecategory');
        view()->share('moduleName','Venue Category');
    }

    public function index(Request $request)
    {
        return view('admin.venuecategory.index');
    }

    public function datatable(Request $request)
    {
        $category = VenueCategories::orderby('id','desc')->get();
        return datatables()->of($category)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.venuecategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'categoryTitle' => 'required|unique:venue_categories|max:40',
            'slug' => ['required', \Illuminate\Validation\Rule::unique('venue_categories', 'slug')]
        ];

        $this->validate($request, $rule);
        $data['categoryTitle'] = $request->categoryTitle;
        $data['slug'] = $this->slugCreate($request->slug);
        $category = VenueCategories::create($data);

        Session::flash('flash_success', 'Category added!');
        return redirect('admin/venuecategory');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$category = VenueCategories::where('id',$id)->first();
        if ($category) {
            return view('admin.venuecategory.edit', compact('category'));
        } else {
            Session::flash('flash_warning', 'Category is not exist!');
            return redirect('admin/venuecategory');
        }

    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'categoryTitle' => 'required|max:40',
            'slug' => 'required|unique:venue_categories,slug,'.$id,
        ]);

        $requestData = $request->all();

        $category = VenueCategories::where('id',$id)->first();
        $requestData['slug'] = $this->slugCreate($request->slug);
        $requestData['status'] = isset($requestData['status'])?1:0;
        $category->update($requestData);

        Session::flash('flash_success', 'Category updated!');
        return redirect('admin/venuecategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $category = VenueCategories::find($id);
        if ($category != null) {
            $venueExistcategory = VenueCategory::where('category_id', $id)->get();
            if (count($venueExistcategory) > 0 ) {
                    return response()->json(['message' => 'Already used in another venue.you can not delete this category.'], 200);
                }
            $category->delete();
        }

        if($request->has('from_index')){
            $message = "category Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'Category deleted!');

            return redirect('admin/venuecategory');
        }

    }



}
