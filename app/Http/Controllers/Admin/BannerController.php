<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use Session;
use File;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'banner');
        view()->share('moduleName','Banners');
    }

    public function index(Request $request)
    {

        return view('admin.banner.index');
    }

    public function datatable(Request $request)
    {
        $banner = Banner::get();
        return datatables()->of($banner)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $rule = [
            'title' => 'required|unique:banner',
            'image' => 'required|mimes:jpg,jpeg,png|dimensions:min_width=825,min_height=340|max:1000',
        ];
        $requestData = $request->all();
        if (isset($requestData['bannerLink']) && ($requestData['bannerLink'] != '')) {
            $rule['bannerLink'] =  'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        $this->validate($request, $rule);

        $orderbymaxvalue = Banner::max('orderby');
        $data['orderby'] = $orderbymaxvalue+1;
        $data['title'] = $request->title;
        $data['content'] = $request->content;
        $data['bannerLink'] = $request->bannerLink;
        $path = storage_path('app/public');
        $destinationPath = $path. '/banner/';
        if($request->hasFile('image')){
            $image = $request->file('image');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' , $fileName);
            $data['image'] =  '/' . $fileName;
        }
        else{
            $data['image'] = '';
        }

        $banner = Banner::create($data);

        Session::flash('flash_success', 'Banner added!');
        return redirect('admin/banner');
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$banner = Banner::where('id',$id)->first();
        if ($banner) {
            return view('admin.banner.edit', compact('banner'));
        } else {
            Session::flash('flash_warning', 'banner is not exist!');
            return redirect('admin/banner');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        // dd($request->all());
        $regex = '_^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:.\d{1,3}){3})(?!(?:169.254|192.168)(?:.\d{1,3}){2})(?!172.(?:1[6-9]|2\d|3[0-1])(?:.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]-)[a-z\x{00a1}-\x{ffff}0-9]+)(?:.(?:[a-z\x{00a1}-\x{ffff}0-9]-)[a-z\x{00a1}-\x{ffff}0-9]+)(?:.(?:[a-z\x{00a1}-\x{ffff}]{2,})).?)(?::\d{2,5})?(?:[/?#]\S)?$_iuS';
        // $regex = '/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        $rule = [
            'title' => 'required',
            'image' => 'sometimes|mimes:jpg,jpeg,png|dimensions:min_width=825,min_height=340|max:1000',
        ];
        $requestData = $request->all();
        if (isset($requestData['bannerLink']) && ($requestData['bannerLink'] != '')) {
            $rule['bannerLink'] =  'required|regex:/^(https?:\/\/)?([\dA-Za-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        }
        $this->validate($request, $rule);



        $banner = Banner::where('id',$id)->first();

        $requestData['status'] = isset($request->status)?1:0;
        $requestData['title'] = $request->title;
        $requestData['bannerLink'] = $request->bannerLink;
        $requestData['content'] = $request->content;

        $path = storage_path('app/public');
        $destinationPath = $path. '/banner/';
        if($request->hasFile('image')){
            $image = $request->file('image');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' , $fileName);
            $requestData['image'] =  '/' . $fileName;
        }

        $banner->update($requestData);

        Session::flash('flash_success', 'Banner updated!');
        return redirect('admin/banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $banner = Banner::find($id);
        $banner->delete();
        if($request->has('from_index')){
            $message = "banner Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'banner deleted!');

            return redirect('admin/banner');
        }

    }
    public function orderbyStatus($id,$orderbyvalue)
    {

        $banner = Banner::find($id);
        if ($banner == null) {
            return response()->json(['message' => 'Banner not Found!'],400);
        }
        $bannerOrderbyValue = $banner->orderby;
        $changeOrderbyValue = $orderbyvalue;

        if ($bannerOrderbyValue == $changeOrderbyValue) {
            return response()->json(['message' => 'Banner Update'],200);
        }
        else {
            $orderbybanner = Banner::where('orderby', $changeOrderbyValue)->first();
            if ($orderbybanner == null) {
                $banner->orderby = $orderbyvalue;
                $banner->save();
            }
            else {
                $orderbybanner->orderby = $bannerOrderbyValue;
                $orderbybanner->save();

                $banner->orderby = $orderbyvalue;
                $banner->save();
            }
             return response()->json(['message' => 'Banner Update'],200);
        }
    }
}

