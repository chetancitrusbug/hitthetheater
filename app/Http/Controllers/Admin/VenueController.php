<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Venue;
use App\Location;
use App\Category;
use App\Facilities;
use App\Transport;
use App\VenueLog;
use App\VenueCategory;
use App\VenueFacilities;
use App\VenueSliders;
use App\VenueMetaData;
use App\VenueTransportInfo;
use Session;
use App\VenueCategories;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'venue');
        view()->share('moduleName','Venue');
        $facilities = Facilities::where('status', '1')->pluck('facilitieTitle', 'id')->prepend('Select Facility', '');
        $transport = Transport::where('status', '1')->pluck('transportTitle', 'id')->prepend('Select Transport', '');
        $location = Location::where('status', '1')->pluck('locationTitle', 'id')->prepend('Select Location', '');
        $locationcodes = Location::where('status', '1')->pluck('locationTitle', 'locationCode')->prepend('Select Location', '');
        $category = VenueCategories::where('status', '1')->pluck('categoryTitle', 'id')->prepend('Select Category', '');
        // $category = Category::where('status', '1')->pluck('categoryTitle', 'id')->prepend('Select Category', '');
        view()->share('facilities', $facilities);
        view()->share('transport', $transport);
        view()->share('category', $category);
        view()->share('location', $location);
        view()->share('locationcodes', $locationcodes);
    }

    public function index(Request $request)
    {
        return view('admin.venue.index');
    }

    public function datatable(Request $request)
    {
        $venue = Venue::select('venues.*', 'locations.locationTitle')
            ->leftjoin('locations', 'venues.locationCode', 'locations.locationCode')
            //->where('venues.status',1)
            ->orderby('venues.orderby', 'desc')->get();
        $venue->each(function ($s) {
            $s->setAppends([]);
        });
        return datatables()->of($venue)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {


        return view('admin.venue.create');
    }

    public function deleteallvenue()
    {
        Venue::truncate();
        VenueFacilities::truncate();
        VenueTransportInfo::truncate();
        VenueMetaData::truncate();

        Session::flash('flash_success', 'Venue deleted!');
        return redirect('admin/venue');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $orderbymaxvalue = Venue::max('orderby');

        $rule = [
            'venueTitle' => 'required|unique:venues|max:40',
            'venueDescription' => 'required',
            'categoryId' => 'required',
            'locationId' => 'required',
            'facilities' => 'required|array|min:1',
            'addressLine1' => 'required',
            'postcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'metaTitle' => 'required',
            'metaDescription' => 'required',
            'poster' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'seat-plan' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'slug' => ['required', \Illuminate\Validation\Rule::unique('venues', 'slug')]
        ];



        $this->validate($request, $rule);
        $data = $request->all();

        // $location = Location::find($data['locationId']);

        $data['locationCode'] =  $data['locationId'];
        $data['venueCode'] = rand(0000,9999);
        $data['status'] = 1;
        $data['slug'] = $this->slugCreate($request->slug);
        $data['orderby'] = $orderbymaxvalue+1;
        $data['createdby'] = 1;
        if ($data['addressLine2'] != null) {
            $data['addressLine2'] =  $data['addressLine2'];
        }
        else {
            $data['addressLine2'] =  '';
        }
        $venue = Venue::create($data);

        $path = storage_path('app/public');
        $destinationPath = $path. '/venue/';
        if($request->hasFile('poster')){
            $image = $request->file('poster');
            $fileName = $this->getImageName($image);

            $image->move($destinationPath . '/' . str_replace(' ','_',$request->venueTitle) . '/' , $fileName);
            $venueSliders['venueId'] = $venue->id;
            $venueSliders['venueCode'] = $venue->venueCode;
            $venueSliders['image'] =  '/' . str_replace(' ','_',$request->venueTitle) . '/' .$fileName;
            $venueSliders['type'] ='primary-image';
            $venueSliders['width'] ='';
            $venueSliders['height'] = '';
            $venueSliders['title'] = $request->venueTitle;
            VenueSliders::insert($venueSliders);
        }
        if($request->hasFile('seat-plan')){
            $seatplan=VenueSliders::where('type','seat-plan')->where('venueCode', $venue->venueCode)->delete();
            $image = $request->file('seat-plan');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' . str_replace(' ','_',$request->venueTitle) . '/' , $fileName);
            $venueSliders['venueId'] = $venue->id;
            $venueSliders['venueCode'] = $venue->venueCode;
            $venueSliders['image'] =  '/' . str_replace(' ','_',$request->venueTitle) . '/' .$fileName;
            $venueSliders['type'] ='seat-plan';
            $venueSliders['width'] ='';
            $venueSliders['height'] = '';
            $venueSliders['title'] = $request->venueTitle;
            VenueSliders::insert($venueSliders);
        }

        if(isset($data['facilities']) && is_array($data['facilities']))
        {
            foreach ($data['facilities'] as $facilityKey => $facilityValue) {

                VenueFacilities::create(['facilitieId'=>$facilityValue,'venueId'=>$venue->id,'venueCode'=>$venue->venueCode]);
            }

        }
        if ($request->metaTitle != null || $request->metaDescription != null) {
            $meta['venue_id'] = $venue->id;
            $meta['venueCode'] = $venue->venueCode;
            $meta['meta_title'] = $request->metaTitle;
            $meta['meta_description'] = $request->metaDescription;
            VenueMetaData::insert($meta);
        }
        if ($request->categoryId != null) {
            foreach ($data['categoryId'] as $categoryIdKey => $categoryIdValue) {
                VenueCategory::create(['category_id'=>$categoryIdValue,'venueCode'=>$venue->venueCode]);
            }
        }

        Session::flash('flash_success', 'Venue added!');
        return redirect('admin/venue');
    }


    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $venue = Venue::where('id',$id)->first();

        if ($venue) {
            $venueFacilities = VenueFacilities::where('venueCode', $venue->venueCode)->pluck('facilitieId');
            $venueCategory = VenueCategory::where('venueCode', $venue->venueCode)->pluck('category_id');
            $seatplan=VenueSliders::where('type','seat-plan')->where('venueCode', $venue->venueCode)->first();
            $metadata = VenueMetaData::where('venueCode', $venue->venueCode)->first();

            $seatplan_url = '';
            if(isset($seatplan->image) && $seatplan->image)
            {
                $path=public_path().'/storage/venue'.$seatplan->image;

                if(file_exists($path))
                {
                    $path= url('').'/storage/venue'.$seatplan->image;
                    $seatplan_url = $path;
                }
                elseif(@fopen($seatplan->image,'r'))
                {
                    $seatplan_url = $seatplan->image;
                }
            }
            return view('admin.venue.edit', compact('venue','venueFacilities','venueCategory','seatplan_url','metadata'));
        } else {
            Session::flash('flash_warning', 'Venue is not exist!');
            return redirect('admin/venue');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'venueTitle' => 'required',
            'venueDescription' => 'required',
            'categoryId' => 'required',
            'facilities' => 'required|array|min:1',
            'addressLine1' => 'required',
            'postcode' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'metaTitle' => 'required',
            'metaDescription' => 'required',
            'poster' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'seat-plan' => 'sometimes|mimes:jpg,jpeg,png|max:2000',
            'slug' => 'required|unique:venues,slug,'.$id,
        ]);

        $requestData = $request->all();

        $venue = Venue::where('id',$id)->first();
        unset($requestData['_method']);
        unset($requestData['_token']);
        $requestData['updatedby'] = 1;
        if ($requestData['addressLine2'] != null) {
            $requestData['addressLine2'] =  $requestData['addressLine2'];
        }
        else {
            $requestData['addressLine2'] =  '';
        }
        $requestData['slug'] = $this->slugCreate($request->slug);

        if (isset($requestData['locationId'])) {
            $requestData['locationCode'] =  $requestData['locationId'];
        }

        $venueLog = VenueLog::where('code', $venue->venueCode)->get()->pluck('value', 'key')->toArray();

        $path = storage_path('app/public');
        $destinationPath = $path. '/venue/';
        if($request->hasFile('poster')){
            $image = $request->file('poster');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' . str_replace(' ','_',$request->venueTitle) . '/' , $fileName);
            $venueSliders['venueId'] = $venue->id;
            $venueSliders['venueCode'] = $venue->venueCode;
            $venueSliders['image'] =  '/' . str_replace(' ','_',$request->venueTitle) . '/' .$fileName;
            $venueSliders['type'] ='primary-image';
            $venueSliders['width'] ='';
            $venueSliders['height'] = '';
            $venueSliders['title'] = $request->venueTitle;
            VenueSliders::insert($venueSliders);
        }

        if($request->hasFile('seat-plan')){
            $seatplan=VenueSliders::where('type','seat-plan')->where('venueCode', $venue->venueCode)->delete();
            $image = $request->file('seat-plan');
            $fileName = $this->getImageName($image);
            $image->move($destinationPath . '/' . str_replace(' ','_',$request->venueTitle) . '/' , $fileName);
            $venueSliders['venueId'] = $venue->id;
            $venueSliders['venueCode'] = $venue->venueCode;
            $venueSliders['image'] =  '/' . str_replace(' ','_',$request->venueTitle) . '/' .$fileName;
            $venueSliders['type'] ='seat-plan';
            $venueSliders['width'] ='';
            $venueSliders['height'] = '';
            $venueSliders['title'] = $request->venueTitle;
            VenueSliders::insert($venueSliders);
        }

        if(isset($requestData['facilities']) && is_array($requestData['facilities']))
        {
            $venueFacilities = VenueFacilities::where('venueCode', $venue->venueCode)->delete();

            foreach ($requestData['facilities'] as $facilityKey => $facilityValue) {
                VenueFacilities::create(['facilitieId'=>$facilityValue,'venueId'=>$venue->id,'venueCode'=>$venue->venueCode]);
            }

        }
        if ($request->metaTitle != null || $request->metaDescription != null) {
            $meatarec = VenueMetaData::where('venueCode', $venue->venueCode)->first();
            if ($meatarec != null) {
                $meatarec->delete();
            }
            $meta['venue_id'] = $venue->id;
            $meta['venueCode'] = $venue->venueCode;
            $meta['meta_title'] = $request->metaTitle;
            $meta['meta_description'] = $request->metaDescription;
            VenueMetaData::insert($meta);
        }


        if(isset($requestData['categoryId']) && is_array($requestData['categoryId']))
        {
            $VenueCategory = VenueCategory::where('venueCode', $venue->venueCode)->delete();

            foreach ($requestData['categoryId'] as $categoryIdKey => $categoryIdValue) {
                VenueCategory::create(['category_id'=>$categoryIdValue,'venueCode'=>$venue->venueCode]);
            }

        }

        foreach ($requestData as $key => $value) {

            if($key == 'venueTitle' || $key == 'venueDescription' || $key == 'addressLine1' || $key == 'addressLine2' || $key == 'postcode' || $key == 'latitude' || $key == 'longitude' || $key == 'locationCode'){
                // echo $key;
                // var_dump((!($value === $venue->$key)));
                // var_dump(array_key_exists($key, $venueLog));
                if (!($value === $venue->$key)) {
                    if (array_key_exists($key, $venueLog)) {
                        $VenueLogData = array('key' => $key, 'value' => $value);
                        $VenueLogDataInsert = VenueLog::where('code', $venue->venueCode)->where('key', $key)->update(['value' => $value]);
                    } else {
                        $VenueLog = array('code' => $venue->venueCode, 'key' => $key, 'value' => $value);
                        $VenueLogDataInsert = VenueLog::create($VenueLog);
                    }
                }
            }
        }
        $venue->update($requestData);

        Session::flash('flash_success', 'Venue updated!');
        return redirect('admin/venue');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $venue = Venue::find($id);
        $venue->delete();
        if($request->has('from_index')){
            $message = "venue Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'venue deleted!');

            return redirect('admin/venue');
        }

    }

    public function orderbyStatus($id,$orderbyvalue)
    {

        $venue = Venue::find($id);
        if ($venue == null) {
            return response()->json(['message' => 'Venue not Found!'],400);
        }
        $venueOrderbyValue = $venue->orderby;
        $changeOrderbyValue = $orderbyvalue;

        if ($venueOrderbyValue == $changeOrderbyValue) {
            return response()->json(['message' => 'Venue Update'],200);
        }
        else {
            $orderbyvenue = Venue::where('orderby', $changeOrderbyValue)->first();
            if ($orderbyvenue == null) {
                $venue->orderby = $orderbyvalue;
                $venue->save();
            }
            else {
                $orderbyvenue->orderby = $venueOrderbyValue;
                $orderbyvenue->save();

                $venue->orderby = $orderbyvalue;
                $venue->save();
            }
             return response()->json(['message' => 'Venue Update'],200);
        }
    }

}
