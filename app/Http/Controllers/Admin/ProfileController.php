<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Session;
use App\User;
use Illuminate\Support\Facades\Lang;

class ProfileController extends Controller
{

    public function __construct(){
        view()->share('route', 'profile');
        view()->share('moduleName','Profile');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        return view('admin.profile.index', compact('user'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {

        $user = Auth::user();

        $id = $user->id;
        return view('admin.profile.edit', compact('user'));
    }

    /**
     * @param Request $request
     */
    public function update(Request $request,$id)
    {
         $this->validate($request, [
            'name' => 'required|max:30'
        ]);

        $requestData = $request->except(['email']);

        $user = User::where('id',$id)->first();

        $user->update($requestData);


        Session::flash('flash_success',__('Your profile updated successfully.'));

        return redirect('admin/profile');

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword()
    {
        return view('admin.profile.changePassword');
    }

    /**
     * @param Request $request
     */
    public function updatePassword(Request $request)
    {
        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);

        $cur_password = $request->input('current_password');

        $user = Auth::user();

        if (Hash::check($cur_password, $user->password)) {
            $user->password = Hash::make($request->input('password'));
            $user->save();

            Session::flash('flash_success','Password changed successfully.');

            return redirect('admin/profile');

        } else {
            // $error = array('current-password' => __('Please enter correct current password'));

            // return redirect()->back()->withErrors($error);
            Session::flash('flash_error','Please enter correct current password.');

            return redirect('admin/profile');

        }
        Session::flash('flash_error','Something wrong. Please try again latter.');

        return redirect('admin/profile');

    }
}
