<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact_us;
use Session;

class ContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'contactus');
        view()->share('moduleName','Contact Us');
    }

    public function index(Request $request)
    {

        return view('admin.contactus.index');
    }

    public function datatable(Request $request)
    {
        $contactus = Contact_us::get();
        return datatables()->of($contactus)->make(true) ;
    }

    public function destroy(Request $request,$id)
    {
        $contactus = Contact_us::find($id);
        $contactus->delete();
        if($request->has('from_index')){
            $message = "contactus Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'contactus deleted!');

            return redirect('admin/contactus');
        }

    }
}
