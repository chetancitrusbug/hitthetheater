<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Role;
use App\User;
use App\LocationLogs;
use App\VenueLog;
use App\ShowLog;
use DB;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon as Carbon;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function __construct(){
        view()->share('route', 'users');
        view()->share('moduleName','Users');
    }

    public function index(Request $request)
    {

        return view('admin.users.index');
    }

    public function datatable(Request $request)
    {
        $users = User::all();

        return datatables()->of($users)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name' => 'required'
        ];

        $this->validate($request, $rule);

        $data = $request->except('password');
        $data['user_type'] = 'user';
        $data['status'] = 1;
        $data['password'] = bcrypt($request->password);

        $data['activation_token'] = sha1(time() . uniqid() . $data['email']);
        $user = User::create($data);

        $user->assignRole('USER');

        Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function afterRegister($user)
    {
        $this->sendEmail($user);
    }

    public function sendEmail(User $user)
    {

        return $user->notify(new  \App\Notifications\ActivationLink($user));
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            Session::flash('flash_error', 'This link is expired.');
            return redirect('/');
        }
        else if ($user->is_active == 1) {
            Session::flash('flash_success', 'Your account have already activated.Please login!');
            return redirect('/login');
        }

        $user->activation_token = null;
        $user->activation_time = null;
        $user->is_active = 1;
        $user->save();


        if (!$this->guard()->check()) {
           // $this->guard()->login($user);
        }
		// Auth::loginUsingId($user->id);
       return redirect('/login')->with('flash_success', 'Your account has been activated.');
       // return redirect()->intended($this->redirectPath())

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$user = User::where('id',$id)->first();
        if ($user) {
            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }

    }

    public function show(Request $request,$id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $requestData = $request->except(['email']);

        $user = User::where('id',$id)->first();

        $requestData['status'] = isset($requestData['status'])?1:0;
        $user->update($requestData);

        Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);

        if($user == null)
        {
            $message= "already deleted !!";
            return response()->json(['message' => $message],200);
        }
        $user->roles()->sync([]);
        $user->delete();

        if($request->has('from_index')){
            $message = "User Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'User deleted!');

            return redirect('admin/users');
        }

    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;
        $arr['status'] = $status;
        if($table == 'locations' && $table == 'venues' && $table == 'show' )
        {
            $arr['updatedby'] = 1;
        }

        DB::table($table)->where('id',$id)->update($arr);
        $tableData = DB::table($table)->where('id', $id)->first();
        if($table == 'locations' && $tableData != null)
        {
            $locationlogdata = LocationLogs::where('code',$tableData->locationCode)->where('key', 'status')->where('value', $status)->first();
            if($locationlogdata == null)
            {
                $data['key'] = "status";
                $data['value'] = $status;
                $data['code'] = $tableData->locationCode;
                $location_logs = LocationLogs::create($data);
            }
        }

        if($table == 'venues' && $tableData != null)
        {
            $logdata = VenueLog::where('code',$tableData->venueCode)->where('key', 'status')->where('value', $status)->first();
            if($logdata == null)
            {
                $data['key'] = "status";
                $data['value'] = $status;
                $data['code'] = $tableData->venueCode;
                $log = VenueLog::create($data);
            }
        }
        if($table == 'show' && $tableData != null)
        {
            $logdata = ShowLog::where('code',$tableData->showLink)->where('key', 'status')->where('value', $status)->first();
            if($logdata == null)
            {
                $data['key'] = "status";
                $data['value'] = $status;
                $data['code'] = $tableData->showLink;
                $log = ShowLog::create($data);
            }
        }

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function popularStatus($table,$status,$id)
    {
        $status = ($status == 0)? '1' : '0';

        DB::table($table)->where('id',$id)->update(['popular'=>$status]);

        return response()->json(['message' => 'Popular Update'],200);
    }
}
