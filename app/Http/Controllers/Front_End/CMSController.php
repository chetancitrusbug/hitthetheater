<?php

namespace App\Http\Controllers\Front_End;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Show_Images;
use App\Category;
use App\Show;
use Carbon\Carbon;
use App\CmsSetting;
use App\Contact_us;
use Session;
use Mail;

class CMSController extends Controller
{
    //
    protected $banner;
    protected $category;
    protected $category_count;
    protected $cms_setting;

    public function __construct(){

        view()->share('route', 'CMS');
        view()->share('pageTitle', 'CMS');
        view()->share('metaTitle', 'CMS');
        view()->share('metaDescription', '');
        $this->category=Category::where('status',1)->select('categoryTitle','id','slug')->get();
        $this->category_count=Category::where('status',1)->count();

        $this->shows = Show::select('show.id','show.slug', 'show.showTitle', 'show.priceFrom', 'show.priceTo', 'show.summary', 'show.indexName', 'show.bookingStarts', 'show.bookingEnds', 'show.offer_id', 'offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->whereDate('show.bookingStarts', '<=', Carbon::now()->format('Y-m-d'))->whereDate('show.bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->orderby('show.orderby', 'ASC')->get();

        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);
        view()->share('category', $this->category);
        view()->share('shows', $this->shows);
        view()->share('category_count', $this->category_count);

    }

    public function about_us(Request $request)
    {
        view()->share('pageTitle', 'About Us');
        view()->share('metaTitle', 'About Us');
        view()->share('metaDescription', '');
        return view('fornt_end.about_us');
    }

    public function contact_us(Request $request)
    {
        view()->share('pageTitle', 'Contact Us');
        view()->share('metaTitle', 'Contact Us');
        view()->share('metaDescription', '');
        return view('fornt_end.contact_us');
    }
    public function maintenance(Request $request)
    {
        view()->share('pageTitle', 'Maintenance');
        return view('fornt_end.maintenance');
    }

    public function contect_us_post(Request $request)
    {

          $rule = [
            'message' => 'required',
            'email' => 'required|email',
            'name' => 'required',
            'subject' => 'required',
        ];
        $this->validate($request,$rule);

        $contact_us=new Contact_us();
        $contact_us->message=$request->message;
        $contact_us->email=$request->email;
        $contact_us->name=$request->name;
        $contact_us->subject=$request->subject;
        $contact_us->save();

        $data= isset($this->cms_setting) ? $this->cms_setting : '';

        Mail::send('emails.emailtemplate',['userdata' => $contact_us], function ($message) use ($contact_us,$data)
        {
            $message->subject($contact_us['subject']);
            $message->from($contact_us['email']);
            $message->to($data->contact_us_email);
        });

        Mail::send('emails.contactusthankyou',['userdata' => $contact_us], function ($message) use ($contact_us,$data)
        {
            $message->subject('Thank You For Contact Us - Hit The Theatre');
            $message->from($data->contact_us_email);
            $message->to($contact_us['email']);
        });
		Session::flash('contact_success', 'We will get back to you asap!');

        return redirect()->back();

    }
}
