<?php

namespace App\Http\Controllers\Front_End;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Venue;
use App\Show;
use App\Category;
use App\VenueCategories;


class SitemapController extends Controller
{
    public function index()
    {
        $category = Category::select('categoryTitle','id','slug')->where('status',1)->get();
        $venueCategories = VenueCategories::select('categoryTitle','id','slug')->where('status',1)->get();
        $venue = Venue::select('venueTitle', 'id','slug')->where('status', 1)->get();
        $show = Show::select('showTitle', 'id','slug')->where('status', 1)->get();


        return response()->view('fornt_end.sitemap', [
            'category' => $category,'venueCategories' => $venueCategories,'venue' => $venue,'show' => $show
        ])->header('Content-Type', 'text/xml');
    }


    // public function show()
    // {
    //     $data = Show::select('showTitle as title', 'id','slug')->where('status', 1)->get();

    //     return response()->view('fornt_end.innerSitemap', [
    //         'data' => $data,'type'=>'show'
    //     ])->header('Content-Type', 'text/xml');
    // }
    // public function venue()
    // {
    //     $data = Venue::select('venueTitle as title', 'id','slug')->where('status', 1)->get();

    //     return response()->view('fornt_end.innerSitemap', [
    //         'data' => $data,'type'=>'venue'
    //     ])->header('Content-Type', 'text/xml');
    // }
    // public function category()
    // {
    //     $data = Category::select('categoryTitle as title','id','slug')->where('status',1)->get();

    //     return response()->view('fornt_end.innerSitemap', [
    //         'data' => $data,'type'=>'category'
    //     ])->header('Content-Type', 'text/xml');
    // }
    // public function venueCategories()
    // {
    //     $data = VenueCategories::select('categoryTitle as title','id','slug')->where('status',1)->get();

    //     return response()->view('fornt_end.innerSitemap', [
    //         'data' => $data,'type'=>'venueCategories'
    //     ])->header('Content-Type', 'text/xml');
    // }


}
