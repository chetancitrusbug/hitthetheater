<?php

namespace App\Http\Controllers\Front_End;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Show_Images;
use App\Category;
use App\Show;
use App\Venue;
use Carbon\Carbon;
use App\CmsSetting;
use App\Setting;
use App\Banner;
use Session;
use App\ShowCategory;

class HomeController extends Controller
{
    //
    protected $banner;
    protected $bannerForFrontendSetting;
    protected $category;
    protected $category_count;

    public function __construct(){

        view()->share('pageTitle', '');
        view()->share('metaTitle', '');
        view()->share('metaDescription', '');
        view()->share('route', 'home');
        $bannerForFrontendSetting = Setting::where('key', 'banner_for_frontend')->first();
        if(isset($bannerForFrontendSetting->value) && $bannerForFrontendSetting->value == 0)
        {
            $this->banner=Show_Images::where('type', 'banner')->inRandomOrder()->limit(5)->get();
            $this->bannerForFrontendSetting = 0 ;
        }
        else {
            $this->bannerForFrontendSetting = 1;
            $this->banner = Banner::where('status', 1)->orderby('orderby', 'ASC')->limit(5)->get();
        }

        $this->category=Category::where('status',1)->select('categoryTitle','id','slug')->get();
        $this->category_count=Category::where('status',1)->count();
        $this->shows = Show::select('show.id','show.slug', 'show.showTitle', 'show.priceFrom', 'show.priceTo', 'show.summary', 'show.indexName','show.bookingStarts','show.bookingEnds','show.offer_id','offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->whereDate('show.bookingStarts', '<=', Carbon::now()->format('Y-m-d'))->whereDate('show.bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->orderby('show.orderby', 'ASC')->get();

        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);
        view()->share('banner', $this->banner);
        view()->share('bannerForFrontendSetting', $this->bannerForFrontendSetting);
        view()->share('category', $this->category);
        view()->share('shows', $this->shows);
        view()->share('category_count', $this->category_count);

    }

    public function index(Request $request)
    {

        $target=(isset($request->target))?$request->target:'Musicals';
        //$offset=(isset($request->offset))?$request->offset:0;
        $category=Category::where('status',1)->where('categoryTitle',$target)->first();

        $show =array();

        if($category != null)
        {
            // select distinct `show`.*, `show_category`.`category_id` as `pivot_category_id`, `show_category`.`showLink` as `pivot_show_id` from `show` inner join `show_category` on `show`.`showLink` = `show_category`.`showLink` where `show_category`.`category_id` = 2 limit 15
            //'show.id','show.showTitle','show.priceFrom','show.priceTo','show.summary','show.square_image'
            // $show=Show::select('show.id','show.showTitle','show.priceFrom','show.priceTo','show.summary','show_images.url as rectangle_poster')->leftjoin('show_category','show.showLink','show_category.showLink','show.indexName','show.bookingStarts','show.bookingEnds')->leftjoin('categories','categories.id','show_category.category_id')->leftjoin('show_images','show.showLink','show_images.showLink')->where('show_images.type','rectangle-poster')
            // ->where('show.status',1)->where('categories.id',$category->id)->groupby('show.showLink')->orderby('show.orderby', 'ASC')->paginate(20);

            $show=Show::select('show.id','show.slug','show.showTitle','show.priceFrom','show.priceTo','show.summary','show.offer_id','offers.title as singleOfferTitle')->leftjoin('show_category','show.showLink','show_category.showLink','show.indexName','show.bookingStarts','show.bookingEnds')->leftjoin('categories','categories.id','show_category.category_id')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')
            ->where('show.status',1)->where('categories.id',$category->id)->groupby('show.showLink')->orderby('show.orderby', 'ASC')->paginate(20);


            if(count($show) > 0)
            {
                $this->shows= $show;
            }
            else
            {
                $this->shows=[];
            }
            view()->share('shows', $this->shows);
            //dd($show);
            // $show = Show::select('show.id as show_main_id','show.*','show_images.url')->where('show.status', '1')
            // ->join('show_category','show_category.showLink','show.showLink')
            // ->join('categories','categories.id', 'show_category.category_id')
            // ->join('show_images',function($query){
            // $query->on('show_images.showLink','=','show.showLink')
            // ->where('show_images.type', 'rectangle-poster');
            // })->limit(15)->groupBy('show.id')->get();

        }
        if ($request->ajax()) {
            $html=view('fornt_end.show_list',compact('target','show'))->render();
            return response()->json(['success' => true, 'html' => $html,'target' => $target]);
        }


        // $popularShow = Show::with('venue','imageShow')->where('status','1')->where('popular','1')->get();
        // dd($popularShow);

        $popularShow = Show::select('show.id as show_main_id','show.slug as show_slug','show.*','venues.*','show_images.*')->where('show.status', '1')->where('show.popular','1')->leftJoin('venues', 'venues.venueCode','show.venueCode')->leftJoin('show_images',function($query){
            $query->on('show_images.showLink','=','show.showLink')
            ->where('show_images.type', 'rectangle-poster');
        })->limit(4)->groupBy('show.id')->get();

        return view('fornt_end.home',compact('target','show', 'popularShow'));
    }

    public function getImageofShow(Request $request,$id)
    {
        $code = 200;
        $message = '';
        $url = null;

        $existShow = Show::where('id', $id)->first();
        if ($existShow != null) {
            $path=public_path().'/storage/show'.$existShow->rectangle_image;

            if(file_exists($path))
            {
                $path=url('').'/storage/show'.$existShow->rectangle_image;
                $url = $path;
            }
            elseif(@fopen($existShow->rectangle_image,'r'))
            {
                $url = $existShow->rectangle_image;

            }
            else
            {
                $url = url('').'/frontend/images/no_image.png';
            }

        }
        else {
            $url = url('').'/frontend/images/no_image.png';
        }
        return response()->json(['url'=>$url], 200);
    }

    public function showGalleryImageGet(Request $request,$id)
    {
        $code = 200;
        $message = '';
        $url = null;
        $existShow = Show_Images::where('id', $id)->first();
        if ($existShow != null) {
            if ($existShow->url != null) {
                $path=public_path().'/storage/show'.$existShow->url;

                if(file_exists($path))
                {
                    $path=url('').'/storage/show'.$existShow->url;
                    $url = $path;
                }
                elseif(@fopen($existShow->url,'r'))
                {
                    $url = $existShow->url;

                }
                else
                {
                    $url = '';
                }

            }
            else {
                $url = '';
            }
        }
        else {
                $url = '';
            }
        return response()->json(['url'=>$url], 200);
    }

	public function globalSearch(Request $request){
		if($request->search != ''){
			$categories=Category::where('status',1)->where('categoryTitle','like','%'.$request->search.'%')->select('categoryTitle','id','slug')->limit(5)->get();
			$shows=Show::where('status',1)->where('showTitle','like','%'.$request->search.'%')->select('showTitle','id','slug')->whereDate('show.bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->limit(5)->get();
            $venues=Venue::where('status',1)->where('venueTitle','like','%'.$request->search.'%')->orWhere('venueDescription','like','%'.$request->search.'%')->select('venueTitle','id','slug')->limit(5)->get();

            $closedshows = Show::where('status',1)->where('showTitle','like','%'.$request->search.'%')->select('showTitle','id','slug')->whereDate('show.bookingEnds', '<=', Carbon::now()->format('Y-m-d'))->limit(5)->get();

			$html=view('fornt_end.search_list',compact('categories','shows','venues','closedshows'))->render();
			return response()->json(['success' => true, 'html' => $html]);
		}else{
			return response()->json(['success' => false]);
		}

		//$category=Category::where('status',1)->where('categoryTitle',$target)->first();
	}

	public function bookTicket(Request $request){

		if($request->selectshow != '' && $request->tickets != ''){
			$shows=Show::where('status',1)->where('id',$request->selectshow)->first();
			if($shows){
				$showsId = explode('/',$shows->showLink);
				$showsId = $showsId[1];
				$showstartDate = implode('',explode('-',$shows->bookingStarts));
				//$url = "https://ticket.londontheatre.co.uk/booking#/calendar?product_id=".$showsId."&product_type=show&venue_id=".$shows->venueCode."&qt=".$request->tickets;
                //$url ="http://demo.entstix.com/book/calendar-3week/".$showsId."/".$shows->venueCode."/".$request->tickets."?bookingDate=".$showstartDate."&type=A";

                $url ="https://hitthetheatre.tixuk.com/booking/seating-plan/?product_id=".$showsId."&product_type=show&performance_type=E&venue_id=".$shows->venueCode."&qt=".$request->tickets."&slot=19:30&date=".date('Y-m-d');
				return redirect($url);
			}else{
				Session::flash('flash_error', 'Show not Found, Please select onother show');
            return redirect()->back();
			}

		}else{
			Session::flash('flash_error', 'Please select show and tickets');
            return redirect()->back();
		}
    }

    public function termsAndConditions(){
        view()->share('pageTitle', 'Terms & Condition');
        view()->share('metaTitle', 'Terms & Condition');
        view()->share('metaDescription', '');
		view()->share('route', 'CMS');
        return view('fornt_end.termsandcondition');
    }

    public function privacyPolicy()
    {	view()->share('route', 'CMS');
        view()->share('pageTitle', 'Privacy Policy');
        view()->share('metaTitle', 'Privacy Policy');
        view()->share('metaDescription', '');
        return view('fornt_end.privacypolicy');
    }
    public function cookiePolicy()
    {
        view()->share('route', 'CMS');
        view()->share('pageTitle', 'Cookie Policy');
        view()->share('metaTitle', 'Cookie Policy');
        view()->share('metaDescription', '');
        return view('fornt_end.cookie');
    }
    public function faq()
    {
        view()->share('route', 'CMS');
        view()->share('pageTitle', 'FAQ');
        view()->share('metaTitle', 'FAQ');
        view()->share('metaDescription', '');
        return view('fornt_end.faq');
    }

	public function tickets(){
        view()->share('pageTitle', 'Tickets');
        view()->share('metaTitle', 'Tickets');
        view()->share('metaDescription', '');
		return view('fornt_end.tickets');
	}
}
