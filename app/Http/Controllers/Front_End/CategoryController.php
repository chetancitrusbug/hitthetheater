<?php

namespace App\Http\Controllers\Front_End;

use App\Category;
use App\CmsSetting;
use App\Http\Controllers\Controller;
use App\Show;
use App\Venue;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\VenueCategory;
use App\ShowCategory;
use App\VenueCategories;

class CategoryController extends Controller
{
    //
    protected $banner;
    protected $category;
    protected $category_count;

    public function __construct(){
        $this->cms_setting=CmsSetting::first();
        $this->VenueCategories = VenueCategories::where('status', 1)->select('categoryTitle','slug', 'id')->get();
        view()->share('VenueCategories', $this->VenueCategories);
        view()->share('cms_setting', $this->cms_setting);
        view()->share('pageTitle', 'Categories');
        view()->share('metaTitle', 'Category');
        view()->share('metaDescription', '');
        /*$this->banner=Show_Images::where('type','banner')->inRandomOrder()->limit(5)->get();

        $this->category_count=Category::where('status',1)->count();
        $this->shows=Show::where('status',1)->whereDate('bookingStarts','<=',Carbon::now()->format('Y-m-d'))
                    ->whereDate('bookingEnds','>=',Carbon::now()->format('Y-m-d'))->get();
        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);

        view()->share('shows', $this->shows);
        view()->share('category_count', $this->category_count); */

    }

    public function index(Request $request)
    {
        view()->share('pageTitle', 'Categories');
        view()->share('route', 'catgoryAll');
        $this->category = Category::where('status', 1)->select('categoryTitle','slug', 'id')->paginate(15);
    // dd($this->category[0]->show_count);
        view()->share('categories', $this->category);
        return view('fornt_end.categoryList');
    }
    public function viewCategory(Request $request, $id)
    {

		view()->share('route', 'catgory');
        $this->category=Category::where('status',1)->select('categoryTitle','slug','id')->limit(35)->get();
		view()->share('category', $this->category);
        $categorySingle = Category::where('status',1)->where('slug',$id)->first();
        $show =array();

        if($categorySingle != null)
        {
            $show=Show::select('show.id','show.slug','show.showTitle','show.priceFrom','show.priceTo','show.summary','show.offer_id','offers.title as singleOfferTitle')->leftjoin('show_category','show.showLink','show_category.showLink','show.indexName','show.bookingStarts','show.bookingEnds')->leftjoin('categories','categories.id','show_category.category_id')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')
            ->where('show.status',1)->where('categories.slug',$id)->groupby('show.showLink')->orderby('show.orderby', 'ASC')->paginate(20);

        }
        view()->share('metaTitle', (isset($categorySingle->categoryTitle) && $categorySingle->categoryTitle != null) ? $categorySingle->categoryTitle : 'Category');
        view()->share('metaDescription', '');
        view()->share('shows',$show);
        view()->share('pageTitle', (isset($categorySingle->categoryTitle) && $categorySingle->categoryTitle != null) ? 'Category | '.$categorySingle->categoryTitle : 'Category');
        return view('fornt_end.category-single', compact('show', 'categorySingle'));
    }

    public function viewTheaterCategory(Request $request, $id)
    {

		view()->share('route', 'theater');
        $this->category=VenueCategories::where('status',1)->select('categoryTitle','slug','id')->limit(35)->get();
		view()->share('category', $this->category);
        $categorySingle = VenueCategories::where('status',1)->where('slug',$id)->first();
        $show =array();

        if($categorySingle != null)
        {
            $venues = Venue::leftjoin('venue_category','venues.venueCode','venue_category.venueCode')->leftjoin('venue_categories','venue_categories.id','venue_category.category_id')
            ->where('venues.status',1)->where('venue_categories.slug',$id)->groupby('venues.venueCode')->orderby('venues.orderby', 'DESC')->paginate(20);

            $this->venues = $venues;

            if(count($venues) > 0)
            {
                $this->venues=$venues;

            }
        }
        view()->share('venues',$this->venues);
        view()->share('pageTitle', (isset($categorySingle->categoryTitle) && $categorySingle->categoryTitle != null) ? 'Category | '.$categorySingle->categoryTitle : 'Category');
        view()->share('metaTitle', (isset($categorySingle->categoryTitle) && $categorySingle->categoryTitle != null) ? $categorySingle->categoryTitle : 'Category');
        view()->share('metaDescription', '');
        return view('fornt_end.category-theater-single', compact('venues', 'categorySingle'));
    }




}
