<?php

namespace App\Http\Controllers\Front_End;

use App\Category;
use App\CmsSetting;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Setting;
use App\Show;
use App\Show_Images;
use App\Offer;
use App\ShowOffers;
use App\VenueFacilities;
use App\VenueTransportInfo;
use App\VenueMetaData;
use App\ShowMetadata;
use App\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use App\VenueCategories;

class TheaterController extends Controller
{
    //
    protected $banner;
    protected $category;
    protected $category_count;

    public function __construct()
    {
        $this->cms_setting = CmsSetting::first();
        $this->category = Category::where('status', 1)->select('categoryTitle', 'id','slug')->get();
        $this->VenueCategories = VenueCategories::where('status', 1)->select('categoryTitle', 'id','slug')->get();
        $this->banner=Show_Images::where('type','banner')->inRandomOrder()->limit(5)->get();
        $this->category_count=Category::where('status',1)->count();

        $this->shows = Show::select('show.id','show.slug', 'show.showTitle', 'show.priceFrom', 'show.priceTo', 'show.summary', 'show.indexName', 'show.bookingStarts', 'show.bookingEnds', 'show.offer_id', 'offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->whereDate('show.bookingStarts', '<=', Carbon::now()->format('Y-m-d'))->whereDate('show.bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->orderby('show.orderby', 'ASC')->get();
        view()->share('pageTitle', 'Theatres');
        view()->share('metaTitle', 'Theatre');
        view()->share('metaDescription', '');
        view()->share('category', $this->category);
        view()->share('VenueCategories', $this->VenueCategories);
        view()->share('cms_setting', $this->cms_setting);
        view()->share('shows', $this->shows);
        view()->share('category_count', $this->category_count);
        view()->share('banner', $this->banner);
    }

    public function index(Request $request)
    {
        view()->share('route', 'theater');
        $perpage = 20;
        $venues = Venue::where('status', 1)->orderby('orderby', 'ASC')->paginate($perpage);

        if(count($venues) > 0){
            $shows = Show::where('status', 1)->whereDate('bookingStarts', '<=', Carbon::now()->format('Y-m-d'))->whereDate('bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->get();

            view()->share('venues', $venues);
            // dd($venues[0]->venue_image);
            //view()->share('shows', $shows);
            return view('fornt_end.theater-list');
        } else {
            Session::flash('flash_error', 'Venue Not found!');
            return redirect()->back();
        }
    }

    public function viewTheater(Request $request, $id)
    {
        view()->share('route', 'theater');
        view()->share('routedeatil', 'theater');
        $venue = Venue::with(['VenueSliders'])->where('status', 1)->where('slug', $id)->first();
        if ($venue) {


            $shows = Show::select('show.*','offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->where('venueCode', $venue->venueCode)->groupby('show.showLink')->paginate(12);
            $showsCurrent = Show::select('show.*','offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->where('venueCode', $venue->venueCode)->whereDate('show.bookingStarts', '<=', Carbon::now()->format('Y-m-d'))->whereDate('show.bookingEnds', '>=', Carbon::now()->format('Y-m-d'))->groupby('show.showLink')->orderby('show.orderby', 'ASC')->limit(3)->get();
            $showsUpcoming = Show::select('show.*', 'offers.title as singleOfferTitle')->leftjoin('offers', 'show.offer_id', 'offers.offer_id')->where('status', 1)->where('venueCode', $venue->venueCode)->whereDate('show.bookingStarts', '>', Carbon::now()->format('Y-m-d'))->groupby('show.showLink')->orderby('show.orderby', 'ASC')->limit(3)->get();
            // dd($showsCurrent);
            $venueFacilities = VenueFacilities::select('facilities.facilitieTitle','venue_facilities.venueCode')->leftJoin('facilities','venue_facilities.facilitieId','facilities.id')->where('venue_facilities.venueCode', $venue->venueCode)->distinct()->get();
            $venueTransportInfo = VenueTransportInfo::leftJoin('transport', 'venue_transport_info.transportId', 'transport.id')->where('venue_transport_info.venueCode', $venue->venueCode)->groupby('transport.id')->get();

            $metadata = VenueMetaData::where('venueCode', $venue->venueCode)->first();

            // $schedule = Schedule::where('show_id', $id)->get();
            // $newschedule = array();
            // foreach ($schedule as $key => $val)
            // {
            //     $newschedule[$val->day_id][] = $val->time;
            // }
            $venueCategory = VenueCategories::leftJoin('venue_category', 'venue_category.category_id', 'venue_categories.id')->where('venue_categories.status', 1)->where('venue_category.venueCode', $venue->venueCode)->pluck('venue_categories.categoryTitle');
                // dd($venueCategory);
            view()->share('venueCategory', $venueCategory);
            view()->share('show', $shows);
            view()->share('showsUpcoming', $showsUpcoming);
            view()->share('showsCurrent', $showsCurrent);
            view()->share('shows', $shows);
            view()->share('venue', $venue);
            view()->share('venueFacilities', $venueFacilities);
            view()->share('venueTransportInfo', $venueTransportInfo);
            view()->share('pageTitle', (isset($metadata->meta_title) && $metadata->meta_title != null) ? $metadata->meta_title : 'Theatre');
            // dd($venue->venue_seating);
            view()->share('metaTitle', (isset($metadata->meta_title) && $metadata->meta_title != null) ? $metadata->meta_title : 'Theatre');
            view()->share('metaDescription', (isset($metadata->meta_description) && $metadata->meta_description != null) ? $metadata->meta_description : '');

            return view('fornt_end.theater_shows');

        } else {
            Session::flash('flash_error', 'Theater Not found!');
            return redirect()->back();
        }
    }

    // public function categoryShowList(Request $request,$id)
    // {
    //     view()->share('route', 'category-show-list');
    //     $showlist = Show::join('show_category', 'show_category.show_id', '=', 'show.id')
    //             ->select('show.*','show_category.show_id','show_category.category_id')
    //             ->where('show_category.category_id',$id)
    //             ->orderBy('show.status' , 1)->get();
    //     if(count($showlist) > 0)
    //     {
    //         view()->share('show', $showlist);
    //         return view('fornt_end.category-show-list');
    //     }
    //     else{
    //         Session::flash('flash_error', 'Show Not found!');
    //         return redirect()->back();
    //     }

    // }

    public function viewShowDetail(Request $request, $id)
    {
        $offers = array();
        $metadata = array();

        view()->share('route', 'showDetail');
        $showdetails =Show::where('status',1)->where('slug',$id)->first();
        if($showdetails != null){
            $banner=Show_Images::where('type','banner')->where('showLink',$showdetails->showLink)->orderBy('id','desc')->get();
            $poster=Show_Images::where('type','rectangle-poster')->where('showLink',$showdetails->showLink)->orderBy('id','desc')->get();
            $metadata = ShowMetadata::where('showLink',$showdetails->showLink)->first();

            if(count($banner) > 0){
                $path=public_path().'/storage/show'.$banner[0]->url;

                if(file_exists($path))
                {
                    $path=  url('').'/storage/show'.$banner[0]->url;
                    $banner = $path;
                }
                elseif(@fopen($banner[0]->url,'r'))
                {
                    $banner = $banner[0]->url;
                }
                else
                {
                    $banner = url('').'/frontend/images/show-img.jpg';
                }
                view()->share('banner', $banner);
            }else{
                view()->share('banner', url('').'/frontend/images/show-img.jpg');
            }

            if(count($poster) > 0){
                view()->share('poster', $poster[0]->url);
            }else{
                view()->share('poster', url('').'/frontend/images/show-small-poster.jpg');
            }
            $shows=Show::with(['showImages' => function($query)
                {
                    $query->whereIn('type', ['production','video'])->whereIn('width',['580',''])->whereIn('height',['387',''])->orderBy('orderBy','asc');
                }])->where('status',1)->where('showLink',$showdetails->showLink)->get();

            if(count($shows) > 0){
                $this->authorization = Setting::where('key', 'authorization')->first();
                $this->usernameSetting = Setting::where('key', 'username')->first();
                $this->passwordSetting = Setting::where('key', 'password')->first();
                $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
                $venue = Venue::with(['VenueSliders'])->where('venues.status', 1)->where('venues.venueCode', $showdetails->venueCode)->first();

                $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
                $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
                $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

                $header['Authorization'] = $authorization;
                $offer = array();

                $showoffer = ShowOffers::join('offers','offers.offer_id','show_offers.offer_id')->where('show_offers.showLink', $showdetails->showLink)->groupby('offers.offer_id')->get();

                if ($showoffer != null) {
                    $offer = $showoffer;
                }



                $schedule = Schedule::where('showLink',$showdetails->showLink)->orderby('time','ASC')->get();
                // dd($shows[0]->rectangle_image);
                $newschedule = array('Monday'=>array(),'Tuesday'=>array(),'Wednesday'=>array(),'Thursday'=>array(),'Friday'=>array(),'Saturday'=>array(),'Sunday'=>array());
                foreach ($schedule as $key => $val)
                {
                    switch ($val->day_id) {
                            case 'Mon':
                                $newschedule['Monday'][$key] = array();
                                $newschedule['Monday'][$key] = (in_array($val->time,$newschedule['Monday']) ? '' : $val->time);
                                if($newschedule['Monday'][$key] == '')
                                unset($newschedule['Monday'][$key]);

                                $newschedule['Monday'] = array_values($newschedule['Monday']);
                            break;

                        case 'Tue':
                            $newschedule['Tuesday'][$key] = array();
                            $newschedule['Tuesday'][$key] = (in_array($val->time,$newschedule['Tuesday']) ? '' : $val->time);
                            if($newschedule['Tuesday'][$key] == '')
                            unset($newschedule['Tuesday'][$key]);

                            $newschedule['Tuesday'] = array_values($newschedule['Tuesday']);
                            break;
                        case 'Wed':
                            $newschedule['Wednesday'][$key] = array();
                            $newschedule['Wednesday'][$key] = (in_array($val->time,$newschedule['Wednesday']) ? '' : $val->time);
                            if($newschedule['Wednesday'][$key] == '')
                            unset($newschedule['Wednesday'][$key]);

                            $newschedule['Wednesday'] = array_values($newschedule['Wednesday']);
                            break;
                        case 'Thu':
                            $newschedule['Thursday'][$key] = array();
                            $newschedule['Thursday'][$key] = (in_array($val->time,$newschedule['Thursday']) ? '' : $val->time);
                            if($newschedule['Thursday'][$key] == '')
                            unset($newschedule['Thursday'][$key]);

                            $newschedule['Thursday'] = array_values($newschedule['Thursday']);
                            break;
                        case 'Fri':
                            $newschedule['Friday'][$key] = array();
                            $newschedule['Friday'][$key] = (in_array($val->time,$newschedule['Friday']) ? '' : $val->time);
                            if($newschedule['Friday'][$key] == '')
                            unset($newschedule['Friday'][$key]);

                            $newschedule['Friday'] = array_values($newschedule['Friday']);
                            break;
                        case 'Sat':
                            $newschedule['Saturday'][$key] = array();
                            $newschedule['Saturday'][$key] = (in_array($val->time,$newschedule['Saturday']) ? '' : $val->time);
                            if($newschedule['Saturday'][$key] == '')
                            unset($newschedule['Saturday'][$key]);

                            $newschedule['Saturday'] = array_values($newschedule['Saturday']);
                            break;
                        case 'Sun':
                            $newschedule['Sunday'][$key] = array();
                            $newschedule['Sunday'][$key] = (in_array($val->time,$newschedule['Sunday']) ? '' : $val->time);
                            if($newschedule['Sunday'][$key] == '')
                            unset($newschedule['Sunday'][$key]);

                            $newschedule['Sunday'] = array_values($newschedule['Sunday']);
                            break;
                        default:
                            break;
                    }


                }
                // dd($venue->VenueSliders);
                $shows[0]->closedshow = 0;
                if ($shows[0]->bookingEnds <= Carbon::now()->format('Y-m-d')) {
                    $shows[0]->closedshow = 1;
                }

                $showCategory = Category::join('show_category','show_category.category_id','categories.id')->where('categories.status', 1)->where('show_category.showLink', $showdetails->showLink)->pluck('categories.categoryTitle');

                view()->share('pageTitle', (isset($metadata->meta_title) && $metadata->meta_title != null) ? $metadata->meta_title : 'Show');
                view()->share('metaTitle', (isset($metadata->meta_title) && $metadata->meta_title != null) ? $metadata->meta_title : 'Show');
                view()->share('metaDescription', (isset($metadata->meta_description) && $metadata->meta_description != null) ? $metadata->meta_description : '');
                view()->share('offers', $offer);
                view()->share('showCategory', $showCategory);
                view()->share('shows', $shows);
                view()->share('venue', $venue);
                view()->share('schedule', $newschedule);

                return view('fornt_end.show-detail');

            }else{
                Session::flash('flash_error', 'Show Not found!');
                return redirect()->back();
            }
        }
        else{
                Session::flash('flash_error', 'Show Not found!');
                return redirect()->back();
            }
    }

    public function venueCategoryFilter(Request $request){
		if($request->search != ''){
            $venueIds = VenueCategories::leftJoin('venue_category', 'venue_category.category_id', 'venue_categories.id')->leftJoin('venues', 'venues.venueCode', 'venue_category.venueCode')->where('venue_categories.status', 1)->where('venue_categories.categoryTitle','like','%'.$request->search.'%')->pluck('venues.id');
            $venues=Venue::where('status',1)->whereIn('id',$venueIds)->limit(20)->get();
            $html=view('fornt_end.venue_search_list',compact('venues'))->render();
			return response()->json(['success' => true, 'html' => $html]);
		}else{
			$venues=Venue::where('status',1)->paginate(20);
            $html=view('fornt_end.venue_search_list',compact('venues'))->render();
			return response()->json(['success' => false, 'html' => $html]);
		}

		//$category=Category::where('status',1)->where('categoryTitle',$target)->first();
	}

}
