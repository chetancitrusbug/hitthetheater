<?php

namespace App\Http\Controllers\Front_End;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CmsSetting;
use Carbon\Carbon;
use App\News;
use Session;
use Mail;

class NewsController extends Controller
{
    protected $cms_setting;

    public function __construct(){

        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);
        view()->share('pageTitle', 'News');
        view()->share('metaTitle', 'News');
        view()->share('metaDescription', '');

    }
    public function index(Request $request)
    {
        view()->share('route', 'news');
        $perpage = 10;
        $generalNews = News::where('isFuture', 0)->where('status', 1)->orderby('id', 'desc')->paginate($perpage);
        $featureNews = News::where('isFuture', 1)->where('status', 1)->orderby('orderby', 'desc')->paginate($perpage);

        if(count($generalNews) == 0 && count($featureNews) == 0){
            Session::flash('flash_error', 'News Not found!');
            return redirect()->back();
        }
        else {
            $arrayCount = max(array($generalNews,$featureNews));
            view()->share('generalNews', $generalNews);
            view()->share('arrayCount', $arrayCount);
            view()->share('featureNews', $featureNews);
            return view('fornt_end.news-list');
        }
    }

    public function viewNews(Request $request, $slug)
    {
        view()->share('route', 'news');
        $news = News::where('slug', $slug)->where('status', 1)->first();
        $recentlyNews = News::where('slug','!=', $slug)->where('status', 1)->orderby('id', 'desc')->limit(3)->get();
        if ($news) {
            view()->share('news', $news);
            view()->share('recentlyNews', $recentlyNews);
            return view('fornt_end.news-details');
        } else {
            Session::flash('flash_error', 'News Not found!');
            return redirect()->back();
        }
    }

}
