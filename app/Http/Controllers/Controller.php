<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getImageName($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileName = substr($fileName, 0, strrpos($fileName, "."));
        $fileName = str_replace(' ', '-', rtrim($fileName));
        $fileName = preg_replace('/[^A-Za-z0-9\-]/', '-', $fileName);
        $fileName = preg_replace('/--+/', '', $fileName);
        $fileName = strtolower($fileName);
        return $fileName.'.'.$image->getClientOriginalExtension();
    }
}
