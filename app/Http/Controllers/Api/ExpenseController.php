<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Invoice;
use App\User;
use App\Client;
use App\Expense;
use App\Category;

class ExpenseController extends Controller{

    public function list(){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 400;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }

        $expense = Expense::select([
            'client_expense.*',
            'client_expense.id as client_expense_id',
            DB::raw('(CASE WHEN upload != "" THEN CONCAT("'.url('/').'","/",upload) ELSE "" END) AS upload'),
        ])
        ->where('user_id',request()->user_id);

        if(request()->has('client_id')){
            $expense->where('client_id',request()->client_id);
        }

        if(request()->has('invoice_id')){
            $expense->where('invoice_id',request()->invoice_id);
        }
        $expense = $expense->get();

        if(empty($expense)){
            $code = 404;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $expense, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function store(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Expense added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'client_id' => 'required',
            'invoice_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'desc' => 'required',
            'net_amount' => 'required',
            'vat' => 'required',
            'upload' => 'required|mimes:jpg,jpeg,png',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();
            $invoice = Invoice::where('id',$request->invoice_id)->first();
            $category = Category::where('id',$request->category_id)->first();

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if(!$client){
                $message = 'Client not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if(!$invoice){
                $message = 'Invoice not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if(!$category){
                $message = 'Category not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $expense = new Expense();
            $expense->user_id = $request->user_id;
            $expense->client_id = $request->client_id;
            $expense->invoice_id = $request->invoice_id;
            $expense->category_id = $request->category_id;
            $expense->title = $request->title;
            $expense->desc = $request->desc;
            $expense->net_amount = $request->net_amount;
            $expense->vat = $request->vat;
            $vat = round(($request->net_amount*$request->vat)/100,2);
            $expense->total_amount = $request->net_amount+$vat;
            $expense->status = 1;

            $name = str_replace(' ','_',$request->title);

            if($request->file('upload')){
                $image = $request->file('upload');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/expense', $filename);

                $expense->upload = 'uploads/expense/'.$filename;
            }

            $expense->save();
            $expense->upload = url('/').'/'.$expense->upload;
            $expense->expense_id = $expense->id;
            $data = $expense;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $expense = Expense::where('id',$request->expense_id)
                        ->where('user_id',$request->user_id)
                        ->where('client_id',$request->client_id)
                        ->where('invoice_id',$request->invoice_id)
                        ->first();

        if(empty($expense)){
            $code = 404;
            $messages = 'Expense not exist';
            $status = false;
        }

        return response()->json(['result' => $expense, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Invoice updated Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'invoice_image' => 'mimes:jpg,jpeg,png',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $expense = Expense::where('id',$request->expense_id)
                            ->where('user_id',$request->user_id)
                            ->where('client_id',$request->client_id)
                            ->where('invoice_id',$request->invoice_id)
                            ->first();

            if(!$expense){
                $message = 'Expense not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $data['category_id'] = ($request->category_id)?$request->category_id:$expense->category_id;
            $data['title'] = ($request->title)?$request->title:$expense->title;
            $data['desc'] = ($request->desc)?$request->desc:$expense->desc;
            $data['net_amount'] = ($request->net_amount)?$request->net_amount:$expense->net_amount;
            $data['vat'] = ($request->vat)?$request->vat:$expense->vat;
            $vat = round(($data['net_amount']*$data['vat'])/100,2);
            $data['total_amount'] = $vat+$data['net_amount'];

            $name = str_replace(' ','_',($request->title)?$request->title:$expense->title);

            if($request->file('upload')){
                if(file_exists($expense->upload)){
                    unlink($expense->upload); //delete previously uploaded image
                }
                $image = $request->file('upload');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/expense', $filename);

                $data['upload'] = 'uploads/expense/'.$filename;
            }
            $expense->update($data);
            $expense->upload = url('/').'/'.$expense->upload;
            $expense->expense_id = $expense->id;
            $data = $expense;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
}