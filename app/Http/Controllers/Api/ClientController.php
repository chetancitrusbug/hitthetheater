<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Client;
use App\User;

class ClientController extends Controller{

    public function list(){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 400;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }

        $client = Client::select([
                    'client.*','client.id as client_id',
                    DB::raw('(CASE WHEN image != "" THEN CONCAT("'.url('/').'","/",image) ELSE "" END) AS image'),
                ])
                ->where('user_id',request()->user_id)->get();

        if(empty($client)){
            $code = 404;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $client, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function store(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Client added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'email' => 'required|email',
            'user_id' => 'required',
            'client_name' => 'required',
            'phone' => 'required|numeric',
        );

        if($request->get('image')){
            $rules['image'] = 'mimes:jpg,jpeg,png';
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $result = Client::where('email',$request->email)->where('user_id',$request->user_id)->first();
            $user = User::find($request->user_id);

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if($result){
                $message = 'Client already exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$request->except(['image']),'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $client = new Client();
            $client->user_id = $request->user_id;
            $client->client_name = $request->client_name;
            $client->email = $request->email;
            $client->phone = $request->phone;
            $client->address = $request->address;
            $client->status = 1;

            $name = str_replace(' ','_',$request->client_name);

            if($request->file('image')){
                $image = $request->file('image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/client', $filename);

                $client->image = 'uploads/client/'.$filename;
            }

            $client->save();
            $client->image = url('/').'/'.$client->image;
            $client->client_id = $client->id;
            $data = $client;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();

        if(empty($client)){
            $code = 404;
            $messages = 'Client not exist';
            $status = false;
        }

        return response()->json(['result' => $client, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Client updated Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'phone' => 'numeric'
        );

        if($request->get('image')){
            $rules['image'] = 'mimes:jpg,jpeg,png';
        }

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();
            if(!$client){
                $message = 'Client not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $data['client_name'] = ($request->client_name)?$request->client_name:$client->client_name;
            $data['phone'] = ($request->phone)?$request->phone:$client->phone;
            $data['address'] = ($request->address)?$request->address:$client->address;

            $name = str_replace(' ','_',($request->client_name)?$request->client_name:$client->client_name);

            if($request->file('image')){

                if(file_exists($client->image)){
                    unlink($client->image); //delete previously uploaded image
                }

                $image = $request->file('image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/client', $filename);

                $data['image'] = 'uploads/client/'.$filename;
            }
            $client->update($data);
            $client->image = url('/').'/'.$client->image;
            $client->client_id = $client->id;
            $data = $client;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
}