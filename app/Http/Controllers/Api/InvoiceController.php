<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use DB;
use App\Invoice;
use App\User;
use App\Client;

class InvoiceController extends Controller{

    public function list(){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = 'true';
        $error = '';

        $user = User::where('id',request()->user_id)->where('api_token',request()->api_token)->first();

        if(!$user){
            $message = 'Not valid user';
            $code = 400;
            $status = false;
            return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
        }

        $invoice = Invoice::select([
                'invoice.*',
                DB::raw('CONCAT("'.url('/').'","/",invoice_image) AS invoice_image'),
                'invoice.id as inv_id',
            ])
            ->where('user_id',request()->user_id);

        if(request()->has('client_id')){
            $invoice->where('client_id',request()->client_id);
        }
        $invoice = $invoice->get();

        if(empty($invoice)){
            $code = 404;
            $messages = 'Failed';
            $status = false;
        }

        return response()->json(['result' => $invoice, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function store(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Invoice added Successfully';
        $status = true;
        $error = '';
        $rules = array(
            'user_id' => 'required',
            'client_id' => 'required',
            'subject' => 'required',
            'description' => 'required',
            'net_amount' => 'required',
            'vat' => 'required',
            'invoice_image' => 'required|mimes:jpg,jpeg,png',
            'date_time' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {
            $user = User::find($request->user_id);
            $client = Client::where('id',$request->client_id)->where('user_id',$request->user_id)->first();

            if(!$user){
                $message = 'User Not found';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }elseif($user->id != $request->user_id || $user->api_token != $request->api_token){
                $message = 'Invalid token';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            if(!$client){
                $message = 'Client not exist';
                $code = 400;
                $status = false;
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $invoice = new Invoice();
            $invoice->user_id = $request->user_id;
            $invoice->client_id = $request->client_id;
            $invoice->subject = $request->subject;
            $invoice->description = $request->description;
            $invoice->net_amount = $request->net_amount;
            $invoice->vat = $request->vat;
            $vat = round(($request->net_amount*$request->vat)/100,2);
            $invoice->total_amount = $request->net_amount+$vat;
            $invoice->date_time = $request->date_time;
            $invoice->status = 1;

            $name = str_replace(' ','_',$request->subject);

            if($request->file('invoice_image')){
                $image = $request->file('invoice_image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/invoice', $filename);

                $invoice->invoice_image = 'uploads/invoice/'.$filename;
            }

            $invoice->save();
            Invoice::where('id',$invoice->id)->update(['invoice_id'=>'#'.str_pad($invoice->id, 10, "0", STR_PAD_LEFT)]);
            $invoice->invoice_id = '#'.str_pad($invoice->id, 10, "0", STR_PAD_LEFT);
            $data = $invoice;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function edit(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Successfull';
        $status = true;
        $error = '';

        $invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();

        if(empty($invoice)){
            $code = 404;
            $messages = 'Invoice not exist';
            $status = false;
        }

        return response()->json(['result' => $invoice, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function update(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Invoice updated Successfully';
        $status = true;
        $error = '';

        $rules = array(
            'invoice_image' => 'mimes:jpg,jpeg,png',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

        } else {
            $invoice = Invoice::where('id',$request->invoice_id)->where('user_id',$request->user_id)->where('client_id',$request->client_id)->first();

            if(!$invoice){
                $message = 'Invoice not found';
                $code = 400;
                $status = 'false';
                return response()->json(['result'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
            }

            $data['subject'] = ($request->subject)?$request->subject:$invoice->subject;
            $data['description'] = ($request->description)?$request->description:$invoice->description;
            $data['net_amount'] = ($request->net_amount)?$request->net_amount:$invoice->net_amount;
            $data['vat'] = ($request->vat)?$request->vat:$invoice->vat;
            $vat = round(($data['net_amount']*$data['vat'])/100,2);
            $data['total_amount'] = $vat+$data['net_amount'];
            $data['date_time'] = ($request->date_time)?$request->date_time:$invoice->date_time;

            $name = str_replace(' ','_',($request->subject)?$request->subject:$invoice->subject);

            if($request->file('invoice_image')){
                if(file_exists($invoice->invoice_image)){
                    unlink($invoice->invoice_image); //delete previously uploaded image
                }
                $image = $request->file('invoice_image');

                $filename = $name.'_'.uniqid(time()) . '.' . $image->getClientOriginalExtension();

                $image->move('uploads/invoice', $filename);

                $data['invoice_image'] = 'uploads/invoice/'.$filename;
            }
            $invoice->update($data);
            $invoice->invoice_image = url('/').'/'.$invoice->invoice_image;
            $invoice->invoice_id = '#'.$invoice->id;
            $data = $invoice;
        }
        return response()->json(['result' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }
}