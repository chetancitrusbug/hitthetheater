<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\User;
use Hash;
use Session;
use Auth;
use App\CmsSetting;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 1;

        return $credentials;
    }
    public function  login(Request $request)
    {
        $this->validate($request, [
            'email'=> 'required|email','password'=>'required'
        ]);

        $credentials = array(
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        );

        $user = User::where("email", $request->email)->with('roles')->first();

        if ($user)
        {
            if (Hash::check($request->password, $user->password))
            {
                if ($user->status == 1 && Auth::attempt($credentials,true))
                {
                    Session::flash('flash_success', 'Login SuccessFull!');
                    return redirect('/admin');
                }
                else
                {
                    Session::flash('flash_error', 'Your account is inactive please contact to Admin');
                }
            }
            else
            {
                Session::flash('flash_error', 'Invalid Password.');
            }
        }
        else
        {
            Session::flash('flash_error', 'Invalid Username or Password.');
        }
        return redirect()->back()->withInput();
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('login');
    }
}
