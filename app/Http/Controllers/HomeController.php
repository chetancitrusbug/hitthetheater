<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CmsSetting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cms_setting=CmsSetting::first();
        view()->share('cms_setting', $this->cms_setting);
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/admin');
        //return view('home');
    }

    public function termsAndConditions()
    {
        return view('terms_and_conditions');
    }

    public function privacyPolicy()
    {
        return view('privacy_policy');
    }
}
