<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempVenue extends Model
{
    protected $table = 'tmp_venue';

    protected $fillable = [
         'locationId','categoryId','venueTitle','venueDescription','postcode','latitude','longitude','status','venueCode','addressLine1','addressLine2','locationCode'
    ];

    public function location()
    {
        return $this->belongsTo('App\Location', 'locationId', 'id');
    }
}
