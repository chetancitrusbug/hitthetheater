<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VenueSliders;

class Venue extends Model
{
    protected $table = 'venues';

    protected $fillable = [
         'locationId','venueTitle','venueDescription','postcode','latitude','longitude','status','venueCode','addressLine1','addressLine2','createdby','updatedby','locationCode','orderby','slug'
    ];

    protected $appends =['venue_image','venue_seating','venue_detail_banner'];

    public function getVenueImageAttribute()
    {

        $image= VenueSliders::where('venueCode',$this->venueCode)->where('type','primary-image')->orderby('id', 'DESC')->first();
        // dd($);
            if ($image != null) {

                $path=public_path().'/storage/venue'.$image->image;
                if(file_exists($path))
                {
                    $path= url('').'/storage/venue'.$image->image;
                    return $path;
                }
                elseif(@fopen($image->image,'r'))
                {
                    return $image->image;

                }
                else
                {
                    return url('').'/frontend/images/no_image.png';
                }
            }
            else
                {
                    return url('').'/frontend/images/no_image.png';
                }
    }

    public function getVenueDetailBannerAttribute()
    {

        $image= VenueSliders::where('venueCode',$this->venueCode)->where('type','primary-image')->orderby('id', 'DESC')->first();
        // dd($);
        if ($image != null) {

            $path=public_path().'/storage/venue'.$image->image;

            if(file_exists($path))
            {
                $path= url('').'/storage/venue'.$image->image;
                return $path;
            }
            elseif(@fopen($image->image,'r'))
            {
                return $image->image;
            }
            else
            {
                return '';
            }
        }
        else
        {
            return '';
        }
    }

    public function getVenueSeatingAttribute()
    {

        $image= VenueSliders::where('venueCode',$this->venueCode)->where('type','seat-plan')->orderby('id', 'DESC')->first();
        // dd($image);
            if ($image != null) {

                $path=public_path().'/storage/venue'.$image->image;

                if(file_exists($path))
                {
                    $path= url('').'/storage/venue'.$image->image;
                    return $path;
                }
                elseif(@fopen($image->image,'r'))
                {
                    return $image->image;

                }
                else
                {
                    return '';
                }
            }
            else
                {
                    return '';
                }
    }

    public function VenueSliders()
    {
        return $this->belongsTo('App\VenueSliders','venueCode','venueCode');
    }

}
