<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldVenueTransportInfo extends Model
{
    protected $table = 'tmp_old_venue_transport_info';

    protected $fillable = [
         'venueId','transportId','transportValue','venueCode'
    ];
}
