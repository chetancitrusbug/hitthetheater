<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TempLocation;
use App\TempOldLocation;
use App\Setting;
use DB;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use Mail;
use Response;
use File;
use App\CmsSetting;

class Location extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Location:Location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Location Corn';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $this->authorization = Setting::where('key', 'authorization')->first();
        $this->usernameSetting = Setting::where('key', 'username')->first();
        $this->passwordSetting = Setting::where('key', 'password')->first();
        $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $URL = $this->liveUrlSetting->value . 'content/location/';

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
              "Authorization:".$authorization
            ),
          ));

        $result = curl_exec($ch);

        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);

        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);
            TempOldLocation::truncate();

            $sql = "INSERT INTO tmp_old_location (locationTitle, locationCode, status)
            SELECT locationTitle, locationCode, status FROM tmp_location;";
            DB::statement($sql);

            TempLocation::truncate();

            $data = array();
            foreach ($array['location'] as $key => $value) {
                $data[$key]['locationTitle'] = $value['title'];
                $data[$key]['locationCode'] = $value['@attributes']['id'];
            }
            $location = TempLocation::insert($data);

            $tempLocation = TempLocation::all();

            if($tempLocation != null)
            {
                $this->mailImport();
                $message = 'Added Temp Successfully';
                echo response()->json(['message' => $message, 'code' => 200], 200);exit;
            }
            else {
                $message = 'No have Location';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;
            }
        }
        else {
                $message = 'Unauthorization';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;

            }
    }

    public function mailImport()
    {
        $tempLocations = TempLocation::all();
        $tempOldLocations = TempOldLocation::all();

        $newArray = array();
        $oldArray = array();
        $codes = array();
        foreach ($tempLocations as $key => $value) {

            $location = \App\Location::where('locationCode', $value->locationCode)->first();
            $tempOldLocation = TempOldLocation::where('locationCode', $value->locationCode)->first();
            if($location == null)
            {
                $newArray[$value->locationCode] = $value;
                $newArray[$value->locationCode]['existrecord'] = 0;
                $newArray[$value->locationCode]['updatedArr'] = 0;
                $codes[] = $value->locationCode;
            }
            else if(isset($tempOldLocation->locationTitle) && isset($value->locationTitle) && $tempOldLocation->locationTitle != $value->locationTitle)
            {
                $newArray[$value->locationCode] = $value;
                $newArray[$value->locationCode]['existrecord'] = 1;
                $newArray[$value->locationCode]['updatedArr'] = 1;
                $codes[] = $value->locationCode;
            }
        }
        foreach ($tempOldLocations as $key => $value) {
            $location = \App\Location::where('locationCode', $value->locationCode)->first();
            $tempLocation = TempLocation::where('locationCode', $value->locationCode)->first();
            if($location == null)
            {
                $oldArray[$value->locationCode] = $value;
                $oldArray[$value->locationCode]['existrecord'] = 0;
                $oldArray[$value->locationCode]['updatedArr'] = 0;
                $codes[] = $value->locationCode;
            }
            else if(isset($tempLocation->locationTitle) && isset($value->locationTitle) && $tempLocation->locationTitle != $value->locationTitle)
            {
                $oldArray[$value->locationCode] = $value;
                $oldArray[$value->locationCode]['existrecord'] = 1;
                $oldArray[$value->locationCode]['updatedArr'] = 1;
                $codes[] = $value->locationCode;
            }
        }

        $tempLocationCodes = TempLocation::pluck('locationCode');
        $tempOldLocationCodes = TempOldLocation::pluck('locationCode');
        if ($tempLocationCodes != null && $tempOldLocationCodes != null) {
            $locationCodes = array_unique($codes);
        }
        else {
            $locationCodes = [];
        }
        if(count($locationCodes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import location.');

            $content = view('admin.dashboard.locationconflictListForMail', compact('locationCodes','newArray','oldArray'))->render();
            if (! File::exists(public_path()."/importfiles")) {
                File::makeDirectory(public_path()."/importfiles");
            }
            $fiename = 'location_import_' . strtotime(date('y-m-d h:i:s')) . '.doc';

            file_put_contents(public_path("importfiles/".$fiename),$content);

            // $content = view('admin.dashboard.locationconflictListForMail', compact('locationCodes','newArray','oldArray'))->render();
            // $phpWord = new PhpWord();

            // $section = $phpWord->addSection();
            // Html::addHtml($section, $content, false);

            // $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
            // $fiename = 'location_import_'.strtotime(date('y-m-d h:i:s')).'.docx';

            // if (! File::exists(public_path()."/importfiles")) {
            //     File::makeDirectory(public_path()."/importfiles");
            // }

            // $objWriter->save('importfiles/' . $fiename);
            $pathToFile = public_path('importfiles/'.$fiename);

            $cmsSetting=CmsSetting::first();

            if ($cmsSetting != null) {
                Mail::send('emails.wordfile',['pathToFile' => $pathToFile], function ($message) use ($pathToFile,$cmsSetting)
                {
                    $message->attach($pathToFile);
                    $message->subject('Hit The Theatre Import Location'. date('Y-m-d h:i:s'));
                    $message->to($cmsSetting->contact_us_email)->cc(['user.citrusbug@gmail.com']);
                });
            }



        }
    }
}
