<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Setting;
use App\Location;
use App\TempVenue;
use App\TempOldVenue;
use App\TempVenueFacilities;
use App\TempVenueSliders;
use App\TempVenueTransportInfo;
use App\Facilities;
use App\VenueFacilities;
use App\VenueSliders;
use App\VenueTransportInfo;
use App\Transport;
use App\TempVenueMetadata;
use App\TempOldVenueTransportInfo;
use App\TempOldVenueMetadata;
use App\TempOldVenueFacilities;
use DB;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use Mail;
use Response;
use File;
use App\CmsSetting;

class Venue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Venue:Venue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Venue Corn';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $this->authorization = Setting::where('key', 'authorization')->first();
        $this->usernameSetting = Setting::where('key', 'username')->first();
        $this->passwordSetting = Setting::where('key', 'password')->first();
        $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $URL = $this->liveUrlSetting->value . 'content/venue/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);

        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);

            TempOldVenue::truncate();
            TempOldVenueTransportInfo::truncate();
            TempOldVenueMetadata::truncate();
            TempOldVenueFacilities::truncate();

            $sql = "INSERT INTO tmp_old_venue (locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude , venueCode, addressLine1 , addressLine2,locationCode, status) SELECT locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude , venueCode, addressLine1 , addressLine2,locationCode, status FROM tmp_venue";
            DB::statement($sql);

            $sql = "INSERT INTO tmp_old_venue_facilities (venueId, facilitieId,venueCode)
            SELECT venueId, facilitieId,venueCode FROM tmp_venue_facilities";
            DB::statement($sql);
            $sql = "INSERT INTO tmp_old_venue_transport_info (venueId, transportId, transportValue,venueCode )
            SELECT venueId, transportId    , transportValue,venueCode  FROM tmp_venue_transport_info";
            DB::statement($sql);
            $sql = "INSERT INTO tmp_old_venue_metadata (venue_id, meta_title, meta_description,venueCode)
            SELECT venue_id, meta_title    , meta_description ,venueCode FROM tmp_venue_metadata";
            DB::statement($sql);


            TempVenue::truncate();
            TempVenueFacilities::truncate();
            TempVenueSliders::truncate();
            TempVenueMetadata::truncate();
            TempVenueTransportInfo::truncate();
            $data = array();

            foreach ($array['venue'] as $key => $value) {
                // if($key > 0 && $key < 15)
                // {
                    //dd($value['location']['@attributes']['id']);
                    $location = Location::where('locationCode',$value['location']['@attributes']['id'])->first();
                    $link = $value['@attributes']['href'];
                    $link_array = explode('/', $link);
                    $venueCode = end($link_array);
                    if($location != null)
                    {
                        $data['venueTitle'] = $value['title'];
                        $data['venueCode'] = $venueCode;
                        $data['locationCode'] = $location->locationCode;
                        $data['locationId'] = $location->id;
                        $value = TempVenue::create($data);
                        $URL = $this->liveUrlSetting->value . 'content/venue/';
                        $URL .= $value->location->locationCode.'/'.$value->venueCode;

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $URL);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                        $result = curl_exec($ch);
                        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
                        curl_close($ch);
                        if($result)
                        {
                            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
                            $resValue = json_decode(json_encode($locationxml), true);

                            $data['description'] = isset($resValue['description']) ? $resValue['description'] : '';
                            $data['addressLine1'] = isset($resValue['address']['line1']) ? $resValue['address']['line1'] : '';
                            $data['addressLine2'] = isset($resValue['address']['line2']) ? $resValue['address']['line2'] : '';
                            $data['postcode'] = isset($resValue['address']['postcode']) ? $resValue['address']['postcode'] : '';
                            $data['latitude'] = isset($resValue['address']['latitude']) ? $resValue['address']['latitude'] : '';
                            $data['longitude'] =  isset($resValue['address']['longitude']) ? $resValue['address']['longitude']: '';


                            $venue = TempVenue::where('id',$value->id)->first();
                            if ($venue) {
                                $venue->update($data);

                                if(isset($resValue['facilities']) && isset($resValue['facilities']['facility']) && is_array($resValue['facilities']['facility']))
                                {
                                    foreach ($resValue['facilities']['facility'] as $facilityKey => $facilityValue) {
                                        $facility = Facilities::where('facilitieTitle',$facilityValue)->first();
                                        if($facility == null)
                                        {
                                            $dataFacility['facilitieTitle'] = $facilityValue;
                                            $facility = Facilities::create($dataFacility);
                                        }
                                        TempVenueFacilities::create(['facilitieId'=>$facility->id,'venueId'=>$value->id,'venueCode'=>$value->venueCode]);
                                    }

                                }
                                if (isset($resValue['resources']['resource'])) {
                                    foreach ($resValue['resources']['resource'] as $resourceKey => $resourceValue) {
                                        $dataResource['venueId'] = $value->id;
                                        $dataResource['venueCode'] = $value->venueCode;
                                        $dataResource['image'] = ((isset($resourceValue['uri']) && $resourceValue['uri'] != null ) ? $resourceValue['uri'] : '');
                                        $dataResource['type'] = ((isset($resourceValue['type']) && $resourceValue['type'] != null ) ? $resourceValue['type'] : '');
                                        $dataResource['title'] = ((isset($resourceValue['title']) && $resourceValue['title'] != null ) ? $resourceValue['title'] : '');
                                        $dataResource['width'] = ((isset($resourceValue['width']) && $resourceValue['width'] != null ) ? $resourceValue['width'] : '');
                                        $dataResource['height'] = ((isset($resourceValue['height']) && $resourceValue['height'] != null ) ? $resourceValue['height'] : '');
                                        TempVenueSliders::create($dataResource);
                                    }

                                }
                                if (isset($resValue['transportInfo'])) {
                                    foreach ($resValue['transportInfo'] as $transportInfoKey => $transportInfoValue) {
                                        // dd($transportInfoValue);
                                        $transport = Transport::where('transportTitle', $transportInfoValue)->first();
                                        if (
                                            $transport == null) {
                                            $dataTransport['transportTitle'] = $transportInfoValue;
                                            $dataTransport['transportKey'] = $transportInfoKey;
                                            $transport = Transport::create($dataTransport);
                                        }
                                        TempVenueTransportInfo::create(['transportId' => $transport->id, 'venueId' => $value->id,'transportValue'=> $transportInfoValue,'venueCode'=>$value->venueCode]);
                                    }
                                }
                            }
                        }


                        $metaURL = $this->liveUrlSetting->value . 'content/venue/';
                        $metaURL .= $value->location->locationCode.'/'.$value->venueCode.'/metadata';

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $metaURL);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                        $metaresult = curl_exec($ch);
                        $meta_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
                        curl_close($ch);
                        //$result = '<venue href="http://www.xyz.com/v1/venue/1"> <metadata> <title>Phoenix Theatre London - Cheap Theatre Tickets</title> <description> <![CDATA[ Whats playing and information about Phoenix Theatre London ]]> </description> </metadata></venue>';
                        if($metaresult)
                        {
                            $locationxml = simplexml_load_string($metaresult,'SimpleXMLElement', LIBXML_NOCDATA);
                            $metaresValue = json_decode(json_encode($locationxml), true);
                            $metadata['venue_id'] = $value->id;
                            $metadata['venueCode'] = $value->venueCode;
                            $metadata['meta_title'] = isset($metaresValue['metadata']['title']) ? $metaresValue['metadata']['title'] : '';
                            $metadata['meta_description'] = isset($metaresValue['metadata']['description']) ? $metaresValue['metadata']['description'] : '';
                            $value = TempVenueMetadata::create($metadata);
                        }
                        else {
                            if($data['venueTitle'] != null && $data['description'] != null)
                            {
                                $metadata['venue_id'] = $value->id;
                                $metadata['venueCode'] = $value->venueCode;
                                $metadata['meta_title'] = $data['venueTitle'];
                                $metadata['meta_description'] = $data['description'];
                                $value = TempVenueMetadata::create($metadata);
                            }
                        }
                    }
            //    }
            }



            $tempVenue = TempVenue::with('location')->get();
            $flag = 0;
            if($tempVenue != null)
            {
                $this->mailImport();
                $message = 'Added Temp Successfully';
                echo response()->json(['message' => $message, 'code' => 200], 200);exit;
            }
            else {
                $message = 'No have Venue';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;
            }
        }
        else {
                $message = 'Unauthorization';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;

            }
    }

    public function mailImport()
    {

        // use App\TempOldVenue;
        // use App\TempOldVenueTransportInfo;
        // use App\TempOldVenueMetadata;
        // use App\TempOldVenueFacilities;
        // use App\TempVenueFacilities;
        // use App\TempVenueSliders;
        // use App\TempVenueTransportInfo;
        // use App\TempVenueMetadata;

        $tempVenues = TempVenue::all()->pluck('venueCode');
        $tempOldVenues = TempOldVenue::all()->pluck('venueCode');

        if ($tempVenues != null && $tempOldVenues != null) {
            $venues = array_unique(array_merge($tempVenues->toArray(), $tempOldVenues->toArray()));
        }
        else {
            $venues = [];
        }


        $newArray = array();
        $oldArray = array();
        $fields = array('venueTitle','venueDescription','locationCode','postcode','latitude','longitude','addressLine1','addressLine2');

        foreach ($venues as $key => $value) {
            // if ($key > 1) {

            $venueCode = $value;

            //new Array
            $currentRec = TempVenue::where('venueCode', $venueCode)->first();
            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->venueCode;

                $tempOld = TempOldVenue::where('venueCode', $currentRec->venueCode)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $newArray[$currentRec->venueCode] = $value->toArray();
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                $tabledata = \App\Venue::where('venueCode', $currentRec->venueCode)->first();

                if($tabledata == null)
                {
                    $newArray[$currentRec->venueCode] = $value->toArray();
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey ) {

                        // if ($currentRec->venueCode == '21') {
                        //     dd(($currentRec[$fieldKey]));
                        // }
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $newArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $newArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $newArray[$currentRec->venueCode][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                // $newArray[$currentRec->venueCode]['facilities'] = '';
                // $newArray[$currentRec->venueCode]['transportinfo'] = '';
                // $newArray[$currentRec->venueCode]['metadata'] = '';
                $tempFacilitiesIds = TempVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempFacilitiesIds != null) {
                    $tempFacilitiesIds = $tempFacilitiesIds->toArray();
                }
                $tempOldFacilitiesIds = TempOldVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_old_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempOldFacilitiesIds != null) {
                    $tempOldFacilitiesIds = $tempOldFacilitiesIds->toArray();
                }

                if ($tempFacilitiesIds != null && $tempOldFacilitiesIds != null) {
                    $facilitiesIds = array_diff($tempFacilitiesIds, $tempOldFacilitiesIds);
                }
                else {
                    $facilitiesIds = [];
                }


                if (count($tempFacilitiesIds) > 0) {
                    $newArray[$currentRec->venueCode]['facilities'] = '';
                    $facilities = Facilities::whereIn('id', $tempFacilitiesIds)->get();
                    foreach ($facilities as $facilitiesKey => $facilitiesValue) {
                        if (isset($facilitiesValue->facilitieTitle) && $facilitiesValue->facilitieTitle != null) {
                            $changeColor = (!in_array($facilitiesValue->id,$tempOldFacilitiesIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->venueCode]['facilities'] .= '<p class="'.$changeColor.'">'.$facilitiesValue->facilitieTitle.'</p>';
                            if ($changeColor != '') {
                               $newArray[$currentRec->venueCode]['updatedArr'][] = 'facilities';
                            }
                        }
                    }
                }

                $transport = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->get();

                $tempTransportIds = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempTransportIds != null) {
                    $tempTransportIds = $tempTransportIds->toArray();
                }
                $tempOldTransportIds = TempOldVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_old_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempOldTransportIds != null) {
                    $tempOldTransportIds = $tempOldTransportIds->toArray();
                }

                if ($tempTransportIds != null && $tempOldTransportIds != null) {
                    $transportIds = array_diff($tempTransportIds, $tempOldTransportIds);
                }
                else {
                    $transportIds = [];
                }


                if (count($tempTransportIds) > 0) {
                    $newArray[$currentRec->venueCode]['transportinfo'] = '';
                    $transport = Transport::whereIn('id', $tempTransportIds)->get();
                    foreach ($transport as $transportKey => $transportValue) {
                        if (isset($transportValue->transportTitle) && $transportValue->transportTitle != null) {
                            $changeColor = (!in_array($transportValue->id,$tempOldTransportIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->venueCode]['transportinfo'] .= '<p class="'.$changeColor.'">'.$transportValue->transportTitle.'</p>';
                            if ($changeColor != '') {
                            $newArray[$currentRec->venueCode]['updatedArr'][] = 'transportinfo';
                            }
                        }
                    }
                }

                $tempMetadata = TempVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                $tempOldmetadata = TempOldVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();

                if($tempMetadata != null)
                {
                    $newArray[$currentRec->venueCode]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_title) && $tempMetadata->meta_title != null) {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->venueCode]['metadata'] .= (isset($tempMetadata->meta_title)) ? '<p><b>Title -</b> '.$tempMetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_description) && $tempMetadata->meta_description != null) {
                        $newArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->venueCode]['metadata'] .= (isset($tempMetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempMetadata->meta_description : '';
                    }
                }
                if (isset($newArray[$currentRec->venueCode]['updatedArr']) && count($newArray[$currentRec->venueCode]['updatedArr']) > 0) {
                    $newArray[$currentRec->venueCode]['updatedArr'] = array_unique($newArray[$currentRec->venueCode]['updatedArr']);
                    if($tabledata == null)
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $newArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else if (isset($newArray[$currentRec->venueCode]['existrecord'])) {
                    if($tabledata == null)
                    $newArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $newArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else {
                    unset($newArray[$currentRec->venueCode]);
                    unset($venues[$key]);
                }
                // dd($newArray);
            }

            //old Array
            $currentRec = TempOldVenue::where('venueCode', $venueCode)->first();
            if($currentRec != null)
            {
                $value = $currentRec;
                $codes[] = $currentRec->venueCode;

                $tempOld = TempVenue::where('venueCode', $currentRec->venueCode)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $oldArray[$currentRec->venueCode] = $value->toArray();
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                $tabledata = \App\Venue::where('venueCode', $currentRec->venueCode)->first();

                if($tabledata == null)
                {
                    $oldArray[$currentRec->venueCode] = $value->toArray();
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey ) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $oldArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $oldArray[$currentRec->venueCode][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $oldArray[$currentRec->venueCode][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                // $oldArray[$currentRec->venueCode]['facilities'] = '';
                // $oldArray[$currentRec->venueCode]['transportinfo'] = '';
                // $oldArray[$currentRec->venueCode]['metadata'] = '';
                $tempFacilitiesIds = TempVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempFacilitiesIds != null) {
                    $tempFacilitiesIds = $tempFacilitiesIds->toArray();
                }
                $tempOldFacilitiesIds = TempOldVenueFacilities::select('facilities.facilitieTitle','facilities.id')->leftJoin('facilities','tmp_old_venue_facilities.facilitieId','facilities.id')->where('venueCode',$currentRec->venueCode)->pluck('facilities.id');
                if ($tempOldFacilitiesIds != null) {
                    $tempOldFacilitiesIds = $tempOldFacilitiesIds->toArray();
                }

                if ($tempFacilitiesIds != null && $tempOldFacilitiesIds != null) {
                    $facilitiesIds = array_diff($tempOldFacilitiesIds, $tempFacilitiesIds);
                }
                else {
                    $facilitiesIds = [];
                }


                if (count($tempOldFacilitiesIds) > 0) {
                    $oldArray[$currentRec->venueCode]['facilities'] = '';
                    $facilities = Facilities::whereIn('id', $tempOldFacilitiesIds)->get();
                    foreach ($facilities as $facilitiesKey => $facilitiesValue) {
                        if (isset($facilitiesValue->facilitieTitle) && $facilitiesValue->facilitieTitle != null) {
                            $changeColor = (!in_array($facilitiesValue->id,$tempFacilitiesIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->venueCode]['facilities'] .= '<p class="'.$changeColor.'">'.$facilitiesValue->facilitieTitle.'</p>';
                            if ($changeColor != '') {
                               $oldArray[$currentRec->venueCode]['updatedArr'][] = 'facilities';
                            }
                        }
                    }
                }

                $transport = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->get();

                $tempTransportIds = TempVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempTransportIds != null) {
                    $tempTransportIds = $tempTransportIds->toArray();
                }
                $tempOldTransportIds = TempOldVenueTransportInfo::select('transport.transportTitle','transport.id')->leftJoin('transport','tmp_old_venue_transport_info.transportId','transport.id')->where('venueCode',$currentRec->venueCode)->pluck('transport.id');
                if ($tempOldTransportIds != null) {
                    $tempOldTransportIds = $tempOldTransportIds->toArray();
                }

                if ($tempTransportIds != null && $tempOldTransportIds != null) {
                    $transportIds = array_diff($tempOldTransportIds, $tempTransportIds);
                }
                else {
                    $transportIds = [];
                }


                if (count($tempOldTransportIds) > 0) {
                    $oldArray[$currentRec->venueCode]['transportinfo'] = '';
                    $transport = Transport::whereIn('id', $tempOldTransportIds)->get();
                    foreach ($transport as $transportKey => $transportValue) {
                        if (isset($transportValue->transportTitle) && $transportValue->transportTitle != null) {
                            $changeColor = (!in_array($transportValue->id,$tempTransportIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->venueCode]['transportinfo'] .= '<p >'.$transportValue->transportTitle.'</p>';
                            if ($changeColor != '') {
                            $oldArray[$currentRec->venueCode]['updatedArr'][] = 'transportinfo';
                            }
                        }
                    }
                }

                $tempMetadata = TempVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                $tempOldmetadata = TempOldVenueMetadata::select('meta_title','meta_description','venueCode')->where('venueCode',$currentRec->venueCode)->first();
                // dd($tempOldmetadata);
                if($tempOldmetadata != null)
                {
                    $oldArray[$currentRec->venueCode]['metadata'] = '';
                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_title) && $tempOldmetadata->meta_title != null) {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->venueCode]['metadata'] .= (isset($tempOldmetadata->meta_title)) ? '<p><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>' : '';
                    }

                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_description) && $tempOldmetadata->meta_description != null) {
                        $oldArray[$currentRec->venueCode]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->venueCode]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->venueCode]['metadata'] .= (isset($tempOldmetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempOldmetadata->meta_description : '';
                    }
                }
                if (isset($oldArray[$currentRec->venueCode]['updatedArr']) && count($oldArray[$currentRec->venueCode]['updatedArr']) > 0) {
                    $oldArray[$currentRec->venueCode]['updatedArr'] = array_unique($oldArray[$currentRec->venueCode]['updatedArr']);
                    if($tabledata == null)
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $oldArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else if (isset($oldArray[$currentRec->venueCode]['existrecord'])) {
                    if($tabledata == null)
                    $oldArray[$currentRec->venueCode]['existrecord'] = 0;
                    else
                    $oldArray[$currentRec->venueCode]['existrecord'] = 1;
                }
                else {
                    unset($oldArray[$currentRec->venueCode]);
                    unset($venues[$key]);
                }
            }
            // dd($oldArray);


        // }
        }


        $codes = $venues;

        $fields = array('venueTitle'=>'Venue Title','venueDescription'=>'Venue Description','locationCode'=>'Location Code','postcode'=>'Post code','latitude'=>'Latitude','longitude'=>'Longitude','addressLine1'=>'Address Line 1','addressLine2'=>'Address Line 2','facilities'=>'Facilities','transportinfo'=>'Transport info','metadata'=>'Meta data');


        if(count($codes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import venue.');

            $content = view('admin.dashboard.venueconflictListForMail', compact('codes','newArray','oldArray','fields'))->render();
            if (! File::exists(public_path()."/importfiles")) {
                File::makeDirectory(public_path()."/importfiles");
            }
            $fiename = 'venue_import_' . strtotime(date('y-m-d h:i:s')) . '.doc';

            file_put_contents(public_path("importfiles/".$fiename),$content);


            // dd($content);

            // $phpWord = new PhpWord();

            // $section = $phpWord->addSection();
            // Html::addHtml($section, $content, false);

            // $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
            // $fiename = 'venue_import_'.strtotime(date('y-m-d h:i:s')).'.docx';

            // if (! File::exists(public_path()."/importfiles")) {
            //     File::makeDirectory(public_path()."/importfiles");
            // }

            // $objWriter->save('importfiles/' . $fiename);
            $pathToFile = public_path('importfiles/'.$fiename);

            $cmsSetting=CmsSetting::first();

            if ($cmsSetting != null) {
                Mail::send('emails.wordfile',['pathToFile' => $pathToFile], function ($message) use ($pathToFile,$cmsSetting)
                {
                    $message->attach($pathToFile);
                    $message->subject('Hit The Theatre Import Venue'. date('Y-m-d h:i:s'));
                    $message->to($cmsSetting->contact_us_email)->cc(['user.citrusbug@gmail.com']);
                });
            }
        }

    }

    function closetags($html) {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];
        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }
        $openedtags = array_reverse($openedtags);
        for ($i=0; $i < $len_opened; $i++) {
            if (!in_array($openedtags[$i], $closedtags)) {
                $html .= '</'.$openedtags[$i].'>';
            } else {
                unset($closedtags[array_search($openedtags[$i], $closedtags)]);
            }
        }
        return $html;
    }
}
