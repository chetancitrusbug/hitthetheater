<?php

namespace App\Console\Commands;
use App\TempShow;
use App\Show as Shows;
use App\Offer;
use App\Setting;
use App\Category;
use App\TempSchedule;
use App\Show_Images;
use App\TempShowMetadata;
use App\TempShowImages;
use App\TempShowCategory;
use App\TempOffers;
use App\TempOldShow;
use App\TempOldOffers;
use App\TempOldShowCategory;
use App\TempOldShowMetadata;
use App\TempOldSchedule;
use DB;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;
use Mail;
use Response;
use File;
use Illuminate\Console\Command;
use App\CmsSetting;

class Show extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Show:Show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show Corn Job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 10000000);
        $this->authorization = Setting::where('key', 'authorization')->first();
        $this->usernameSetting = Setting::where('key', 'username')->first();
        $this->passwordSetting = Setting::where('key', 'password')->first();
        $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->mailImport();exit;

        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $URL = $this->liveUrlSetting->value . 'content/show/';

        $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);

        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);

            TempOldShow::truncate();
            TempOldSchedule::truncate();
            TempOldOffers::truncate();
            TempOldShowCategory::truncate();
            TempOldShowMetadata::truncate();

            $sql = "INSERT INTO `tmp_old_show` ( showTitle, indexName, showLink, bookingStarts, bookingEnds, isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status) SELECT showTitle, indexName, showLink, bookingStarts, bookingEnds, isEvent, summary, description, priceFrom, priceTo, onSaleDate, runningTime, ageRestriction, limitedStock, whitelabelURI, importantInfo, bannerBackgroundColour, showType,avg_rating,venueCode, status FROM tmp_show";
            DB::statement($sql);

            $sql = "INSERT INTO `temp_old_schedule` (show_id, day_id,time,showLink)
            SELECT show_id, day_id,time,showLink FROM temp_schedule ";
            DB::statement($sql);
            $sql = "INSERT INTO tmp_old_offers (title,type,valid,start_date,end_date,exclusive,facevalue,price,description,offer_id,showLink,created_by)
            SELECT  title,type,valid,start_date,end_date,exclusive,facevalue,price,description,offer_id,showLink,created_by FROM tmp_offers  ";
            DB::statement($sql);
            $sql = "INSERT INTO tmp_old_show_category (show_id, category_id,showLink)
            SELECT show_id, category_id,showLink  FROM tmp_show_category ";
            DB::statement($sql);
            $sql = "INSERT INTO tmp_old_show_meta_data (show_id, meta_title, meta_description,showLink)
            SELECT show_id, meta_title, meta_description,showLink  FROM tmp_show_meta_data ";
            DB::statement($sql);

            TempShow::truncate();
            TempShowMetadata::truncate();
            TempSchedule::truncate();
            TempShowCategory::truncate();
            TempShowImages::truncate();
            TempOffers::truncate();

            foreach ($array['show'] as $key => $value) {

                // if($key > 0 && $key < 5)
                // {
                    $data = array();
                    $originalShowdata = array();
                    $link = $value['@attributes']['href'];

                    $link_rating=$link.'/quote';
                    $link_array = explode('/', $link, 9);
                    $showLink = end($link_array);
                    $existShow = TempShow::where('showLink', $showLink)->first();
                    $originalShow = Shows::where('showLink', $showLink)->first();


                    if($existShow == null)
                    {
                        $data['showTitle'] = $value['name'];
                        $data['showLink'] = $showLink;

                        $show_details=$this->show_details($link);
                        $show_rating=$this->show_rating($link_rating);
                        $show_details_xml = simplexml_load_string($show_details,'SimpleXMLElement',LIBXML_NOCDATA);

                        $show_details_array = json_decode(json_encode($show_details_xml), true);

                        if(isset($show_rating) && $show_rating != null)
                        {
                            $data['avg_rating']=$show_rating;
                        }
                        else
                        {
                            $data['avg_rating'] = "";
                        }

                        if(isset($show_details_array['indexName']) && $show_details_array['indexName'] != null)
                        {
                            $data['indexName']=$show_details_array['indexName'];
                        }
                        else
                        {
                            $data['indexName'] = "";
                        }


                        if(isset($show_details_array['isEvent']) && $show_details_array['isEvent'] != null)
                        {
                            $data['isEvent']=$show_details_array['isEvent'];
                        }
                        else
                        {
                            $data['isEvent'] = "";
                        }

                        if(isset($show_details_array['priceFrom']) && $show_details_array['priceFrom'] != null)
                        {
                            $data['priceFrom']=$show_details_array['priceFrom'];
                        }
                        else
                        {
                            $data['priceFrom'] = "";
                        }

                        if(isset($show_details_array['priceTo']) && $show_details_array['priceTo'] != null)
                        {
                            $data['priceTo']=$show_details_array['priceTo'];
                        }
                        else
                        {
                            $data['priceTo'] = "";
                        }

                        if(isset($show_details_array['onSaleDate']) && $show_details_array['onSaleDate'] != null)
                        {
                            $data['onSaleDate']=$show_details_array['onSaleDate'];
                        }
                        else
                        {
                            $data['onSaleDate'] = "";
                        }

                        if(isset($show_details_array['runningTime']) && $show_details_array['runningTime'] != null)
                        {
                            $data['runningTime']=$show_details_array['runningTime'];
                        }
                        else
                        {
                            $data['runningTime'] = "";
                        }

                        if(isset($show_details_array['ageRestriction']) && $show_details_array['ageRestriction'] != null)
                        {
                            $data['ageRestriction']=$show_details_array['ageRestriction'];
                        }
                        else
                        {
                            $data['ageRestriction'] = "";
                        }

                        if(isset($show_details_array['limitedStock']) && $show_details_array['limitedStock'] != null)
                        {
                            $data['limitedStock']=$show_details_array['limitedStock'];
                        }
                        else
                        {
                            $data['limitedStock'] = "";
                        }

                        if(isset($show_details_array['whitelabelURI']) && $show_details_array['whitelabelURI'] != null)
                        {
                            $data['whitelabelURI']=$show_details_array['whitelabelURI'];
                        }
                        else
                        {
                            $data['whitelabelURI'] = "";
                        }

                        if(isset($show_details_array['importantInfo']) && $show_details_array['importantInfo'] != null)
                        {

                            $data['importantInfo']=$show_details_array['importantInfo'];
                        }
                        else
                        {
                            $data['importantInfo'] = "";
                        }

                        if(isset($show_details_array['summary']) && $show_details_array['summary'] != null)
                        {

                            $data['summary']=$show_details_array['summary'];
                        }
                        else
                        {
                            $data['summary'] = "";
                        }

                        if(isset($show_details_array['description']) && $show_details_array['description'] != null)
                        {

                            $data['description']=$show_details_array['description'];
                        }
                        else
                        {
                            $data['description'] = "";
                        }

                        if(isset($show_details_array['showType']) && $show_details_array['showType'] != null)
                        {

                            $data['showType']=$show_details_array['showType'];
                        }
                        else
                        {
                            $data['showType'] = "";
                        }

                        if(isset($show_details_array['bannerBackgroundColour']) && $show_details_array['bannerBackgroundColour'] != null)
                        {
                            $data['bannerBackgroundColour']=$show_details_array['bannerBackgroundColour'];
                        }
                        else
                        {
                            $data['bannerBackgroundColour'] = "";
                        }

                        if(isset($value['venues']['venue']['@attributes']['href']) && $value['venues']['venue']['@attributes']['href'] != null)
                        {
                            $link = $value['venues']['venue']['@attributes']['href'];
                            $link_array = explode('/', $link);
                            $venueCode = end($link_array);
                            $data['venueCode'] = $venueCode;
                        }
                        else
                        {
                            $data['venueCode'] = "";
                        }

                        if(isset($value['venues']['venue']['bookingStarts']) && $value['venues']['venue']['bookingStarts'] != null)
                        {
                            $data['bookingStarts'] = $value['venues']['venue']['bookingStarts'];
                            $originalShowdata['bookingStarts'] = $value['venues']['venue']['bookingStarts'];
                        }
                        else
                        {
                            $data['bookingStarts'] = "";
                        }
                        if(isset($value['venues']['venue']['bookingEnds']) && $value['venues']['venue']['bookingEnds'] != null)
                        {
                            $data['bookingEnds'] = $value['venues']['venue']['bookingEnds'];
                            $originalShowdata['bookingEnds'] = $value['venues']['venue']['bookingEnds'];
                        }
                        else
                        {
                            $data['bookingEnds'] = "";
                        }

                        if(isset($show_details_array['resources']['resource']))
                        {
                            $data['resource_image'] = json_encode($show_details_array['resources']['resource']);
                        }
                        else
                        {
                            $data['resource_image'] = "";
                        }

                        if(isset($show_details_array['categories']['category']))
                        {
                            $data['categories'] = json_encode($show_details_array['categories']['category']);
                        }
                        else
                        {
                            $data['categories'] = "";
                        }

                        //print_r($data);
                        $show = TempShow::create($data);

                        if (count($originalShowdata) > 0 && $originalShow != null) {
                            $originalShow->update($originalShowdata);
                        }

                        if($show != null)
                        {

                            if($show->categories != NULL)
                            {
                                $categories_data=json_decode($show->categories,true);

                                if($categories_data != null && is_array($categories_data))
                                {
                                    foreach ($categories_data as $key => $value) {
                                        $category=Category::where('categoryTitle',$value)->first();

                                        if($category == null)
                                        {
                                            $categorydata = array();
                                            $categorydata['categoryTitle']=$value;
                                            $categorydata['slug'] = $this->slugCreate($value);
                                            $category= Category::create($categorydata);

                                            $data = array();
                                            $data['show_id']=$show->id;
                                            $data['showLink'] = $show->showLink;
                                            $data['category_id']=$category->id;
                                            $data = TempShowCategory::create($data);

                                        }
                                        else
                                        {
                                            $data = array();
                                            $data['show_id']=$show->id;
                                            $data['showLink'] = $show->showLink;
                                            $data['category_id']=$category->id;
                                            $data = TempShowCategory::create($data);
                                        }

                                    }
                                }

                            }
                            //dd($categories_data);

                            // Offer Section Start
                            $URLoffer = $this->liveUrlSetting->value . 'content/show/'. $show->showLink. '/offer';

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $URLoffer);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                            $specialOffer = curl_exec($ch);
                            $specialOffer_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
                            curl_close($ch);

                            if ($specialOffer) {

                                $specialOffer = simplexml_load_string($specialOffer, 'SimpleXMLElement', LIBXML_NOCDATA);
                                $array = json_decode(json_encode($specialOffer), true);
                                $offers = $array['offers']['offer'];
                                if($offers != NULL && isset($offers))
                                {
                                    if (array_key_exists("@attributes",$offers) != 1) {
                                        foreach($offers as $key=>$value_data)
                                        {

                                                $offer['title']=(isset($value_data['title']) && $value_data['title'] != null) ? $value_data['title'] : '';
                                                $offer['showLink'] = $show->showLink;
                                                $offer['offer_id']=   (isset($value_data['@attributes']['id']) && $value_data['@attributes']['id'] != null) ? $value_data['@attributes']['id'] : '';
                                                $offer['type']=(isset($value_data['type']) && $value_data['type'] != null) ? $value_data['type']  : '';
                                                $offer['valid']=(isset($value_data['valid']) && $value_data['valid'] != null) ? $value_data['valid']  : '';
                                                $offer['start_date']=(isset($value_data['starts']) && $value_data['starts'] != null) ? $value_data['starts']  : '';
                                                $offer['end_date'] = (isset($value_data['ends']) && $value_data['ends'] != null) ? $value_data['ends']  : '';
                                                $offer['facevalue']=(isset($value_data['faceValue']) && $value_data['faceValue'] != null) ? $value_data['faceValue']  : '';
                                                $offer['price']=(isset($value_data['price']) && $value_data['price'] != null) ? $value_data['price']  : '';
                                                $offer['description'] = (isset($value_data['description']) && $value_data['description'] != null) ? $value_data['description']  : '';
                                                $offer_data = TempOffers::insert($offer);

                                        }
                                    }
                                    else {
                                            $offer['title']=(isset($offers['title']) && $offers['title'] != null) ? $offers['title'] : '';
                                            $offer['showLink'] = $show->showLink;
                                            $offer['offer_id']=   (isset($offers['@attributes']['id']) && $offers['@attributes']['id'] != null) ? $offers['@attributes']['id'] : '';
                                            $offer['type']=(isset($offers['type']) && $offers['type'] != null) ? $offers['type']  : '';
                                            $offer['valid']=(isset($offers['valid']) && $offers['valid'] != null) ? $offers['valid']  : '';
                                            $offer['start_date']=(isset($offers['starts']) && $offers['starts'] != null) ? $offers['starts']  : '';
                                            $offer['end_date'] = (isset($offers['ends']) && $offers['ends'] != null) ? $offers['ends']  : '';
                                            $offer['facevalue']=(isset($offers['faceValue']) && $offers['faceValue'] != null) ? $offers['faceValue']  : '';
                                            $offer['price']=(isset($offers['price']) && $offers['price'] != null) ? $offers['price']  : '';
                                            $offer['description'] = (isset($offers['description']) && $offers['description'] != null) ? $offers['description']  : '';
                                            $offer_data = TempOffers::insert($offer);

                                    }
                                }
                            }

                            $metaURL = $link.'/metadata';

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $metaURL);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

                            $metaresult = curl_exec($ch);
                            $meta_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
                            curl_close($ch);
                            //$metaresult = '<show href="https://api.entstix.com/api/v1/xlive/content/show/LDN/1001"> <metadata> <title>The Lion King tickets - heap tickets for The Lion King at LyceumTheatre, London</title> <description> <![CDATA[ Whats playing and information about hoenix Theatre London ]]> </description> <h1Heading>The Lion King</h1Heading> <copy> <![CDATA[ Whats playing and information bout Phoenix Theatre London ]]> </copy></metadata></show>';

                            if($metaresult)
                            {
                                $locationxml = simplexml_load_string($metaresult,'SimpleXMLElement', LIBXML_NOCDATA);
                                $metaresValue = json_decode(json_encode($locationxml), true);
                                $metadata['show_id'] = $show->id;
                                $metadata['showLink'] = $show->showLink;
                                $metadata['meta_title'] = isset($metaresValue['metadata']['title']) ? $metaresValue['metadata']['title'] : '';
                                $metadata['meta_description'] = isset($metaresValue['metadata']['description']) ? $metaresValue['metadata']['description'] : '';
                                $value = TempShowMetadata::create($metadata);
                            }
                            else {
                                if($show->id != null && $show->showTitle != null && $show->description != null)
                                {
                                    $metadata['show_id'] = $show->id;
                                    $metadata['showLink'] = $show->showLink;
                                    $metadata['meta_title'] = $show->showTitle;
                                    $metadata['meta_description'] = $show->description;
                                    $value = TempShowMetadata::create($metadata);
                                }
                            }


                            if($show->resource_image != NULL)
                            {
                                $image_data=json_decode($show->resource_image,true);

                                foreach($image_data as $key=>$value_data)
                                {
                                    if(count($value_data) > 0){
                                        $image['show_id']=$show->id;
                                        $image['url']=$value_data['@attributes']['uri'];
                                        $image['type']=$value_data['@attributes']['type'];
                                        $image['title']=$value_data['@attributes']['title'];
                                        $image['width']=$value_data['@attributes']['width'];
                                        $image['height']=$value_data['@attributes']['height'];
                                        $image['showLink'] = $show->showLink;
                                        $show_data=TempShowImages::insert($image);
                                    }
                                }
                            }



                            $schedule_details = $this->schedule_details($show->showLink);
                            $schedule_details_xml = simplexml_load_string($schedule_details, 'SimpleXMLElement', LIBXML_NOCDATA);
                            $schedule_details_array = json_decode(json_encode($schedule_details_xml), true);

                            if($schedule_details_array != NULL && isset($schedule_details_array['schedule']['day']))
                            {
                                foreach($schedule_details_array['schedule']['day'] as $key=>$value_data)
                                {
                                    if(isset($value_data['@attributes']['time']))
                                    {
                                        $str = $value_data['@attributes']['time'];
                                        if (($timestamp = strtotime($str)) != false) {
                                            $schedule['show_id']=$show->id;
                                            $schedule['showLink'] = $show->showLink;
                                            $schedule['day_id']=$value_data['@attributes']['id'];
                                            $schedule['time']=date('H:i:s', $timestamp);
                                            $show_data = TempSchedule::insert($schedule);
                                        }
                                    }
                                }
                            }


                        }
                    }

                // }
            }




            $temp = TempShow::all();
            $flag = 0;
            if($temp != null)
            {
                $this->mailImport();
                $message = 'Added Temp Successfully';
                echo response()->json(['message' => $message, 'code' => 200], 200);exit;
            }
            else {
                $message = 'No have Venue';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;
            }
        }
        else {
                $message = 'Unauthorization';
                echo response()->json(['message' => $message, 'code' => 400], 200);exit;

            }
    }

    public function mailImport()
    {
        $recCode = array();
        $tempShows = TempShow::all()->pluck('showLink');
        $tempOldShows = TempOldShow::all()->pluck('showLink');

        if ($tempShows != null && $tempOldShows != null) {
            $shows = array_unique(array_merge($tempShows->toArray(), $tempOldShows->toArray()));
        }
        else {
            $shows = [];
        }



        $fields = array('showTitle'=>'Show Title','summary'=>'Summary','description'=>'Description', 'priceFrom'=>'Price From','priceTo'=>'Price To','runningTime'=>'Running Time','ageRestriction'=>'Age Restriction','importantInfo'=>'ImportantInfo','venueCode'=>'Venue Code','bannerBackgroundColour'=>'Banner Background Colour');

        $newArray = array();
        $oldArray = array();
        foreach ($shows as $key => $value) {

            $showLink = $value;
            //new Array

            $currentRec = TempShow::where('showLink',$showLink)->first();

            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->showLink;

                $tempOld = TempOldShow::where('showLink', $currentRec->showLink)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $newArray[$currentRec->showLink] = $value->toArray();
                    $newArray[$currentRec->showLink]['existrecord'] = 0;
                }
                $tabledata = Shows::where('showLink',$currentRec->showLink)->first();

                if($tabledata == null)
                {
                    $newArray[$currentRec->showLink] = $value->toArray();
                    $newArray[$currentRec->showLink]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey => $fieldValue) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $newArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $newArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $newArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $newArray[$currentRec->showLink][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }


                $tempMetadata = TempShowMetadata::where('showLink',$currentRec->showLink)->first();
                $tempOldmetadata = TempOldShowMetadata::where('showLink',$currentRec->showLink)->first();

                if($tempMetadata != null)
                {
                    $newArray[$currentRec->showLink]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_title) && $tempMetadata->meta_title != null) {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempMetadata->meta_title.'</p>';
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->showLink]['metadata'] .= (isset($tempMetadata->meta_title)) ? '<p><b>Title -</b> '.$tempMetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempMetadata->meta_description) && $tempMetadata->meta_description != null) {
                        $newArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempMetadata->meta_description;
                        $newArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $newArray[$currentRec->showLink]['metadata'] .= (isset($tempMetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempMetadata->meta_description : '';
                    }
                }

                $tempOffers = TempOffers::where('showLink', $currentRec->showLink)->get();
                $offerFields = array('title'=>'Title','type'=>'Type','valid'=>'Valid','start_date'=>'Start date','end_date'=>'End date', 'exclusive'=>'Exclusive','facevalue'=>'Facevalue','price'=>'Price','description'=>'Description');
                if ($tempOffers != null) {
                    $newArray[$currentRec->showLink]['offers'] = '';
                    foreach ($tempOffers as $offersKey => $offersValue) {
                        // $offersValue = $offersValue->toArray();

                        $tempOldOffer = TempOldOffers::where('offer_id', $offersValue['offer_id'])->first();
                        foreach ($offerFields as $offerFieldKey => $offerFieldValue) {
                            if(isset($tempOldOffer[$offerFieldKey]) && isset($offersValue[$offerFieldKey]) && $tempOldOffer[$offerFieldKey] != $offersValue[$offerFieldKey])
                            {
                                $newArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else if (!isset($tempOldOffer[$offerFieldKey]) && $offersValue[$offerFieldKey] != null) {
                                $newArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else {
                                $newArray[$currentRec->showLink]['offers'] .= (isset($offersValue[$offerFieldKey])) ? '<p><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>' : '';
                            }


                        }
                    }
                }


                $tempCategoryIds = TempShowCategory::leftJoin('categories','tmp_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempCategoryIds != null) {
                    $tempCategoryIds = $tempCategoryIds->toArray();
                }
                $tempOldCategoryIds = TempOldShowCategory::leftJoin('categories','tmp_old_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempOldCategoryIds != null) {
                    $tempOldCategoryIds = $tempOldCategoryIds->toArray();
                }

                if ($tempCategoryIds != null && $tempOldCategoryIds != null) {
                    $categoryIds = array_diff($tempCategoryIds, $tempOldCategoryIds);
                }
                else {
                    $categoryIds = [];
                }

                if (count($tempCategoryIds) > 0) {
                    $newArray[$currentRec->showLink]['category'] = '';
                    $category = Category::whereIn('id', $tempCategoryIds)->get();
                    foreach ($category as $categoryKey => $categoryValue) {
                        if (isset($categoryValue->categoryTitle) && $categoryValue->categoryTitle != null) {
                            $changeColor = (!in_array($categoryValue->id,$tempOldCategoryIds)) ? 'changecolor' : '';
                            $newArray[$currentRec->showLink]['category'] .= '<p class="'.$changeColor.'">'.$categoryValue->categoryTitle.'</p>';
                            if ($changeColor != '') {
                                $newArray[$currentRec->showLink]['updatedArr'][] = 'category';
                            }
                        }
                    }
                }

                $tempSchedule = TempSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();
                $tempoldSchedule = TempOldSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();

                $tempoldSchedule = $this->scheduleArrReturn($tempoldSchedule);
                $tempSchedule = $this->scheduleArrReturn($tempSchedule);
                if ($tempSchedule != null) {
                   $newArray[$currentRec->showLink]['schedule'] = '';
                    foreach ($tempSchedule as $key => $value) {
                        if (count($tempSchedule[$key]) > 0) {
                            if (isset($tempoldSchedule[$key]) && count($tempSchedule[$key]) == count($tempoldSchedule[$key])) {
                                $diff = array_diff($tempSchedule[$key], $tempoldSchedule[$key]);
                                if (count($diff) > 0) {
                                    $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // foreach ($diff as $valueKey) {
                                    //     $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    //     $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // }
                                }
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    // $changeColor = (in_array($valueKey,$diff)) ? 'changecolor' : '';
                                    $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';

                                }

                            }
                            else {
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $newArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    $newArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                }
                            }
                        }
                    }
                }
                // if ($newArray[$currentRec->showLink]['schedule'] == '') {
                //     if (in_array('schedule',$newArray[$showLink]['updatedArr'])) {
                //         if (count($newArray[$showLink]['updatedArr']) == 1) {
                //             unset($newArray[$currentRec->showLink]);
                //             unset($shows[$key]);
                //         }
                //     }
                // }
                    // dd($newArray);
                if (isset($newArray[$showLink]) && isset($newArray[$showLink]['updatedArr']) && count($newArray[$showLink]['updatedArr']) > 0) {
                    $newArray[$showLink]['updatedArr'] = array_unique($newArray[$showLink]['updatedArr']);
                    if($tabledata == null)
                    $newArray[$showLink]['existrecord'] = 0;
                    else
                    $newArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else if (isset($newArray[$showLink]['existrecord'])) {
                    if($tabledata == null)
                    $newArray[$showLink]['existrecord'] = 0;
                    else
                    $newArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else {
                    unset($newArray[$showLink]);
                }
                // dd($newArray);
            }

            // Old Array
            $currentRec = TempOldShow::where('showLink',$showLink)->first();

            if($currentRec != null)
            {
                $value = $currentRec;

                $codes[] = $currentRec->showLink;

                $tempOld = TempShow::where('showLink', $currentRec->showLink)->first();
                if ($tempOld != null) {
                    $tempOld = $tempOld->toArray();
                }
                else {
                    $oldArray[$currentRec->showLink] = $value->toArray();
                    $oldArray[$currentRec->showLink]['existrecord'] = 0;
                }
                $tabledata = Shows::where('showLink',$currentRec->showLink)->first();

                if($tabledata == null)
                {
                    $oldArray[$currentRec->showLink] = $value->toArray();
                    $oldArray[$currentRec->showLink]['existrecord'] = 0;
                }
                else {
                    foreach ($fields as $fieldKey => $fieldValue) {
                        if(isset($tempOld[$fieldKey]) && isset($currentRec[$fieldKey]) && $tempOld[$fieldKey] != $currentRec[$fieldKey])
                        {
                            $oldArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else if (!isset($tempOld[$fieldKey]) && $currentRec[$fieldKey] != null) {
                            $oldArray[$currentRec->showLink][$fieldKey] = $currentRec[$fieldKey];
                            $oldArray[$currentRec->showLink]['updatedArr'][] = $fieldKey;
                        }
                        else {
                            $oldArray[$currentRec->showLink][$fieldKey] = (isset($currentRec[$fieldKey])) ? $currentRec[$fieldKey] : '';
                        }
                    }
                }

                $tempMetadata = TempShowMetadata::where('showLink',$currentRec->showLink)->first();
                $tempOldmetadata = TempOldShowMetadata::where('showLink',$currentRec->showLink)->first();

                if($tempOldmetadata != null)
                {
                    $oldArray[$currentRec->showLink]['metadata'] = '';
                    $fieldKey = 'meta_title';

                    if(isset($tempOldmetadata->meta_title) && isset($tempMetadata->meta_title) && $tempOldmetadata->meta_title != $tempMetadata->meta_title)
                    {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_title) && $tempOldmetadata->meta_title != null) {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>';
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->showLink]['metadata'] .= (isset($tempOldmetadata->meta_title)) ? '<p><b>Title -</b> '.$tempOldmetadata->meta_title.'</p>' : '';
                    }

                    $fieldKey = 'meta_description';
                    if(isset($tempOldmetadata->meta_description) && isset($tempMetadata->meta_description) && $tempOldmetadata->meta_description != $tempMetadata->meta_description)
                    {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else if (!isset($tempOldmetadata->meta_description) && $tempOldmetadata->meta_description != null) {
                        $oldArray[$currentRec->showLink]['metadata'] .= '<p class="changecolor"><b>Description -</b> </p>'.$tempOldmetadata->meta_description;
                        $oldArray[$currentRec->showLink]['updatedArr'][] = 'metadata';
                    }
                    else {
                        $oldArray[$currentRec->showLink]['metadata'] .= (isset($tempOldmetadata->meta_description)) ? '<p><b>Description -</b> </p>'.$tempOldmetadata->meta_description : '';
                    }
                }

                $tempOffers = TempOldOffers::where('showLink', $currentRec->showLink)->get();
                $offerFields = array('title'=>'Title','type'=>'Type','valid'=>'Valid','start_date'=>'Start date','end_date'=>'End date', 'exclusive'=>'Exclusive','facevalue'=>'Facevalue','price'=>'Price','description'=>'Description');
                if ($tempOffers != null) {

                    $oldArray[$currentRec->showLink]['offers'] = '';
                    foreach ($tempOffers as $offersKey => $offersValue) {
                        $offersValue = $offersValue->toArray();
                        $tempOldOffer = TempOffers::where('offer_id', $offersValue['offer_id'])->first();
                        foreach ($offerFields as $offerFieldKey => $offerFieldValue) {
                            if(isset($tempOldOffer[$offerFieldKey]) && isset($offersValue[$offerFieldKey]) && $tempOldOffer[$offerFieldKey] != $offersValue[$offerFieldKey])
                            {
                                $oldArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else if (!isset($tempOldOffer[$offerFieldKey]) && $offersValue[$offerFieldKey] != null) {
                                $oldArray[$currentRec->showLink]['offers'] .= '<p class="changecolor"><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>';
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'offers';
                            }
                            else {
                                $oldArray[$currentRec->showLink]['offers'] .= (isset($offersValue[$offerFieldKey])) ? '<p><b>'.$offerFieldKey.' -</b> '.$offersValue[$offerFieldKey].'</p>' : '';
                            }

                        }
                    }
                }

                $tempCategoryIds = TempShowCategory::leftJoin('categories','tmp_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempCategoryIds != null) {
                    $tempCategoryIds = $tempCategoryIds->toArray();
                }
                $tempOldCategoryIds = TempOldShowCategory::leftJoin('categories','tmp_old_show_category.category_id','categories.id')->where('showLink',$currentRec->showLink)->pluck('categories.id');
                if ($tempOldCategoryIds != null) {
                    $tempOldCategoryIds = $tempOldCategoryIds->toArray();
                }

                if ($tempCategoryIds != null && $tempOldCategoryIds != null) {
                    $categoryIds = array_diff($tempOldCategoryIds, $tempCategoryIds);
                }
                else {
                    $categoryIds = [];
                }

                if (count($tempOldCategoryIds) > 0) {
                    $oldArray[$currentRec->showLink]['category'] = '';
                    $category = Category::whereIn('id', $tempOldCategoryIds)->get();
                    foreach ($category as $categoryKey => $categoryValue) {
                        if (isset($categoryValue->categoryTitle) && $categoryValue->categoryTitle != null) {
                            $changeColor = (!in_array($categoryValue->id,$tempCategoryIds)) ? 'changecolor' : '';
                            $oldArray[$currentRec->showLink]['category'] .= '<p class="'.$changeColor.'">'.$categoryValue->categoryTitle.'</p>';
                            if ($changeColor != '') {
                                $oldArray[$currentRec->showLink]['updatedArr'][] = 'category';
                            }
                        }
                    }
                }

                $tempSchedule = TempOldSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();
                $tempoldSchedule = TempSchedule::where('showLink', $currentRec->showLink)->orderby('time', 'ASC')->get();

                $tempoldSchedule = $this->scheduleArrReturn($tempoldSchedule);
                $tempSchedule = $this->scheduleArrReturn($tempSchedule);
                if ($tempSchedule != null) {
                   $oldArray[$currentRec->showLink]['schedule'] = '';
                    foreach ($tempSchedule as $key => $value) {
                        if (count($tempSchedule[$key]) > 0) {
                            if (isset($tempoldSchedule[$key]) && count($tempSchedule[$key]) == count($tempoldSchedule[$key])) {
                                $diff = array_diff($tempSchedule[$key], $tempoldSchedule[$key]);
                                if (count($diff) > 0) {
                                    $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // foreach ($diff as $valueKey) {
                                    //     $oldArray[$currentRec->showLink]['schedule'] .= '<p class="changecolor">'.$key.'-'.$valueKey.'</p>';
                                    //     $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                    // }
                                }
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $oldArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                }
                            }
                            else {
                                foreach ($tempSchedule[$key] as $valueKey) {
                                    $oldArray[$currentRec->showLink]['schedule'] .= '<p>'.$key.'-'.$valueKey.'</p>';
                                    $oldArray[$currentRec->showLink]['updatedArr'][] = 'schedule';
                                }
                            }
                        }
                    }
                }


                if ($oldArray[$currentRec->showLink]['schedule'] == '') {
                    unset($oldArray[$currentRec->showLink]['schedule']);
                }

                if (isset($oldArray[$showLink]) && isset($oldArray[$showLink]['updatedArr'])  && count($oldArray[$showLink]['updatedArr']) > 0) {
                    $oldArray[$showLink]['updatedArr'] = array_unique($oldArray[$showLink]['updatedArr']);
                    if($tabledata == null)
                    $oldArray[$showLink]['existrecord'] = 0;
                    else
                    $oldArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else if (isset($oldArray[$showLink]['existrecord'])) {
                    if($tabledata == null)
                    $oldArray[$showLink]['existrecord'] = 0;
                    else
                    $oldArray[$showLink]['existrecord'] = 1;
                    $recCode[] = $showLink;
                }
                else {
                    unset($oldArray[$showLink]);
                }

            }
        }


        $codes = array_unique($recCode);

        $fields = array('showTitle'=>'Show Title','summary'=>'Summary','description'=>'Description', 'priceFrom'=>'Price From','priceTo'=>'Price To','runningTime'=>'Running Time','ageRestriction'=>'Age Restriction','importantInfo'=>'ImportantInfo','venueCode'=>'Venue Code','bannerBackgroundColour'=>'Banner Background Colour','schedule'=>'Schedule','offers'=>'Offers','metadata'=>'Metadata','category'=>'Category');
        // dd($oldArray);
        if(count($codes) > 0)
        {
            // Session::flash('flash_warning', 'Please check the below record to import show.');
            // return view('admin.dashboard.showconflictList', compact('codes','newArray','oldArray','fields'));

            $content = view('admin.dashboard.showconflictListForMail', compact('codes','newArray','oldArray','fields'))->render();
            // dd($content);
            if (! File::exists(public_path()."/importfiles")) {
                File::makeDirectory(public_path()."/importfiles");
            }
            $fiename = 'show_import_' . strtotime(date('y-m-d h:i:s')) . '.doc';

            file_put_contents(public_path("importfiles/".$fiename),$content);

            $pathToFile = public_path('importfiles/'.$fiename);

            $cmsSetting=CmsSetting::first();

            if ($cmsSetting != null) {
                Mail::send('emails.wordfile',['pathToFile' => $pathToFile], function ($message) use ($pathToFile,$cmsSetting)
                {
                    $message->attach($pathToFile);
                    $message->subject('Hit The Theatre Import Show'. date('Y-m-d h:i:s'));
                    $message->to($cmsSetting->contact_us_email)->cc(['user.citrusbug@gmail.com']);
                });
            }
        }

    }

    public function scheduleArrReturn($schedules)
    {
        $newschedule = array('Mon'=>array(),'Tue'=>array(),'Wed'=>array(),'Thu'=>array(),'Fri'=>array(),'Sat'=>array(),'Sun'=>array());
            foreach ($schedules as $key => $val)
            {
                switch ($val->day_id) {
                    case 'Mon':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Tue':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Wed':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Thu':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Fri':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sat':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;
                    case 'Sun':
                        $newschedule[$val->day_id][$key] = array();
                        $newschedule[$val->day_id][$key] = (in_array($val->time,$newschedule[$val->day_id]) ? '' : $val->time);
                        if($newschedule[$val->day_id][$key] == '')
                        unset($newschedule[$val->day_id][$key]);

                        $newschedule[$val->day_id] = array_values($newschedule[$val->day_id]);
                        break;

                    default:
                        break;
                }
            }
        return $newschedule;
    }

    public function slugCreate($slug)
    {
        $slug = str_replace(' ', '-', rtrim($slug));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '-', $slug);
        $slug = preg_replace('/--+/', '', $slug);
        $slug = strtolower($slug);
        return $slug;
    }

    private function show_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    private function show_rating($showlink)
    {

        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        //CURLOPT_URL => 'https://api.entstix.com/api/v1/xlive/content/show/LDN/1014/quote',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else
        {

            $show_rating_xml = simplexml_load_string($response);
            $show_rating_array = json_decode(json_encode($show_rating_xml), true);
            if(isset($show_rating_array['quotes']) && is_array($show_rating_array['quotes']))
            {
                $show_rating=$show_rating_array['quotes']['quote'];
                $rating=0;

                foreach($show_rating as $value)
                {

                    if(isset($value['starRating']) && is_integer($value['starRating']))
                    {
                        $rating=$rating+$value['starRating'];
                    }
                }

                $avg_rating = $rating / count($show_rating);

            }
            else
            {
                return 0;
            }

        }
    }

    public function schedule_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;
        $URL = $this->liveUrlSetting->value . 'content/show/'.$showlink."/schedule";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }

    }
}


// $URLoffer = $this->liveUrlSetting->value . 'content/show/LDN/1014/offer';

//                 $ch = curl_init();
//                 curl_setopt($ch, CURLOPT_URL, $URLoffer);
//                 curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                 curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//                 curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

//                 $specialOffer = curl_exec($ch);
//                 $specialOffer_status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
//                 curl_close($ch);

//                 if ($specialOffer) {

//                     $specialOffer = simplexml_load_string($specialOffer, 'SimpleXMLElement', LIBXML_NOCDATA);
//                     $array = json_decode(json_encode($specialOffer), true);
//                     $offers = $array['offers']['offer'];
//                     if($offers != NULL && isset($offers))
//                     {
//                         if (array_key_exists("@attributes",$offers) != 1) {
//                             foreach($offers as $key=>$value_data)
//                             {

//                                     $offer['title']=(isset($value_data['title']) && $value_data['title'] != null) ? $value_data['title'] : '';
//                                     $offer['showLink'] = $show->showLink;
//                                     $offer['offer_id']=   (isset($value_data['@attributes']['id']) && $value_data['@attributes']['id'] != null) ? $value_data['@attributes']['id'] : '';
//                                     $offer['type']=(isset($value_data['type']) && $value_data['type'] != null) ? $value_data['type']  : '';
//                                     $offer['valid']=(isset($value_data['valid']) && $value_data['valid'] != null) ? $value_data['valid']  : '';
//                                     $offer['start_date']=(isset($value_data['starts']) && $value_data['starts'] != null) ? $value_data['starts']  : '';
//                                     $offer['end_date'] = (isset($value_data['ends']) && $value_data['ends'] != null) ? $value_data['ends']  : '';
//                                     $offer['facevalue']=(isset($value_data['faceValue']) && $value_data['faceValue'] != null) ? $value_data['faceValue']  : '';
//                                     $offer['price']=(isset($value_data['price']) && $value_data['price'] != null) ? $value_data['price']  : '';
//                                     $offer['description'] = (isset($value_data['description']) && $value_data['description'] != null) ? $value_data['description']  : '';
//                                     $offer_data = TempOffers::insert($offer);

//                             }
//                         }
//                         else {
//                                 $offer['title']=(isset($offers['title']) && $offers['title'] != null) ? $offers['title'] : '';
//                                 $offer['showLink'] = $show->showLink;
//                                 $offer['offer_id']=   (isset($offers['@attributes']['id']) && $offers['@attributes']['id'] != null) ? $offers['@attributes']['id'] : '';
//                                 $offer['type']=(isset($offers['type']) && $offers['type'] != null) ? $offers['type']  : '';
//                                 $offer['valid']=(isset($offers['valid']) && $offers['valid'] != null) ? $offers['valid']  : '';
//                                 $offer['start_date']=(isset($offers['starts']) && $offers['starts'] != null) ? $offers['starts']  : '';
//                                 $offer['end_date'] = (isset($offers['ends']) && $offers['ends'] != null) ? $offers['ends']  : '';
//                                 $offer['facevalue']=(isset($offers['faceValue']) && $offers['faceValue'] != null) ? $offers['faceValue']  : '';
//                                 $offer['price']=(isset($offers['price']) && $offers['price'] != null) ? $offers['price']  : '';
//                                 $offer['description'] = (isset($offers['description']) && $offers['description'] != null) ? $offers['description']  : '';
//                                 $offer_data = TempOffers::insert($offer);

//                         }
//                     }
//                 }
