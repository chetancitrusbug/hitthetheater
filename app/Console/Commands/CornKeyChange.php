<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Setting;

class CornKeyChange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CornKeyChange:CornKeyChange';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Corn Key Change';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dataArr = array(
        [
            'name' => 'Location Cron Key',
            'key' => 'location_cron_key',
            'value' => '1',
        ],
        [
            'name' => 'Venue Cron Key',
            'key' => 'venue_cron_key',
            'value' => '1',
        ],
        [
            'name' => 'Show Cron Key',
            'key' => 'show_cron_key',
            'value' => '1',
        ],
        [
            'name' => 'Footer Dispay',
            'key' => 'footer_dispay',
            'value' => '1',
        ]);

        foreach ($dataArr as $key => $value) {
            $cronKeySetting = Setting::where('key',$value['key'])->first();
            if($cronKeySetting != null)
            {
                $cronKeySetting->value = $value['value'];
                $cronKeySetting->save();
            }
            else {
                Setting::create($value);
            }
        }

        $message = 'Updated Successfully';
        echo response()->json(['message' => $message, 'code' => 200], 200);exit;
    }
}
