<?php

namespace App;
use App\VenueCategory;
use Illuminate\Database\Eloquent\Model;

class VenueCategories extends Model
{
    protected $table = 'venue_categories';

    protected $fillable = [
         'categoryTitle', 'status','slug'
    ];

    protected $appends =['venue_count'];


    public function getVenueCountAttribute()
    {
        $data_count=VenueCategory::leftjoin('venues','venues.venueCode','venue_category.venueCode')->leftjoin('venue_categories','venue_categories.id','venue_category.category_id')
                ->where('venues.status',1)->where('venue_categories.id',$this->attributes['id'])->groupby('venues.venueCode')->get();
        return count($data_count);
    }

}
