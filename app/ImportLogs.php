<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportLogs extends Model
{
    protected $table = 'importlogs';

    protected $fillable = [
         'import_type'
    ];

    public function getCreatedAtAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d M Y H:i A');
        }
        return $value;
    }
}
