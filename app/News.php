<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
         'title','slug','description','featuredImage','isFuture','orderby','status','publishDate'
    ];

    protected $appends =['image'];


    public function getImageAttribute()
    {
        $path=public_path().'/storage/news'.$this->featuredImage;

        if(file_exists($path))
        {
            $path=url('').'/storage/news'.$this->featuredImage;
            return $path;
        }

            return '';
    }
}
