<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempLocation extends Model
{
    //
    protected $table = 'tmp_location';

    protected $fillable = [
         'locationTitle','locationCode', 'status'
    ];
}
