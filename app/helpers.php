<?php

function show_details($file,$user_id)
{
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.entstix.com/api/v1/xlive/content/show/LDN/1001",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(
        "Authorization: Basic aGl0dGhldGhlYXRyZXA6VVd0Z0tFWldGY2lHUGxWSzRZdGw=,Basic aGl0dGhldGhlYXRyZXA6VVd0Z0tFWldGY2lHUGxWSzRZdGw=",
        "Postman-Token: 925c0fd5-b89a-46f9-8662-69c9c0a8899a",
        "cache-control: no-cache"
    ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
    echo $response;
    }
}
function truncate($text, $chars = 150) {

    if(strlen($text) > $chars) {
        $text = $text.' ';
        $text = substr($text, 0, $chars);
        $text = substr($text, 0, strrpos($text ,' '));
        $text = $text.'...';
    }
    return $text;
    }