<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowMetadata extends Model
{

    protected $table = 'show_meta_data';

    protected $fillable = [
        'show_id', 'meta_title', 'meta_description','showLink'
    ];
}
