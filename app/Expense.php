<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //
    protected $table = 'client_expense';

    protected $fillable = [
        'invoice_id', 'user_id','category_id','client_id','category_id','title','desc','upload','net_amount','vat','total_amount','status'
    ];
}
