<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldVenueMetadata extends Model
{
    protected $table = 'tmp_old_venue_metadata';

    protected $fillable = [
        'venue_id', 'meta_title', 'meta_description','venueCode'
    ];
}
