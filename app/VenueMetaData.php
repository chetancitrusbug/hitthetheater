<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueMetaData extends Model
{
    protected $table = 'venue_meta_data';

    protected $fillable = [
        'venue_id', 'meta_title', 'meta_description','venueCode'
    ];
}
