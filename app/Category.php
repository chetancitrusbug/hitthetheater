<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VenueCategory;
use App\ShowCategory;

class Category extends Model
{
    //
    protected $table = 'categories';

    protected $fillable = [
         'categoryTitle', 'status','slug'
    ];

    protected $appends =['show_count','venue_count'];

    public function Show()
    {
        return $this->belongsToMany('App\Show', 'show_category', 'category_id', 'show_id');
    }

    public function getShowCountAttribute()
    {
        $data_count=ShowCategory::leftjoin('show','show.showLink','show_category.showLink')->leftjoin('categories','categories.id','show_category.category_id')
                ->where('show.status',1)->where('categories.id',$this->attributes['id'])->groupby('show.showLink')->get();
        return count($data_count);
    }

    public function getVenueCountAttribute()
    {
        $data_count=VenueCategory::leftjoin('venues','venues.venueCode','venue_category.venueCode')->leftjoin('categories','categories.id','venue_category.category_id')
                ->where('venues.status',1)->where('categories.id',$this->attributes['id'])->groupby('venues.venueCode')->get();
        return count($data_count);
    }

    public function myshow(){
        return $this->hasMany('App\ShowCategory','category_id')->with('show');
    }


}
