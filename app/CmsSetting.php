<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsSetting extends Model
{
    //
    protected $table = 'cms_setting';

    protected $fillable = [
        'about_us','contact_us_address', 'contact_us_email', 'contact_us_phone','contact_us','contact_us_email_tamplate','banner','facebook_link','twitter_link','youtube_link','instagram_link','copy_right_content','logo','category_for_header','about_us_display','faq_display','contact_us_display','termandcondition_display','privacypolicy_display','cookiepolicy_display','trust_factor_display','trust_factor'
    ];
}
