<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner';

    protected $fillable = [
         'title','image', 'status','orderby','content','bannerLink'
    ];

    protected $appends =['banner'];


    public function getBannerAttribute()
    {
        $path=public_path().'/storage/banner'.$this->image;

        if(file_exists($path))
        {
            $path=url('').'/storage/banner'.$this->image;
            return $path;
        }

            return '';
    }

}
