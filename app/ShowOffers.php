<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowOffers extends Model
{
    protected $table = 'show_offers';

    protected $fillable = [
        'showLink', 'offer_id'
    ];
}
