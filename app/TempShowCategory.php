<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempShowCategory extends Model
{
    //tmp_show_category
    protected $table = 'tmp_show_category';

    protected $fillable = [
         'show_id', 'category_id','showLink'
    ];
}
