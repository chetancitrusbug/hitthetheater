<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowCategory extends Model
{
    protected $table = 'show_category';

    protected $fillable = [
         'show_id', 'category_id','showLink'
    ];

   public function show()
    {
        return $this->belongsTo('App\Show','showLink','showLink');
    }

}
