<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempShowMetadata extends Model
{
    protected $table = 'tmp_show_meta_data';

    protected $fillable = [
        'show_id', 'meta_title', 'meta_description','showLink'
    ];
}
