<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempShowImages extends Model
{
    protected $table = 'tmp_show_images';

    protected $fillable = ['show_id', 'url', 'type', 'title', 'width', 'height','showLink' ];
}
