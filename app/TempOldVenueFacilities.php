<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldVenueFacilities extends Model
{
    protected $table = 'tmp_old_venue_facilities';

    protected $fillable = [
         'venueId','facilitieId','venueCode'
    ];
}
