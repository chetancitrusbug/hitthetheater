<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempVenueSliders extends Model
{
    protected $table = 'tmp_venue_sliders';

    protected $fillable = [
         'venueId','image','type','title','width','height','venueCode'
    ];
}
