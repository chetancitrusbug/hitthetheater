<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempVenueTransportInfo extends Model
{
    protected $table = 'tmp_venue_transport_info';

    protected $fillable = [
         'venueId','transportId','transportValue','venueCode'
    ];
}
