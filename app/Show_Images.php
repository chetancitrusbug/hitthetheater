<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Show_Images extends Model
{
    //
    protected $table = 'show_images';

    protected $fillable = ['show_id', 'url', 'type', 'title', 'width', 'height','showLink','created_by','alt','orderBy' ];

    protected $appends =['banner','poster'];


    public function getPosterAttribute()
    {
        $show_images=Show_Images::where('showLink',$this->showLink)->where('type','rectangle-poster')->orderBy('id','desc')->first();
        //dd($show_images);
        if($show_images != null)
        {

            $path=public_path().'/storage/show'.$show_images->url;

            if(file_exists($path))
            {
                $path=url('').'/storage/show'.$show_images->url;
                return $path;
            }
            elseif(@fopen($show_images->url,'r'))
            {
                return $show_images->url;

            }
            else
            {
                return '';
            }

        }
        else
        {
           return '';

        }
    }

    public function getBannerAttribute()
    {
        $show_images=Show_Images::where('showLink',$this->showLink)->where('type','banner')->orderBy('id','desc')->first();
        //dd($show_images);
       $path=public_path().'/storage/show'.$show_images->url;

            if(file_exists($path))
            {
                $path=url('').'/storage/show'.$show_images->url;
                return $path;
            }

                return '';
    }
}
