<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = 'invoice';

    protected $fillable = [
        'invoice_id', 'user_id', 'client_id','subject','description','net_amount','vat','total_amount','invoice_image','date_time','status'
    ];
}
