<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempSchedule extends Model
{
    protected $table = 'temp_schedule';

    protected $fillable = ['show_id', 'day_id', 'time','showLink'];
}
