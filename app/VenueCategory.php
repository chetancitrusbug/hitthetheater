<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueCategory extends Model
{
    protected $table = 'venue_category';

    protected $fillable = [
         'venueCode', 'category_id'
    ];

   public function venues()
    {
        return $this->belongsTo('App\Venue','venueCode','venueCode');
    }


}
