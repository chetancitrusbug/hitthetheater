<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueTransportInfo extends Model
{
    protected $table = 'venue_transport_info';

    protected $fillable = [
         'venueId','transportId','transportValue','venueCode'
    ];
}
