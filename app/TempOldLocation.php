<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldLocation extends Model
{
    protected $table = 'tmp_old_location';

    protected $fillable = [
         'locationTitle','locationCode', 'status'
    ];
}
