<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldShowMetadata extends Model
{
    protected $table = 'tmp_old_show_meta_data';

    protected $fillable = [
        'show_id', 'meta_title', 'meta_description','showLink'
    ];
}
