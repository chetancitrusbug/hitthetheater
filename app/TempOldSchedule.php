<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldSchedule extends Model
{
    protected $table = 'temp_old_schedule';

    protected $fillable = ['show_id', 'day_id', 'time','showLink'];
}
