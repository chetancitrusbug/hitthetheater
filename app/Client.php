<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $table = 'client';

    protected $fillable = [
        'user_id','email', 'client_name', 'address','phone','image','status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','id');
    }
}
