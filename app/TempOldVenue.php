<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldVenue extends Model
{
    protected $table = 'tmp_old_venue';

    protected $fillable = [
         'locationId','categoryId','venueTitle','venueDescription','postcode','latitude','longitude','status','venueCode','addressLine1','addressLine2','locationCode'
    ];

}
