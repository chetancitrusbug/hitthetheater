<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOldShowCategory extends Model
{
    //tmp_show_category
    protected $table = 'tmp_old_show_category';

    protected $fillable = [
         'show_id', 'category_id','showLink'
    ];
}
