<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VenueLog extends Model
{
    protected $table = 'venue_logs';

    protected $fillable = [
        'code', 'key', 'value'
    ];
}
