<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Setting;
class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataArr = array([
            'name' => 'Authorization',
            'key' => 'authorization',
            'value' => 'Basic aGl0dGhldGhlYXRyZXA6VVd0Z0tFWldGY2lHUGxWSzRZdGw=',
        ],
        [
            'name' => 'Username',
            'key' => 'username',
            'value' => 'hitthetheatrep',
        ],
        [
            'name' => 'Password',
            'key' => 'password',
            'value' => 'UWtgKEZWFciGPlVK4Ytl',
        ],
        [
            'name' => 'Live URL',
            'key' => 'live_url',
            'value' => '1',
        ],
        [
            'name' => 'Location Cron Key',
            'key' => 'location_cron_key',
            'value' => '0',
        ],
        [
            'name' => 'Venue Cron Key',
            'key' => 'venue_cron_key',
            'value' => '0',
        ],
        [
            'name' => 'Show Cron Key',
            'key' => 'show_cron_key',
            'value' => '0',
        ],
        [
            'name' => 'Banner For Frontend (0 for show and 1 for custom)',
            'key' => 'banner_for_frontend',
            'value' => '0',
        ]);

        Setting::insert($dataArr);

    }
}
