<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOldShowCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_old_show_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('show_id');
            $table->integer('category_id');
            $table->timestamps();
            $table->string('showLink')->nullable(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('tmp_old_show_category');
    }
}
