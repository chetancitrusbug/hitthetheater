<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_setting', function ($table) {
            $table->text('faq')->nullable();
            $table->text('termandcondition')->nullable();
            $table->text('privacypolicy')->nullable();
            $table->text('cookiepolicy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_setting', function ($table) {
            $table->dropColumn('faq');
            $table->dropColumn('termandcondition');
            $table->dropColumn('privacypolicy');
            $table->dropColumn('cookiepolicy');
        });
    }
}
