<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOldShowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_old_show', function (Blueprint $table) {
           $table->increments('id');
            $table->string('showTitle')->nullable();
            $table->string('indexName')->nullable();
            $table->string('showLink')->nullable();
            $table->date('bookingStarts')->nullable();
            $table->date('bookingEnds')->nullable();
            $table->string('isEvent')->nullable();
            $table->text('summary')->nullable();
            $table->text('description')->nullable();
            $table->double('priceFrom', 8, 2)->nullable();
            $table->double('priceTo', 8, 2)->nullable();
            $table->double('onSaleDate', 8, 2)->nullable();
            $table->string('runningTime')->nullable();
            $table->text('ageRestriction')->nullable();
            $table->string('limitedStock')->nullable();
            $table->string('whitelabelURI')->nullable();
            $table->text('importantInfo')->nullable();
            $table->string('bannerBackgroundColour')->nullable();
            $table->string('showType')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->text('resource_image')->nullable();
            $table->text('categories')->nullable();
            $table->float('avg_rating')->default(0);
            $table->string('venueCode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_old_show');
    }
}
