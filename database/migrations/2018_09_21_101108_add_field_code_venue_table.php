<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCodeVenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venues', function (Blueprint $table) {
            $table->string('venueCode')->nullable();
            $table->dropColumn('address');
            $table->text('addressLine1')->nullable();
            $table->text('addressLine2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venues', function (Blueprint $table) {
            $table->dropColumn('venueCode');
            $table->text('address')->nullable();
            $table->dropColumn('addressLine1')->nullable();
            $table->dropColumn('addressLine2')->nullable();

        });



    }
}
