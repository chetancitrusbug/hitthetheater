<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShowYoutubeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('show',function($table){
           $table->string('youtube_link');
           $table->tinyInteger('iframe_display')->default(0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('show',function($table){
            $table->dropColumn('orderby');
            $table->dropColumn('iframe_display');
        });
    }
}
