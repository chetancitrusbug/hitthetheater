<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('cms_setting',function($table){
            $table->tinyInteger('trust_factor_display')->default(0);
            $table->text('trust_factor');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_setting',function($table){
            $table->dropColumn('trust_factor_display');
            $table->dropColumn('trust_factor');
        });
    }
}
