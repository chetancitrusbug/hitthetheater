<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOldVenuetransportinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_old_venue_transport_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('venueId');
            $table->integer('transportId');
            $table->text('transportValue');
            $table->timestamps();
            $table->string('venueCode')->nullable(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_old_venue_transport_info');
    }
}
