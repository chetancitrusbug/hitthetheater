<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->text('about_us')->nullable();
            $table->text('contact_us_address')->nullable();
            $table->string('contact_us_email')->nullable();
            $table->string('contact_us_phone')->nullable();
            $table->text('contact_us')->nullable();
            $table->text('contact_us_email_tamplate')->nullable();
            $table->string('banner')->default(1);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_setting');
    }
}
