<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('cms_setting',function($table){
            $table->tinyInteger('about_us_display')->default(0);
            $table->tinyInteger('faq_display')->default(0);
            $table->tinyInteger('contact_us_display')->default(0);
            $table->tinyInteger('termandcondition_display')->default(0);
            $table->tinyInteger('privacypolicy_display')->default(0);
            $table->tinyInteger('cookiepolicy_display')->default(0);
            $table->tinyInteger('contact_us_address_display')->default(0);
            $table->string('facebook_link_display');
            $table->string('twitter_link_display');
            $table->string('youtube_link_display');
            $table->string('instagram_link_display');
            $table->string('copy_right_content_display');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_setting',function($table){
            $table->dropColumn('about_us_display');
            $table->dropColumn('faq_display');
            $table->dropColumn('contact_us_display');
            $table->dropColumn('termandcondition_display');
            $table->dropColumn('privacypolicy_display');
            $table->dropColumn('cookiepolicy_display');
            $table->dropColumn('contact_us_address_display');
            $table->dropColumn('facebook_link_display');
            $table->dropColumn('twitter_link_display');
            $table->dropColumn('youtube_link_display');
            $table->dropColumn('instagram_link_display');
            $table->dropColumn('copy_right_content_display');
        });
    }
}
