<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempShowOfferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('type');
            $table->string('valid');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('exclusive');
            $table->string('facevalue');
            $table->float('price');
            $table->text('description');
            $table->timestamps();
            $table->string('offer_id');
           $table->string('showLink');
            $table->tinyInteger('created_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

    public function down()
    {
        Schema::dropIfExists('tmp_offers');
    }
}
