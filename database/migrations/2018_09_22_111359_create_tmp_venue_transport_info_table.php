<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmpVenueTransportInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('tmp_venue_transport_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('venueId');
            $table->integer('transportId');
            $table->text('transportValue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_venue_transport_info');
    }
}
