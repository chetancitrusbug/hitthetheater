<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOldScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_old_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->string('show_id');
            $table->string('day_id');
            $table->time('time');
            $table->timestamps();
            $table->string('showLink')->nullable(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_old_schedule');
    }
}
