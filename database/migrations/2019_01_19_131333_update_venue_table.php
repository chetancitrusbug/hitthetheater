<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVenueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('venues',function($table){
           $table->tinyInteger('createdby')->default(0);
           $table->tinyInteger('updatedby')->default(0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venues',function($table){
            $table->dropColumn('createdby');
            $table->dropColumn('updatedby');
        });
    }
}
