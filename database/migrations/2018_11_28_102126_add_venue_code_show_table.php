<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVenueCodeShowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('show', function (Blueprint $table) {
            $table->string('venueCode')->nullable();
            $table->enum('showStatus', ['import', 'custom'])->default('import');
            $table->enum('popular', ['0', '1'])->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('show', function (Blueprint $table) {
            $table->dropColumn('venueCode')->nullable();
            $table->dropColumn('showStatus');
            $table->dropColumn('popular');
        });
    }
}
