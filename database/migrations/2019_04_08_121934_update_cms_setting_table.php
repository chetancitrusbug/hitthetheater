<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCmsSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('cms_setting',function($table){
           $table->string('facebook_link');
           $table->string('twitter_link');
           $table->string('youtube_link');
           $table->string('instagram_link');
           $table->string('copy_right_content');
           $table->string('logo');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_setting',function($table){
            $table->dropColumn('facebook_link');
            $table->dropColumn('twitter_link');
            $table->dropColumn('youtube_link');
            $table->dropColumn('instagram_link');
            $table->dropColumn('copy_right_content');
            $table->dropColumn('logo');
        });
    }
}
