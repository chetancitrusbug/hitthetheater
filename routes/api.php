<?php

use Illuminate\Http\Request;
   
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1.0/', 'namespace' => 'Api'], function () {
    Route::post('user/login', 'UsersController@login');
    Route::post('user/register', 'UsersController@register');
    Route::post('user/forgotpassword', 'UsersController@forgotPassword');
    Route::get('countrylist', 'CountriesController@list');

    Route::post('user/socialmedialogin', 'UsersController@socialMediaLogin');
});

Route::group(['prefix'=>'v1.0/','middleware'=>['api_pass'],'namespace'=>'Api'],function(){
    Route::post('user/profileupdate', 'UsersController@profileUpdate');
    Route::post('user/changepassword', 'UsersController@changePassword');

    //client route
    Route::get('clientlist','ClientController@list');
    Route::post('clientstore','ClientController@store');
    Route::get('client','ClientController@edit');
    Route::post('clientupdate','ClientController@update');

    //invoice route
    Route::get('invoicelist','InvoiceController@list');
    Route::post('invoicestore','InvoiceController@store');
    Route::get('invoice','InvoiceController@edit');
    Route::post('invoiceupdate','InvoiceController@update');

    //expense route
    Route::get('expenselist','ExpenseController@list');
    Route::post('expensestore','ExpenseController@store');
    Route::get('expense','ExpenseController@edit');
    Route::post('expenseupdate','ExpenseController@update');
});
