<?php
use App\CarBrand;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();

Route::get('showincrement', 'Admin\DashboardController@showincrement');
Route::get('showslug', 'Admin\DashboardController@showslug');
Route::get('venueslug', 'Admin\DashboardController@venueslug');
Route::get('categoryslug', 'Admin\DashboardController@categoryslug');
Route::get('venueincrement', 'Admin\DashboardController@venueincrement');
Route::get('bannerincrement', 'Admin\DashboardController@bannerincrement');

Route::group(['middleware' => ['frontend']], function () {

Route::get('/', 'Front_End\HomeController@index')->name('/');
Route::get('/showGalleryImageGet/{id}', 'Front_End\HomeController@showGalleryImageGet')->name('showGalleryImageGet');
Route::get('/getImageofShow/{id}', 'Front_End\HomeController@getImageofShow')->name('showImageGet');

// sitemap
Route::get('/sitemap.xml', 'Front_End\SitemapController@index');
// Route::get('/sitemap/categories', 'Front_End\SitemapController@category');
// Route::get('/sitemap/venues', 'Front_End\SitemapController@venue');
// Route::get('/sitemap/venueCategories', 'Front_End\SitemapController@venueCategories');
// Route::get('/sitemap/shows', 'Front_End\SitemapController@show');


Route::get('/tickets', 'Front_End\HomeController@tickets');

Route::get('/category','Front_End\CategoryController@index')->name('category');
Route::get('/category/{id}','Front_End\CategoryController@viewCategory')->name('single-category');
Route::get('/category-theater/{id}','Front_End\CategoryController@viewTheaterCategory')->name('single-category-theater');
Route::get('/show/{id}','Front_End\TheaterController@viewShowDetail')->name('show-detail');
//Route::get('/category_listing/{id}','Front_End\TheaterController@categoryShowList')->name('category-show-list');
Route::get('search-text', 'Front_End\HomeController@globalSearch')->name('search');
Route::get('venue_category_filter', 'Front_End\TheaterController@venueCategoryFilter')->name('venue_category_filter');


Route::get('/admin', 'HomeController@index')->name('/');
Route::post('/bookTicket','Front_End\HomeController@bookTicket')->name('bookTicket');

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about-us', 'Front_End\CMSController@about_us')->name('about-us');

//theater
Route::get('/theater', 'Front_End\TheaterController@index')->name('theater');
Route::get('/theater/{id}','Front_End\TheaterController@viewTheater')->name('single-theater');
//end theater

// News
Route::get('/news', 'Front_End\NewsController@index')->name('news');
Route::get('/news/{slug}','Front_End\NewsController@viewNews')->name('single-news');


Route::get('/contact-us', 'Front_End\CMSController@contact_us')->name('contact-us');
Route::post('/contect_us_post', 'Front_End\CMSController@contect_us_post');
Route::get('/maintenance', 'Front_End\CMSController@maintenance')->name('maintenance');


// Hit The Theatre routes start
Route::get('/terms-and-conditions', 'Front_End\HomeController@termsAndConditions')->name('terms-and-conditions');
Route::get('/privacy-policy', 'Front_End\HomeController@privacyPolicy')->name('privacy-policy');
Route::get('/cookie-policy', 'Front_End\HomeController@cookiePolicy')->name('privacy-policy');
Route::get('/faq', 'Front_End\HomeController@faq')->name('privacy-policy');

Route::get('/activate-account/{token}', 'Auth\RegisterController@activateAccount');
});


Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/home', 'HomeController@redirect');



    Route::group(['prefix' => 'admin' ], function () {

        Route::get('/', 'Admin\AdminController@index');

        Route::get('/dashboard', 'Admin\DashboardController@index');
        Route::get('/getLocation', 'Admin\DashboardController@getLocation');
        Route::get('/getCategory', 'Admin\DashboardController@getCategory');
        Route::get('/getVenue', 'Admin\DashboardController@getVenue');
        Route::get('/getVenueDetails', 'Admin\DashboardController@getVenueDetails');
        Route::get('/getShow', 'Admin\DashboardController@getShow');
        Route::get('/dashboard/importLocation', 'Admin\DashboardController@importLocation');
        Route::get('/dashboard/importShow', 'Admin\DashboardController@importShow');
        Route::get('/dashboard/importVenue', 'Admin\DashboardController@importVenues');
        Route::post('/dashboard/solveConflictLocation', 'Admin\DashboardController@solveConflictLocation');
        Route::post('/dashboard/solveConflictVenue', 'Admin\DashboardController@solveConflictVenue');
        Route::post('/dashboard/solveConflictShow', 'Admin\DashboardController@solveConflictShow');


        //Role & Permissions
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');

        //Users
        Route::get('/users-data', 'Admin\UsersController@datatable');
        Route::resource('/users', 'Admin\UsersController');

        //Location
        Route::get('/location-data', 'Admin\LocationController@datatable');
        Route::resource('/location', 'Admin\LocationController');

        //offers
        Route::post('/offeradd', 'Admin\OfferController@offeradd');
        Route::post('/offerupdate', 'Admin\OfferController@offerupdate');
        Route::get('/offerdelete/{offer_id}', 'Admin\OfferController@offerdelete');
        Route::get('/offerdetail/{offer_id}', 'Admin\OfferController@offerdetail');
        Route::get('/offerlist/{location}/{show_link}', 'Admin\OfferController@offerlist');

        //Contactus
        Route::get('/contactus-data', 'Admin\ContactusController@datatable');
        Route::resource('/contactus', 'Admin\ContactusController');


        //Banner
        Route::get('/banner-data', 'Admin\BannerController@datatable');
        Route::get('/orderby-status/banner/{id}/{orderbyvalue}', 'Admin\BannerController@orderbyStatus');
        Route::resource('/banner', 'Admin\BannerController');

        //news
        Route::get('/news-data', 'Admin\NewsController@datatable');
        Route::get('/orderby-status/news/{id}/{orderbyvalue}', 'Admin\NewsController@orderbyStatus');
        Route::resource('/news', 'Admin\NewsController');

        //Venue
        Route::get('/venue-data', 'Admin\VenueController@datatable');
        Route::get('/deleteallvenue', 'Admin\VenueController@deleteallvenue');
        Route::get('/orderby-status/venues/{id}/{orderbyvalue}', 'Admin\VenueController@orderbyStatus');
        Route::resource('/venue', 'Admin\VenueController');

        // Venue Category
        // Route::post('/categoryadd', 'Admin\CategoryController@categoryadd');
        // Route::post('/categoryupdate', 'Admin\CategoryController@categoryupdate');
        // Route::get('/categorydelete/{category_id}/{venueCode}', 'Admin\CategoryController@categorydelete');
        // Route::get('/categorydetail/{category_id}', 'Admin\CategoryController@categorydetail');
        // Route::get('/categorylist/{venueCode}', 'Admin\CategoryController@categorylist');

        //Show
        Route::get('/show-data', 'Admin\ShowController@datatable');
        Route::get('/deleteallshows', 'Admin\ShowController@deleteallshows');
        Route::get('/show/showImageRemove/{showLocationCode}/{showId}/{id}', 'Admin\ShowController@showImageRemove');
        Route::get('/showImageDetail/{imgId}', 'Admin\ShowController@showImageDetail');
        Route::post('/showImageupdate', 'Admin\ShowController@showImageupdate');
        Route::get('/showImageList/{showLocationCode}/{showId}', 'Admin\ShowController@showImageList');
        Route::get('/orderby-status/show/{id}/{orderbyvalue}', 'Admin\ShowController@orderbyStatus');
        Route::resource('/show', 'Admin\ShowController');



        //Category
        Route::get('/category-data', 'Admin\CategoryController@datatable');
        Route::resource('/category', 'Admin\CategoryController');

        //  Venue category
        Route::get('/venuecategory-data', 'Admin\VenueCategoryController@datatable');
        Route::resource('/venuecategory', 'Admin\VenueCategoryController');

        //setting
        Route::resource('/setting', 'Admin\SettingController');
        Route::get('/setting-data', 'Admin\SettingController@datatable');

        //cms setting
        Route::get('/cms_setting', 'Admin\CmsSettingController@index');
        Route::post('/post_cms_setting', 'Admin\CmsSettingController@post_cms_setting');

        //status change
        Route::get('/change-status/{table}/{status}/{id}', 'Admin\UsersController@changeStatus');
        Route::get('/popular-status/{table}/{status}/{id}', 'Admin\UsersController@popularStatus');

        //Profile

        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
        Route::resource('/profile', 'Admin\ProfileController');

    });
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/change_password', 'ProfileController@change_password');
    Route::get('/profile/change_location', 'ProfileController@change_location');
});

Route::get('/db',function(){
    try {
        DB::connection()->getPdo();
        if(DB::connection()->getDatabaseName()){
            echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
        }
    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration.");
    }
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/cornLocation', function () {
    Artisan::call('Location:Location');
});

Route::get('/cornVenue', function () {
    Artisan::call('Venue:Venue');
});
Route::get('/cornShow', function () {
    Artisan::call('Show:Show');
});

Route::get('/CornKeyChange', function () {
    Artisan::call('CornKeyChange:CornKeyChange');
});

Route::get('/clear-all-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('clear-compiled');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:cache');
    return '<h1>Cache cleared</h1>';
});
