{{-- @if (isset($routedeatil) && $routedeatil == "theater")
                            <div class="left-box-blk clearfix">
                                <div class="showlist showlist-theater">
                                    <h3>CURRENT/UPCOMING SHOWS</h3>

                                    @if(isset($show) && count($show) > 0)
                                        <div class="listing-blk-div clearfix" id="show_list">
                                            <div class="row">

                                                @foreach($show as $showkey => $value)
                                                    @if($showkey == 3)
                                                        @break
                                                    @endif
                                                    @if(isset($value->show))
                                                        @php
                                                            $value = $value->show;
                                                        @endphp
                                                    @endif
                                                    <div class="col-lg-12 col-xl-12 col-12">
                                                        <a href="{{route('show-detail',[$value->slug])}}">
                                                            <div class="card">
                                                                <div class="card-img">
                                                                    <div class="thumb-img">
                                                                        <img src="{{$value->rectangle_image}}" alt="{{$value->showTitle}}/" title="{{$value->showTitle}}/" class="img-fluid">
                                                                    </div>
                                                                </div>
                                                                <div class="card-details lang-eng clearfix">

                                                                    <div class="card-name">
                                                                        @if(($value->singleOfferTitle) != '')
                                                                        <div class="card-singleOfferTitle">
                                                                            <h3 class="singleOfferTitle-txt">{{$value->singleOfferTitle}}</h3>
                                                                        </div>
                                                                        @endif

                                                                    @if ( strlen($value->showTitle) > 35)
                                                                    <h2>{{substr($value->showTitle , 0 , 35).'...'}}</h2>
                                                                    @else
                                                                    <h2>{{substr($value->showTitle , 0 , 35)}}</h2>
                                                                    @endif
                                                                    </div>
                                                                    <div class="card-price"><h3 class="price-txt">&pound;{{$value->priceFrom}} - &pound;{{$value->priceTo}}</h3></div>
                                                                    {
                                                                </div>

                                                                <div class="card-desc">
                                                                    <p>
                                                                        <?php
                                                                        // $string = strip_tags(html_entity_decode(html_entity_decode($value->summary)));
                                                                        // if(strlen($string) > 100)
                                                                        //     echo substr($string, 0, 100).'...';
                                                                        // else
                                                                        //     echo $string;
                                                                    ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>

                                                @endforeach

                                            </div>
                                        </div>
                                    @else
                                        <div>Show Not Found</div>
                                    @endif
                                </div>
                            </div>
                        @endif --}}