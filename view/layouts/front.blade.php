<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->

<head>
    @if( isset($route) && $route == "showDetail")
    <title> {{( isset($pageTitle) && $pageTitle != '') ? $pageTitle : config('app.name') }} </title>
    @elseif (isset($routedeatil) && $routedeatil == "theater")
    <title> {{( isset($pageTitle) && $pageTitle != '') ? $pageTitle : config('app.name') }} </title>
    @else
    <title> {{( isset($pageTitle) && $pageTitle != '') ? config('app.name').' | '.$pageTitle : config('app.name') }}
    </title>
    @endif
    <meta name="description" content="{!!strip_tags($metaDescription)!!}" />
    <meta name="keywords"
        content="{{( isset($metaTitle) && $metaTitle != '') ? $metaTitle.', '.config('app.name') : config('app.name') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
    <meta content='yes' name='apple-mobile-web-app-capable'>
    <meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>

    @include('include.front-end.css')
    <style type="text/css">
        .error {
            color: #ff0000
        }
    </style>
</head>

<body>

    <div id="wrapper">
        <div id="load">
            <div class="loader_root_div">
                <div class="loader_container">
                    <div class="dot-container">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                    <div class="dot-container">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                    <div class="dot-container">
                        <div class="dot"></div>
                        <div class="dot"></div>
                        <div class="dot"></div>
                    </div>
                </div>
            </div>
        </div>

        @include('include.front-end.header')

        <div class="main-content show-detail-page clearfix">
            @if( isset($route) && $route == "home")
            <div class="container">
                <div class="row">
                    @include('include.front-end.banner')
                    <div class="col-sm-12 col-xl-3 pull-left" style="order:1;">
                        <div class="white-box-blk bookform">
                            @include('include.front-end.bookform')
                        </div>
                    </div><!-- end of col -->
                </div><!-- end of row -->
            </div><!-- end of container -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if(isset($cms_setting->trust_factor) && $cms_setting->trust_factor_display != 0)
                        {!!html_entity_decode($cms_setting->trust_factor)!!}
                        @endif
                    </div>
                </div>
            </div>
            @include('include.front-end.latest-update')
            @endif


            @if( isset($route) && $route == "showDetail")
            @php
            $show = $shows[0];
            @endphp
            <div class="show-top-img-div">
                <div class="container ">
                    <div class="row mobile-display-none">
                        <div class="col-xl-12 col-sm-12">
                            <div class="show-top-content">

                                <h2>{{$show->showTitle}}</h2>
                                @if (isset($show->closedshow) && $show->closedshow == 1)
                                <span class="closedshow">Closed</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="show-top-img desk-img" style="background: url('{{ $banner }}') no-repeat center center;">
                </div>
            </div>
            @endif
            <section class="two-block-section clearfix">
                <div class="container">
                    <div class="row">

                        @if($route == 'news')
                        <div class="col-sm-12 col-xl-12">
                            <section class="general-box01 theature-div d-flex flex-column">
                                @yield('content')
                            </section><!-- end of listing-blk-section -->
                        </div><!-- end of col -->
                        @else
                        @if($route != 'showDetail')
                        <div class="col-sm-4 col-xl-3 pull-left">
                            @if(isset($route) && $route != "home")
                            <div class="white-box-blk mb20 ">
                                @include('include.front-end.bookform')
                            </div>
                            @endif

                            @include('include.front-end.popular_show')

                            @if(isset($route) && $route != "catgoryAll" && !isset($routedeatil))
                            <div class="left-box-blk clearfix">
                                @include('include.front-end.category_list')
                            </div><!-- end of left-box-blk -->
                            @endif

                            @if (isset($routedeatil) && $routedeatil == "theater")
                            <div class="left-box-blk clearfix">
                                <div class="showlist showlist-theater">
                                    <h3>SHOWS</h3>

                                    @if(isset($show) && count($show) > 0)
                                    <div class="listing-blk-div clearfix" id="show_list">
                                        <div class="row">

                                            @foreach($show as $showkey => $value)
                                            @if($showkey == 3)
                                            @break
                                            @endif
                                            @if(isset($value->show))
                                            @php
                                            $value = $value->show;
                                            @endphp
                                            @endif
                                            <div class="col-lg-12 col-xl-12 col-12">
                                                <a href="{{route('show-detail',[$value->slug])}}">
                                                    <div class="card">
                                                        <div class="card-img">
                                                            <div class="thumb-img">
                                                                <img src="{{$value->rectangle_image}}"
                                                                    alt="{{$value->showTitle}}/"
                                                                    title="{{$value->showTitle}}/" class="img-fluid">
                                                            </div>
                                                        </div>
                                                        <div class="card-details lang-eng clearfix">

                                                            <div class="card-name">
                                                                @if(($value->singleOfferTitle) != '')
                                                                <div class="card-singleOfferTitle">
                                                                    <h3 class="singleOfferTitle-txt">
                                                                        {{$value->singleOfferTitle}}</h3>
                                                                </div>
                                                                @endif

                                                                @if ( strlen($value->showTitle) > 35)
                                                                <h2>{{substr($value->showTitle , 0 , 35).'...'}}</h2>
                                                                @else
                                                                <h2>{{substr($value->showTitle , 0 , 35)}}</h2>
                                                                @endif
                                                            </div>
                                                            <div class="card-price">
                                                                <h3 class="price-txt">&pound;{{$value->priceFrom}} -
                                                                    &pound;{{$value->priceTo}}</h3>
                                                            </div>
                                                        </div>

                                                        <div class="card-desc">
                                                            <p>
                                                                <?php
                                                                            $string = strip_tags(html_entity_decode(html_entity_decode($value->summary)));
                                                                            if(strlen($string) > 100)
                                                                                echo substr($string, 0, 100).'...';
                                                                            else
                                                                                echo $string;
                                                                        ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>

                                            @endforeach

                                        </div>
                                    </div>
                                    @else
                                    <div>Show Not Found</div>
                                    @endif
                                </div>
                            </div>
                            @endif
                        </div>
                        @else
                        @php
                        $show = $shows[0];
                        @endphp
                        <div class="col-sm-4 col-xl-3 pull-left">


                            @if (isset($show->closedshow) && $show->closedshow != 1)
                            <div class="white-box-blk mb20 detail-booking-box ">
                                @include('include.front-end.bookform')
                            </div>
                            @endif

                            @if(@fopen($show->rectangle_image,'r'))
                            <div class="left-box-blk  clearfix">
                                <div class="show-poster-div">
                                    <a href="#">
                                        <img src="{{$show->rectangle_image}}" alt="{{$show->showTitle}}"
                                            class="img-fluid">
                                    </a>
                                </div>
                            </div><!-- end of left-box-blk -->

                            @endif
                            @if(isset($venue->venueTitle) || $venue->venueTitle != null || isset($show->bookingStarts)
                            ||
                            $show->bookingStarts != null || isset($show->bookingEnds) || $show->bookingEnds != null)
                            <div class="left-box-blk clearfix">
                                <h2>BOOKING PERIOD</h2>
                                <div class="white-box-blk p20 clearfix">
                                    <div class="booking-blk-div">
                                        @if ($show->bookingOpen != null)
                                        <p>
                                            <span class="cap-txt">SHOW OPENED:</span>
                                            <span
                                                class="normal-txt">{{date('M d Y', strtotime($show->bookingOpen))}}</span>
                                        </p>
                                        @endif

                                        <p>
                                            <span class="cap-txt">BOOKING FROM:</span>
                                            <span
                                                class="normal-txt">{{date('M d Y', strtotime($show->bookingStarts))}}</span>
                                        </p>

                                        <p>
                                            <span class="cap-txt">BOOKING UNTIL:</span>
                                            <span
                                                class="normal-txt">{{date('M d Y', strtotime($show->bookingEnds))}}</span>
                                        </p>
                                    </div>
                                </div>
                            </div><!-- end of left-box-blk -->
                            @endif

                            @if($show->runningTime != null || $show->ageRestriction != null)
                            <div class="left-box-blk clearfix">
                                <h2>Show Information</h2>
                                <div class="white-box-blk ptlr20 clearfix">
                                    <div class="show-info-blk-div">
                                        @if($show->runningTime != null)
                                        <p>
                                            <span class="glyph-icon flaticon-clock"></span>
                                            <span class="txt">{{$show->runningTime}}</span>
                                        </p>
                                        @endif
                                        @if($show->ageRestriction != null)
                                        <p>
                                            <span class="glyph-icon flaticon-child"></span>
                                            <span class="txt">{{strip_tags($show->ageRestriction)}}</span>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- end of left-box-blk -->
                            @endif

                            @if(isset($schedule) && $schedule != null)
                            <div class="left-box-blk heading-maroon  clearfix">
                                <h2>SHOW SCHEDULE</h2>
                                <div class="white-box-blk ptlr20 clearfix">
                                    <div class="show-sch-blk-div">

                                        @foreach ($schedule as $key => $value)
                                        <p>
                                            <span class="day-txt">{{substr($key, 0, 3)}}</span>

                                            @php

                                            $stringtime = '';
                                            if(count($value) > 0)
                                            {
                                            $seqArr = array();
                                            foreach ($value as $itemKey => $item)
                                            {
                                            if ($itemKey > 1) {
                                            break;
                                            }
                                            $time = date("h",strtotime($item));
                                            if(count($value) > 1){
                                            if ($time <='03' ) { echo '<span class="time-txt"> ' .((isset($item) &&
                                                $item !=null) ? date( 'h:i A' , strtotime($item)) : '-' ). '</span>' ; }
                                                if ($time>=
                                                '04') {
                                                echo '<span class="time-txt"> '.((isset($item) && $item != null) ?
                                                    date('h:i A',
                                                    strtotime($item)) : '-').'</span>';
                                                }

                                                }else{
                                                if ($time <= '03' ) { echo '<span class="time-txt">' .((isset($item) &&
                                                    $item !=null) ? date('h:i A', strtotime($item)) : '-' ).'</span>
                                                    <span class="time-txt">-</span>';
                                                    }

                                                    if ($time >= '04') {
                                                    echo '<span class="time-txt">-</span><span class="time-txt">
                                                        '.((isset($item) && $item != null) ? date('h:i A',
                                                        strtotime($item)) :
                                                        '-').'</span>';
                                                    }
                                                    }
                                                    }
                                                    }
                                                    else {
                                                    echo '<span class="time-txt">-</span><span
                                                        class="time-txt">-</span>';
                                                    }

                                                    @endphp
                                        </p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            @endif


                            <div class="left-box-blk clearfix">
                                <h3>SEATING PLAN</h3>
                                <div class="white-box-blk p20 clearfix">

                                    <div class="playing-blk-div">
                                        <h3> <a href="{{route('single-theater',[@$venue->slug])}}">
                                                {{@$venue->venueTitle}} </a>
                                        </h3>
                                        @if($venue != null && $venue->venue_seating != null)
                                        <ul>
                                            <li>

                                                <div class="seatplan-img-div">
                                                    <a id="single_image" href="{{$venue->venue_seating}}"
                                                        target="_blank" class="seatplan-download" title="Seating Plan"
                                                        download=""><img src={{$venue->venue_seating}}
                                                            class="seatplan-img">
                                                        <p class="txt-center">Click to enlarge</p>
                                                    </a>

                                                </div>
                                            </li>
                                        </ul>
                                        @else
                                        <p>No Seating Plan Available at this time</p>
                                        @endif
                                    </div>

                                </div>
                            </div>
                            @if($showCategory != null && count($showCategory) > 0)
                            <div class="left-box-blk clearfix mobile-display-none">
                                <h3>Categories</h3>
                                <div class="white-box-blk p20 clearfix">

                                    <div class="playing-blk-div">
                                        @foreach ($showCategory as $item)
                                        <p>{{$item}}</p>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                            @endif

                        </div>
                        @endif
                        {{-- @include('layouts.flash-message') --}}
                        <div class="col-sm-8 col-xl-9 ">
                            <section class="general-box01 theature-div d-flex flex-column">
                                @yield('content')
                            </section><!-- end of listing-blk-section -->
                        </div><!-- end of col -->
                        @endif

                    </div>
                </div>

            </section><!-- end fo home-banner-div -->
        </div><!-- end of main-content -->
        @if (isset($footer_display) && $footer_display == 1)
        @include('include.front-end.footer')
        @endif


    </div><!-- end of wrapper -->

    @include('include.front-end.js')
    <script type="text/javascript" src="https://inventory-service.tixuk.io/js/quicksearch.js"></script>
    <script>
        $(document).load(function () {
        var imgurl = $(".imgurl");
        imgurl.each(function () {
            showid = $(this).attr('showid');
            url = getImageofShow(showid, $(this));
            // console.log(url);
            // $(this).attr('src',url);
        });

            function getImageofShow(showid, element) {
                var url = "{{ route('showImageGet',':showid') }}";
                url = url.replace(':showid',showid);
                jQuery.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        if (response.url != null) {
                            element.attr('src', response.url);
                        }
                        else {
                            element.attr('src', "{{url('')}}/frontend/images/no_image.png");
                        }
                    },
                    error: function (xhr, status, error) {
                        element.attr('src', "{{url('')}}/frontend/images/no_image.png");
                    }
                });
            }
        });
    </script>
</body>

</html>