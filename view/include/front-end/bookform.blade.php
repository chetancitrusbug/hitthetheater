<div class="book-ticket-blk-div clearfix">

    <h2>BOOK Theatre Tickets</h2>
    <style>
        .gr-col {
            max-width: 100% !important;
            flex-basis: 100% !important;
            margin-bottom: 5px !important;
        }

        .gr-col--quantity,
        .gr-col--calenda,
        .gr-col--time,
        .gr-col--block-calendar {
            width: 100% !important;
            flex-basis: 100% !important;
            margin-bottom: 5px !important;
        }
    </style>
    <div class="left_side_ticket">
        @if($route == 'showDetail')
        @php
        $showLinkExplode = explode('/',$shows[0]->showLink);
        @endphp
        <div data-app="inventory-app" class="inventory__c-quick-search"
            data-action-url="https://hitthetheatre.tixuk.com/booking/seating-plan" data-affiliate-id="hitthetheatrep"
            data-with-calendar="true" data-product-id="<?php echo $showLinkExplode[1]; ?>"
            data-product-type="show|attraction" data-venue-id="199" data-api-path="https://inventory-service.tixuk.io"
            data-booking-ends="2019-09-29" data-booking-starts="2019-03-19"></div>
        @else
        <div data-app="inventory-app" class="inventory__c-quick-search search"
            data-action-url="https://hitthetheatre.tixuk.com/booking/seating-plan" data-affiliate-id="hitthetheatrep"
            data-with-calendar="true" data-api-path="https://inventory-service.tixuk.io" data-labels-config='{
					"searchFieldPlaceholder": "Select a show...",
					"singleTicketLabel": "ticket",
					"multipleTicketsLabel": "tickets",
					"datePlaceholder": "Pick a date",
					"timePlaceholder": "Choose a time",
					"searchButton": "Find Tickets!"
					}'></div>
        @endif

    </div>
    <script type="text/javascript" src="https://inventory-service.tixuk.io/v1/js/quicksearch.js"></script>



</div><!-- end of book-ticket-blk-div -->