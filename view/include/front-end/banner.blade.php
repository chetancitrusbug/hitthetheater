@if(isset($banner) && isset($bannerForFrontendSetting))
<div class="col-sm-12 col-xl-9 pull-right" style="order:2;">
    <div class="home-banner-div clearfix">
        <div class="home-banner owl-carousel owl-theme">

            @foreach($banner as $value)
            @if($bannerForFrontendSetting == 0)
            <div class="item"><img src="{{$value->url}} " alt=""></div>
            @else
            @if($value->banner != null)
            <div class="item">
                <a href={{($value->bannerLink != '') ? $value->bannerLink : url('')}}><img src={{  $value->banner }}
                        alt=""></a>
                <div class="banner_content">{!! $value->content !!}</div>
            </div>
            @endif
            @endif
            @endforeach

        </div>
    </div><!-- end fo home-banner-div -->
</div><!-- end of col -->
@endif