@if(isset($popularShow) && count($popularShow) > 0)
<div class="left-box-blk clearfix mobile-display-block">
    <h2>Most Popular Shows</h2>
    <div class="white-box-blk clearfix">
        <ul>
            @foreach($popularShow as $show)

            <li>
                <a href="{{route('show-detail',[$show->slug])}}">
                    <span class="title-txt">{{$show->venueTitle}} - {{$show->showTitle}}</span>
                    <span class="price-txt">&pound;{{$show->priceFrom}} - &pound;{{$show->priceTo}}</span>
                </a>
            </li>

            @endforeach
        </ul>
    </div>
    {{-- <div class="link-more-div float-right"><a href="#" class="blue-link">View All</a></div> --}}
</div>
<!-- end of left-box-blk -->
@endif