<script src="{{asset('frontend/js/modernizr.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('frontend/plugin/slider_lightbox/js/jquery.bxslider.min.js')}}"></script>
<script src="{{asset('frontend/plugin/slider_lightbox/js/jquery.fancybox.min.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.js"></script>
<script>
    $(document).ready(function() {
                sortList();
              $("#bookTicket").validate({
                    // Specify validation rules
                    rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                      selectshow: { required:true, },
                      tickets: { required:true, }
                    },
                    // Specify validation error messages
                    messages: {
                      selectshow: { required:"Please Select a show",},
                      tickets: { required: "Please Select a show ticket",},
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                      form.submit();
                    }
                });
              $('.home-banner').owlCarousel({
                loop: true,
								item:1,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
					loop: true,
                    nav: false
                  },
                  600: {
                    items: 1,autoHeight: true,autoHeightClass: 'owl-height',
					loop: true,
                    nav: false
                  },
                  1000: {
                    items: 1,
                    nav: false,
                    loop: true,
                    margin: 0
                  }
                }
              });

              $('.inner-img-banner').owlCarousel({
                loop: true,
								item:1,
                responsiveClass: true,
                dots:true,autoHeight: true,autoHeightClass: 'owl-height',
                responsive: {
                  0: {
                    items: 1,
					loop: true,
                    nav: false,
                    dots:true
                  },
                  600: {
                    items: 2,
					loop: true,
                    nav: false, dots:true
                  },
                  1000: {
                    items: 2,
                    nav: false,
                    loop: true,
                    margin: 0, dots:true
                  }
                }
              })


			  $('.listing-link').click(function(){
				  $('.targetDiv').hide();
				  $('.listing-link').removeClass('active');
				 $('#div'+$(this).attr('target')).show();
				  $(this).addClass('active');


				  //var divId = "#div" + $(this).attr("id").replace("showdiv", "");
				  //$(".targetDiv").hide();
				 // $(divId).show();


			});

			    $('#globalSearch').keyup(function() {
					$.ajax({
                        url:"{{route('search')}}",
                        type: 'get',
                        data : {search:$(this).val()},

                        success:function(response){
                            if(response.success){
								$("#searchResult").html(response.html)
							}else{
								$("#searchResult").html('')
							}

                        },

                        error:function(xhr, status, error){
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err);
							$("#load").hide();
                        }
                    });
				});

                $('#venue_category_filter').keyup(function() {
                    venue_category_filter();
                });


            });

            function venue_category_filter() {
                $("#loading").show();
                $('.pagination-div-none').hide();
					$.ajax({
                        url:"{{route('venue_category_filter')}}",
                        type: 'get',
                        data : {search:$('#venue_category_filter').val()},

                        success:function(response){
                            $("#loading").hide();
                            if(response.success){
                                $("#venue_list_html").html(response.html);
							}else{
                                $("#venue_list_html").html(response.html);
                                $('.pagination-div-none').show();
							}

                        },

                        error:function(xhr, status, error){
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err);
							$("#load").hide();
                        }
                    });
            }
            function category_filter() {
                var input, filter, ul, li, a, i, txtValue;
                input = document.getElementById("category_filter");
                filter = input.value.toUpperCase();
                ul = document.getElementById("category_filter_id");
                li = ul.getElementsByTagName("li");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

            function sortList() {
                var list, i, switching, b, shouldSwitch;
                list = document.getElementById("category_filter_id");
                switching = true;
                /* Make a loop that will continue until
                no switching has been done: */
                while (switching) {
                    // start by saying: no switching is done:
                    switching = false;
                    if (list != null) {
                    b = list.getElementsByTagName("li");

                    // Loop through all list-items:
                        for (i = 1; i < (b.length - 1); i++) {
                        // start by saying there should be no switching:
                        shouldSwitch = false;
                        /* check if the next item should
                        switch place with the current item: */
                        if (b[i].getElementsByTagName("a")[0].innerHTML.toLowerCase() > b[i + 1].getElementsByTagName("a")[0].innerHTML.toLowerCase()) {
                            /* if next item is alphabetically
                            lower than current item, mark as a switch
                            and break the loop: */
                            shouldSwitch = true;
                            break;
                        }
                        }
                    }
                    if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark the switch as done: */
                    b[i].parentNode.insertBefore(b[i + 1], b[i]);
                    switching = true;
                    }
                }
            }

            function usortList() {
                var list, i, switching, b, shouldSwitch;
                list = document.getElementById("category_filter_id");
                switching = true;
                /* Make a loop that will continue until
                no switching has been done: */
                while (switching) {
                    // start by saying: no switching is done:
                    switching = false;
                    b = list.getElementsByTagName("LI");
                    // Loop through all list-items:
                    for (i = 1; i < (b.length - 1); i++) {
                    // start by saying there should be no switching:
                    shouldSwitch = false;
                    /* check if the next item should
                    switch place with the current item: */
                    if (b[i].getElementsByTagName("a")[0].innerHTML.toLowerCase() < b[i + 1].getElementsByTagName("a")[0].innerHTML.toLowerCase()) {
                        /* if next item is alphabetically
                        lower than current item, mark as a switch
                        and break the loop: */
                        shouldSwitch = true;
                        break;
                    }
                    }
                    if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark the switch as done: */
                    b[i].parentNode.insertBefore(b[i + 1], b[i]);
                    switching = true;
                    }
                }
            }

            $("#sort").show();
            $("#unsort").hide();
            $("#sort").on("click", function(){
            $("#sort").hide();
            $("#unsort").show();
            });
            $("#unsort").on("click", function(){
            $("#sort").show();
            $("#unsort").hide();
            });


</script>

<script type="text/javascript" src="{{asset('frontend/js/custom.js')}}"></script>
@yield('js')