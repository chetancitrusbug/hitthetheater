<header>
    <div class="header-div">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="logo-div">
                        <a href="{{url('')}}">
                            @if(isset($cms_setting->logo) && $cms_setting->logo)
                            @php
                            $path=public_path().'/storage/cms'.$cms_setting->logo;
                            if(file_exists($path))
                            {
                            $path=url('').'/storage/cms'.$cms_setting->logo;
                            echo '<img class="brand-logo" alt="Hit The Theatre" src='.$path.' width="100px">';
                            }
                            @endphp
                            @else
                            <img class="brand-logo" alt="Hit The Theatre" src="{{asset('frontend/images/logo.png')}}">
                            @endif
                        </a>
                    </div><!-- end of logo-div -->

                    <div class="menu-bar-div"><a href="#" onclick="openNav()" class="" data-placement="bottom" title=""
                            data-original-title="Menu"><i class="menu-bars"></i></a></div>

                    <div class="top-right-blk-div">
                        <div class="clearfix">
                            <div class="top-right-div m-top-right-div" style="order:2">
                                <!--<div class="log-reg-div">
                                                <a href="{{url('/maintenance')}}" class="blue-btn">Sign In</a>
                                            </div> -->
                                <div class="top-phone">Call Us: <strong>
                                        <a class="a_tag_text"
                                            href="tel:{{isset($cms_setting->contact_us_phone)?is_null($cms_setting->contact_us_phone)?null:$cms_setting->contact_us_phone:null}}">
                                            {{isset($cms_setting->contact_us_phone)?is_null($cms_setting->contact_us_phone)?null:$cms_setting->contact_us_phone:null}}
                                        </a>
                                    </strong></div>
                            </div><!-- end of top-right-div -->

                            <div class="top-left-blk  m-top-left-div" style="order:1">
                                <div class="top-search-div clearfix">
                                    <div class="search-div">
                                        <button class="search-i"><i class="fa fa-search"></i></button>
                                        <input type="text" id="globalSearch"
                                            placeholder="Search for Show name, Musical, Play, Theatre name, or by Category"
                                            class="search-input">
                                    </div>
                                    <div id="searchResult">
                                    </div>
                                </div><!-- end of top-search-div -->
                            </div><!-- end of top-left-blk -->

                        </div><!-- end of clearfix-->

                        <div id="mySidenav" class="sidenav">
                            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

                            {{-- <div class="m-log-reg-div">
                                        <a href="{{url('/maintenance')}}" class="blue-btn">Sign In</a>
                            <div class="welcome-txt">Welcome to <a href="{{url('/maintenance')}}">User name</a></div>
                        </div> --}}

                        <div class="clearfix">
                            <div class="top-left-blk">
                                <div class="nav-div clearfix">
                                    @if(isset($headercategory) && count($headercategory) > 0)
                                    <ul class="nav">
                                        @foreach ($headercategory as $item)
                                        @if(isset($item) && $item != null)
                                        @php
                                        $selected = '';
                                        if (isset($categorySingle->id) && $categorySingle->id != null) {
                                        if ($item->id == $categorySingle->id) {
                                        $selected = 'active';
                                        }
                                        }
                                        @endphp
                                        <li><a href="{{url('/category').'/'.$item->slug}}" class="{{$selected}}">
                                                {{-- @if($item->categoryTitle == 'Family & Kids')
                                                                    Family
                                                                @elseif($item->categoryTitle == 'Opera')
                                                                    Opera & Dance
                                                                @elseif($item->categoryTitle == 'Telegraph Tickets Exclusives & Special Offers')
                                                                    Special
                                                                @else --}}
                                                {{$item->categoryTitle}}
                                                {{-- @endif --}}
                                            </a></li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    @endif
                                </div>
                            </div><!-- end of top-left-blk -->
                            <div class="top-right-div">
                                <div class="nav-right-div clearfix">
                                    <ul class="nav">
                                        <li><a href="{{url('/theater')}}">Theatres</a></li>
                                        <li><a href="{{url('/news')}}">News</a></li>
                                        {{-- <li class="m-logout"><a href="{{url('/maintenance')}}">Logout</a></li> --}}
                                    </ul>
                                </div>

                            </div><!-- end of top-right-div -->
                        </div><!-- end of clearfix-->
                    </div><!-- end of sidenav -->

                </div><!-- end of top-right-blk-div -->


            </div>
        </div>
    </div>
    </div>

</header>