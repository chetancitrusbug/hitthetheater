@if(isset($route) && $route == "theater")
{{-- <h2>Venues Categories
    <div class="cat-short">
        <span onclick="sortList()" id="sort">A-Z <i class="fa fa-sort"></i></span><span id="unsort" onclick="usortList()">Z-A <i class="fa fa-sort" ></i></span></div>
</h2>
    <div class="white-box-blk catList clearfix">
        <input type="text" class="form-control" id="category_filter" onkeyup="category_filter()" placeholder="Search for category.." title="">
        <button type="button" class="btn btn-category" data-toggle="collapse" data-target="#category_filter_id" aria-expanded="false"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
        <ul id="category_filter_id">
            @if(isset($category))
                @foreach($VenueCategories as $value)
                @if($value->venue_count > 0)
                    @php
                        $selected = '';
                        if (isset($categorySingle->id) && $categorySingle->id != null) {
                            if ($value->id == $categorySingle->id) {
                                $selected = 'active';
                            }
                        }
                    @endphp
                        <li><a href="{{route('single-category-theater',[$value->slug])}}"
class="{{$selected}}">{{$value->categoryTitle}}({{$value->venue_count}})</a></li>
@endif
@endforeach
@endif
</ul>
</div> --}}
@else
<h2>Shows Categories
    <div class="cat-short">
        <span onclick="sortList()" id="sort">A-Z <i class="fa fa-sort"></i></span><span id="unsort"
            onclick="usortList()">Z-A <i class="fa fa-sort"></i></span></div>
</h2>
<div class="white-box-blk catList clearfix">
    <input type="text" class="form-control" id="category_filter" onkeyup="category_filter()"
        placeholder="Search for category.." title="">

    <button type="button" class="btn btn-category" data-toggle="collapse" data-target="#category_filter_id"
        aria-expanded="false"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
    <ul id="category_filter_id">
        @if(isset($category))
        <li><a href="{{route('category')}}"> All </a></li>
        @foreach($category as $value)
        @if($value->show_count > 0)
        @php
        $selected = '';
        if (isset($categorySingle->id) && $categorySingle->id != null) {
        if ($value->id == $categorySingle->id) {
        $selected = 'active';
        }
        }
        @endphp
        <li><a href="{{route('single-category',[$value->slug])}}"
                class="{{$selected}}">{{$value->categoryTitle}}({{$value->show_count}})</a></li>
        @endif
        @endforeach @endif
    </ul>
</div>
@endif