<!--Default Alerts-->
@if (Session::has('flash_success'))
    <div class="alert alert-icon-right alert-success alert-dismissible mb-2" role="alert">
        <span class="alert-icon"><i class="fa fa-info"></i></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>{{ Session::get('flash_success') }}</strong>
    </div>
@endif

@if (Session::has('flash_message'))
    <div class="alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
        <span class="alert-icon"><i class="fa fa-info"></i></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        <strong>{{ Session::get('flash_message') }}</strong>
    </div>

@endif

@if (Session::has('flash_warning'))
    <div class="alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
        <span class="alert-icon"><i class="fa fa-info"></i></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
        <strong>{{ Session::get('flash_warning') }}</strong>
    </div>
@endif

@if (Session::has('flash_error'))
    <div class="alert alert-icon-right alert-danger alert-dismissible mb-2" role="alert">
        <span class="alert-icon"><i class="fa fa-info"></i></span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
        <strong>{{ Session::get('flash_error') }}</strong>
    </div>
@endif

@if (Session::has('status'))
<div class="alert alert-icon-right alert-info alert-dismissible mb-2" role="alert">
    <span class="alert-icon"><i class="fa fa-info"></i></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
    <strong>{{ Session::get('status') }}</strong>
</div>
@endif

<div class="alert alert-icon-right alert-success alert-dismissible mb-2 messagevalalert" role="alert">
    <span class="alert-icon"><i class="fa fa-info"></i></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    <strong class="messageval"></strong>
</div>