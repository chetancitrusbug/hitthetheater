<div class="header-navbar navbar-expand-sm navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border"
    role="navigation" data-menu="menu-wrapper">
    <!-- Horizontal menu content-->
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include ../../../includes/mixins-->
        <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">

            <li  class="nav-item {{($route == 'dashboard')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/dashboard') }}"><i class="ft-home"></i><span>Dashboard</span></a>
            </li>

            <li  class="nav-item {{($route == 'users')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/users') }}"><i class="ft-users"></i><span>Users</span></a>
            </li>

            <li  class="nav-item {{($route == 'location')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/location') }}"><i class="ft-navigation"></i><span>Location</span></a>
            </li>


            <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="ft-tag"></i><span>Category</span></a>
                <ul class="dropdown-menu">
                    <li class="{{($route == 'category')?'active':''}}" data-menu="" data-toggle="dropdown"><a class="dropdown-item" href="{{ url('admin/category') }}" data-toggle="dropdown">Show Category</a>
                    </li>
                    <li class="{{($route == 'venuecategory')?'active':''}}" data-menu="" data-toggle="dropdown"><a class="dropdown-item" href="{{ url('admin/venuecategory') }}" data-toggle="dropdown">Venue Category</a>
                    </li>
                </ul>
            </li>


            <li  class="nav-item {{($route == 'venue')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/venue') }}"><i class="ft-map-pin"></i><span>Venue</span></a>
            </li>

            <li  class="nav-item {{($route == 'show')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/show') }}"><i class="ft-film"></i><span>Show</span></a>
            </li>

            <li  class="nav-item {{($route == 'setting')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/setting') }}"><i class="ft-settings"></i><span>Setting</span></a>
            </li>

            <li  class="nav-item {{($route == 'cms_setting')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/cms_setting') }}"><i class="ft-book"></i><span>CMS Setting</span></a>
            </li>

            <li  class="nav-item {{($route == 'contactus')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/contactus') }}"><i class="ft-clipboard"></i><span>Contact Us</span></a>
            </li>

            <li  class="nav-item {{($route == 'banner')?'active':''}}"><a class="dropdown-toggle nav-link"   href="{{ url('admin/banner') }}"><i class="ft-maximize"></i><span>Banner</span></a>
            </li>

            <li class="nav-item {{($route == 'news')?'active':''}}"><a class="dropdown-toggle nav-link"
                    href="{{ url('admin/news') }}"><i class="ft-layers"></i><span>News</span></a>
            </li>

            <li class="nav-item"><a class="dropdown-toggle nav-link" href="{{ url('') }}" target="_blank"><i class="ft-tablet"></i><span>View Site</span></a>
            </li>


        </ul>
    </div>
    <!-- /horizontal menu content-->
</div>