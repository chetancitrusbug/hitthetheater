@extends('layouts.front')

@section('content')
<section class="about-txt-div">
    <h4>Terms And Condition</h4>
    @if(isset($cms_setting->termandcondition))
    @if($cms_setting->termandcondition == null)

    @else
    {!!html_entity_decode($cms_setting->termandcondition)!!}
    @endif
    @else

    @endif

</section>

@endsection