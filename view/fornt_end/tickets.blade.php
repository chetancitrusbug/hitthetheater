<!doctype html>
<html lang="en">

<head>
    <title>Quick search example</title>
</head>

<body>
    <div style="margin:20px;padding:10px">
        <h3 style="width:10px">With Search </h3>

        <div data-app="inventory-app" class="inventory__c-quick-search search" style="border:1px solid #0f0f0f"
            data-action-url="https://hitthetheatre.tixuk.com/booking/seating-plan" data-affiliate-id="hitthetheatrep"
            data-with-calendar="true" data-api-path="https://inventory-service.tixuk.io"></div>

    </div>
    <hr>
    <div style="border:1px solid #0f0f0f">
        <h3>Ticket With selected show in Detail page </h3>


        <div data-app="inventory-app" class="inventory__c-quick-search"
            data-action-url="https://hitthetheatre.tixuk.com/booking/seating-plan" data-affiliate-id="hitthetheatrep"
            data-with-calendar="true" data-product-id="1001" data-product-type="show|attraction" data-venue-id="199"
            data-api-path="https://inventory-service.tixuk.io" data-booking-ends="2019-09-29"
            data-booking-starts="2019-03-19"></div>

    </div>

    <!-- js -->
    <script type="text/javascript" src="https://inventory-service.tixuk.io/js/quicksearch.js"></script>
</body>

</html>