@if(isset($venues) && count($venues) > 0)

<div class="latest-update-blk">

    <div class="row">
        @foreach($venues as $key => $venue)
        <div class=" col-6 col-sm-12 col-xl-6 grid-06 float-left">
            <div class="latest-update-blk-div clearfix">
                <div class="row">
                    <div class="col-xl-5 col-sm-4 grid-05">
                        <a href="{{route('single-theater',[$venue->slug])}}">
                            <figure>
                                <img src="{{$venue->venue_image}}" alt="{{@$venue->venueTitle}}"
                                    title="{{@$venue->venueTitle}}" class="img-fluid width-100">
                            </figure>
                        </a>
                    </div>
                    <div class="col-xl-7 col-sm-7 grid-07 general-grid7">
                        <a href="{{route('single-theater',[$venue->slug])}}">
                            <h3>{{$venue->venueTitle}}</h3>

                        </a>
                        {{--
                        <p><a href="#">Kinky Boots</a></p>
                        <p><a href="#">The Roy Orbison Story</a></p>
                        <p><a href="#">Waitress</a></p>
                        <p><a href="#">Waitress</a></p> --}} @if($venue->addressLine2 != null &&$venue->latitude !=
                        null &&$venue->longitude
                        != null)
                        <p><a data-toggle="modal" data-target="#mapmodel{{$venue->id}}" href="#"><i
                                    class="fa fa-map-marker right-p2" aria-hidden="true"></i> {!!
                                strip_tags($venue->addressLine2) !!}</a></p>

                        @endif

                    </div>
                </div>
            </div>
            <!-- end of latest-update-blk-div  -->
        </div>
        <!-- end of col -->
        <div class="modal fade" id="mapmodel{{$venue->id}}" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Map</h4>
                    </div>
                    <div class="modal-body">
                        <iframe
                            src="https://maps.google.com/maps?q={{@$venue->latitude}},{{@$venue->longitude}}&hl=es;zoom=25&amp;output=embed"
                            width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                            allowfullscreen></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        @endforeach
    </div>
    <!-- end of col -->
</div>
@else
<div class="record_not_found">Theatre Not Found</div>
@endif