<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{url('/')}}</loc>
    </sitemap>
    @if(count($show) > 0)
    @php $route = 'show-detail'; @endphp
    @foreach ($show as $item)
    <sitemap>
        @if ($route != '')
        <loc>{{route($route,[$item->slug])}}</loc>
        @else
        <loc>{{url($item->slug)}}</loc>
        @endif
    </sitemap>
    @endforeach
    @endif

    @if(count($venue) > 0)
    <sitemap>
        <loc>{{url('theater')}}</loc>
    </sitemap>
    @php $route = 'single-theater'; @endphp
    @foreach ($venue as $item)
    <sitemap>
        @if ($route != '')
        <loc>{{route($route,[$item->slug])}}</loc>
        @else
        <loc>{{url($item->slug)}}</loc>
        @endif
    </sitemap>
    @endforeach
    @endif

    @if(count($category) > 0)
    @php $route = 'single-category'; @endphp
    @foreach ($category as $item)
    <sitemap>
        @if ($route != '')
        <loc>{{route($route,[$item->slug])}}</loc>
        @else
        <loc>{{url($item->slug)}}</loc>
        @endif
    </sitemap>
    @endforeach
    @endif

    @if(count($venueCategories) > 0)
    @php $route = 'single-category-theater'; @endphp
    @foreach ($venueCategories as $item)
    <sitemap>
        @if ($route != '')
        <loc>{{route($route,[$item->slug])}}</loc>
        @else
        <loc>{{url($item->slug)}}</loc>
        @endif
    </sitemap>
    @endforeach
    @endif

</sitemapindex>