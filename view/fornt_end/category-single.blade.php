@extends('layouts.front')

@section('content')

<section class="listing-blk-section clearfix">
    <div class="listing-blk-links-div clearfix">
        <div class="listing-blk-links pull-left clearfix" id="listing-link">
            <div class="media-link media-link-facilities border_bottom">
                <h2><span
                        class="single-category-title single-category-title-unset-pointer">{{isset($categorySingle->categoryTitle) ? $categorySingle->categoryTitle : ''}}</span>
                </h2>
            </div>
        </div>
        <!--<div class="listing-blk-links pull-right clearfix">
            <div class="link-div"><a href="#" class="blue-link">View All</a></div>
        </div> -->
    </div><!-- end of listing-blk-links-div -->

    <div class="listing-blk-div clearfix" id="show_list">
        @include('fornt_end.show_list')
    </div>

</section>

@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {


            });

</script>
@endsection