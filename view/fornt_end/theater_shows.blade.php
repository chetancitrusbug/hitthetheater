@extends('layouts.front')

@section('content')
@if($venue != null)
<div class="tab-label1 category_div clearfix">
    <div class="left-box-blk clearfix">

        <div class="playing-blk-div border_bottom media-link-facilities venutitle venutitle-img"
            style="background-image:url({{@$venue->venue_detail_banner}})">
            <h2 class="venue-address"><span>{{@$venue->venueTitle}}</span></h2>
            <p><i class="fa fa-map-marker"></i> {{@$venue->addressLine1}},
                {{ (isset($venue->addressLine2) && $venue->addressLine2 != '') ? $venue->addressLine2.',' : '' }}
                {{@$venue->postcode}}</p>
        </div>
        <div class="white-box-blk p20 clearfix">
            {{-- <h3 class="venue-dtl"><span>Venue Details<span></h3> --}}

            {{-- <div class="venue-img-section">
                        <div class="col-md-6">
                            <div class="field field-name-field-image field-type-image field-label-hidden">
                                <div class="field-items">
                                    <div class="field-item even"><img src="{{@$venue->venue_image}}" width="450"
            height="350" alt="{{@$venue->venueTitle}}" title="{{@$venue->venueTitle}}"></div>
    </div>
</div>
</div>
<div class="col-md-6">
    <div class="col-sm-12 venue-address">
        <h1 class="venue-address">{{@$venue->venueTitle}}</h1>

        <div>
            <div class="field field-name-field-venue-api-address field-type-text-long field-label-inline clearfix">
                <div class="field-label"><span>Address:</span></div>
                <div class="field-items">
                    <div class="field-item even">{{@$venue->addressLine1}}, {{@$venue->addressLine2}},
                        {{@$venue->postcode}}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
</div> --}}



@if($venue->venueDescription != null)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>About the</span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        {!!($venue->venueDescription)!!}
    </div>
</div>
@endif

@if($venue != null && $venue->venue_seating != null)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>Seating Plan</span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        <div class="seatplan-img-div">
            <a id="single_image" href="{{$venue->venue_seating}}" class="seatplan-download"
                title="Download Seating Plan" target="blank"><img src={{$venue->venue_seating}} class="seatplan-img">
            </a>
        </div>
    </div>
</div>
@endif

@if(isset($venue->latitude) && isset($venue->latitude) && $venue->latitude != null && $venue->longitude != null)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>Map</span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        <iframe
            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDWgvWPNqIMlrW0_oV6uCYPnJMWfgZWPyA
                                    &q={{@$venue->venueTitle}},{{@$venue->addressLine1}},{{@$venue->addressLine2}},{{@$venue->postcode}}"
            width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
            allowfullscreen></iframe>
    </div>
</div>
@endif

@if($venueCategory != null && count($venueCategory) > 0)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>Categories </span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        <div class="field-items div-faciliti-item">
            @foreach ($venueCategory as $item)
            <div class="field-item even">{{$item}}</div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if($venueFacilities != null && count($venueFacilities) > 0)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>Facilities </span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        <div class="field-items div-faciliti-item">
            @foreach ($venueFacilities as $item)
            <div class="field-item even">{{$item->facilitieTitle}}</div>
            @endforeach
        </div>
    </div>
</div>
{{-- <li class=""><span class="glyph-icon flaticon-cinema-chair"></span>Facilities

                            <div class="seatplan-img-div">
                                @foreach ($venueFacilities as $item)
                                <span class="facilitieTitle">{{$item->facilitieTitle}}</span> @endforeach

</div>
</li> --}}
@endif
{{-- @php dd(count($venueTransportInfo));@endphp --}}
@if(isset($venueTransportInfo) && $venueTransportInfo != null && count($venueTransportInfo) > 0)
<div class="media-link media-link-facilities border_bottom">
    <h2><span>Transport Info </span></h2>
    <div
        class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
        <div class="field-items div-faciliti-item">
            @foreach ($venueTransportInfo as $item)
            <div class="field-item even">{!! strip_tags($item->transportValue) !!}</div>
            @endforeach
        </div>
    </div>
</div>
@endif

@if(isset($showsCurrent) && count($showsCurrent) > 0)
<div class="media-link media-link-facilities ">
    <h2><span>Current Shows </span></h2>
    <div class="left-box-blk clearfix">
        <div class="showlist showlist-theater">
            <div class="listing-blk-div clearfix" id="show_list">
                <div class="row">

                    @foreach($showsCurrent as $showkey => $value)

                    @if(isset($value->show))
                    @php
                    $value = $value->show;
                    @endphp
                    @endif
                    <div class="col-lg-4 col-xl-4 col-6">
                        <a href="{{route('show-detail',[$value->slug])}}">
                            <div class="card">
                                <div class="card-img">
                                    <div class="thumb-img">
                                        <img src="{{$value->rectangle_image}}" alt="{{$value->showTitle}}/"
                                            title="{{$value->showTitle}}/" class="img-fluid">
                                    </div>
                                </div><!-- end of card img -->
                                <div class="card-details lang-eng clearfix">

                                    <div class="card-name">
                                        @if(($value->singleOfferTitle) != '')
                                        <div class="card-singleOfferTitle">
                                            <h3 class="singleOfferTitle-txt">{{$value->singleOfferTitle}}</h3>
                                        </div>
                                        @endif
                                        @if ( strlen($value->showTitle) > 35)
                                        <h2>{{substr($value->showTitle , 0 , 35).'...'}}</h2>
                                        @else
                                        <h2>{{substr($value->showTitle , 0 , 35)}}</h2>
                                        @endif
                                    </div>
                                    <div class="card-price">
                                        <h3 class="price-txt">&pound;{{$value->priceFrom}} - &pound;{{$value->priceTo}}
                                        </h3>
                                    </div>
                                </div>

                                <div class="card-desc">
                                    <p><?php
                                                                    $string = strip_tags(html_entity_decode(html_entity_decode($value->summary)));
                                                                    if(strlen($string) > 100)
                                                                        echo substr($string, 0, 100).'...';
                                                                    else
                                                                        echo $string;
                                                                ?></p>
                                </div>
                            </div><!-- end of card-->
                        </a>
                    </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($showsUpcoming) && count($showsUpcoming) > 0)
<div class="media-link media-link-facilities ">
    <h2><span>Upcoming Shows </span></h2>
    <div class="left-box-blk clearfix">
        <div class="showlist showlist-theater">
            <div class="listing-blk-div clearfix" id="show_list">
                <div class="row">

                    @foreach($showsUpcoming as $showkey => $value)

                    @if(isset($value->show))
                    @php
                    $value = $value->show;
                    @endphp
                    @endif
                    <div class="col-lg-4 col-xl-4 col-6">
                        <a href="{{route('show-detail',[$value->slug])}}">
                            <div class="card">
                                <div class="card-img">
                                    <div class="thumb-img">
                                        <img src="{{$value->rectangle_image}}" alt="{{$value->showTitle}}/"
                                            title="{{$value->showTitle}}/" class="img-fluid">
                                    </div>
                                </div><!-- end of card img -->
                                <div class="card-details lang-eng clearfix">

                                    <div class="card-name">
                                        @if(($value->singleOfferTitle) != '')
                                        <div class="card-singleOfferTitle">
                                            <h3 class="singleOfferTitle-txt">{{$value->singleOfferTitle}}</h3>
                                        </div>
                                        @endif
                                        @if ( strlen($value->showTitle) > 35)
                                        <h2>{{substr($value->showTitle , 0 , 35).'...'}}</h2>
                                        @else
                                        <h2>{{substr($value->showTitle , 0 , 35)}}</h2>
                                        @endif
                                    </div>
                                    <div class="card-price">
                                        <h3 class="price-txt">&pound;{{$value->priceFrom}} - &pound;{{$value->priceTo}}
                                        </h3>
                                    </div>
                                </div>

                                <div class="card-desc">
                                    <p><?php
                                                                    $string = strip_tags(html_entity_decode(html_entity_decode($value->summary)));
                                                                    if(strlen($string) > 100)
                                                                        echo substr($string, 0, 100).'...';
                                                                    else
                                                                        echo $string;
                                                                ?></p>
                                </div>
                            </div><!-- end of card-->
                        </a>
                    </div>

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endif

</div>
</div>
</div>
</div>
@endif

<div class="modal fade" id="seatingmodel{{$venue->id}}" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Seating Plan</h4>
            </div>
            <div class="modal-body">
                <img src={{$venue->venue_seating}} class="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>



@endsection