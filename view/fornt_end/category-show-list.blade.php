@extends('layouts.front')

@section('content')

<section class="listing-blk-section clearfix">

    <div class="listing-blk-links-div clearfix">
        <h3 class="blk-title">Plays & Drama</h3>
        <div class="listing-blk-links pull-right clearfix">
            {{-- <div class="link-div"><a href="#" class="blue-link">View All</a></div> --}}
        </div>
    </div>
    <!-- end of listing-blk-links-div -->


    <div class="listing-blk-div clearfix">
        @include('fornt_end.show_list')
    </div>
    <!-- end of listing-blk-div -->




</section>
<!-- end of listing-blk-section -->

@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {

            });
</script>
@endsection