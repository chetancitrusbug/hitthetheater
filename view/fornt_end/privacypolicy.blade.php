@extends('layouts.front')

@section('content')
<section class="about-txt-div">
    <h4>Privacy Policy</h4>
    @if(isset($cms_setting->privacypolicy))
    @if($cms_setting->privacypolicy == null)

    @else
    {!!html_entity_decode($cms_setting->privacypolicy)!!}
    @endif
    @else

    @endif

</section>

@endsection