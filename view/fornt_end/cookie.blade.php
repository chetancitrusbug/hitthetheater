@extends('layouts.front')

@section('content')
<section class="about-txt-div">
    <h4>Cookie Policy</h4>
    @if(isset($cms_setting->cookiepolicy))
    @if($cms_setting->cookiepolicy == null)

    @else
    {!!html_entity_decode($cms_setting->cookiepolicy)!!}
    @endif
    @else

    @endif

</section>

@endsection