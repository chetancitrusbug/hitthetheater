@extends('layouts.front')

@section('content')



<div class="tab-label1 category_div clearfix">
    <h3>Categories</h3>
    <div class="category-box-blk clearfix">
        <ul>
            @if(count($categories) > 0)
            @foreach($categories as $key => $category)
            @if($category->show_count > 0)
            <li><a href="{{route('single-category',[$category->slug])}}">{{$category->categoryTitle}}</a></li>
            @endif

            @endforeach
            @endif
        </ul>

    </div>

</div><!-- end of left-box-blk -->





@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {

            });

</script>
@endsection