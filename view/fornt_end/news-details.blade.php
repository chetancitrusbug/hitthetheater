@extends('layouts.front')

@section('content')

<div class="blog-page-details">
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <!--Blog Single-->
                    <div class="blog-single">
                        <div class="inner-box">
                            <div class="image">
                                <img src="{{$news->image}}" alt="{{$news->title}}">
                            </div>
                            <div class="lower-content">
                                <ul class="post-meta">
                                    <li>@if ($news->publishDate != null) <span class="icon fa fa-clock-o"></span>
                                        {{ date("M d,Y", strtotime($news->publishDate)) }} @endif</li>
                                </ul>
                                <h2>{{$news->title}}</h2>
                                <div class="text">
                                    {!! $news->description !!}
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <aside class="sidebar blog-sidebar">

                        @if (isset($recentlyNews) && count($recentlyNews) > 0)

                        <!-- Popular Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h2>Recent News</h2>
                            </div>
                            @foreach($recentlyNews as $key => $value)
                            <article class="post">
                                <figure class="post-thumb"><a href="{{route('single-news',[$value->slug])}}"><img
                                            src="{{$value->image}}" alt="" /></a></figure>
                                <div class="text"><a
                                        href="{{route('single-news',[$value->slug])}}">{{$value->title}}</a></div>
                                <div class="post-info">
                                    @if ($value->publishDate != null)
                                    <span
                                        class="icon fa fa-clock-o"></span>{{ date("M d,Y", strtotime($value->publishDate)) }}
                                    @endif
                                </div>
                            </article>
                            @endforeach

                        </div>

                        @endif
                    </aside>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection