@if(isset($venues) && count($venues) > 0)
<div class="row venue_category_filter_div">
    <div class="col-xl-12 col-sm-12">
        <div class="top-search-div top-search-div-venue clearfix">
            <div class="search-div">
                <button class="search-i"><i class="fa fa-search"></i></button>
                <input type="text" id="venue_category_filter" placeholder="Search for category.." class="search-input">
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($venues) && count($venues) > 0)
<div class="loading_upper_cls">
    <div id="loading">
        <div class="loader_root_div">
            <div class="loader_container">
                <div class="dot-container">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                </div>
                <div class="dot-container">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                </div>
                <div class="dot-container">
                    <div class="dot"></div>
                    <div class="dot"></div>
                    <div class="dot"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="venue_list_html">
        <div class="latest-update-blk">

            <div class="row">

                @foreach($venues as $key => $venue)
                <div class=" col-6 col-sm-12 col-xl-6 grid-06 float-left">
                    <div class="latest-update-blk-div clearfix">
                        <div class="row">
                            <div class="col-xl-5 col-sm-4 grid-05">
                                <a href="{{route('single-theater',[$venue->slug])}}">
                                    <figure>
                                        <img src="{{$venue->venue_image}}" alt="{{@$venue->venueTitle}}"
                                            title="{{@$venue->venueTitle}}" class="img-fluid width-100">
                                    </figure>
                                </a>
                            </div>
                            <div class="col-xl-7 col-sm-7 grid-07 general-grid7">
                                <a href="{{route('single-theater',[$venue->slug])}}">
                                    <h3>{{$venue->venueTitle}}</h3>

                                </a>
                                {{--
                                        <p><a href="#">Kinky Boots</a></p>
                                        <p><a href="#">The Roy Orbison Story</a></p>
                                        <p><a href="#">Waitress</a></p>
                                        <p><a href="#">Waitress</a></p> --}} @if($venue->addressLine2 != null
                                &&$venue->latitude != null &&$venue->longitude
                                != null)
                                <p><a data-toggle="modal" data-target="#mapmodel{{$venue->id}}" href="#"><i
                                            class="fa fa-map-marker right-p2" aria-hidden="true"></i> {!!
                                        strip_tags($venue->addressLine2) !!}</a></p>

                                @endif

                            </div>
                        </div>
                    </div>
                    <!-- end of latest-update-blk-div  -->
                </div>
                <!-- end of col -->
                <div class="modal fade" id="mapmodel{{$venue->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Map</h4>
                            </div>
                            <div class="modal-body">
                                <iframe
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDWgvWPNqIMlrW0_oV6uCYPnJMWfgZWPyA
                                    &q={{@$venue->venueTitle}},{{@$venue->addressLine1}},{{@$venue->addressLine2}},{{@$venue->postcode}}"
                                    width="100%" height="500px" frameborder="0" scrolling="no" marginheight="0"
                                    marginwidth="0" allowfullscreen></iframe>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

            </div>
            <!-- end of col -->
        </div>
        @else
        <div class="record_not_found">Theatre Not Found</div>
        @endif
    </div>
    <div class="row pagination-div-none">
        <div class="col-xl-12 col-sm-12">
            <div class="pagination-div clearfix">
                {{ $venues->links() }}
            </div>
        </div>
    </div>
</div>