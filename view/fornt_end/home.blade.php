@extends('layouts.front')

@section('content')

<section class="listing-blk-section clearfix">
    <div class="listing-blk-links-div man0 clearfix">
        <div class="listing-blk-links pull-left clearfix" id="listing-link">
            <ul>
                <li><span class="{{ $target == 'Musicals' ? 'active' : '' }} listing-link" target="Musicals">Top
                        Musicals</span></li>
                <li><span class="{{ $target == 'Plays' ? 'active' : '' }}listing-link" target="Plays">Top Plays</span>
                </li>
                <li><span class="{{ $target == 'Drama' ? 'active' : '' }}listing-link" target="Drama">Top Drama</span>
                </li>
            </ul>

        </div>
        <div class="listing-blk-links pull-right clearfix">
            <!--<div class="link-div"><a href="#" class="blue-link">View All</a></div> -->
        </div>
    </div><!-- end of listing-blk-links-div -->

    <div class="listing-blk-div clearfix" id="show_list">


        @include('fornt_end.show_list')
    </div>

</section>

@endsection

@section('js')

<script type="text/javascript">
    function setHeight(){
				var highestBox = 0;
					$('.playList').each(function(){
							if($(this).height() > highestBox){
							highestBox = $(this).height();
					}
				});
				$('.playList').height(highestBox);
			}
            $(document).ready(function() {
                //$("#load").show(); return false;
				$("#load").hide();
				setHeight();

                $('.listing-link').click(function() {
                    var url = "{{url('/')}}";
                    var target = $(this).attr('target')
					$("#load").show();

                    $.ajax({
                        url:url,
                        type: 'get',
                        data : {target:target},

                        success:function(response){
                            if(response.success == 1)
                            {

                                $('#show_list').html(response.html);
                                setHeight();
                                var imgurl = $(".imgurl");
                                imgurl.each(function () {
                                    showid = $(this).attr('showid');
                                    url = getImageofShow(showid, $(this));
                                    // console.log(url);
                                    // $(this).attr('src',url);
                                });

                                function getImageofShow(showid, element) {
                                    var url = "{{ route('showImageGet',':showid') }}";
                                    url = url.replace(':showid',showid);
                                    jQuery.ajax({
                                        url: url,
                                        type: 'GET',
                                        success: function (response) {
                                            if (response.url != null) {
                                                element.attr('src', response.url);
                                            }
                                            else {
                                                element.attr('src', "{{url('')}}/frontend/images/no_image.png");
                                            }
                                        },
                                        error: function (xhr, status, error) {
                                            element.attr('src', "{{url('')}}/frontend/images/no_image.png");
                                        }
                                    });
                                }
                            }
							$("#load").hide();

                        },

                        error:function(xhr, status, error){
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err);
							$("#load").hide();
                        }
                    });
                });

            });

</script>
@endsection