<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->

<head>
    @include('include.front-end.css')

</head>

<body>


    <div id="wrapper">

        <header class="header-center clearfix">
            <div class="header-div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center ">

                            <div class="logo-div justify-content-center">
                                <a href="{{url('')}}">
                                    @if(isset($cms_setting->logo) && $cms_setting->logo)
                                    @php
                                    $path=public_path().'/storage/cms'.$cms_setting->logo;
                                    if(file_exists($path))
                                    {
                                    $path=url('').'/storage/cms'.$cms_setting->logo;
                                    echo '<img class="brand-logo" alt="Hit The Theatre" src='.$path.' width="100px">';
                                    }
                                    @endphp
                                    @else
                                    <img class="brand-logo" alt="Hit The Theatre"
                                        src="{{asset('frontend/images/logo.png')}}">
                                    @endif
                                </a>
                            </div><!-- end of logo-div -->

                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="main-content center-blk clearfix">

            <section class="two-block-section clearfix">

                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-xl-12">

                            <div class="general-all-div under-construct clearfix">
                                <div class="row">
                                    <div class="col-12 col-lg-12 mx-auto">
                                        <h4 class="text-center h4-big">Page is Under Construction</h4>
                                        <div class="policy-div clearfix">

                                            <div class="col-lg-12 col-sm-12 text-center link-blk-div clearfix">
                                                <a href="{{url('/')}}" class="redirect-link">Redirect to Home Page</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- end of col -->
                    </div>
                </div>

            </section><!-- end fo home-banner-div -->

        </div><!-- end of main-content -->

    </div><!-- end of wrapper -->

    @include('include.front-end.js')
</body>

</html>