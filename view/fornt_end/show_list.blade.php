<div class="row">


    @if(isset($show) && count($show) > 0)

    @php
    // function file_exists_remote($url) {
    // $curl = curl_init($url);
    // curl_setopt($curl, CURLOPT_NOBODY, true);
    // //Check connection only
    // $result = curl_exec($curl);
    // //Actual request
    // $ret = false;
    // if ($result !== false) {
    // $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    // //Check HTTP status code
    // if ($statusCode == 200) {
    // $ret = true;
    // }
    // }
    // curl_close($curl);
    // return $ret;
    // }
    @endphp
    <div class="listing-blk-div listing-blk-div-xs-15 clearfix" id="show_list">
        <div class="row">

            @foreach($show as $value)
            @if(isset($value->show))
            @php
            $value = $value->show;

            @endphp
            @endif

            <div class="col-lg-3 col-xl-3 col-6">
                <a href="{{route('show-detail',[$value->slug])}}">
                    <div class="card">
                        <div class="card-img">
                            <div class="thumb-img">
                                @php
                                // $path=public_path().'/storage/show'.$value->rectangle_poster;
                                // $img = '';
                                // if(file_exists($path))
                                // {
                                // $path=url('').'/storage/show'.$value->rectangle_poster;
                                // $img = $path;
                                // }
                                // elseif(@fopen($value->rectangle_poster,'r'))
                                // {
                                // $img = $value->rectangle_poster;
                                // }

                                @endphp
                                {{-- @if($img != '')
                                <img src="{{$img}}" alt="{{$value->showTitle}}/" title="{{$value->showTitle}}/"
                                class="img-fluid">
                                @else --}}
                                <img src="{{url('frontend/images/image_loader.gif')}}" alt="{{$value->showTitle}}/"
                                    title="{{$value->showTitle}}/" class="img-fluid imgurl" showid="{{$value->id}}">
                                {{-- @endif --}}
                            </div>
                        </div><!-- end of card img -->
                        <div class="card-details lang-eng clearfix">
                            <div class="card-name">
                                @if(($value->singleOfferTitle) != '')
                                <div class="card-singleOfferTitle">
                                    <h3 class="singleOfferTitle-txt">{{$value->singleOfferTitle}}</h3>
                                </div>
                                @endif
                                {{-- @if(@fopen($value->square_image,'r'))
                                <div class="thumb-img">
                                    <img src="{{$value->square_image}}" alt="{{$value->showTitle}}/" class="img-fluid">
                            </div>
                            @elseif(($value->rectangle_image))
                            <div class="thumb-img">
                                <img src="{{$value->rectangle_image}}" alt="{{$value->showTitle}}/" class="img-fluid">
                            </div>
                            @else
                            <div class="thumb-img">
                                <img src="{{url('frontend\images\no_image.png')}}" alt="{{$value->showTitle}}/"
                                    class="img-fluid">
                            </div>
                            @endif --}}
                            @if ( strlen($value->showTitle) > 35)
                            <h2>{{substr($value->showTitle , 0 , 35).'...'}}</h2>
                            @else
                            <h2>{{substr($value->showTitle , 0 , 35)}}</h2>
                            @endif
                        </div>

                        <div class="card-price">
                            <h3 class="price-txt">&pound;{{$value->priceFrom}} - &pound;{{$value->priceTo}}</h3>
                        </div>
                        {{-- <div class="card-offer"><p href="">Save Up to 40%</p></div>	 --}}
                    </div><!-- end of card-details  -->

                    <div class="card-desc">
                        <p><?php
                                $string = strip_tags(html_entity_decode(html_entity_decode($value->summary)));
                                if(strlen($string) > 100)
                                    echo substr($string, 0, 100).'...';
                                else
                                    echo $string;
                            ?></p>
                    </div>
            </div><!-- end of card-->
            </a>
        </div>
        @endforeach

        @else
        <div>Show Not Found</div>
        @endif

    </div><!-- end of row-->

    @if(isset($show) && count($show) > 0)
    <div class="row">
        <div class="col-xl-12 col-sm-12">
            <div class="pagination-div clearfix">
                {{ $show->links() }}
            </div>
        </div>
    </div>
    @endif
</div>