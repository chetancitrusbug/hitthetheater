@extends('layouts.front')

@section('content')

@php
$show = $shows[0];
@endphp

{{-- @if (isset($show->closedshow) && $show->closedshow != 1)
    <div class="white-box-blk mb20 detail-booking-box mobile-display-none ">
        @include('include.front-end.bookform')
    </div>
    @endif

    @if(@fopen($show->rectangle_image,'r'))
    <div class="left-box-blk clearfix  mobile-display-none ">
        <div class="show-poster-div">
            <a href="#">
                <img src="{{$show->rectangle_image}}" alt="{{$show->showTitle}}" class="img-fluid">
</a>
</div>
</div><!-- end of left-box-blk -->
@endif --}}

<div class="spl-offers-div mobile-display-block spl-order-mobile1">
    <h4>{{$show->showTitle}}</h4>
    @if (isset($show->closedshow) && $show->closedshow == 1)
    <span class="closedshow">Closed</span>
    @endif
</div>

@if(isset($offers) && count($offers) > 0)
<div class="spl-offers-div spl-order-mobile2">
    <div class="sp-offer">
        <h4>Special Offers</h4>
    </div>
    @foreach($offers as $offer)

    @php
    if ($offer->facevalue != $offer->price) {
    $dis = (($offer->facevalue - $offer->price) / $offer->facevalue) * 100 ;
    if ($dis == 0) {
    $dis = 'No Booking fee';
    }
    else {
    $dis = 'Save '.number_format(@$dis,0).'%';
    }
    }
    else
    { $dis = 'No Booking fee'; }


    @endphp

    <div class="spl-offers-blk-txt clearfix">
        <div class="txt-1">
            <p><strong>{{$offer->title}}</strong> <br>
                {{$offer->valid}}.</p>
        </div>
        <div class="txt-2">
            <p>Was<br><span class="price old_price"><strike>£{{number_format(@$offer->facevalue,2)}}</strike></span></p>
        </div>
        <div class="txt-3">
            <p>Now<br><span class="price priceofr">£{{number_format(@$offer->price,2)}}</span></p>
        </div>
        <div class="txt-4">
            <p>Discount<br><span class="price dispriceofr">{{ $dis }}</span></p>
        </div>
        {{-- <div class="txt-4">
                            <p>starts<br><span class="price">{{date('d-m-Y',strtotime($offer->start_date))}}</span></p>
    </div>
    <div class="txt-5">
        <p>ends<br><span class="price">{{date('d-m-Y',strtotime($offer->end_date))}}</span></p>
    </div> --}}
</div><!-- end of spl-offers-div-->
@endforeach
</div><!-- end of spl-offers-div-->
@endif


<div class=" spl-offers-div spl-order-mobile3">
    <h4>About the show</h4>
    <div class="detail-content-div bg-detail-content">
        {!!($show->description)!!}
    </div>
</div>

@if(isset($show->showImages) && count($show->showImages) > 0)
<div class="spl-offers-div img-gallery-div spl-order-mobile5">
    <h4>Image Gallery</h4>
    <ul class="bxslider">
        @foreach($show->showImages as $showImages)
        @php
        $path=public_path().'/storage/show'.$showImages->url;

        $img = $showImages->url;

        if(file_exists($path))
        {
        $path= url('').'/storage/show'.$showImages->url;
        $img = $path;
        }
        elseif($showImages->created_by == 0)
        {
        $img = $showImages->url;
        }
        @endphp
        @if(isset($showImages->url) && $showImages->url != null)
        @if($showImages->type == 'video')
        <li><a data-fancybox="gallery" href="{{$img}}"><video class="video_of_gallery" controls
                    title="{{$showImages->title}}" alt="{{$showImages->alt}}" order={{$showImages->orderBy}}>
                    <source src="{{$img}}" type="video/mp4">
                </video></a></li>
        <li>
            @else
        <li><a data-fancybox="gallery" href="{{$img}}"><img src="{{url('frontend/images/image_loader.gif')}}"
                    class="load_images" showimgurl="{{$showImages->id}}" title="{{$showImages->title}}"
                    alt="{{$showImages->alt}}" order={{$showImages->orderBy}}></a></li>
        @endif
        @endif
        @endforeach
    </ul>
</div>
<!-- end of spl-offers-div-->
@endif


@if(isset($venue->VenueSliders) || $venue->VenueSliders != null || isset($venue->latitude) || isset($venue->latitude) ||
$venue->latitude != null || $venue->longitude != null)
<div class="left-box-blk text-center spl-order-mobile6 clearfix">
    <div class="spl-offers-div">
        <h4>Theatre Information</h4>
        <div class="white-box-blk p20 clearfix">
            <div class="playing-blk-div">
                <h3> <a href="{{route('single-theater',[@$venue->slug])}}"> {{@$venue->venueTitle}} </a></h3>
                <p><i class="fa fa-map-marker"></i> {{@$venue->addressLine1}},
                    {{ (isset($venue->addressLine2) && $venue->addressLine2 != '') ? $venue->addressLine2.',' : '' }}
                    {{@$venue->postcode}}</p>
                <ul>
                    @if(isset($venue->latitude) && isset($venue->latitude) && $venue->latitude != null &&
                    $venue->longitude != null)
                    <li><span class="glyph-icon flaticon-gps"></span>Map
                        <iframe
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDWgvWPNqIMlrW0_oV6uCYPnJMWfgZWPyA
                                    &q={{@$venue->venueTitle}},{{@$venue->addressLine1}},{{@$venue->addressLine2}},{{@$venue->postcode}}"
                            width="100%" class="youtube_video" height="" frameborder="0" scrolling="no" marginheight="0"
                            marginwidth="0" allowfullscreen></iframe>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

@endif

@if($showCategory != null && count($showCategory) > 0)
<div class=" spl-offers-div spl-order-mobile7">
    <h4>Categories</h4>
    <div class="detail-content-div bg-detail-content">
        <div class="media-link media-link-facilities border_bottom">
            <div
                class="field field-name-field-venue-api-facilities field-type-taxonomy-term-reference field-label-inline clearfix">
                <div class="field-items div-faciliti-item">
                    @foreach ($showCategory as $item)
                    <div class="field-item even">{{$item}}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if(isset($show->iframe_display) && $show->iframe_display == 1 && $show->youtube_link != null)
<div class="spl-offers-div spl-order-mobile4">
    <h4>Video</h4>
    <iframe src="{{$show->youtube_link}}" class="youtube_video" height="" width="100%" allowfullscreen></iframe>
</div>
@endif

    @endsection



    @section('js')
    <script>
         setTimeout(function(){

    var load_images = $(".load_images");
    load_images.each(function () {
        showimgurl = $(this).attr('showimgurl');
        getImageofShow(showimgurl, $(this));
        // console.log(url);
        // $(this).attr('src',url);
    });
    function getImageofShow(showimgurl, element) {
        var url = "{{ route('showGalleryImageGet',':id') }}";
        url = url.replace(':id',showimgurl);
        jQuery.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                if (response.url != '') {
                    element.attr('src', response.url);
                }
                else {
                    element.closest("li").remove();
                    if(element.closest("ul").children().length == 0)
                    {
                        $('.img-gallery-div').hide();
                    }
                }
            },
            error: function (xhr, status, error) {
                element.closest("li").remove();
                if(element.closest("ul").children().length == 0)
                {
                    $('.img-gallery-div').hide();
                }
            }
        });
    }
}, 5000);
    </script>
    @endsection