<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    @php
    if ($type == 'show')
    $route = 'show-detail';
    elseif ($type == 'venue')
    $route = 'single-theater';
    elseif ($type == 'category')
    $route = 'single-category';
    elseif ($type == 'venueCategories')
    $route = 'single-category-theater';
    else
    $route = '';

    @endphp
    @foreach ($data as $item)
    <sitemap>
        @if ($route != '')
        <loc>{{route($route,[$item->slug])}}</loc>
        @else
        <loc>{{url($item->slug)}}</loc>
        @endif

    </sitemap>
    @endforeach

</sitemapindex>