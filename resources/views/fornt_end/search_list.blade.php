<div class="container1">
    <div class="autocomplete-search-div">

        <div class="row">

            <div class="col-sm-4 col-xl-4">
                <h3>
                    Show
                </h3>

                <ul>
                    @foreach($shows as $show)
                    <li>
                        <a href="{{route('show-detail',[$show->slug])}}">{{$show->showTitle}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-4 col-xl-4">
                <h3>
                    Theatre
                </h3>

                <ul>
                    @foreach($venues as $venue)
                    <li>
                        <a href="{{route('single-theater',[$venue->slug])}}">{{$venue->venueTitle}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-4 col-xl-4">
                <h3>
                    Category
                </h3>

                <ul>
                    @foreach($categories as $category)
                    <li>
                        <a href="{{route('single-category',[$category->slug])}}">{{$category->categoryTitle}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @if($closedshows != null && count($closedshows) > 0 )
            <div class="col-sm-12 col-xl-12">
                <h3>
                    Closed Show
                </h3>

                <ul>
                    @foreach($closedshows as $show)
                    <li>
                        <a href="{{route('show-detail',[$show->slug])}}">{{$show->showTitle}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>