@extends('layouts.front')
@section('content')

{{-- @if(isset($generalNews) && count($featureNews) > 0) --}}
<div class="latest-update-blk latestnews-box2 mrg-top-40">

    <div class="pull-left width-50">
        <div class="title-sidebar col-lg-12 col-sm-12">
            <h2>General</h2>
        </div>
        @foreach($generalNews as $key => $generalNewsValue)

        {{-- @if (isset($generalNews[$key]) && $generalNews[$key] != null)
                @php $generalNewsValue = $generalNews[$key]; $cls12 = 6; @endphp --}}

        <div class="sidebar-side col-lg-12 col-sm-12">
            <aside class="sidebar blog-sidebar">
                <div class="sidebar-widget popular-posts">
                    <article class="post">
                        <figure class="post-thumb"><a href="{{route('single-news',[$generalNewsValue->slug])}}"><img
                                    src="{{$generalNewsValue->image}}" alt="" /></a></figure>
                        <div class="text heading-txt"><a
                                href="{{route('single-news',[$generalNewsValue->slug])}}">{{$generalNewsValue->title}}</a>
                        </div>

                        @if ($generalNewsValue->publishDate != null)
                        <div class="post-info news-date">
                            <span class="icon fa fa-clock-o"></span>
                            {{ date("M d,Y", strtotime($generalNewsValue->publishDate)) }}
                        </div>
                        @endif

                        <a href="{{route('single-news',[$generalNewsValue->slug])}}">
                            <div class="text heading-prag">{!! substr($generalNewsValue->description,0,100) !!}</div>
                        </a>


                    </article>
                </div>
            </aside>
        </div>
        @endforeach
    </div>
    <div class="pull-left width-50">
        <div class="title-sidebar col-lg-12 col-sm-12">
            <h2>Featured</h2>
        </div>
        @foreach($featureNews as $key => $featureNewsValue)
        {{-- @else
            @php $cls12 = 12; @endphp
            @endif --}}

        {{-- @if (isset($featureNews[$key]) && $featureNews[$key] != null)
            @php $featureNewsValue = $featureNews[$key] @endphp --}}
        <div class="sidebar-side col-lg-12 col-sm-12">
            <aside class="sidebar blog-sidebar">
                <div class="sidebar-widget popular-posts">
                    <article class="post">
                        <figure class="post-thumb"><a href="{{route('single-news',[$featureNewsValue->slug])}}"><img
                                    src="{{$featureNewsValue->image}}" alt="" /></a></figure>
                        <div class="text heading-txt"><a
                                href="{{route('single-news',[$featureNewsValue->slug])}}">{{$featureNewsValue->title}}</a>
                        </div>
                        <a href="{{route('single-news',[$featureNewsValue->slug])}}">
                            <div class="text heading-prag">{!! substr($featureNewsValue->description,0,100) !!}</div>
                        </a>

                    </article>
                </div>
            </aside>
        </div>
        {{-- @else
            <div class="news-block-two style-four col-lg-5 col-md-12 col-sm-12 col-xs-12 pull-right">
            </div>
            @endif --}}

        @endforeach
    </div>

    <!-- end of col -->
</div>
{{-- @else
<div class="record_not_found">Theatre Not Found</div>
@endif --}}

<div class="row pagination-div-none">
    <div class="col-xl-12 col-sm-12">
        <div class="pagination-div clearfix">
            {{ $generalNews->links() }}
        </div>
    </div>
</div>

@endsection