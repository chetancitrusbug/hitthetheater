@extends('layouts.front')

@section('content')

<section class="listing-blk-section clearfix">
    <div class="listing-blk-links-div clearfix">
        <div class="listing-blk-links pull-left clearfix" id="listing-link">
            <ul>
                <li><span class="active listing-link"
                        target="{{$categorySingle->categoryTitle}}">{{$categorySingle->categoryTitle}}</span></li>
            </ul>

        </div>
        <!--<div class="listing-blk-links pull-right clearfix">
            <div class="link-div"><a href="#" class="blue-link">View All</a></div>
        </div> -->
    </div><!-- end of listing-blk-links-div -->

    <div class="listing-blk-div clearfix" id="show_list">
        @include('fornt_end.theater-loop')
    </div>

</section>

@endsection

@section('js')

<script type="text/javascript">
    $(document).ready(function() {


            });

</script>
@endsection