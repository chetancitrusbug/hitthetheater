@extends('layouts.front')
@section('content')
<section class="about-txt-div">
    <h4>About Us</h4>
    @if(isset($cms_setting->about_us))
    @if($cms_setting->about_us == null)

    @else
    {!!html_entity_decode($cms_setting->about_us)!!}
    @endif
    @else

    @endif

</section>

@endsection