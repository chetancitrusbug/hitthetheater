@extends('layouts.front')

@section('content')
<section class="contact-txt-div">
    <h4>Contact Us</h4>
    @if(Session::has('contact_success'))
    <p class="alert alert-success">{{ Session::get('contact_success') }}</p>
    @endif

    @if(isset($cms_setting->contact_us) && $cms_setting->contact_us != null)
    {!!html_entity_decode($cms_setting->contact_us)!!}
    @endif

    <div class="contact-form-div">

        {!! Form::open(['url' => '/contect_us_post','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

        <div class="row">
            <label class="col-xl-3 col-sm-4">Your Name</label>
            <div class="col-xl-9 col-sm-8">
                <input type="text" class="form-control" placeholder="Enter your name" name="name" />
                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <label class="col-xl-3 col-sm-4">Your Email</label>
            <div class="col-xl-9 col-sm-8">
                <input type="email" class="form-control" placeholder="Enter your Email" name="email" />
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <label class="col-xl-3 col-sm-4">Subject</label>
            <div class="col-xl-9 col-sm-8">
                <input type="text" class="form-control" placeholder="Enter your subject" name="subject" />
                {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <label class="col-xl-3 col-sm-4">Your Message</label>
            <div class="col-xl-9 col-sm-8">
                <textarea class="form-control" placeholder="Enter your message" name="message"></textarea>
                {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-sm-12">
                <div class="pull-right text-right"><button class="sbtn">Submit</button></div>
            </div>
        </div>

        {!! Form::close() !!}
    </div>

</section>

@endsection