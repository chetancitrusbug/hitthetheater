@extends('layouts.front')

@section('content')
<section class="about-txt-div">
    <h4>Faq</h4>
    @if(isset($cms_setting->faq))
    @if($cms_setting->faq == null)

    @else
    {!!html_entity_decode($cms_setting->faq)!!}
    @endif
    @else

    @endif

</section>

@endsection