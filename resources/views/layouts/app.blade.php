<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <link rel="shortcut icon" href="{{asset('frontend/images/logo.png')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') {{ config('app.name') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/images/logo.png')}}">
    <link
        href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
        rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/vendors.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/app.css')}}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/horizontal-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/palette-gradient.css')}}s">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('backend/css/style.css')}}">

    <style type="text/css">
        .error,
        .help-block {
            color: #ff0000;
        }
    </style>



    @stack('css')
</head>

<body class="app-layout horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="hover"
    data-menu="horizontal-menu" data-col="2-columns">
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <a href="{{route('login')}}">
                        @if(isset($cms_setting->logo) && $cms_setting->logo)
                        @php
                        $path=public_path().'/storage/cms'.$cms_setting->logo;
                        if(file_exists($path))
                        {
                        $path=url('').'/storage/cms'.$cms_setting->logo;
                        echo '<img class="brand-logo" alt="Hit The Theatre" src='.$path.' width="100px">';
                        }
                        @endphp
                        @else
                        <img class="brand-logo" alt="Hit The Theatre" src="{{asset('frontend/images/logo.png')}}">
                        @endif
                    </a>
                </div>
            </div>
            <div class="content-body">
                <!-- Form actions layout section start -->
                @yield('content')
                <!-- // Form actions layout section end -->
            </div>
        </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a
                    class="text-bold-800 grey darken-2" href="#">Hit The Theatre </a>, All rights reserved. </span>
        </p>
    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset('backend/js/vendors.min.js')}}" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script type="text/javascript" src="{{asset('backend/js/jquery.sticky.js')}}"></script>
    <script type="text/javascript" src="{{asset('backend/js/jquery.sparkline.min.js')}}"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="{{asset('backend/js/app-menu.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/app.js')}}" type="text/javascript"></script>
    <script src="{{asset('backend/js/customizer.js')}}" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script type="text/javascript" src="{{asset('backend/js/breadcrumbs-with-stats.js')}}"></script>
    <!-- END PAGE LEVEL JS-->

    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            // App.init();
            // App.dashboard();
            setTimeout(function(){
                jQuery('.alert-dismissible').hide();
            }, 10000);

            $(".datetimepicker").datetimepicker({
                autoclose: true,
                format: 'dd-mm-yyyy',
                componentIcon: '.mdi.mdi-calendar',
                navIcons:{
                    rightIcon: 'mdi mdi-chevron-right',
                    leftIcon: 'mdi mdi-chevron-left'
                }
            });


        });


    </script>
    @stack('js')
</body>

</html>