@extends('layouts.app')

@section('title','FORGOT PASSWORD')

@section('content')
<section id="form-action-layouts">
    <div class="row justify-content-md-center">
        <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="from-actions-top-bottom-center">Forgot Password</h4>
                </div>
                <div class="card-content collpase show">
                    <div class="card-body">
                        <div class="card-text">
                            @include('include.admin.alerts')
                        </div>
                        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-12 mb-2 ">
                                        <input type="email" id="email" class="form-control {{ $errors->has('email') ? ' has-error' : ''}} " placeholder="Email" name="email"
                                            value="{{ old('email') }}"> {!! $errors->first('email',
                                        '
                                        <p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions center">
                                <button type="reset" class="btn btn-warning mr-1">
                                            <i class="ft-x"></i> Cancel
                                            </button>
                                <button type="submit" class="btn btn-primary">
                                            <i class="fa fa-check-square-o"></i> Reset Password
                                            </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

