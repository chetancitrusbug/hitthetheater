<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-center">
    <div class="navbar-wrapper">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
          <li class="nav-item">
            <a class="navbar-brand" href="{{url('admin/dashboard')}}">
                 @if(isset($cms_setting->logo) && $cms_setting->logo)
                    @php
                        $path=public_path().'/storage/cms'.$cms_setting->logo;
                        if(file_exists($path))
                        {
                            $path=url('').'/storage/cms'.$cms_setting->logo;
                            echo '<img class="brand-logo" alt="Hit The Theatre Logo" src='.$path.' width="100px">';
                        }
                    @endphp
                @else
                    <img class="brand-logo" alt="Hit The Theatre Logo" src="{{asset('frontend/images/logo.png')}}">
                @endif
              {{-- <h2 class="brand-text">Hit The Theatre</h2> --}}
            </a>
          </li>
          <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
          </li>
        </ul>
      </div>
      <div class="navbar-container content">
        <div class="collapse navbar-collapse" id="navbar-mobile">
          <ul class="nav navbar-nav mr-auto float-left">
            <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>

          </ul>
          <ul class="nav navbar-nav float-right">

            <li class="dropdown dropdown-user nav-item">
              <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">


                <span class="user-name">{{auth()->user()->name}}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="{{url('admin/profile')}}"><i class="ft-user"></i> Edit Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> <i class="ft-power"></i> Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>

              </div>
            </li>

          </ul>
        </div>
      </div>
    </div>
  </nav>