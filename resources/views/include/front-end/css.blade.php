<link href="{{asset('frontend/images/logo.png')}}" rel='shortcut icon'>
<link href="{{asset('frontend/images/logo.png')}}" rel='icon' type='image/ico'>

<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<link href="{{asset('frontend/css/style.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/css/bootstrap.min.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{asset('frontend/css/font-awesome.min.css')}}" media="all" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/owl.theme.default.min.css')}}">


<link rel="stylesheet" href="{{asset('frontend/plugin/slider_lightbox/css/jquery.fancybox.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/plugin/slider_lightbox/css/jquery.bxslider.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/plugin/slider_lightbox/css/style.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/blog.css')}}">