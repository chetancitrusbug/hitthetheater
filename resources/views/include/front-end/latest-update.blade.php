@if(count($popularShow) > 0)
<section class="latest-update-blk-section desktop-display-block">
    <div class="container">
        <div class="row">
            <div class="latest-update-blk">


                @foreach($popularShow as $show)
                <div class="col-sm-6 col-xl-6 float-left">
                    <div class="latest-update-blk-div clearfix">
                        <div class="row">
                            <div class="col-xl-5 col-sm-5"><a href="{{route('show-detail',[$show->show_slug])}}">
                                    <figure><img src="{{url('frontend/images/image_loader.gif')}}" alt=""
                                            class="img-fluid imgurl" showid="{{$show->show_main_id}}"></figure>
                                </a></div>
                            <div class="col-xl-7 col-sm-7">
                                <a href="{{route('show-detail',[$show->show_slug])}}">
                                    <h5>
                                        {{date('d M', strtotime($show->bookingStarts))}} -
                                        {{date('d M', strtotime($show->bookingEnds))}}
                                    </h5>

                                    <h2> <a class="a_tag_text" href="{{route('show-detail',[$show->show_slug])}}">
                                            {{$show->venueTitle}} - {{$show->showTitle}} </a></h2>
                                    {{-- {{html_entity_decode($show->summary)}} --}}
                                    {!!substr(html_entity_decode($show->summary), 0, 200);!!}
                                    <div class="price-txt">&pound;{{$show->priceFrom}} - &pound;{{$show->priceTo}}</div>
                                </a>
                            </div>
                        </div>
                    </div><!-- end of latest-update-blk-div  -->
                </div><!-- end of col -->
                @endforeach



            </div>
        </div><!-- end of row -->
    </div><!-- end of container -->
</section><!-- end of latest-update-blk-section-->
@endif