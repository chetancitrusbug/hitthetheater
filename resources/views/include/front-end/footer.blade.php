<footer>
    <div class="footer-div clearfix">

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="newsletter-div clearfix">
                        <!-- <div class="newsletter-box">
                            <input type="text" placeholder="Sign Up to newsletter" class="newsletter-input">
                            <button class="newsletter-btn">Submt</button>
                        </div>
                            end of top-search-div -->
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-top-div clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <ul class="footer-li clearfix">

                            <li><a href="{{url('/')}}">Home</a></li>
                            @if($cms_setting->about_us_display != 0) <li><a href="{{url('/about-us')}}">About Us</a>
                            </li> @endif
                            @if($cms_setting->faq_display != 0) <li><a href="{{url('/faq')}}">FAQS</a></li> @endif
                            @if($cms_setting->contact_us_display != 0) <li><a href="{{url('/contact-us')}}">Contact
                                    Us</a></li> @endif
                            @if($cms_setting->termandcondition_display != 0) <li><a
                                    href="{{url('/terms-and-conditions')}}">Terms & Conditions</a></li> @endif
                            @if($cms_setting->privacypolicy_display != 0) <li><a
                                    href="{{url('/privacy-policy')}}">Privacy Policy</a></li> @endif
                            @if($cms_setting->cookiepolicy_display != 0) <li><a href="{{url('/cookie-policy')}}">Cookie
                                    Policy</a></li> @endif
                            <li><a href="{{url('/sitemap.xml')}}">Sitemap</a></li>

                        </ul>
                    </div><!-- end of col -->
                    <div class="col-md-4 col-sm-4">
                        @if(isset($cms_setting->contact_us_address) && $cms_setting->contact_us_address_display != 0)
                        {!!html_entity_decode($cms_setting->contact_us_address)!!}
                        @endif
                        </p>
                    </div><!-- end of col -->

                    <div class="col-md-4 col-sm-4">
                        <div class="footer-img-div">
                            <span><img src="{{asset('frontend/images/visa.png')}}" alt=""></span>
                            <span><img src="{{asset('frontend/images/star-img.png')}}" alt=""></span>
                        </div>
                    </div><!-- end of col -->


                </div><!-- end of row -->
            </div><!-- end of container -->
        </div>

        <div class="copyright-div clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <span
                            class="copy-txt">{{(isset($cms_setting->copy_right_content) && $cms_setting->copy_right_content_display != 0 ?  $cms_setting->copy_right_content : '')}}</span>
                    </div><!-- end of col -->
                    <div class="col-md-6 col-sm-6">
                        <span class="footer-social">
                            @if($cms_setting->facebook_link_display != 0) <span><a
                                    href="{{$cms_setting->facebook_link}}" target="blank"><i
                                        class="fa fa-facebook"></i></a></span> @endif
                            @if($cms_setting->twitter_link_display != 0) <span><a href="{{$cms_setting->twitter_link}}"
                                    target="blank"><i class="fa fa-twitter"></i></a></span> @endif
                            @if($cms_setting->youtube_link_display != 0) <span><a href="{{$cms_setting->youtube_link}}"
                                    target="blank"><i class="fa fa-youtube"></i></a></span> @endif
                            @if($cms_setting->instagram_link_display != 0) <span><a
                                    href="{{$cms_setting->instagram_link}}" target="blank"><i
                                        class="fa fa-instagram"></i></a></span> @endif
                        </span>
                    </div><!-- end of col -->
                </div><!-- end of row -->
            </div><!-- end of container -->
        </div>

    </div>
    @include('cookieConsent::index')
</footer>