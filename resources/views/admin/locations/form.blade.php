<div class="row">
<div class="col-md-6">
        <div class="form-group{{ $errors->has('locationTitle') ? ' has-error' : ''}}">
            {!! Form::label('locationTitle', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('locationTitle', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('locationTitle', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @if(isset($location->locationCode))
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('locationCode') ? ' has-error' : ''}}">
            {!! Form::label('locationCode', '* Code: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('locationCode', old('email',$location->locationCode), ['class' => 'form-control input-sm','disabled' => 'disabled']) !!}
                {!! $errors->first('locationCode', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @else
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('locationCode') ? ' has-error' : ''}}">
            {!! Form::label('locationCode', '* Code: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('locationCode', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('locationCode', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    @endif
</div>
<div class="row">
<div class="col-md-6">
    <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-12">
            {!! Form::checkbox('status',1, isset($location->status)?$location->status:1, ['class'=>['status']]) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
</div>