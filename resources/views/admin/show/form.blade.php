<div class="row">
    <div class="col-md-12">
        <fieldset>
            <legend>Basic Details</legend>
            <div class="form-group{{ $errors->has('showTitle') ? ' has-error' : ''}}">
                {!! Form::label('showTitle', '* Show Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('showTitle', null, ['class' => 'form-control input-sm']) !!}
                    {!! $errors->first('showTitle', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('slug') ? ' has-error' : ''}}">
                {!! Form::label('slug', '* Slug: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('slug', null, ['class' => 'form-control input-sm'])
                    !!} {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('venue') ? ' has-error' : ''}}">
                {!! Form::label('venue', '* Show Venue : ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::select('venue',$venueslist,isset($show->venueCode) ? $show->venueCode : '',['class' => 'form-control tags' ]) !!} {!! $errors->first('venue', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group {{ $errors->has('price_form') ? ' has-error' : ''}}">
                <label class="col-sm-3 control-label">* Price From : </label>
                <div class="col-sm-12">
                    <input type="text" name="price_form" data-slider-value="[10,90]" data-slider-step="1" data-slider-max="500"
                        data-slider-min="10" value="<?php echo isset($show->priceFrom) ? $show->priceFrom : '' ;?>"
                        class="form-control"> {!! $errors->first('price_form', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group {{ $errors->has('price_to') ? ' has-error' : ''}}">
                <label class="col-sm-3 control-label">* Price To : </label>
                <div class="col-sm-12">
                    <input type="text" name="price_to" data-slider-value="[10,90]" data-slider-step="1" data-slider-max="500"
                        data-slider-min="10" value="<?php echo isset($show->priceTo) ? $show->priceTo : '' ;?>"
                        class="form-control"> {!! $errors->first('price_to', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module  form-group{{ $errors->has('banner') ? ' has-error' : ''}}">
                {!! Form::label('banner', '* Show Banner (1920 * 370 for best resulation): ', ['class' => 'col-sm-12 control-label']) !!}
                <div class="col-sm-12">
                    {!!Form::file('banner',['class' => 'input-sm'] ) !!} {!! $errors->first('banner', '
                    <p class="help-block">:message</p>') !!}
                    @if(isset($banner->banner) && $banner->banner)
                    <a href={{ $banner->banner }} target="_blank" >
                    <img src={{ $banner->banner }} alt="image" width="100">
                    </a> @endif
                </div>

            </div>

            <div class="width-50-module  form-group{{ $errors->has('poster') ? ' has-error' : ''}}">
                {!! Form::label('rectangle-poster', '* Show Poster (300 * 300 for best resulation): ', ['class' => 'col-sm-12 control-label']) !!}
                <div class="col-sm-12">

                    {!!Form::file('rectangle-poster',['class' => 'input-sm'] ) !!} {!! $errors->first('rectangle-poster', '
                    <p class="help-block">:message</p>') !!}

                    @if(isset($rectanglePoster->poster) && $rectanglePoster->poster)
                    <a href={{ $rectanglePoster->poster }} target="_blank" >
                    <img src={{ $rectanglePoster->poster }} alt="image" width="100">
                    </a>
                    @endif
                </div>

            </div>

            <div class="width-50-module form-group{{ $errors->has('category') ? ' has-error' : ''}}">
                {!! Form::label('category', '* Categories:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::select('category[]', $category, isset($showCategory) ? $showCategory : [], ['id' =>
                    'facilities','class' => 'form-control
                    tags' ,'multiple' => 'multiple']) !!} {!! $errors->first('category', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="width-50-module form-group{{ $errors->has('runningTime') ? ' has-error' : ''}}">
                {!! Form::label('runningTime', '* Running Time: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('runningTime', isset($show->runningTime) ? $show->runningTime : '', ['class' => 'form-control
                    input-sm']) !!} {!! $errors->first('runningTime', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            @if(isset($show))
            <div class="width-50-module form-group{{ $errors->has('showLink') ? ' has-error' : ''}}">
                {!! Form::label('showLink', 'Show Link: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('showLink', $show->showLink, ['class' => 'form-control input-sm', 'disabled' =>'disabled']) !!}
                </div>
            </div>
            @else
            <div class="width-50-module form-group{{ $errors->has('location') ? ' has-error' : ''}}">
                {!! Form::label('location', '* Location For Show: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::select('location',$locationcodes,isset($show->location) ? $show->location : '',['class' =>
                    'form-control tags' ]) !!} {!! $errors->first('location', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            @endif
        </fieldset>

        <fieldset>
            <legend>Meta Details</legend>
            <div class="form-group{{ $errors->has('metaTitle') ? ' has-error' : ''}}">
                {!! Form::label('metaTitle', '* Meta Title: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('metaTitle', isset($showMetadata->meta_title) ? $showMetadata->meta_title : '', ['class' =>
                    'form-control input-sm']) !!} {!! $errors->first('metaTitle', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('metaDescription') ? ' has-error' : ''}}">
                {!! Form::label('metaDescription', '* Meta Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('metaDescription',isset($showMetadata->meta_description) ?
                    $showMetadata->meta_description : '', ['class' => 'summernote form-control input-sm',
                    'id'=>'metaDescription']) !!} {!! $errors->first('metaDescription', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </fieldset>

        <fieldset>
            <legend>Booking & Offers Details</legend>

            <div class="width-50-module form-group{{ $errors->has('bookingStarts') ? ' has-error' : ''}}">
                {!! Form::label('bookingStarts', '* Booking Starts Date:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    <input type="date" id="bookingStarts" class="form-control" name="bookingStarts" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" data-title="Booking Starts"
                        value="{{isset($show->bookingStarts) ? $show->bookingStarts : ''}}">
                    {!! $errors->first('bookingStarts', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('bookingEnds') ? ' has-error' : ''}}">
                {!! Form::label('bookingEnds', '* Booking Ends Date:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    <input type="date" id="bookingEnds" class="form-control" name="bookingEnds" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" data-title="Booking Ends"
                        value="{{isset($show->bookingEnds) ? $show->bookingEnds : ''}}">
                    {!! $errors->first('bookingEnds', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('bookingOpen') ? ' has-error' : ''}}">
                {!! Form::label('bookingOpen', '* Booking Open Date:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    <input type="date" id="bookingOpen" class="form-control" name="bookingOpen" data-toggle="tooltip"
                        data-trigger="hover" data-placement="top" data-title="Booking Open"
                        value="{{isset($show->bookingOpen) ? $show->bookingOpen : ''}}">
                    {!! $errors->first('bookingOpen', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            @if(isset($show))
            <div class="width-30-module form-group{{ $errors->has('offer_ids') ? ' has-error' : ''}}">
                {!! Form::label('offer_ids', 'Offers:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {{-- <select name="offer_ids[]" class="form-control tags offer_id_selectbox" id="offer_id" multiple>
                                        @foreach ($offer as $key => $item)
                                            <option value="{{$key}}"
                    {{ (isset($showoffer) && in_array($key,$showoffer)) ? 'selected' : '' }}>{{$item}}</option>
                    @endforeach
                    </select> --}}
                    {!! Form::select('offer_ids[]', $offer, isset($showoffer) ? $showoffer : [], ['id' => 'offer_ids','class' =>
                    'form-control tags offer_id_selectbox' ,'multiple' => 'multiple']) !!} {!! $errors->first('offer_ids', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="width-20-module">
                <button type="button" class="plusoffers btn"><i class="fa fa-plus"></i></button>
                <button type="button" class="minusoffers btn"><i class="fa fa-trash"></i></button>
                <button type="button" class="editoffers btn"><i class="ft-edit"></i></button>
            </div>
            @endif
            @if(isset($showsingleOfferSelection) && count($showsingleOfferSelection) > 0)
            <div class="form-group{{ $errors->has('offer_id') ? ' has-error' : ''}}">
                {!! Form::label('offer_id', 'Single Offer for List: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::select('offer_id', $showsingleOfferSelection, null , ['class' => 'form-control
                    tags offer_id_single_selectbox' ]) !!} {!! $errors->first('offer_id', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
            @endif
        </fieldset>

        <fieldset>
            <legend>Gallery</legend>

            <div class="form-group{{ $errors->has('youtube_link') ? ' has-error' : ''}}">
                {!! Form::label('youtube_link', 'Youtube Link: ', ['class' => 'col-sm-3 control-label']) !!}
                {!! Form::checkbox('iframe_display', null ,(isset($show->iframe_display) && $show->iframe_display == 1 ) ? true
                :
                false ,
                ['class' => 'control-label youtube_checkbox']) !!}
                <div class="col-sm-12">
                    {!! Form::text('youtube_link', isset($show->youtube_link) ? $show->youtube_link : '', ['class' =>
                    'form-control
                    input-sm youtube_link'])
                    !!} {!! $errors->first('youtube_link', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('showImages', 'Show Gallery Images: ', ['class' => 'col-sm-3 control-label']) !!}
                <div id="showImagesMultiple">
                </div>
            </div>

            @if(isset($show->showImages) && count($show->showImages) > 0)
            <div class="form-group imgGlry">
                <ul class="img-grp">

                </ul>
            </div>
            @endif

        </fieldset>

        <fieldset>
            <legend>Schedules of Show</legend>
                @php
                $daysArray = ['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
                @endphp
                @foreach ($daysArray as $daysArrayValue)
                    <div class="inner-mrg-10">
                        <div class="width-10-module form-group">
                            {!! Form::text('scheduleDays['.$daysArrayValue.']', $daysArrayValue , ['class' => 'form-control
                            input-sm' ,'disabled' => 'disabled']) !!}
                        </div>
                        @for ($i = 0; $i < 3; $i++)
                        <div class="width-30-module scheduleDays form-group scheduleDaysdivs">
                            {!! Form::time('scheduleDays['.$daysArrayValue.']['.$i.']', (isset($newschedule[$daysArrayValue][$i]) ?
                            $newschedule[$daysArrayValue][$i] : null) , ['class' => 'form-control input-sm' ]) !!}
                        </div>
                        @endfor
                        {{-- <div class="col-md-1 plusSchedule" id={{$daysArrayValue}}>
                        <button type="button" class="btn"><i class="fa fa-plus"></i></button>
                        </div> --}}
                    </div>
                @endforeach
        </fieldset>

        <fieldset>
            <legend>Description of Show</legend>
            <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                {!! Form::label('description', '* Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('description',isset($show->description) ? $show->description : '', ['class' => 'form-control
                    input-sm',
                    'id'=>'description']) !!} {!! $errors->first('description', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('ageRestriction') ? ' has-error' : ''}}">
                {!! Form::label('ageRestriction', '* Age Restriction: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('ageRestriction',isset($show->ageRestriction) ? $show->ageRestriction : '', ['class' =>
                    'form-control input-sm',
                    'id'=>'ageRestriction']) !!} {!! $errors->first('ageRestriction', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('summary') ? ' has-error' : ''}}">
                {!! Form::label('summary', '* Summary: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('summary', isset($show->summary) ? $show->summary : '' , ['class' => 'form-control input-sm',
                    'id'=>'summary']) !!} {!! $errors->first('summary', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </fieldset>

    </div>
</div>

@push('js')
    <script>
        $(".tags").select2({tags: true, width: '100%'});
        $('#description').summernote({
            height: 250
        });
        $('#ageRestriction').summernote({ height: 250 });
        $('#metaDescription').summernote({ height: 250 });
        $('#summary').summernote({
            height: 250
        });


        $(document).on('click', '.removeTextBox', function(e) {
            e.preventDefault();
            var ele = $(this).parent().parent();
            ele.remove();
        });


        $( ".plusSchedule" ).on( "click",function(){
            $( '<div class="col-md-2 scheduleDays" ><div class="form-group scheduleDaysdivs"><input class="form-control input-sm" name="scheduleDays['+this.id+'][]" type="time"><i class="fa fa-close btn removeTextBox"></i></div></div>' ).insertBefore( this );
        });



        $( "input[type=checkbox]" ).on( "click",function(){
            if($(this).prop("checked") == true){
                $('.youtube_link').show();
            }
            else if($(this).prop("checked") == false){
                $('.youtube_link').hide();
            }
        });
        $(function(){
            if($('input[name=iframe_display]').prop("checked") == true){
            $('.youtube_link').show();
            }
            else if($('input[name=iframe_display]').prop("checked") == false){
            $('.youtube_link').hide();
            }
			$("#showImagesMultiple").spartanMultiImagePicker({
				fieldName:  'showImages[]',
                dropFileLabel: 'Drop file here',
                allowedExt: 'mp4|png|jpg|jpeg',
                disableImageResize: false,
                // imageMaxWidth: 800,                   // 800 is an example value - no default
                // imageMaxHeight: 600,                  // 600 is an example value - no default
                // maxFileSize: 20000000,                // 20MB is an example value - no default
                // loadImageMaxFileSize: 20000000,
                maxCount : 5,
                groupClassName : 'col-sm-1',
				directUpload : {
					loaderIcon: '<i class="fas fa-sync fa-spin"></i>',
					success : function(data, textStatus, jqXHR){
					},
					error : function(jqXHR, textStatus, errorThrown){
					}
				}
			});
		});
    </Script>
@endpush

