@extends('layouts.admin')
@section('title',"Edit Show")

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/show') }}">show</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="col-md-6">
                            <div class="form-group col-sm-6">
                                <a href="{{ url('/admin/show') }}" title="Back">
                                    <button class="btn btn-space btn-warning">Back</button>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
           {!! Form::model($show, ['method' => 'PATCH','url' => ['/admin/show', $show->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                @include ('admin.show.form')
                <div class="col-md-6">
                <div class="form-group col-sm-4">
                    {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary']) !!}
                </div>
                </div>
            {!! Form::close() !!}
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>


        <div class="modal fade" id="offeraddmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Add Offer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="add_offer_form" name="add_offer_form">
                                        @if(isset($show))

                                        <input name="showLink" id="showLink" type="hidden" value="{{$show->showLink}}"> {{csrf_field()}}
                                        @endif

                                        <span class="errmsgsoffer"></span>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer ID: </label>
                                                        <div class="col-sm-12">
                                                            <input type="number" name="offer_id" id="add_offer_id" class="form-control">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Name: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="title" id="add_title" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Start Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="start_date" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer End Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="end_date" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer valid: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="valid" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer type: </label>
                                                        <div class="col-sm-12">
                                                            <select name="type" class="form-control">
                                                                    <option value="single_facevalue">Single Facevalue</option>
                                                                    <option value="multiple_facevalue">Multiple Facevalue</option>
                                                                    <option value="no_booking_multiple_facevalue">No Booking Multiple Facevalue</option>
                                                                </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer facevalue: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="facevalue" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer price: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="price" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer description: </label>
                                                        <div class="col-sm-12">
                                                            <textarea name="description" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="offereditmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Edit Offer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="edit_offer_form" name="edit_offer_form">
                                        {{csrf_field()}}
                                        @if(isset($show))

                                        <input name="showLink" id="showLink" type="hidden" value="{{$show->showLink}}">
                                        @endif

                                        <span class="errmsgsoffer"></span>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <input type="hidden" name="offer_id" id="edit_offer_id">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Name: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="title" id="edit_title" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer Start Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="start_date" id="edit_start_date" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer End Date: </label>
                                                        <div class="col-sm-12">
                                                            <input type="date" name="end_date" id="edit_end_date"class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer valid: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="valid"  id="edit_valid" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer type: </label>
                                                        <div class="col-sm-12">
                                                            <selectaria-invalid name="type" id="edit_type" class="form-control">
                                                                <option value="single_facevalue">Single Facevalue</option>
                                                                <option value="multiple_facevalue">Multiple Facevalue</option>
                                                                <option value="no_booking_multiple_facevalue">No Booking Multiple Facevalue</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer facevalue: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="facevalue" id="edit_facevalue" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer price: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="price" id="edit_price" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Offer description: </label>
                                                        <div class="col-sm-12">
                                                            <textarea name="description" id="edit_description" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                                Submit
                                                            </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="offerminusmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Delete Offer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="minus_offer_form" name="minus_offer_form">

                                        <span class="errmsgsoffer"></span>

                                        <div class="form-group prepend-top">
                                            <div class="row offer_div">

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="imageEditModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="account_form_model_lable">Edit Image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="bidder_list">
                            <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                                <div class=" clearfix details-container details-port-container">
                                    <form id="edit_image_form" name="edit_image_form">
                                        {{csrf_field()}} @if(isset($show))

                                        <input name="showLink" id="showLink" type="hidden" value="{{$show->showLink}}"> @endif

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <input type="hidden" name="img_id" id="img_id">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Title: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="title" id="title" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Alter: </label>
                                                        <div class="col-sm-12">
                                                            <input type="text" name="alt" id="alt" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-sm-12 control-label">* Orderby: </label>
                                                        <div class="col-sm-12">
                                                            <input type="number" name="orderBy" id="orderBy" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group prepend-top">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('js')
<script>
        $(".tags").select2({tags: true, width: '100%'});
        $('#description').summernote({
            height: 250
        });
        $('#metaDescription').summernote({ height: 250 });
        $('#summary').summernote({
            height: 250
        });

        $(document).ready(function (e) {
           listdrow();
           listForImage();
        });
        $(document).on('click', '.plusoffers', function (e) {
            $("#add_offer_form")[0].reset();
            $('.errmsgsoffer').html('');
            $('#offeraddmodel').modal('show');
        });

        $(document).on('click', '.minusoffers', function (e) {
            listdrow();
            $('#offerminusmodel').modal('show');
            $("#minus_offer_form")[0].reset();
            $('.errmsgsoffer').html('');
        });
         $(document).on('click', '.editoffers', function (e) {
            listdrow();
            $('#offerminusmodel').modal('show');
        });



        function listdrow() {
            $('.offer_div').html('');
            $('.offer_id_selectbox').html('');
            $('.offer_id_single_selectbox').html('');
            var show_link = '{{$show->showLink}}';
            var url = '{{url("/admin/offerlist/")}}/'+show_link;
            var method = "get";
            $.ajax({
                type: method,
                url: url,
                beforeSend: function () {
                },
                success: function (formdata)
                {
                    if (formdata.offersStr != '') {
                        $('.offer_id_selectbox').html(formdata.offersStr);
                        $('.offer_id_single_selectbox').html(formdata.offerSingleStr);
                        $('.minusoffers').show();
                        $('.editoffers').show();
                    }
                    else
                    {
                        $('.minusoffers').hide();
                        $('.editoffers').hide();
                    }

                    if (formdata.offerDeletedStr != '') {
                        $('.offer_div').append(formdata.offerDeletedStr);
                    }
                    else{
                        $('.minusoffers').hide();
                        $('.editoffers').hide();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

        $(document).on('click', '.imgRemove', function (e) {
            e.preventDefault();
            id = $(this).data('imgid');
            url = $(this).data('url');
            url = url + "/" + id;
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        type: "get",
                        url: url,
                        data: {
                            from_index: true
                        },
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (data) {
                            listForImage();
                            swal({
                                title: 'Success!',
                                type: 'success'
                            });
                            id = url = null;
                        },
                        error: function (xhr, status, error) {
                            swal("Cancel", "", "error");
                        }
                    });



                } else {
                    swal("Cancel", "", "error");
                }
            });
        });


        $('#add_offer_form').validate(
            {
                rules: {
                    title: { required: true },
                    type: { required: true },
                    valid: { required: true },
                    start_date: { required: true },
                    end_date: { required: true },
                    facevalue: { required: true , number: true},
                    price: { required: true , number: true },
                    description: { required: true },
                    offer_id: { required: true }
                },
                messages: {
                    title: { required:  'Please enter title', },
                    type: { required: 'Please Select type', },
                    valid: { required: 'Please enter valid', },
                    start_date: { required: 'Please enter start date', },
                    end_date: { required: 'Please enter end date', },
                    facevalue: { required: 'Please enter facevalue', },
                    price: { required: 'Please enter price', },
                    description: { required: 'Please enter description', },
                    offer_id: { required: 'Please enter offer id', }

                },
                errorPlacement: function(error, element)
                {
                    error.insertAfter( element );
                },
                submitHandler: function (form) {
                    var addformdata  =$('#add_offer_form').serialize();
                    var url = '{{url('/admin/offeradd')}}';
                    var method = "post";
                    add_offer_id =$('#add_offer_id').val();
                    title = $('#add_title').val();
                    $.ajax({
                        type: method,
                        url: url,
                        data: $('#add_offer_form').serialize(),
                        beforeSend: function () {
                        },
                        success: function (formdata)
                        {

                            if(formdata.code == '200')
                            {
                                $('#offeraddmodel').modal('hide');
                                $('.minusoffers').show();
                                $('.editoffers').show();
                                listdrow();
                            }
                            else
                                $('.errmsgsoffer').html(formdata.message);
                        },
                        error: function (error) {
                            $('#offeraddmodel').modal('hide');
                        // toastr.error(data.message);
                        }

                    });
                    return false;

                }
        });

        $('#edit_offer_form').validate(
        {
                rules: {
                    title: { required: true },
                    valid: { required: true },
                    start_date: { required: true },
                    end_date: { required: true },
                    facevalue: { required: true , number: true},
                    price: { required: true , number: true },
                    description: { required: true }
                },
                messages: {
                    title: { required:  'Please enter title', },
                    valid: { required: 'Please enter valid', },
                    start_date: { required: 'Please enter start date', },
                    end_date: { required: 'Please enter end date', },
                    facevalue: { required: 'Please enter facevalue', },
                    price: { required: 'Please enter price', },
                    description: { required: 'Please enter description', }
                },
                errorPlacement: function(error, element)
                {
                    error.insertAfter( element );
                },
                submitHandler: function (form) {
                    var editformdata  =$('#edit_offer_form').serialize();
                    var url = '{{url('/admin/offerupdate')}}';
                    var method = "post";
                    $.ajax({
                        type: method,
                        url: url,
                        data: $('#edit_offer_form').serialize(),
                        beforeSend: function () {
                        },
                        success: function (formdata)
                        {
                            if(formdata.code == '200')
                            {
                                $('#offereditmodel').modal('hide');
                                $('.minusoffers').show();
                                $('.editoffers').show();
                                listdrow();
                            }
                            else
                                $('.errmsgsoffer').html(formdata.message);
                        },
                        error: function (error) {
                            $('#offereditmodel').modal('hide');
                        // toastr.error(data.message);
                        }

                    });
                    return false;

                }
        });


        $('#edit_image_form').validate(
        {
            rules: {
                alt: { required: true },
                title: { required: true },
                orderBy: { required: true }
            },
            messages: {
                alt: { required:  'Please enter alt', },
                title: { required:  'Please enter title', },
                orderBy: { required: 'Please enter order By', }
            },
            errorPlacement: function(error, element)
            {
                error.insertAfter( element );
            },
            submitHandler: function (form) {
                var url = '{{url('/admin/showImageupdate')}}';
                var method = "post";
                $.ajax({
                    type: method,
                    url: url,
                    data: $('#edit_image_form').serialize(),
                    beforeSend: function () {
                    },
                    success: function (formdata)
                    {
                        if(formdata.code == '200')
                        {
                            $('#imageEditModel').modal('hide');
                            swal("", formdata.message, "success");
                            listForImage();
                        }
                        else
                        swal("Cancel", formdata.message, "error");
                    },
                    error: function (error) {
                        $('#offereditmodel').modal('hide');
                        swal("Cancel", "", "error");
                    }

                });
                return false;

            }
        });

        $(document).on('click', '.offer_btn', function (e) {

            // var offer_btn_div = $(".offer_div").length;

            var offer_id = $(this).data('id');
            var div = $(this).parent('div');
            var url = '{{url("/admin/offerdelete/")}}/'+offer_id;
            var method = "get";
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: method,
                        url: url,
                        beforeSend: function () {
                        },
                        success: function (formdata)
                        {

                            if(formdata.code == '200')
                            {
                                listdrow();
                                $('#offerminusmodel').modal('hide');
                                swal({ title: 'Success!', type: 'success' });

                                //$(".offer_id_selectbox").append('<option value="'+add_offer_id+'">'+title+'</option>');
                            }
                            else
                                swal("Cancel", formdata.message, "error");
                        },
                        error: function (error) {
                            $('#offerminusmodel').modal('hide');
                        }

                    });
                } else {
                    swal("Cancel", "", "error");
                }
            });

        });

        $(document).on('click', '.edit_offer_btn', function (e) {

            // var offer_btn_div = $(".offer_div").length;

            var offer_id = $(this).data('id');
            var div = $(this).parent('div');
            var url = '{{url("/admin/offerdetail/")}}/'+offer_id;
            var method = "get";

            $.ajax({
                type: method,
                url: url,
                beforeSend: function () {
                },
                success: function (formdata)
                {
                    console.log(formdata.data.facevalue);
                    if(formdata.code == '200')
                    {
                        $('#edit_offer_id').val(formdata.data.offer_id);
                        $('#edit_title').val(formdata.data.title);
                        $('#edit_start_date').val(formdata.data.start_date);
                        $('#edit_end_date').val(formdata.data.end_date);
                        $('#edit_valid').val(formdata.data.valid);
                        // $( "#edit_type" ).val([formdata.data.type]);
                        // $("#edit_type").val(formdata.data.type).attr("selected","selected");
                        $('#edit_facevalue').val(formdata.data.facevalue);
                        $('#edit_price').val(formdata.data.price);
                        $('#edit_description').val(formdata.data.description);

                        $('#offerminusmodel').modal('hide');
                        $('#offereditmodel').modal('show');
                    }
                    else
                        swal("Cancel", formdata.message, "error");
                },
                error: function (error) {
                    $('#offerminusmodel').modal('hide');
                }

            });

        });

        $(document).on('click', '.imgEdit', function (e) {
            // var offer_btn_div = $(".offer_div").length;

            var imgid = $(this).data('imgid');
            var url = '{{url("/admin/showImageDetail/")}}/'+imgid;
            var method = "get";
            $.ajax({
                type: method,
                url: url,
                beforeSend: function () {
                },
                success: function (formdata)
                {
                    if(formdata.code == '200')
                    {
                        $('#alt').val(formdata.data.alt);
                        $('#title').val(formdata.data.title);
                        $('#img_id').val(formdata.data.id);
                        $('#orderBy').val(formdata.data.orderBy);
                        $('#imageEditModel').modal('show');
                    }
                    else
                        swal("Cancel", formdata.message, "error");
                },
                error: function (error) {
                    $('#offerminusmodel').modal('hide');
                }

            });

        });


        function listForImage() {
            $('.img-grp').html('');
            var show_link = '{{$show->showLink}}';
            var url = '{{url("/admin/showImageList/")}}/' + show_link;
            var method = "get";
            $.ajax({
                type: method,
                url: url,
                beforeSend: function () {},
                success: function (formdata) {
                    if (formdata.data != '') {
                        $('.img-grp').html(formdata.data);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }

</Script>

@endpush