
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
            {!! Form::label('name', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('name', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!} {!! $errors->first('name',
                '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('value') ? ' has-error' : ''}}">
            {!! Form::label('value', '* Value: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('value', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!} {!! $errors->first('value',
                '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>