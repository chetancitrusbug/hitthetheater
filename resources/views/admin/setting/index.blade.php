@extends('layouts.admin')
@section('title',"Setting")
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/setting') }}">Setting</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <a href="{{ url('/admin/setting/create') }}" title="Create">
                                <button class="btn btn-space btn-success">Create</button>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table id="setting-table" class="table table-striped table-bordered default-ordering">
                                <thead class="thead-dark">
                                <tr>
                                <th>Name</th>
                                <th>Value</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                        </table>
          </div>
                    </div>
                </div>
            </div>


            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/setting-data') }}";
        var edit_url = "{{ url('/admin/setting') }}";

        datatable = $('#setting-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            stateSave: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": true},
                { data: 'value',name : 'value',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e = v = d = m = "";
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info' title='Edit' ><i class='ft-edit' ></i></button></a>&nbsp;";

                        //d = "<a href='{{url('admin/setting')}}"+"/"+o.id+"' class='btn btn-danger del-item' data-id="+o.id+" data-url={{url('admin/venue')}} data-msg='setting' data-backdrop='static' data-keyboard='false'><i class='ft-delete' aria-hidden='true'></i></a>&nbsp;";

                        return m+v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/setting')}}/" + id;
            var r = confirm("Are you sure you want to Delete Setting?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $(document).on('change', '.status-change', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var table = $(this).data('table');

            url = "{{url('admin/change-status')}}/" + table + '/' + status + '/' + id;

            $.ajax({
                url: url ,
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });
    </Script>
@endpush