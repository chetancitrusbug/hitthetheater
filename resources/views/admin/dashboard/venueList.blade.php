<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <span class="panel-subtitle">

                    <a href="{{ url('/admin/dashboard/importVenue') }}" title="Import Data">
                        <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                    </a>

                    {{-- <button type="button" class="btn btn-success" id="import_venue_details"> Get Details </button> --}}

                </span>
            </div>
            <div class="panel-body">
                <table id="Venue-table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Location</th>
                            <th>Venue Code</th>
                            <th>Post Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tempVenue as $item)
                        <tr>
                            <td>{{$item->venueTitle}}</td>
                            <td>{{(isset($item->location->locationTitle) ? $item->location->locationTitle : '')}}</td>
                            <td>{{$item->venueCode}}</td>
                            <td>{{$item->postcode}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
