@extends('layouts.admin')
@section('title',"Import Location")
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="#">Import Location</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $moduleName }} - Import Location</h4>
            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
            <div class="heading-elements">
                <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                </ul>
            </div>
        </div>

        <div class="card-content collapse show">
            <div class="card-body">
                <div class="panel-heading">
                    {{-- <p>Please check the below record to import location.</p>
                    <p class="note">If you do not select any value than Location doesn't import.</p> --}}
                </div>

            <div class="panel-body">
                {!! Form::open(['url' => '/admin/dashboard/solveConflictLocation', 'class' => 'form-horizontal group-border-dashed','id'
                => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                <div class="col-12 pull-right">
                    <button type="button" class="btn pull-left dropdown_open_all margin-bottom-15">Expand All</button><button type="button" class="btn pull-left dropdown_minus_all margin-bottom-15">Expand Close</button>
                    {!! Form::submit('Submit', ['class' => 'btn btn-space btn-success pull-right margin-bottom-15']) !!}
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th> Location Code</th>
                            <th> <input name="all" type="radio" class="btn check_all_today_records"> Check All Today Records </th>
                            <th> <input name="all" type="radio" class="btn check_all_yesterday_records"> Check All Yesterday Records </th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($locationCodes as $codekey => $code)
                    @php $existClass = ($newArray[$code]['existrecord'] == 0 || $oldArray[$code]['existrecord'] == 0) ? 'existRecColor' : ''; @endphp
                    <tr class={{$existClass}}>
                        <td><strong> {{$code}}</strong></td>
                        <td colspan="2"><button type="button" class="btn pull-right dropdown_open dropdown_open_{{$code}}" id="{{$code}}"><i class="fa fa-plus"></i></button><button type="button" class="btn pull-right dropdown_minus dropdown_minus_{{$code}}" id="{{$code}}"><i class="fa fa-minus"></i></button></td>
                    </tr>
                    <tr class="location_{{$code}} location_dropdown_close">
                        <td colspan="3">
                            <table class="table innertable">
                                <thead>
                                    <tr class={{$existClass}}>
                                        <th> Field</th>
                                        <th> Today</th>
                                        <th> Yesterday</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php

                                    $changeNewArrColor = ((isset($newArray[$code]) && isset($newArray[$code]['updatedArr'])) &&
                                    $newArray[$code]['updatedArr'] == 1 ? 'changecolor' : '' );
                                    $changeOldArrColor = ((isset($oldArray[$code]) && isset($oldArray[$code]['updatedArr']))
                                    && $oldArray[$code]['updatedArr'] == 1 ? 'changecolor' : '' );

                                    // dd($changeNewArrColor);
                                    @endphp
                                   <tr class={{$existClass}}>
                                       <td width="20%"><strong>Title</strong></td>
                                        <td width="40%" class={{$changeNewArrColor}}>
                                            @if(isset($newArray[$code]->locationTitle))
                                            <input type="radio" class="today_record" name="data[{{$code}}-locationTitle]" id="newdata[{{$code}}-locationTitle]" value="{{$newArray[$code]->locationTitle}}">
                                            <label class="description" for="newdata[{{$code}}-locationTitle]">{{$newArray[$code]->locationTitle}}</label>
                                            @endif
                                        </td>
                                        <td width="40%" class={{$changeOldArrColor}}>
                                            @if(isset($oldArray[$code]->locationTitle))
                                            <input type="radio" class="yesterday_record" name="data[{{$code}}-locationTitle]" id="olddata[{{$code}}-locationTitle]" value="{{$oldArray[$code]->locationTitle}}">
                                            <label class="description" for="olddata[{{$code}}-locationTitle]">{{$oldArray[$code]->locationTitle}}</label>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-12 pull-right margin-bottom-15">
                    {!! Form::submit('Submit', ['class' => 'btn btn-space btn-success pull-right']) !!}
                    {!! Form::reset('Reset All', ['class' => 'btn btn-space btn-primary pull-left reset-all-btn-mrgn']) !!}
                </div>

                {!! Form::close() !!}
            </div>
            </div>
        </div>
        </div>
        </div>
</div>


</div>
</div>
</div>
</div>
@endsection
@push('js')
<script>

    $(document).on('click','.dropdown_open',function () {
        $('.location_dropdown_close').hide();
        var id = this.id;
        $('.dropdown_minus').hide();
        $('.dropdown_open').show();
        $('.location_'+id).show();
        $('.dropdown_open_'+id).hide();
        $('.dropdown_minus_'+id).show();
    });

    $(document).on('click','.dropdown_minus',function () {
        var id = this.id;
        // $('.dropdown_minus').hide();
        // $('.dropdown_open').show();
        $('.location_'+id).hide();
        $('.dropdown_open_'+id).show();
        $('.dropdown_minus_'+id).hide();

    });

    $(document).on('click','.dropdown_open_all',function () {
        $('.location_dropdown_close').show();
        $('.dropdown_minus_all').show();
        $('.dropdown_open_all').hide();
        $('.dropdown_minus').show();
        $('.dropdown_open').hide();
    });

    $(document).on('click','.dropdown_minus_all',function () {
        $('.location_dropdown_close').hide();
        $('.dropdown_open_all').show();
        $('.dropdown_minus_all').hide();
        $('.dropdown_minus').hide();
        $('.dropdown_open').show();
    });
</script>
@endpush
