<html>
<body>
<table style="border: 1px #000000 solid;">
    <thead>
        <tr>
            <th colspan="3"> <strong>Venue Code</strong></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($codes as $codekey => $code)
        @php

        if(isset($newArray[$code]['existrecord']) && $newArray[$code]['existrecord'] == 0)
        {
            $existClass = 'color: red;';
        }
        else if(isset($oldArray[$code]['existrecord']) && $oldArray[$code]['existrecord'] == 0)
        {
            $existClass = 'color: red;';
        }
        else {
            $existClass = '';
        }

        @endphp
        <tr style="{{$existClass}}">
            <td colspan="3"><strong> {{$code}}</strong></td>
        </tr>
        <tr class="venue_{{$code}} venue_dropdown_close">
            <td colspan="3">
                <table style="border: 1px #000000 solid;" class="table innertable">
                    <thead>
                        <tr style="{{$existClass}}">
                            <th style="border: 1px #000000 solid;"> Field</th>
                            <th style="border: 1px #000000 solid;"> Today</th>
                            <th style="border: 1px #000000 solid;"> Yesterday</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($fields as $fieldKey => $fieldValue)
                        @php

                        $changeNewArrColor = ((isset($newArray[$code]) && isset($newArray[$code]['updatedArr'])) && in_array($fieldKey,$newArray[$code]['updatedArr']) ? 'color: red;border: 1px #000000 solid;' : 'border: 1px #000000 solid;' );
                        $changeOldArrColor = ((isset($oldArray[$code]) && isset($oldArray[$code]['updatedArr'])) &&in_array($fieldKey,$oldArray[$code]['updatedArr']) ? 'color: red;border: 1px #000000 solid;' : 'border: 1px #000000 solid;' );

                        // dd($changeNewArrColor);
                        @endphp
                            @if ($fieldKey == 'facilities' || $fieldKey == 'transportinfo' || $fieldKey == 'metadata')
                        {{-- @if(isset($newArray[$code][$fieldKey]) && $fieldKey == 'metadata')
                            {{$newArray[$code][$fieldKey]}}
                            @php exit; @endphp
                            @endif --}}
                                <tr style="{{$existClass}}">
                                    <td width="20%" style="border: 1px #000000 solid;"><strong>{{$fieldValue}}</strong></td>
                                    <td width="40%" style="{{$changeNewArrColor}}">
                                        @if(isset($newArray[$code][$fieldKey]))
                                            <label class="description" for="newdata[{{$code}}][{{$fieldKey}}]"> {!!html_entity_decode($newArray[$code][$fieldKey])!!} </label>
                                        @endif
                                    </td>
                                    <td width="40%" style="{{$changeOldArrColor}}">
                                        @if(isset($oldArray[$code][$fieldKey]))
                                            <label class="description" for="olddata[{{$code}}][{{$fieldKey}}]"> {!!html_entity_decode($oldArray[$code][$fieldKey])!!} </label>
                                        @endif
                                    </td>
                                </tr>
                            @else
                                <tr style="{{$existClass}}">
                                    <td width="20%" style="border: 1px #000000 solid;"><strong>{{$fieldValue}}</strong></td>
                                    <td width="40%" style="{{$changeNewArrColor}}">
                                        @if(isset($newArray[$code][$fieldKey]))
                                            <label class="description" for="newdata[{{$code}}][{{$fieldKey}}]"> {!!html_entity_decode($newArray[$code][$fieldKey])!!} </label>
                                        @endif
                                    </td>
                                    <td width="40%" style="{{$changeOldArrColor}}">
                                        @if(isset($oldArray[$code][$fieldKey]))
                                            <label  class="description"for="olddata[{{$code}}][{{$fieldKey}}]"> {!!html_entity_decode($oldArray[$code][$fieldKey])!!} </label>
                                        @endif
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</body>
</html>
