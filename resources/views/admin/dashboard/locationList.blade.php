<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <span class="panel-subtitle">
                    <a href="{{ url('/admin/dashboard/importLocation') }}" title="Import Data">
                        <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                    </a>
                </span>
            </div>
            <div class="panel-body">
                <table id="table1" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tempLocation as $item)
                        <tr>
                            <td>{{$item->locationTitle}}</td>
                            <td>{{$item->locationCode}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>