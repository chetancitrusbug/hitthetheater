@extends('layouts.admin')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                        <div class="card-body">
    {{-- <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            <div class="my-1 text-center">
                                <div class="card-header mb-2 pt-0">
                                    <h5 class="primary">Steps</h5>
                                    <h3 class="font-large-2 text-bold-200">3,261</h3>
                                </div>
                                <div class="card-content">
                                    <input type="text" value="65" class="knob hide-value responsive angle-offset" data-angleOffset="40" data-thickness=".15"
                                        data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true"
                                        data-fgColor="#00B5B8" data-knob-icon="icon-trophy">
                                    <ul class="list-inline clearfix pt-1 mb-0">
                                        <li class="border-right-grey border-right-lighten-2 pr-2">
                                            <h2 class="grey darken-1 text-bold-400">65%</h2>
                                            <span class="primary">
                                  <span class="ft-arrow-up"></span> Completed</span>
                                        </li>
                                        <li class="pl-2">
                                            <h2 class="grey darken-1 text-bold-400">35%</h2>
                                            <span class="danger">
                                  <span class="ft-arrow-down"></span> Remaining</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            <div class="my-1 text-center">
                                <div class="card-header mb-2 pt-0">
                                    <h5 class="danger">Distance</h5>
                                    <h3 class="font-large-2 text-bold-200">7.6
                                        <span class="font-medium-1 grey darken-1 text-bold-400">mile</span>
                                    </h3>
                                </div>
                                <div class="card-content">
                                    <input type="text" value="70" class="knob hide-value responsive angle-offset" data-angleOffset="0" data-thickness=".15" data-linecap="round"
                                        data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true" data-fgColor="#FF7588"
                                        data-knob-icon="icon-pointer">
                                    <ul class="list-inline clearfix pt-1 mb-0">
                                        <li>
                                            <h2 class="grey darken-1 text-bold-400">10</h2>
                                            <span class="danger">Miles Today's Target</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12 border-right-blue-grey border-right-lighten-5">
                            <div class="my-1 text-center">
                                <div class="card-header mb-2 pt-0">
                                    <h5 class="warning">Calories</h5>
                                    <h3 class="font-large-2 text-bold-200">4,025
                                        <span class="font-medium-1 grey darken-1 text-bold-400">kcal</span>
                                    </h3>
                                </div>
                                <div class="card-content">
                                    <input type="text" value="81" class="knob hide-value responsive angle-offset" data-angleOffset="20" data-thickness=".15"
                                        data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true"
                                        data-fgColor="#FFA87D" data-knob-icon="icon-energy">
                                    <ul class="list-inline clearfix pt-1 mb-0">
                                        <li>
                                            <h2 class="grey darken-1 text-bold-400">5000</h2>
                                            <span class="warning">kcla Today's Target</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-12">
                            <div class="my-1 text-center">
                                <div class="card-header mb-2 pt-0">
                                    <h5 class="success">Heart Rate</h5>
                                    <h3 class="font-large-2 text-bold-200">102
                                        <span class="font-medium-1 grey darken-1 text-bold-400">BPM</span>
                                    </h3>
                                </div>
                                <div class="card-content">
                                    <input type="text" value="75" class="knob hide-value responsive angle-offset" data-angleOffset="20" data-thickness=".15"
                                        data-linecap="round" data-width="130" data-height="130" data-inputColor="#BABFC7" data-readOnly="true"
                                        data-fgColor="#16D39A" data-knob-icon="icon-heart">
                                    <ul class="list-inline clearfix pt-1 mb-0">
                                        <li>
                                            <h2 class="grey darken-1 text-bold-400">125</h2>
                                            <span class="success">BPM Highest</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
                        <div class="row">
                            <!--Default Tabs-->
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"></div>
                                    <div class="tab-container ">
                                        <ul class="nav nav-tabs">
                                            <li><a class="active" href="#location" data-toggle="tab">Location</a></li>
                                            {{-- <li><a href="#category" data-toggle="tab">Category</a></li> --}}
                                            <li><a href="#venue" data-toggle="tab">Venue</a></li>
                                            <li><a href="#show" data-toggle="tab">Show</a></li>
                                            {{--<li><a href="#messages" data-toggle="tab">Messages</a></li> --}}
                                        </ul>
                                        <div class="tab-content pdng-20px">
                                            <div id="location" class="tab-pane active cont">
                                                <a href="{{ url('/admin/dashboard/importLocation') }}" title="Import Data">
                                                    <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                                                </a>
                                                {{-- <button type="button" class="btn btn-success" id="import_location"> Import </button> --}}

                                                @if(isset($latestLocation->created_at))
                                                <h4 class='padding-top-10px'>Last Import on {{ $latestLocation->created_at }}. </h4>
                                                @endif
                                                {{-- <div id='temp_section'>
                                                    @if($locationLogs > 0)
                                                    <span class="panel-subtitle">
                                                        <a href="{{ url('/admin/dashboard/importLocation') }}" title="Clear Conflict">
                                                            <button class="btn btn-space btn-danger pull-right conflict-btn">Show Conflict of Location</button>
                                                        </a>
                                                    </span> @endif
                                                </div> --}}
                                            </div>
                                            {{-- <div id="category" class="tab-pane cont">
                                                    <h4> Category</h4>
                                                    <button type="button" class="btn btn-success" id="import_category"> Import </button>
                                                    @if(isset($latestCategory->created_at))
                                                    <h4>Last Import on {{ $latestCategory->created_at }}. </h4>
                                                    @endif
                                                    <div id='temp_section_category'></div>
                                                </div> --}}
                                            <div id="venue" class="tab-pane cont">
                                                <a href="{{ url('/admin/dashboard/importVenue') }}" title="Import Data">
                                                    <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                                                </a>
                                                {{-- <button type="button" class="btn btn-success" id="import_venue"> Import </button> --}}
                                                @if(isset($latestVenue->created_at))
                                                <h4 class='padding-top-10px'>Last Import on {{ $latestVenue->created_at }}. </h4>
                                                @endif
                                                {{-- <div id='temp_section_venue'>
                                                    @if($venueLogs > 0)
                                                    <span class="panel-subtitle">
                                                        <a href="{{ url('/admin/dashboard/importVenue') }}" title="Clear Conflict">
                                                            <button class="btn btn-space btn-danger pull-right conflict-btn">Show Conflict of Venue</button>
                                                        </a>
                                                    </span> @endif
                                                </div> --}}
                                            </div>
                                            <div id="show" class="tab-pane cont">
                                                <a href="{{ url('/admin/dashboard/importShow') }}" title="Import Data">
                                                    <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                                                </a>
                                                {{-- <button type="button" class="btn btn-success" id="import_show"> Import </button> --}}
                                                @if(isset($latestShow->created_at))
                                                <h4 class='padding-top-10px'>Last Import on {{ $latestShow->created_at }}. </h4>
                                                @endif
                                                {{-- <div id='temp_section_show'>
                                                    @if($showLogs > 0)
                                                    <span class="panel-subtitle">
                                                        <a href="{{ url('/admin/dashboard/importShow') }}" title="Clear Conflict">
                                                            <button class="btn btn-space btn-danger pull-right conflict-btn">Show Conflict of Show</button>
                                                        </a>
                                                    </span> @endif
                                                </div>--}}
                                            </div>
                                            {{--
                                            <div id="messages" class="tab-pane">
                                                <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia
                                                    soluta aperiam, recusandae.</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur
                                                    atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga
                                                    placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita
                                                    itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- Form actions layout section start -->
<!-- // Form actions layout section end -->
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@push('js')
<script>
    $(document).on('click', '#import_location', function (e) {
        $("#loading").show();
        var url ="{{ url('/admin/getLocation') }}";
        $.ajax({
            type: "get",
            url: url ,
            async: true,
            crossDomain: true,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                $("#loading").hide();

                if(data.code == 200)
                {
                    $("#import_location").hide();
                    $('#temp_section').html(data.html);
                    $('.success-text').html(data.message);
                    $('#success').niftyModal();
                }
                else{
                    $('.delete-confirm-text').html(data.message);
                    $('.delete-title').html("Action not proceed!");
                    $("#import_location").show();
                    $('#danger').niftyModal();
                }
                setTimeout(function () { $('#success').niftyModal('hide'); }, 3000);
            },
            error: function (xhr, status, error) {
                $('.delete-confirm-text').html("Something Wrong!");
                $('.delete-title').html("Action not proceed!");
                $("#import_location").show();
                $('#danger').niftyModal();
            }
        });

    });
    $(document).on('click', '#import_category', function (e) {
        $("#loading").show();
        var url ="{{ url('/admin/getCategory') }}";
        $.ajax({
            type: "get",
            url: url ,
            async: true,
            crossDomain: true,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                $("#loading").hide();

                if(data.code == 200)
                {
                    $("#import_category").hide();
                    $('#temp_section_category').html(data.html);
                    $('.success-text').html(data.message);
                    $('#success').niftyModal();
                }
                else{
                    $("#import_category").show();
                    $('.delete-confirm-text').html(data.message);
                    $('.delete-title').html("Action not proceed!");
                    $('#danger').niftyModal();
                }
                setTimeout(function () { $('#success').niftyModal('hide'); }, 3000);
            },
            error: function (xhr, status, error) {
                $("#import_category").show();
                $('.delete-confirm-text').html("");
                $('.delete-title').html("Action not proceed!");
                $('#danger').niftyModal();
            }
        });

    });
    $(document).on('click', '#import_venue', function (e) {
        $("#loading").show();
        var url ="{{ url('/admin/getVenue') }}";
        $.ajax({
            type: "get",
            url: url ,
            async: true,
            crossDomain: true,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                $("#loading").hide();

                if(data.code == 200)
                {
                    $("#import_venue").hide();
                    $('#temp_section_venue').html(data.html);
                    $('.success-text').html(data.message);
                    $('#success').niftyModal();
                }
                else{
                    $('.delete-confirm-text').html(data.message);
                    $('.delete-title').html("Action not proceed!");
                    $("#import_venue").show();
                    $('#danger').niftyModal();
                }
                setTimeout(function () { $('#success').niftyModal('hide'); }, 3000);
            },
            error: function (xhr, status, error) {
                $('.delete-confirm-text').html("Something Wrong!");
                $('.delete-title').html("Action not proceed!");
                $("#import_venue").show();
                $('#danger').niftyModal();
            }
        });

    });
    $(document).on('click', '#import_venue_details', function (e) {
        $("#loading").show();
        var url ="{{ url('/admin/getVenueDetails') }}";
        $.ajax({
            type: "get",
            url: url ,
            async: true,
            crossDomain: true,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                $("#loading").hide();

                if(data.code == 200)
                {
                    $("#import_venue_details").hide();
                    $('#temp_section_venue').html(data.html);
                    $('.success-text').html(data.message);
                    $('#success').niftyModal();
                }
                else{
                    $('.delete-confirm-text').html(data.message);
                    $('.delete-title').html("Action not proceed!");
                    $("#import_venue_details").show();
                    $('#danger').niftyModal();
                }
                setTimeout(function () { $('#success').niftyModal('hide'); }, 3000);
            },
            error: function (xhr, status, error) {
                $('.delete-confirm-text').html("Something Wrong!");
                $('.delete-title').html("Action not proceed!");
                $("#import_venue_details").show();
                $('#danger').niftyModal();
            }
        });

    });
    $(document).on('click', '#import_show', function (e) {
        $("#loading").show();
        var url ="{{ url('/admin/getShow') }}";
        $.ajax({
            type: "get",
            url: url ,
            async: true,
            crossDomain: true,
            headers: {
                "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
            },
            success: function (data) {
                $("#loading").hide();

                if(data.code == 200)
                {
                    $("#import_show").hide();
                    $('#temp_section_show').html(data.html);
                    $('.success-text').html(data.message);
                    $('#success').niftyModal();
                }
                else{
                    $('.delete-confirm-text').html(data.message);
                    $('.delete-title').html("Action not proceed!");
                    $("#import_show").show();
                    $('#danger').niftyModal();
                }
                setTimeout(function () { $('#success').niftyModal('hide'); }, 3000);
            },
            error: function (xhr, status, error) {
                $('.delete-confirm-text').html("Something Wrong!");
                $('.delete-title').html("Action not proceed!");
                $("#import_show").show();
                $('#danger').niftyModal();
            }
        });

    });
</Script>
@endpush