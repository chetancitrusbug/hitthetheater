
<html>
    <body>
    <table style="border: 1px #000000 solid">
        <thead>
            <tr>
                <th colspan="2"> <strong>Location Code</strong></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($locationCodes as $codekey => $code)
        @php $existClass = ($newArray[$code]['existrecord'] == 0 || $oldArray[$code]['existrecord'] == 0) ? 'color: red;' : ''; @endphp
        <tr style="{{$existClass}}">
            <td colspan="2"><strong> {{$code}}</strong></td>
        </tr>
        <tr class="location_{{$code}} location_dropdown_close">
            <td colspan="3">
                <table style="border: 1px #000000 solid" class="table innertable">
                    <thead>
                        <tr style="{{$existClass}}">
                            <th style="border: 1px #000000 solid;"> Field</th>
                            <th style="border: 1px #000000 solid;"> Today</th>
                            <th style="border: 1px #000000 solid;"> Yesterday</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php

                        $changeNewArrColor = ((isset($newArray[$code]) && isset($newArray[$code]['updatedArr'])) &&
                        $newArray[$code]['updatedArr'] == 1 ? 'color: red;border: 1px #000000 solid;' : 'border: 1px #000000 solid;' );
                        $changeOldArrColor = ((isset($oldArray[$code]) && isset($oldArray[$code]['updatedArr']))
                        && $oldArray[$code]['updatedArr'] == 1 ? 'color: red;border: 1px #000000 solid;' : 'border: 1px #000000 solid;' );

                        // dd($changeNewArrColor);
                        @endphp
                        <tr style="{{$existClass}}">
                            <td width="20%" style="border: 1px #000000 solid;"><strong>Title</strong></td>
                            <td width="40%" style="{{$changeNewArrColor}}">
                                @if(isset($newArray[$code]->locationTitle))
                                <label class="description" for="newdata[{{$code}}-locationTitle]">{{$newArray[$code]->locationTitle}}</label>
                                @endif
                            </td>
                            <td width="40%" style="{{$changeOldArrColor}}">
                                @if(isset($oldArray[$code]->locationTitle))
                                <label class="description" for="olddata[{{$code}}-locationTitle]">{{$oldArray[$code]->locationTitle}}</label>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </body>
</html>