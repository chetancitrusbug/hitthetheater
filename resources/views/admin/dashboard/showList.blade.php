<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <span class="panel-subtitle">
                    <a href="{{ url('/admin/dashboard/importShow') }}" title="Import Data">
                        <button class="btn btn-space btn-success pull-right btn-import">Import Data</button>
                    </a>
                </span>
            </div>
            <div class="panel-body">
                <table id="show-table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Show Title</th>
                            <th>Booking Starts</th>
                            <th>Booking Ends</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tempShow as $item)
                        <tr>
                            <td>{{$item->showTitle}}</td>
                            <td>{{$item->bookingStarts}}</td>
                            <td>{{$item->bookingEnds}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
