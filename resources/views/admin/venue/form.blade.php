<div class="row">
    <div class="col-md-12">
    <fieldset>
        <legend>Basic Details</legend>

        <div class="width-50-module  form-group{{ $errors->has('venueTitle') ? ' has-error' : ''}}">
            {!! Form::label('venueTitle', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('venueTitle', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('venueTitle', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="width-50-module  form-group{{ $errors->has('slug') ? ' has-error' : ''}}">
            {!! Form::label('slug', '* Slug: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('venueDescription') ? ' has-error' : ''}}">
            {!! Form::label('venueDescription', '*Description: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('venueDescription', isset($venue->venueDescription)? $venue->venueDescription : '' ,
                ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('venueDescription', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="width-50-module form-group{{ $errors->has('categoryId') ? ' has-error' : ''}}">
            {!! Form::label('categoryId', '* Category:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::select('categoryId[]', $category, isset($venueCategory) ? $venueCategory : [], ['id' => 'categoryId','class' =>
                'form-control ','multiple' => 'multiple']) !!} {!! $errors->first('categoryId', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="width-50-module form-group{{ $errors->has('facilities') ? ' has-error' : ''}}">
            {!! Form::label('facilities', '* Facilities:', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::select('facilities[]', $facilities, isset($venueFacilities) ? $venueFacilities : [], ['id' =>
                'facilities','class'
                => 'form-control ' ,'multiple' => 'multiple']) !!} {!! $errors->first('facilities', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="width-50-module form-group{{ $errors->has('poster') ? ' has-error' : ''}}">
            {!! Form::label('poster', 'Poster: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!!Form::file('poster',['class' => 'input-sm'] ) !!} {!! $errors->first('poster', '
                <p class="help-block">:message</p>') !!}
                @if(isset($venue->venue_image) && $venue->venue_image)
                <a href={{ $venue->venue_image }} target="_blank">
                    <img src={{ $venue->venue_image }} alt="image" width="100">
                </a>
                @endif
            </div>

        </div>

        <div class="width-50-module form-group{{ $errors->has('seat-plan') ? ' has-error' : ''}}">
            {!! Form::label('seat-plan', 'Seating Plan: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!!Form::file('seat-plan',['class' => 'input-sm'] ) !!} {!! $errors->first('seat-plan', '
                <p class="help-block">:message</p>') !!} @if(isset($seatplan_url) && $seatplan_url)
                <a href={{ $seatplan_url }} target="_blank">
                    <img src={{ $seatplan_url }} alt="image" width="100">
                </a> @endif
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Meta Details</legend>
        <div class="form-group{{ $errors->has('metaTitle') ? ' has-error' : ''}}">
            {!! Form::label('metaTitle', '* Meta Title: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('metaTitle', isset($metadata->meta_title) ? $metadata->meta_title : '', ['class' =>
                'form-control input-sm']) !!} {!! $errors->first('metaTitle', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('metaDescription') ? ' has-error' : ''}}">
            {!! Form::label('metaDescription', '* Meta Description: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('metaDescription',isset($metadata->meta_description) ?
                $metadata->meta_description : '', ['class' => 'summernote form-control input-sm',
                'id'=>'metaDescription']) !!} {!! $errors->first('metaDescription', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Address Details</legend>

            <div class="width-50-module form-group{{ $errors->has('addressLine1') ? ' has-error' : ''}}">
                {!! Form::label('addressLine1', '* Address Line 1: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('addressLine1', null, ['class' => 'form-control input-sm']) !!} {!!
                    $errors->first('addressLine1',
                    '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('addressLine2') ? ' has-error' : ''}}">
                {!! Form::label('addressLine2', 'Address Line 2: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('addressLine2', null, ['class' => 'form-control input-sm']) !!} {!!
                    $errors->first('addressLine2',
                    '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('latitude') ? ' has-error' : ''}}">
                {!! Form::label('latitude', '* Latitude: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('latitude', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('latitude', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('longitude') ? ' has-error' : ''}}">
                {!! Form::label('longitude', '* Longitude: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('longitude', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('longitude',
                    '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('locationId') ? ' has-error' : ''}}">
                {!! Form::label('locationId', '* Location:', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    @if(!isset($venue->locationCode)) {!! Form::select('locationId', $locationcodes, isset($venue->locationCode) ?
                    $venue->locationCode
                    : '', ['id' => 'locationId','class' => 'form-control ' ]) !!} @elseif(isset($venue->locationCode) &&
                    $venue->locationCode
                    == null ) {!! Form::select('locationId', $locationcodes, isset($venue->locationCode) ? $venue->locationCode
                    : '', ['id' => 'locationId','class' => 'form-control ' ]) !!} @else {!! Form::select('locationId',
                    $locationcodes,
                    isset($venue->locationCode) ? $venue->locationCode : '', ['id' => 'locationId','class' => 'form-control ',
                    'disabled' => 'disabled' ]) !!} @endif {!! $errors->first('locationId', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="width-50-module form-group{{ $errors->has('postcode') ? ' has-error' : ''}}">
                {!! Form::label('postcode', '* Postcode: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::text('postcode', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('postcode', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>



    </fieldset>
    </div>
</div>



@push('js')
    <script>
        $(document).ready(function() {
            $("#categoryId").select2({width: '100%'});
            // $('#categoryId').select2();
            $('#locationId').select2();
            $('#facilities').select2({width: '100%',tags: true});
            $('.summernote').summernote({ height: 250 });
        });
    </Script>
@endpush
