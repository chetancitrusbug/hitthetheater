<div class="modal fade" id="categoryaddmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                        <div class=" clearfix details-container details-port-container">
                            <form id="add_category_form" name="add_category_form">
                                @if(isset($venue))

                                <input name="venueCode" id="venueCode" type="hidden" value="{{$venue->venueCode}}"> {{csrf_field()}}
                                @endif

                                <span class="errmsgscategory"></span>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">* Title: </label>
                                                <div class="col-sm-12">
                                                    <input type="text" name="categoryTitle" id="add_title" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="categoryeditmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">Edit category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                        <div class=" clearfix details-container details-port-container">
                            <form id="edit_category_form" name="edit_category_form">
                                {{csrf_field()}} @if(isset($venue))

                                <input name="venueCode" id="venueCode" type="hidden" value="{{$venue->venueCode}}"> @endif

                                <span class="errmsgscategory"></span>

                                <div class="form-group prepend-top">
                                    <div class="row">

                                        <div class="form-group prepend-top">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="col-sm-12 control-label">* Title: </label>
                                                    <div class="col-sm-12">
                                                        <input type="hidden" name="category_id" id="edit_category_id">
                                                        <input type="text" name="categoryTitle" id="edit_title" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn addl btn-read btn-inverted" type="submit" name="submit" value="Submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="categoryminusmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">Delete category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                        <div class=" clearfix details-container details-port-container">
                            <form id="minus_category_form" name="minus_category_form">

                                <span class="errmsgscategory"></span>

                                <div class="form-group prepend-top">
                                    <div class="row category_div">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>

    $(document).ready(function (e) {
        listdrow();
    });
    $(document).on('click', '.pluscategory', function (e) {
        $("#add_category_form")[0].reset();
        $('.errmsgscategory').html('');
        $('#categoryaddmodel').modal('show');
    });

    $(document).on('click', '.minuscategory', function (e) {
        listdrow();
        $('#categoryminusmodel').modal('show');
        $("#minus_category_form")[0].reset();
        $('.errmsgscategory').html('');
    });
    $(document).on('click', '.editcategory', function (e) {
        listdrow();
        $('#categoryminusmodel').modal('show');
        $('.errmsgscategory').html('');
    });


    function listdrow() {
        $('.category_div').html('');
        $('.category_id_selectbox').html('');
        var venue_link = '{{$venue->venueCode}}';
        var url = '{{url("/admin/categorylist/")}}/'+venue_link;
        var method = "get";
        $.ajax({
            type: method,
            url: url,
            beforeSend: function () {
            },
            success: function (formdata)
            {
                if (formdata.categoryStr != '') {
                    $('.category_id_selectbox').html(formdata.categoryStr);
                    $('.minuscategory').show();
                    $('.editcategory').show();
                }
                else
                {
                    $('.minuscategory').hide();
                    $('.editcategory').hide();
                }

                if (formdata.categoryDeletedStr != '') {
                    $('.category_div').append(formdata.categoryDeletedStr);
                }
                else{
                    $('.minuscategory').hide();
                    $('.editcategory').hide();
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    $('#add_category_form').validate(
        {
            rules: {
                categoryTitle: { required: true },
            },
            messages: {
                categoryTitle: { required:  'Please enter title', }

            },
            errorPlacement: function(error, element)
            {
                error.insertAfter( element );
            },
            submitHandler: function (form) {
                var addformdata  =$('#add_category_form').serialize();
                var url = '{{url('/admin/categoryadd')}}';
                var method = "post";
                add_category_id =$('#add_category_id').val();
                title = $('#add_title').val();
                $.ajax({
                    type: method,
                    url: url,
                    data: $('#add_category_form').serialize(),
                    beforeSend: function () {
                    },
                    success: function (formdata)
                    {

                        if(formdata.code == '200')
                        {
                            listdrow();
                            $('#categoryaddmodel').modal('hide');
                            $('.minuscategory').show();
                            $('.editcategory').show();
                            swal("", formdata.message, "success");
                        }
                        else { swal("", formdata.message, "error"); $('.errmsgscategory').html(formdata.message); }
                    },
                    error: function (error) {
                        $('#categoryaddmodel').modal('hide');
                    // toastr.error(data.message);
                    }

                });
                return false;

            }
    });

    $('#edit_category_form').validate(
    {
            rules: {
                categoryTitle: { required: true }
            },
            messages: {
                categoryTitle: { required:  'Please enter title', }
            },
            errorPlacement: function(error, element)
            {
                error.insertAfter( element );
            },
            submitHandler: function (form) {
                var editformdata  =$('#edit_category_form').serialize();

                var url = '{{url('/admin/categoryupdate')}}';
                var method = "post";
                $.ajax({
                    type: method,
                    url: url,
                    data: $('#edit_category_form').serialize(),
                    beforeSend: function () {
                    },
                    success: function (formdata)
                    {
                        if(formdata.code == '200')
                        {
                            $('#categoryeditmodel').modal('hide');
                            $('.minuscategory').show();
                            $('.editcategory').show();
                            listdrow();
                            swal("", formdata.message, "success");
                        }
                        else
                        {
                            swal("", formdata.message, "error");
                            $('.errmsgscategory').html(formdata.message);
                        }
                    },
                    error: function (error) {
                        $('#categoryeditmodel').modal('hide');
                    // toastr.error(data.message);
                    }

                });
                return false;

            }
    });

    $(document).on('click', '.category_btn', function (e) {

        // var category_btn_div = $(".category_div").length;

        var venueCode = "{{$venue->venueCode}}";
        var category_id = $(this).data('id');
        var div = $(this).parent('div');
        var url = '{{url("/admin/categorydelete/")}}/'+category_id+'/'+venueCode;
        var method = "get";
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Record!",
            type: "warning",
            venueCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: method,
                    url: url,
                    beforeSend: function () {
                    },
                    success: function (formdata)
                    {

                        if(formdata.code == '200')
                        {
                            listdrow();
                            $('#categoryminusmodel').modal('hide');
                            swal({ title: 'Success!', type: 'success' });

                            //$(".category_id_selectbox").append('<option value="'+add_category_id+'">'+title+'</option>');
                        }
                        else
                            swal("Cancel", formdata.message, "error");
                    },
                    error: function (error) {
                        $('#categoryminusmodel').modal('hide');
                    }

                });
            } else {
                swal("Cancel", "", "error");
            }
        });

    });

    $(document).on('click', '.edit_category_btn', function (e) {

        // var category_btn_div = $(".category_div").length;

        var category_id = $(this).data('id');
        var div = $(this).parent('div');
        var url = '{{url("/admin/categorydetail/")}}/'+category_id;
        var method = "get";

        $.ajax({
            type: method,
            url: url,
            beforeSend: function () {
            },
            success: function (formdata)
            {
                if(formdata.code == '200')
                {
                    $('#edit_category_id').val(formdata.data.id);
                    $('#edit_title').val(formdata.data.categoryTitle);
                    $('#categoryminusmodel').modal('hide');
                    $('#categoryeditmodel').modal('show');

                }
                else
                    swal("Cancel", formdata.message, "error");
            },
            error: function (error) {
                $('#categoryminusmodel').modal('hide');
            }

        });

    });

</script>


@endpush