<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
            {!! Form::label('title', '* Title: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('slug') ? ' has-error' : ''}}">
            {!! Form::label('slug', '* Slug: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('slug', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('isFuture') ? ' has-error' : ''}}">
            {!! Form::label('isFuture', 'Is Featured: ', ['class' => 'col-sm-3 control-label']) !!}

            <div class="col-sm-12">
                {!! Form::checkbox('isFuture',1 ,isset($news->isFuture)?$news->isFuture:0, ['class'=>['isFuture']]) !!}
                {!! $errors->first('isFuture', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('featuredImage') ? ' has-error' : ''}}">

            @if(isset($news) && $news->featuredImage)
            {!! Form::label('featuredImage', 'Featured Image: ', ['class' => 'col-sm-3
            control-label']) !!}
            <div class="col-sm-12">
                {!! Form::file('featuredImage', ['class' => '']) !!} {!! $errors->first('featuredImage','
                <p class="help-block">:message</p>') !!}
            </div>
            @else {!! Form::label('featuredImage', '* Featured Image: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::file('featuredImage', ['class' => '', 'required' => 'required']) !!} {!! $errors->first('featuredImage','
                <p class="help-block">:message</p>') !!}
            </div>
            @endif

        </div>
        @if(isset($news) && $news->image)
        <a href={{$news->image }} target="_blank">
            <img src={{ $news->image }} alt="featuredImage" width="100">
        </a>
        @endif
    </div>

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('publishDate') ? ' has-error' : ''}}">
            {!! Form::label('publishDate', '* Publish Date: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                <input type="date" id="publishDate" class="form-control" name="publishDate" data-toggle="tooltip"
                    data-trigger="hover" data-placement="top" data-title="Booking Starts"
                    value="{{isset($news->publishDate) ? $news->publishDate : ''}}">
                {!! $errors->first('publishDate', '<p class="help-block">:message</p>') !!}
                {{-- {!! Form::text('publishDate', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('publishDate', '<p class="help-block">:message</p>') !!} --}}
            </div>
        </div>
    </div>


</div>
<div class="row">

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
            {!! Form::label('description', '* Description: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('description', isset($news->description) ? $news->description : '', ['class' => 'form-control
                input-sm' , 'id' => 'description']) !!}
                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
            {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::checkbox('status',1 ,isset($news->status)?$news->status:1, ['class'=>['status']]) !!}
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#description').summernote({
            height: 250
        });
</Script>
@endpush