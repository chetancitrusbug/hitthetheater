@extends('layouts.admin')
@section('title',"Create Banner")
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/banner') }}">banner</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="col-md-6">
                            <div class="form-group col-sm-6">
                                <a href="{{ url('/admin/banner') }}" title="Back">
                                    <button class="btn btn-space btn-warning">Back</button>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['url' => '/admin/banner', 'class' => 'form-horizontal
                            group-border-dashed','id' => 'banner_create','autocomplete'=>'off','files'=>true]) !!}
                            @include ('admin.banner.form')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group col-sm-4">
                                        {!! Form::submit('Create', ['class' => 'btn btn-space btn-primary pull-right'])
                                        !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@push('js')
<script type="text/javascript">
    $(document).ready(function(){
              // Initialize form validation on the registration form.
              // It has the name attribute "registration"
                $("#banner_create").validate({
                    // Specify validation rules
                    rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    title: {
                        required:true,
                        maxlength: 40,
                    },
                    image: {
                        required:true,
                        accept: "image/*"
                    }
                    },
                    // Specify validation error messages
                    messages: {
                    title: {
                        required:"Please enter your Banner Title",
                    },
                    image: {
                        required: "Please Select a Banner Image",

                    },
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                    form.submit();
                    }
                });
            });
</script>
@endpush
@endsection