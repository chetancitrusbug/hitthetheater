<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
            {!! Form::label('title', '* Title: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">

            @if(isset($banner) && $banner->image) {!! Form::label('image', 'Image: ', ['class' => 'col-sm-3
            control-label']) !!}
            <div class="col-sm-12">
                {!! Form::file('image', ['class' => '']) !!} {!! $errors->first('image','
                <p class="help-block">:message</p>') !!}
            </div>
            @else {!! Form::label('image', '* Image: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::file('image', ['class' => '', 'required' => 'required']) !!} {!! $errors->first('image','
                <p class="help-block">:message</p>') !!}
            </div>
            @endif

        </div>
        @if(isset($banner) && $banner->banner)
        <a href={{$banner->banner }} target="_blank">
            <img src={{ $banner->banner }} alt="image" width="100">
        </a>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('bannerLink') ? ' has-error' : ''}}">
            {!! Form::label('bannerLink', 'Banner Link: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('bannerLink', null, ['class' => 'form-control input-sm']) !!}
                {!! $errors->first('bannerLink', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('content') ? ' has-error' : ''}}">
            {!! Form::label('content', 'Content: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('content', isset($banner->content) ? $banner->content : '', ['class' => 'form-control
                input-sm' , 'id' => 'content']) !!}
                {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
            {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::checkbox('status',1 ,isset($banner->status)?$banner->status:1, ['class'=>['status']]) !!}
                {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
    $('#content').summernote({
            height: 250
        });
</Script>
@endpush