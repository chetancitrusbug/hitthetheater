<div class="row">

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
            {!! Form::label('email', 'Email: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('email',isset($cms_setting->contact_us_email)?is_null($cms_setting->contact_us_email)?null:$cms_setting->contact_us_email:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('email', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('contact_us_phone') ? ' has-error' : ''}}">
            {!! Form::label('contact_us_phone', 'Contact Us Phone: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::text('contact_us_phone',isset($cms_setting->contact_us_phone)?is_null($cms_setting->contact_us_phone)?null:$cms_setting->contact_us_phone:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('contact_us_phone', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('facebook_link') ? ' has-error' : ''}}">
            {!! Form::label('facebook_link', 'Facebook link: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('facebook_link_display', null ,(isset($cms_setting->facebook_link_display) && $cms_setting->facebook_link_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::text('facebook_link',isset($cms_setting->facebook_link)?is_null($cms_setting->facebook_link)?null:$cms_setting->facebook_link:null,['class' => 'form-control input-sm']) !!} {!! $errors->first('facebook_link', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('twitter_link') ? ' has-error' : ''}}">
            {!! Form::label('twitter_link', 'Twitter link: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('twitter_link_display', null ,(isset($cms_setting->twitter_link_display) && $cms_setting->twitter_link_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::text('twitter_link',isset($cms_setting->twitter_link)?is_null($cms_setting->twitter_link)?null:$cms_setting->twitter_link:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('twitter_link', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('youtube_link') ? ' has-error' : ''}}">
            {!! Form::label('youtube_link', 'Youtube link: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('youtube_link_display', null ,(isset($cms_setting->youtube_link_display) && $cms_setting->youtube_link_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::text('youtube_link',isset($cms_setting->youtube_link)?is_null($cms_setting->youtube_link)?null:$cms_setting->youtube_link:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('youtube_link', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('instagram_link') ? ' has-error' : ''}}">
            {!! Form::label('instagram_link', 'Instagram link: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('instagram_link_display', null ,(isset($cms_setting->instagram_link_display) && $cms_setting->instagram_link_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::text('instagram_link',isset($cms_setting->instagram_link)?is_null($cms_setting->instagram_link)?null:$cms_setting->instagram_link:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('instagram_link', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('copy_right_content') ? ' has-error' : ''}}">
            {!! Form::label('copy_right_content', 'Copy right content: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('copy_right_content_display', null ,(isset($cms_setting->copy_right_content_display) && $cms_setting->copy_right_content_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::text('copy_right_content',isset($cms_setting->copy_right_content)?is_null($cms_setting->copy_right_content)?null:$cms_setting->copy_right_content:null,
                ['class' => 'form-control input-sm']) !!} {!! $errors->first('copy_right_content', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('logo') ? ' has-error' : ''}}">
            {!! Form::label('logo', '* Site Logo: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">

                {!!Form::file('logo',['class' => 'input-sm'] ) !!} {!! $errors->first('logo', '
                <p class="help-block">:message</p>') !!}
                @if(isset($cms_setting->logo) && $cms_setting->logo)
                    @php
                        $path=public_path().'/storage/cms'.$cms_setting->logo;
                        if(file_exists($path))
                        {
                            $path=url('').'/storage/cms'.$cms_setting->logo;
                            echo '<a href="'.$path.'" target="_blank" ><img src='.$path.' alt="image" width="100"></a>';
                        }
                    @endphp
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('category_for_header') ? ' has-error' : ''}}">
            {!! Form::label('category_for_header', 'Category for header: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-12">
                {!! Form::select('category_for_header[]', $category_for_header, isset($cms_setting->category_for_header) ? $cms_setting->category_for_header : [], ['class' => 'form-control tags' ,'multiple' => 'multiple']) !!}
                {!! $errors->first('category_for_header', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('about_us') ? ' has-error' : ''}}">
            {!! Form::label('about_us', 'About US: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('about_us_display', null ,(isset($cms_setting->about_us_display) && $cms_setting->about_us_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('about_us', isset($cms_setting->about_us)? $cms_setting->about_us : '' , ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('about_us', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('contact_us_address') ? ' has-error' : ''}}">
            {!! Form::label('contact_us_address', 'Contact US Address: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('contact_us_address_display', null ,(isset($cms_setting->contact_us_address_display) && $cms_setting->contact_us_address_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('contact_us_address', isset($cms_setting->contact_us_address)? $cms_setting->contact_us_address : '' , ['class' => 'form-control input-sm
                summernote']) !!} {!! $errors->first('contact_us_address', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('contact_us_email_tamplate') ? ' has-error' : ''}}">
            {!! Form::label('contact_us_email_tamplate', 'Contact US Email Template: ', ['class' => 'col-sm-3 control-label']) !!}

            <div class="col-sm-12">
                {!! Form::textarea('contact_us_email_tamplate', isset($cms_setting->contact_us_email_tamplate)? $cms_setting->contact_us_email_tamplate : '' ,
                ['class' => 'form-control input-sm summernote']) !!} {!! $errors->first('contact_us_email_tamplate', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('contact_us_details') ? ' has-error' : ''}}">
            {!! Form::label('contact_us_details', 'Contact US details: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('contact_us_display', null ,(isset($cms_setting->contact_us_display) && $cms_setting->contact_us_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('contact_us_details', isset($cms_setting->contact_us)? $cms_setting->contact_us
                : '' , ['class' => 'form-control input-sm summernote']) !!} {!! $errors->first('contact_us_details', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('faq') ? ' has-error' : ''}}">
            {!! Form::label('faq', 'Faq: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('faq_display', null ,(isset($cms_setting->faq_display) && $cms_setting->faq_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('faq', isset($cms_setting->faq)? $cms_setting->faq : '' , ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('faq', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('termandcondition') ? ' has-error' : ''}}">
            {!! Form::label('termandcondition', 'Terms and Condition: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('termandcondition_display', null ,(isset($cms_setting->termandcondition_display) && $cms_setting->termandcondition_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('termandcondition', isset($cms_setting->termandcondition)? $cms_setting->termandcondition : '' , ['class' => 'form-control input-sm summernote'])
                !!} {!! $errors->first('termandcondition', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('privacypolicy') ? ' has-error' : ''}}">
            {!! Form::label('privacypolicy', 'Privacy Policy: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('privacypolicy_display', null ,(isset($cms_setting->privacypolicy_display) && $cms_setting->privacypolicy_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('privacypolicy', isset($cms_setting->privacypolicy)? $cms_setting->privacypolicy : '' , ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('privacypolicy', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group{{ $errors->has('cookiepolicy') ? ' has-error' : ''}}">
            {!! Form::label('cookiepolicy', 'Cookie Policy: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('cookiepolicy_display', null ,(isset($cms_setting->cookiepolicy_display) && $cms_setting->cookiepolicy_display == 1 ) ? true : false ,
                ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('cookiepolicy', isset($cms_setting->cookiepolicy)? $cms_setting->cookiepolicy : '' , ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('cookiepolicy', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group{{ $errors->has('trust_factor') ? ' has-error' : ''}}">
            {!! Form::label('trust_factor', 'Trust Factor: ', ['class' => 'col-sm-3 control-label']) !!}
            {!! Form::checkbox('trust_factor_display', null ,(isset($cms_setting->trust_factor_display) && $cms_setting->trust_factor_display == 1 ) ?
            true : false ,
            ['class' => 'control-label cms_setting_checkbox']) !!}
            <div class="col-sm-12">
                {!! Form::textarea('trust_factor', isset($cms_setting->trust_factor)? $cms_setting->trust_factor : '' , ['class' => 'form-control
                input-sm summernote']) !!} {!! $errors->first('trust_factor', '
                <p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group col-sm-12">
            {!! Form::submit('Save', ['class' => 'btn btn-space btn-primary ']) !!}
        </div>
    </div>
</div>

@push('js')
<script>
    $(".tags").select2({maximumSelectionLength: 5,tags: true, width: '100%'});
</script>
@endpush