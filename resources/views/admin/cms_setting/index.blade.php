  @extends('layouts.admin')
@section('title',"Create Setting")
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/cms_setting') }}">cms setting</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="card-body">
                        {!! Form::open(['url' => '/admin/post_cms_setting', 'class' => 'form-horizontal','id' => 'cms_setting','autocomplete'=>'off','files'=>true]) !!}

                            @include ('admin.cms_setting.form')
                        {!! Form::close() !!}
                 </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="{{asset('backend/lib/bootstrap-slider/js/bootstrap-slider.js')}}" type="text/javascript"></script>
<script>

        $('.summernote').summernote({
            height: 200
        });
         $(document).ready(function(){
               $.validator.addMethod("phoneno", function(phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
                return phone_number.length > 9 ;
                }, "Please specify a valid phone number");
              // Initialize form validation on the registration form.
              // It has the name attribute "registration"
              $("#cms_setting").validate({
                // Specify validation rules
                rules: {
                  // The key name on the left side is the name attribute
                  // of an input field. Validation rules are defined
                  // on the right side
                  email: {
                     required:true,
                     email: true
                  },
                  contact_us_phone: {
                     required:true,
                     phoneno:true
                  },
                  about_us:{required:true},
                  contact_us_address:{required:true},
                  contact_us_email_template:{required:true},
                  faq:{required:true},
                  termandcondition:{required:true},
                  privacypolicy:{required:true},
                  cookiepolicy:{required:true}
                },
                // Specify validation error messages
                messages: {
                  email: {
                    required:"Please enter a CMS setting Email",
                  },
                   contact_us_phone: {
                    required:"Please enter a Contact Us contact_us_phone",

                    }
                },
                // Make sure the form is submitted to the destination defined
                // in the "action" attribute of the form when valid
                submitHandler: function(form) {
                  form.submit();
                }
              });
           });
</Script>

@endpush