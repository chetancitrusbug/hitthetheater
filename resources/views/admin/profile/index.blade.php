@extends('layouts.admin')


@section('title','My Profile')

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/profile') }}">Profile</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ url('admin/profile/'.$user->id.'/edit') }}" class="btn btn-success btn-sm" title="Edit Profile">
                                                        <i class="fa fa-edit" aria-hidden="true"></i> Edit Profile
                                                    </a>
                                <a href="{{ url('admin/profile/change-password') }}" class="btn btn-success btn-sm" title="Change Password">
                                                        <i class="fa fa-lock" aria-hidden="true"></i> Change Password
                                                    </a>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>

                    <hr>

                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tr>
                                <td class="">Name</td>
                                <td>{{$user->name}}</td>
                            </tr>

                            <tr>
                                <td>Email</td>
                                <td>{{$user->email}}</td>
                            </tr>

                        </table>
                    </div>


                    @if($user->people)
                        <h2>@lang('people.label.person_details')</h2>
                        <div class="table-responsive">
                            <table class="table table-borderless">

                                <tr>
                                    <td class="">@lang('people.label.first_name')</td>
                                    <td>{{$user->people->first_name}}</td>
                                </tr>
                                <tr>
                                    <td class="">@lang('people.label.last_name')</td>
                                    <td>{{$user->people->last_name}}</td>
                                </tr>

                                <tr>
                                    <td>@lang('people.label.phone_number_1')</td>
                                    <td>{{$user->people->phone_number_1}}</td>
                                </tr>

                                <tr>
                                    <td>@lang('people.label.phone_type_1')</td>
                                    <td>{{$user->people->phone_type_1}}</td>
                                </tr>


                                <tr>
                                    <td>@lang('people.label.phone_number_2')</td>
                                    <td>{{$user->people->phone_number_2 or null}}</td>
                                </tr>

                                <tr>
                                    <td>@lang('people.label.phone_type_2')</td>
                                    <td>{{$user->people->phone_type_2 or null}}</td>
                                </tr>


                                <tr>
                                    <td>@lang('people.label.photo')</td>
                                    <td>
                                        @if($user->photo)
                                            <img src="{!! asset('uploads/'.$user->photo) !!}" alt="" width="150">
                                        @endif
                                    </td>
                                </tr>


                            </table>
                        </div>

                    @endif

                    {{--<pre>--}}
                    {{--{!! json_encode(Auth::user(),JSON_PRETTY_PRINT) !!}--}}
                    {{--</pre>--}}

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
@endsection