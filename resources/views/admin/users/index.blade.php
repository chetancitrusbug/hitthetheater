@extends('layouts.admin')
@section('title',"Users")
@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/users') }}">Users</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <a href="{{ url('/admin/users/create') }}" title="Create">
                                <button class="btn btn-space btn-success">Create</button>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table id="users-table" class="table table-striped table-bordered default-ordering">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/users-data') }}";
        var edit_url = "{{ url('/admin/users') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#users-table').DataTable({
            dom:
            "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            stateSave: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": true},
                { data: 'email',name : 'email',"searchable": true, "orderable": true},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": true,
                    "render": function (o) {
                        if(o.id != 1){
                            if(o.status == 1){
                                return "<input type='checkbox' class='status status-change switch' checked data-table='users' data-status="+o.status+" onchange=statusChange() data-url={{url('admin/change-status')}} value="+o.id+" data-id="+o.id+">&nbsp;";
                            }
                            return "<input type='checkbox' class='status status-change switch' data-url={{url('admin/change-status')}} data-table='users' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        else
                        {
                            return '';
                        }
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var e="";  var d= "";

                        if(o.id != 1){
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info' title='Edit' ><i class='ft-edit' ></i></button></a>&nbsp;";

                            d = "<a href='javascript:void(0);' class='btn btn-danger del-item' data-id="+o.id+" data-url={{url('admin/users')}} ><i class='ft-delete' aria-hidden='true'></i></a>&nbsp;";
                        }
                        return e+d;
                    }

                }
            ]
        });

        $('#areas').change(function() {
            datatable.draw();
        });



</Script>


@endpush