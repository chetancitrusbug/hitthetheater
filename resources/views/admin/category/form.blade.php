<div class="row">
<div class="col-md-6">
    <div class="form-group{{ $errors->has('categoryTitle') ? ' has-error' : ''}}">
        {!! Form::label('categoryTitle', '* Name: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-12">
            {!! Form::text('categoryTitle', null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
            {!! $errors->first('categoryTitle', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
        {!! Form::label('status', 'Status: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-12">
            {!! Form::checkbox('status',1, isset($category->status)?$category->status:1, ['class'=>['status']]) !!}
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group{{ $errors->has('slug') ? ' has-error' : ''}}">
        {!! Form::label('slug', '* Slug: ', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-12">
            {!! Form::text('slug', null, ['class' => 'form-control input-sm']) !!} {!! $errors->first('slug', '
            <p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
</div>