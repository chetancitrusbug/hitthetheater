@extends('layouts.admin')
@section('title',"Edit Category")

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <h3 class="content-header-title mb-0"></h3>
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a> </li>
                    <li class="breadcrumb-item"><a href="{{ url('admin/category') }}">Category</a> </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <div id="configuration">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $moduleName }}</h4>
                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-content collapse show">
                        <div class="col-md-6">
                            <div class="form-group col-sm-6">
                                <a href="{{ url('/admin/category') }}" title="Back">
                                    <button class="btn btn-space btn-warning">Back</button>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                    {!! Form::model($category, ['method' => 'PATCH','url' => ['/admin/category', $category->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}
                            @include ('admin.category.form')
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group col-sm-4">
                                    {!! Form::submit('Update', ['class' => 'btn btn-space btn-primary ']) !!}
                                </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection