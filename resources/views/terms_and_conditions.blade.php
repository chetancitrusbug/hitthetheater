<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" media="all" />

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="minimal-ui, width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="images/favicon.ico">
    <title>3Y - Make Life Easy - Terms and conditions</title>


    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <style>
        html,
        body {
            margin: 0 auto;
            padding: 0;
            height: 100%;
        }

        /**/

        html {
            text-rendering: optimizeLegibility;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            text-size-adjust: 100%;
        }

        * {
            -webkit-overflow-scrolling: touch;
        }

        body {
            /*	background:#fff;*/
            font-size: 12px;
            color: #222;
            -webkit-font-smoothing: antialiased;
            /* Fix for webkit rendering */
            -webkit-text-size-adjust: 100%;
            /*-webkit-overflow-scrolling: touch;*/
            font-family: 'Raleway', sans-serif;
        }

        h2 {
            margin: 0 0 15px 0;
            padding: 0;
            font-size: 20px;
            font-weight: bold;
            color: #222;
            line-height: 18px;
        }

        p {
            margin: 0 0 15px 0;
            padding: 0;
            font-size: 14px;
            color: #222;
            line-height: 18px;
            position: relative;
        }

        ol {
            margin: 0 0 15px 15px;
            padding: 0;
            font-size: 14px;
            color: #222;
            line-height: 18px;
            position: relative;
        }

        ol li {
            margin: 0 0 15px 0;
            padding: 0 0 0 15px;
            font-size: 14px;
            color: #222;
            line-height: 18px;
            position: relative;
            list-style: decimal;
        }

        ol li ol {
            margin: 0 0 15px 20px;
            padding: 0;
            font-size: 14px;
            color: #222;
            line-height: 18px;
            position: relative;
        }

        .p-heading {
            font-weight: 400!important;
            padding: 0px 0;
            line-height: 1.2;
        }

        .p-heading:first-child {
            padding: 15px 0 0 0;
        }
    </style>

</head>

<body>

    <div style=" margin:0; padding:40px 20px; position:relative;">

        <h2>Licence Agreement</h2>
        <p>These are the terms and conditions subject to which we license our product to you. By buying a licence and using
            the 3Y App, you agree to be bound by them. You may not use our “App” without the consent of a parent or guardian
            if you are under the age of 18 years.</p>
        <p>We are Tria Limited, a company registered in England and Wales number 11206576.</p>
        <p>Our address is: 10d Wincombe Business Park, Shaftesbury, SP7 9QJ, United Kingdom.</p>
        <p>You are: Anyone who buys a licence from us or otherwise use3Y App as set out in these terms and conditions.</p>

        <p><strong>It is now agreed as follows:</strong></p>

        <ol>
            <li style="font-weight:bold;">Definitions</li>
            <div style="list-style:none;padding-left:15px;">
                <div style="font-weight:normal; margin-top:20px;">
                    In this agreement, the following words shall have the following meanings, unless the context requires otherwise:
                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"3Y App"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means the software application and related services for use on any Device or thing offered for licence by Tria Limited on the 3Y App and Our Website, and whether or not bought by you.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Content"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means the textual, visual or aural content that is encountered as part of your experience on 3Y App or Our Website. It may include, among other things: text, images, sounds, videos and animations. It includes all data and content of any sort Posted by you.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Copy or Publish"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">with reference to an App, means reproducing or publishing in whole or in part, using any means, in any medium. It includes breaking up, changing, cropping or any other change or use as part of some other created work.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Device"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">includes any mobile, device, work station, electronic application or electronic receiving device.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Guest User"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means any person or entity, other than You, who is a client of yours and who is invited by you to use Limited Services of 3Y App for the purposes of facilitating the use of 3Y App and our Services in the fulfilment of any legitimate commercial contract between you and your client.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Intellectual Propert"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means intellectual property owned by us, of every sort, whether or not registered or registrable in any country, including all Apps, intellectual property of all kinds coming into existence after today; and including, among others, patents, trademarks, unregistered marks, designs, copyrights, domain names, discoveries, creations and inventions, together with all rights which are derived from those rights.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Licence"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means a licence granted by us to you in the terms of this agreement for use of 3Y App.</span>
                    </p>

                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Limited Use"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means the Services that are provided under limited use and it can be changed by us at any time at our sole discretion. Currently it includes scheduling services provided and the receipt and sending of documents and emails.</span>
                    </p>
                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Our Website"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means any website or service designed for electronic access by mobile or fixed devices which is owned or operated by us. It includes all web pages controlled by us.</span>
                    </p>
                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Post"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means placed on or into 3Y AppContent or material of any sort by any means.</span>
                    </p>


                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Restrictions on Use"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means first, the restrictions set out in this agreement and second, all restrictions or limitations arising from choices you made at the time of purchase and/or registration. These may relate to limitations on use, territory, duration, or any other choice which defines Hit The Theatre. Third, restrictions include those made by you, if any, in private correspondence between us before your purchase.</span>
                    </p>
                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Services"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means the services available from 3Y App, whether free or charged.</span>
                    </p>
                    <p style="margin-top:20px">
                        <span style="position:absolute; display:inline-block; left:0;">"Software"</span>
                        <span style="position:relative; display:inline-block; margin:0 0 0 180px;">means the software which constitutes 3Y App or which provides any electronic function which supports the use of 3Y App.</span>
                    </p>
                </div>
            </div>

            <li style="font-weight:bold;">Interpretation</li>
            <div style="list-style:none;padding-left:15px;">
                <p>In this agreement unless the context otherwise requires:</p>


                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">a reference to a person includes a human individual, a corporate entity and any organisation which is managed or controlled as a unit.</span>
                </p>

                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">a reference to a person includes reference to that person’s successors, legal representatives, permitted assignsand any person, to whom rights and obligations are transferred or passed as a result of a merger, division, reconstruction or other re-organisation involving that person.</span>
                </p>


                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">in the context of permission, “may not” in connection with an action of yours, means “must not”.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">the headings to the paragraphs and schedules (if any) to this agreement are inserted for convenience only and do not affect the interpretation.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">any agreement by either party not to do or omit to do something includes an obligation not to allow some other person to do or omit to do that same thing;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">a reference to an act or regulation includes new law of substantially the same intent as the act or regulation referred to.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">a reference to one gender shall include any or all genders and a reference to the singular may be interpreted where appropriate as a reference to the plural and vice versa.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">2.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">this agreement is made only in the English language. If there is any conflict in meaning between the English language version of this agreement and any version or translation of this agreement in any other language, the English language version shall prevail.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Warranty for your authority</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">3.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You confirm that you have authority to enter into this agreement and have obtained all necessary approvals to do so.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">3.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">In entering into this contract, you have not relied on any representation or information from any source except that on 3Y App or on Our Website.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">3.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You accept responsibility for compliance with the laws and importation procedures of your jurisdiction which might affect your right to import, export or use the Software, and you represent that you have or will comply with all such laws and procedures.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Basis of contract</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">In entering into this agreement, you have not relied on any representation or information from any source except the definition and explanation of the Services given on 3Y App and on Our Website.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Subject to these terms and conditions, we agree to provide to you some or all of the Services described on 3Y App and on Our Website at the prices we charge from time to time.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Some of our Services are now or may in future, be available to you only subject to additional terms. Those terms will be set out on 3Y App.You now agree that if you choose to use any such service, the relevant terms will become part of this agreement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You acknowledge that you understand exactly what is included in the Services and you are satisfied that the Services are suitable and satisfactory for your requirements;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">So far as we allow use of our Intellectual Property, we grant a Licence to you, limited to the terms set out in this agreement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Our contract with you and Licence to you last for period for which you subscribe for. Any continuation by us or by you after the expiry of this subscription period is a new contract under the terms then Posted on 3Y App. Your continued use of our Services after that shall be deemed acceptance by you of the changed Service, system and/or terms.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The contract between us comes into existence when we receive payment from you for a Service.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If we give you free access to a Service or feature on 3Y App which is normally a charged feature, and that Service or feature is usually subject to additional contractual terms, you now agree that you will abide by those terms.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.9</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We may change this agreement and/or the Services, and the way we provide them, at any time. If we do:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">4.9.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The change will take effect when we Post it on 3Y App.</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">4.9.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree to be bound by any changes. If you do not agree to be bound by them, you should not use 3Y App or the Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">4.10</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you make any payment for Services in the future, you will do so under the terms Posted on 3Y App at that time.</span>
                </p>

            </div>

            <li style="font-weight:bold;">Grant of Licence</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">5.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Subject to payment of the licence fee and to the other terms of this agreement, we grant to you a Licence to use 3Y App for the period for which you subscribe for throughout the World.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">5.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We do not offer the Licence in all countries. We may refuse or revoke a Licence and return your payment if you live in a country we do not serve.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">5.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The Licence is non-exclusive, non-assignable, non-transferable and otherwise as limited by the terms of this agreement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">5.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">No express or implied licence of3Y App or any other material is granted to you other than the express Licence granted in this agreement.</span>
                </p>

            </div>

            <li style="font-weight:bold;">Further requirements of the Licence</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">6.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You must not allow any other person to use 3Y App except in the situation or context for which you have bought it.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">6.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Every publication or appearance of 3Y App on a Device must be protected as far as the law allows by separate, specific or general provisions against copying or publishing. We allow you to use the definition of "Copy or Publish" used in this agreement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">6.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You must not use 3Y App:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">6.3.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">except for the use specified at the time of purchase;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">6.3.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">in part or as a whole, to incorporate it in any intellectual property of yours except as intended by the Services provided on 3Y App;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">6.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If any information you give us is inaccurate, we may terminate your Licence and no refund of money will be due to you.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Guest Users</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We grant you the right to invite Guest Users to use limited Services of 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You are responsible for all Guest Users you invite to register for 3Y App and its Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Guest Users will only have Limited Use of 3Y App and its Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Guest User will be required to go through the same registration process asa subscriber in order to useHit The Theatre and its Services</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">A Guest User will not be required to pay for Limited Use of Hit The Theatre and its Services.He can apply to register as full fee-paying subscribers at any time if he wishes to have full use of3Y App and its Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We can refuse to accept any person as a Guest User and terminate his access to 3Y App, use of its Services at any time at our sole discretion.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">7.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You may not terminate Guest User access to 3Y App however you may terminate any link between you and the Guest User you have invited at your own discretion.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Your account and personal information</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">8.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">When you use3Y App, you accept responsibility for any action done by any person using your name, account or password. You should take all necessary steps to ensure that the password is kept confidential and secure and should inform us immediately if you have any reason to believe that your password has become known to anyone else, or if the password is being, or is likely to be, used in an unauthorised manner.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">8.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree that you have provided accurate, up to date, and complete information about yourself. We are not responsible for any error made as a result of such information being inaccurate.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">8.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree to notify us of any changes in your information immediately it occurs. If you do not do so, we may terminate your account.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Subscription terms</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Details of the costs of 3Y App subscription and payment details are as set out in 3Y App and in Our Website and accordingly updated from time to time.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You can make payment by either credit card or debit card.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Payment for 3Y App subscription is for a fixed period of time.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Depending on the jurisdiction, regulations may exist which give subscribers a “cooling off period” during which they may cancel their subscription and ask for a full refund of their money.However, our subscribers want to use our Services immediately. For this reason, our Service is designed so that you can start immediately - or as soon as we are reasonably able to deliver it.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">So, if you wish to subscribe, you must first instruct us to allow you to use our Service immediately, knowing that you will lose your right to any “cooling off” period.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You do not have to take any action for this to apply. By accepting these terms and conditions, you do instruct us to provide Hit The Theatre with Services. You know that by doing so, you may not be entitled to a refund.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">9.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We reserve the right to modify 3Y App subscription rules or system and to change the terms and conditions of this agreement at any time, without notice. Your continued use of 3Y App after such modifications shall be deemed an acceptance by you to be bound by the terms of the modified agreement. The terms that apply to you are those Posted here on 3Y App on the day you subscribe to 3Y App.</span>
                </p>
            </div>

            <li style="font-weight:bold;">The price</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The price payable for 3Y App is clearly set out on 3Y App and in Our Website.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The price charged for 3Y App may differ from one country to another. You may not be entitled to the lowest price unless you reside in the qualifying country.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Prices are inclusive of any applicable value added tax (VAT) or other sales tax.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">When you subscribe for a subscription Service, that payment may not cover other Services which may be offered, for which we will ask you to pay either by addition to your subscription or by a single payment.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If, by mistake, we have under-priceda Service, we will not be liable to supply that Service to you at the stated price, provided that we notify you before we send it to you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Prices are exclusive of import duty or any other tax charged or imposed by the country of importation, all of which are payable by you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You will pay all sums due to us under this agreement by the means specified without any set-off, deduction or counterclaim.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">10.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">All monies paid by you to us are non-refundable and cancellation and/or termination of this agreement by you or us at any time for any reason will not entitle you to a refund of monies paid.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Free Trials and Beta Tests</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">11.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">For the purpose of this paragraph the term: “Beta Test” means is the second phase of any software testing of 3Y App and/or any of its Services which we may ask you to try out. “Free Trial” means an invitation by us to you to use 3Y App and its Services for free for one month - so that you can try it out.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">11.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">No charge for use of 3Y App and its Services will be made during any Free Trial period or any Beta Test period. You are not however entitled to benefit from more than one Free Trial. If you request more than one Free Trial you will become liable for payment of the subscription fee for all use of 3Y App and its Services made after the first Free Trial period.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Renewal payments</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">12.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Once any Free Trial or Beta Trial period or subscription period is over, we will automatically renew your subscription and bill you as instructed at registration (either monthly or annually) using the credit or debit card details provided to us, until the agreement is terminated.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">12.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We shall be under no obligation to provide the Services if the subscription fee (including VAT or any other applicable sales tax) is not paid to us on time. You must ensure that we have complete and accurate billing and contact information throughout the subscription period, including your full name (either personal or business as appropriate) your address, and a billing contact email address. If subscription fees become overdue, because for example your credit card has expired, we reserve the right to suspend your access to 3Y App until the balance is paid and we may terminate this agreement permanently.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">12.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">At any time before expiry of your subscription, you may use the “My Account” tab on 3Y App to access your payment information in order to edit it and also cancel renewal should you so wish.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">12.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">On renewal the subscription terms set out in this agreement apply.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">12.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The subscription is non-refundable and non-transferable.</span>
                </p>

            </div>

            <li style="font-weight:bold;">Security of your credit/debit card We take care to make 3Y App safe for you to use.</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">13.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Card payments are not processed through pages controlled by us. We use one or more online payment service providers who will encrypt your card details in a secure environment.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">13.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you have asked us to remember your credit/debit card details in readiness for your next purchase or subscription, we will securely store your payment details. These details will be fully encrypted and only used to process your automatic monthly/yearly payments or other transactions which you have initiated.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Restrictions on Use of 3Y App You agree that you will not:</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">use 3Y App for any purpose or in any way except as you selected and paid when you bought it and as provided in this agreement, separate the component parts of the Software for use on more than one Device.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">reverse engineer, decompile, or disassemble the Software.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">sub-license, lease, or lend the Software or 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Copy or Publish 3Y App except as specifically allowed in this agreement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">represent or give the impression that you are the owner or originator of 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">remove any identification or reference number or other information which may be embedded in any file of 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">14.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">allow any other person to use 3Y App except in the situation or context for which you have bought it.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Copying 3Y App</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">15.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You may install and use one copy of the Software on a single Device.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">15.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You must not install the Software on more than two Devices and you must not use the Software on more than one Device at a time. The Software must not be used simultaneously on your home Device and on your office Device.</span>
                </p>
            </div>

            <li style="font-weight:bold;">How we handle your Content</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Our privacy policy is clear and precise.You are required to read and “Accept” our privacy policy which is part of Hit The Theatre registration process before being granted a Licence.You will be taken to have accepted that policy when you accept these terms and conditions.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you Post Content to any public area of 3Y App or Our Website it becomes available in the public domain. We have no control over who sees it or what anyone does with it.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Even if access to your text is behind a user registration it remains effectively in the public domain because someone has only to register and log in, to access it. You should therefore avoid Posting unnecessary confidential information which is not relevant to the use of 3Y App or its Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Posting Content of any sort does not change your ownership of the copyright in it.We have no claim over it and we will not protect your rights for you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You understand that you are personally responsible for your breach of someone else’s intellectual property rights, defamation, or any law, which may occur as a result of any Content having been Posted by you;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You accept all risk and responsibility for determining whether anyContent is in the public domain and not confidential.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">16.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Please notify us of any security breach or unauthorised use of your account.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Restrictions on what you may Post to 3Y APP
                <p class="p-heading">We invite you to Post Content to 3Y App in several ways and for different purposes as part of the Service
                    we provide. We have to regulate your use of 3Y App to protect our business and our staff, to protect
                    other users of 3Y App and to comply with the law. These provisions apply to all users of 3Y App.</p>
                <p class="p-heading">We do not undertake to moderate or check every item Posted, but we do protect our business vigorously. If
                    we believe Content Posted breaches the law, we shall co-operate fully with the law enforcement authorities
                    in whatever way we can.</p>
                <p class="p-heading">You agree that you will not use or allow anyone else to use 3Y App to Post Content or undertake any activity
                    which is or may:</p>
            </li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We invite you to Post Content to 3Y App in several ways and for different purposes as part of the Service we provide. We have to regulate your use of 3Y App to protect our business and our staff, to protect other users of 3Y App and to comply with the law. These provisions apply to all users of 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">be unlawful, or tend to incite another person to commit a crime;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">consist in commercial audio, video or music files;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">be obscene, offensive, threatening, violent, malicious or defamatory;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">be sexually explicit or pornographic;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">be likely to deceive any person or be used to impersonate any person, or to misrepresent your identity, age or affiliation with any person;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">request or collect passwords or other personal information from another user without his permission, nor Post any unnecessary personal information about yourself;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">include anything other than words (i.e. you will not include any symbols or photographs) except for legitimate purposes intended as part of the App Services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">facilitate the provision of unauthorised copies of another person's copyright work;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.9</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">link to any of the material specified in this paragraph;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.10</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Post excessive or repeated off-topic messages to any forum or group;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">17.11</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">sending age-inappropriate communications or Content to anyone under the age of 18.</span>
                </p>

            </div>

            <li style="font-weight:bold;">Your Posting: restricted content
                <p class="p-heading">In connection with the restrictions set out below, we may refuse or edit or remove a Posting which does not
                    comply with these terms.</p>
                <p class="p-heading">In addition to the restrictions set out above, a Posting must not contain:</p>
            </li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">18.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">hyperlinks, other than those specifically authorized by us;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">18.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">keywords or words repeated, which are irrelevant to the Content Posted.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">18.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">the name, logo or trademark of any organisation other than that of yours or your client.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">18.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">inaccurate, false, or misleading information;</span>
                </p>
            </div>

            <li style="font-weight:bold;">Removal of offensive Content</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">19.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">For the avoidance of doubt, this paragraph is addressed to any person who uses 3Y App or Our Website for any purpose.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">19.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We are under no obligation to monitor or record the activity of any user or visitor for any purpose, nor do we assume any responsibility to monitor or police Internet-related activities. However, we may do so without notice to you and without giving you a reason.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">19.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you are offended by any Content, the following procedure applies:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.3.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">Your claim or complaint must be submitted via the “Contact Us” link in 3Y App.</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.3.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">we shall remove the offending Content at our discretion as soon as we are reasonably able;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.3.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">after we receive notice of a claim or complaint, we shall investigate so far as we alone decide;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.3.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">we may re-instate the Content about which you have complained or not.</span>
                </p>
                <p style="margin-top:20px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">In respect of any complaint made by you or any person on your behalf, whether using our form of complaint or not, you now irrevocably grant to us a licence to publish the complaint and all ensuing correspondence and communication, without limit.</span>
                </p>
                <p style="margin-top:20px;">
                    <span style="position:absolute; display:inline-block; left:0;">19.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You now agree that if any complaint is made by you frivolously or vexatiously you will repay us the cost of our investigation including legal fees, if any.</span>
                </p>
            </div>


            <li style="font-weight:bold;">Security of 3Y App and Our Website</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">20.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you violate 3Y App or Our Website we shall take legal action against you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">20.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You now agree that you will not, and will not allow any other person to:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">modify, copy, or cause damage or unintended effect to any portion of 3Y App or Our Website, or any software used within it.</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">link to 3Y App or Our Website in any way that would cause the appearance or presentation of 3Y App or Our Website to be different from what would be seen by a user who accessed Our Website by typing the URL into a standard browser;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">download any part3Y App or Our Website, without our express written consent;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">collect or use any product or service listings, descriptions, or prices;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">collect or use any information obtained from or about 3Y App or Our Website or the Content except as intended by this agreement;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">aggregate, copy or duplicate in any manner any of the Content or information available from 3Y App or Our Website, other than as permitted by this agreement or as is reasonably necessary for your use of the Services;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">20.2.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">share with a third party any login credentials to 3Y App or Our Website;</span>
                </p>
            </div>

            <li style="font-weight:bold;">Data and Content</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Title to, and all Intellectual Property rights in, your Content remain your property. However, your access to the Content is contingent on full payment ofHit The Theatre subscription fee when due.You grant us the licence to use, copy, transmit, store, and back-up your information and Content for the purposes of enabling you to access and use the Services and for any other purpose related to provision of services to you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We maintain reasonable procedures for general backup of data and Content for our own purposes but we give no warranty that yourContent will be saved or backed up. You must maintain copies of allContent inputted into the Service. We expressly exclude liability for any loss of data and/or Content no matter how caused.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If third party applications are made available on 3Y App and you enable these applications for use in conjunction with the Services, you acknowledge that we may allow the providers of these third-party applications to access your Content as required for the interoperation of such third-party applications with the Services.We shall not be responsible for any disclosure, modification or deletion of yourContent resulting from any such access by third-party application providers.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree that we may preserve Content where and to the extent required for compliance with any order or notice, whether issued to us or to you, and whether issued by any court, tribunal, regulatory authority, arbitral tribunal or anybody or authority exercising judicial, quasi-judicial, regulatory, or arbitral power or authority, requiring the preservation of any data, information documents, data or Content (a "Preservation Order"). To the extent that a Preservation Order has been issued to you, you acknowledge that you are solely responsible for your compliance with it.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We may, from time to time, set a limit on the number of messages, documents, data and content you may send, store, or receive through the Service. We may delete message, documents, data and content in excess of that limit. We shall give you notice of any change to your limit, except in an emergency.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We assume no responsibility for the deletion or failure to store or deliver email, other messages, data and Content.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You accept that we cannot be liable to you for any such deletion or failure to deliver to you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">21.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We assume no responsibility for the accuracy of data and/or Content entered by you of for any data and or Content which is provided on 3Y App based on data and/or Content submitted by you.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Termination</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">22.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">This agreement terminates on the expiry of your subscription.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">22.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You may also terminate this agreement at any time, for any reason, with immediate effect.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">22.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We may terminate this agreement and to terminate any and all parts of the Services at any time, for any reason, with immediate effect by sending you notice to that effect by email.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">22.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Termination by any party shall have the following effects:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">22.4.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">your right and the right of the Guest User to use the Services immediately ceases;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">22.4.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">access to all your and Guest User data will be terminated.</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">22.4.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">we are under no obligation to forward any unread or unsent messages to you, Guest User or any third party;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">22.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">All monies paid by you to us are non-refundable and cancellation and/or termination of this agreement by you or us at any time for any reason will not entitle you to a refund of monies paid.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Interruption to Services</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">It is intended that use of these Services should be made available through a compatible mobile device, the Internet, and may require Software.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree that you are solely responsible for these requirements, including any applicable changes, updates and fees as well as the terms of your agreement with your mobile device and telecommunications provider.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We makes no warranties or representations of any kind, express, statutory or implied as to:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">23.3.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">the availability of telecommunication services from your provider and access to the services at any time or from any location;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">23.3.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">any loss, damage, or other security intrusion of the telecommunication services; and</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">23.3.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">any disclosure of information to third parties or failure to transmit any data, communications or settings connected with the services.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If it is necessary for us to interrupt the Services, we will give you reasonable notice where this is possible and when we judge the down time is such as to justify telling you.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You acknowledge that the Services may also be interrupted for many reasons beyond our control.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">23.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree that we are not liable to you for any loss, foreseeable or not, arising from any interruption to the Services.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Disclaimers and limitation of liability</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The law differs from one country to another. This paragraph applies so far as the applicable law allows.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We do not represent that 3Y App and its Services, the data and/or the Content within the Services is appropriate or available for use in all jurisdictions or countries.We prohibit accessing content from within countries or states where such content is illegal. You are responsible for compliance with all applicable laws pertaining to your use and access to the Services in your jurisdiction.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">All implied conditions, warranties and terms are excluded from this agreement. If in any jurisdiction an implied condition, warrant or term cannot be excluded, then this sub paragraph shall be deemed to be reduced in effect, only to the extent necessary to release that specific condition, warranty or term.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Your use of 3y App and its Services is entirely at your own risk.3Y App and its Services are provided “as is”. We make no representation or warranty that the 3Y App will be:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.4.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">useful to you;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.4.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">of satisfactory quality;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.4.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">fit for a particular purpose;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.4.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">data-secure;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.4.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">available or accessible, without interruption, or without error.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We claim no expert knowledge in any subject. We disclaim any obligation or liability to you arising directly or indirectly from information you take from 3Y App or Our Website.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We are not an accountant and use of the Services does not constitute the receipt of accounting advice.Furthermore, we do not provide any professional advice e.g. accounting, financial, tax or legal advice or any other professional advice.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">It is your sole responsibility to determine that the Services meet the needs of your business and are suitable for the purposes for which they are used.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You remain solely responsible for complying with all applicable accounting, tax and other laws.It is your responsibility to check that storage of and access to your data via 3Y App and comply with laws applicable to you (including any laws requiring you to retain records).</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.9</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">We make no representation or warranty and accept no responsibility in law for:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">accuracy of any data and Content or the impression or effect it gives;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">delivery of Content, material or any message;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">the conduct, whether online or offline, of any user of Hit The Theatre or its Services or Our Website;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">failure or malfunction of computer hardware or software or technical equipment or system connected directly or indirectly to your use of the Services;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">any act or omission of any person or the identity of any person who introduces himself to you through 3Y App or Our Website;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.9.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">any aspect or characteristic of any services advertised on 3Y App or Our Website;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.10</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">You agree that in any circumstances when we may become liable to you, the limit of our liability is licence fee subscription of the period you have paid for.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.11</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">We have no liability for Trial users, Beta Test users and Guest Users.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.12</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">We shall not be liable to you for any loss or expense which is:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.12.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 50px;">indirect or consequential loss; or</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">24.12.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 50px;">economic loss or other loss of turnover, profits, business or goodwill even if such loss was reasonably foreseeable or we knew you might incur it.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.13</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">This paragraph (and any other paragraph which excludes or restricts our liability) applies to our directors, officers, employees, subcontractors, agents and affiliated companies under the Contracts (Rights of Third Parties) Act 1999.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.14</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">If you become aware of any breach of any term of this agreement by any person, please tell us by via the “Contact Us” link on 3Y App.We welcome your input but do not guarantee to agree with your judgement.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">24.15</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">Nothing in this agreement shall be construed as limiting or excluding our liability for death or personal injury caused by our negligence.s</span>
                </p>
            </div>

            <li style="font-weight:bold;">U.S. Government end users
                <p class="p-heading" style="line-height:1.4;">3Y App is a "commercial item" as that term is defined at 48 C.F.R. 2.101. Consistent with 48 C.F.R. 12.212
                    and 48 C.F.R. 227.7202-1 through 227.7202-4, all U.S. Government end users buy a licence to useHit The Theatre
                    and any related documentation or service with only those rights set out in this Licence.</p>
            </li>

            <li style="font-weight:bold;">You indemnify us
                <p class="p-heading">You agree to indemnify us against all costs, claims and expense arising directly or indirectly from:</p>
            </li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">26.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">your failure to comply with the law of any country;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">26.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">your breach of this agreement;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">26.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">any act, neglect or default by any agent, employee, Guest User or customer of yours;</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">26.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">a contractual claim or breach of the intellectual property rights of any person arising from your use of 3Y App and its Services.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Copyright and other Intellectual Property</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You agree that at all times you will:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.1.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">not cause or permit anything which may damage or endanger our title to 3Y App or other Intellectual Property or the title of any other person whose work has been made available to us as 3Y App;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.1.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">notify us of any suspected infringement of the Intellectual Property.</span>
                </p>

                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">so far as concerns Software provided or made accessible by us to you, you will not:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.2.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">copy, or make any change to any part of its code;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.2.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">use it in any way not anticipated by this agreement;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.2.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">give access to it to any other person than you, the licensee in this agreement;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.2.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">in any way provide any information about it to any other person or generally.</span>
                </p>

                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">not use the Intellectual Property except directly in our interest.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you use 3Y App in a way not allowed by this agreement we may take legal action anywhere in the World. If loss to us or any other person results from your wrongful action, you will be liable to pay.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If we terminate the Licence on account of your breach, you agree that you will:</span>
                </p>

                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.5.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">immediately stop using 3Y App;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.5.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">destroy all copies of 3Y App in your possession or control;</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">27.5.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">destroy any work of yours derived from 3Y App.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">To assure us that you are using 3Y App in accordance with the terms of the Licence, you agree that you will give us copies of your works and materials containing or using 3Y App if we request it. We will give you fair notice of this requirement. You also agree to provide access to relevant pages which have restricted access or are fire-walled.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">27.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If we reasonably believe that you are using 3Y App beyond the scope of this Licence, you agree to provide written confirmation of your compliance, in a form to be drawn by us.</span>
                </p>
            </div>

            <li style="font-weight:bold;">Miscellaneous matters</li>
            <div style="list-style:none;padding-left:15px;">
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">You undertake to provide to us your current land address, e-mail address and telephone as often as they are changed together with all information that we may require to enable us to fulfil our obligations under this contract.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">So far as any time, date or period is mentioned in this agreement, time shall be of the essence.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.3</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If any term or provision of this agreement is at any time held by any jurisdiction to be void, invalid or unenforceable, then it shall be treated as changed or reduced, only to the extent minimally necessary to bring it within the laws of that jurisdiction and to prevent it from being void and it shall be binding in that changed or reduced form. Subject to that, each provision shall be interpreted as severable and shall not in any way affect any other of these terms.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.4</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">The rights and obligations of the parties set out in this agreement shall pass to any permitted successor in title.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.5</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">If you are in breach of any term of this agreement, we may:</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">28.5.1</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">terminate your account and refuse access to 3Y App and its Services</span>
                </p>
                <p style="margin-top:20px;margin-left:30px;">
                    <span style="position:absolute; display:inline-block; left:0;">28.5.2</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">notify us of any suspected infringement of the Intellectual Property.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.6</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Any obligation in this agreement intended to continue to have effect after termination shall so continue.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.7</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">No failure or delay by any party to exercise any right, power or remedy will operate as a waiver of it nor indicate any intention to reduce that or any other right in the future.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.8</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">When you use 3Y App or Our Website or send messages to us by email via the “Contact Us” link, you are communicating with us electronically. We communicate with you by e-mail or by posting notices on 3Y App or Our Website. You agree that all our electronic communications satisfy any legal requirement that such communications be in writing.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.9</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 30px;">Any communication to be served on either party by the other shall be delivered by hand or sent by first class post or recorded delivery or by e-mail.</span>
                </p>
                <p style="margin-top:10px">
                    <span style="position:relative;padding:0px 0 10px 0; display:block; margin:0 0 0 30px;line-height:1.4;">It shall be deemed to have been delivered:</span>
                    <span style="position:relative;padding:0px 0 10px 0; display:block; margin:0 0 0 30px;line-height:1.4;">if delivered by hand: on the day of delivery;</span>
                    <span style="position:relative;padding:0px 0 10px 0; display:block; margin:0 0 0 30px;line-height:1.4;">if sent by post to the correct address: within four working days of posting to an address in the European Union and eight working days to any other address;</span>
                    <span style="position:relative;padding:0px 0 10px 0; display:block; margin:0 0 0 30px;line-height:1.4;">If sent by e-mail to the address from which the receiving party has last sent e-mail: within 24 hours if no notice of non-receipt has been received by the sender.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.10</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">In the event of a dispute between the parties to this agreement, then they undertake to attempt to settle the dispute by engaging in good faith with the other in a process of mediation before commencing arbitration or litigation.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.11</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">This agreement does not give any right to any third party under the UK Contracts (Rights of Third Parties) Act 1999 or otherwise, except that any provision in this agreement which excludes or restricts the liability of our directors, officers, employees, subcontractors, agents and affiliated companies, may be enforced under that Act.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.12</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">In the event of any conflict between any term of this agreement and the provisions of the articles of a limited company or any comparable document intended to regulate any other corporate or collective body, then the terms of this agreement shall prevail.</span>
                </p>
                <p style="margin-top:20px">
                    <span style="position:absolute; display:inline-block; left:0;">28.13</span>
                    <span style="position:relative; display:inline-block; margin:0 0 0 40px;">The validity, construction and performance of this agreement shall be governed by the laws of England and Wales and you agree that any dispute arising from it shall be litigated only in that country.</span>
                </p>
            </div>
        </ol>
    </div>

</body>

</html>