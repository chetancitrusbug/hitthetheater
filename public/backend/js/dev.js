jQuery(document).ready(function ($) {
    statusChange();
});

function statusChange() {
    $('.statuschange').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.popular').checkboxpicker({
        offLabel: 'No',
        onLabel: 'Yes',
    });
    $('.isFuture').checkboxpicker({
        offLabel: 'General',
        onLabel: 'Featured',
    });
    $('.status1').checkboxpicker();

}

function changeImage(id) {
    $('.changeImage' + id).hide();
}


var id;
var url;


$(document).on('click', '.del-item', function (e) {
    e.preventDefault();
    id = $(this).data('id');
    url = $(this).data('url');
    url = url + "/" + id;

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Record!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function (isConfirm) {
        if (isConfirm) {

        $.ajax({
            type: "delete",
            url: url,
            data: {
                from_index: true
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if (data.message != '') {
                    swal("", data.message, "success");
                }
                else
                {
                    swal({
                        title: 'Success!',
                        type: 'success'
                    });
                }
                datatable.draw();
                id = url = null;
            },
            error: function (xhr, status, error) {
                swal("Cancel", "", "error");
            }
        });



        } else {
            swal("Cancel", "", "error");
        }
    });
});

$(document).on('change', '.status-change', function (e) {
    var id = $(this).data('id');
    var status = $(this).data('status');
    var table = $(this).data('table');
    var url = $(this).data('url');

    url = url + "/" + table + '/' + status + '/' + id;

    $.ajax({
        url: url,
        success: function (data) {
             swal({
                 title: 'Success!',
                 type: 'success'
             });
            datatable.draw();
        },
        error: function (xhr, status, error) {
             swal("Cancel", "Action not proceed!", "error");
        }
    });
});


$(document).on('focusout', '.orderbybox', function (e) {
    var id = $(this).data('id');
    var table = $(this).data('table');
    var url = $(this).data('url');
    var valid = $(this).val();

    url = url + "/" + table + '/' + id + '/' + valid;
    $.ajax({
        url: url,
        success: function (data) {
                swal("Success", data.message, "success");
                datatable.draw();
        },
        error: function (xhr, status, error) {
            swal("Cancel", "Action not proceed!", "error");
        }
    });

});


$(document).on('click', '.check_all_today_records', function () {
    if ($(".check_all_today_records").is(':checked')) {
        $('.today_record').prop('checked', true);
    } else {
        $('.today_record').prop('checked', false);
    }
});

$(document).on('click', '.check_all_yesterday_records', function () {
    if ($(".check_all_yesterday_records").is(':checked')) {
        $('.yesterday_record').prop('checked', true);
    } else {
        $('.yesterday_record').prop('checked', false);
    }
});

$(document).on('click', '.allrecords', function (e) {
    $id = this.id;
    if ($('#' + $id).is(':checked')) {
        $('.' + $id).prop('checked', true);
    } else {
        $('.' + $id).prop('checked', false);
    }
});

// $('input[type="radio"]').on('change', function () {
//     if ($(this).is(':checked') == true) {
//         // $(this).prop('checked', false);
//     } else {
//         $(this).prop('checked', true);
//     }
// });
$(document).on('click', '.btn-import ', function (e) {
    $("#loading").show();
});

$(document).ready(function () {
    $("#loading").hide();
});