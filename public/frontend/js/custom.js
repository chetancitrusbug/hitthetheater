/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
    //$('#nav-res').show().fadeIn('slow');
    document.getElementById("nav-res").style.opacity = "100%";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
	document.getElementById("nav-res").style.opacity = "0";
	//$('#nav-res').hide().fadeIn('slow');
}

/* Set the width of the side navigation to 0 */
function closelogout() {
  document.getElementById("mySidenav-logout").style.width = "0";
  document.getElementById("nav-res").style.opacity = "0";
  document.getElementById("mySidenav").style.width = "0";
//$('#nav-res').hide().fadeIn('slow');
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


$(window).scroll(function(){
  if ($(window).scrollTop() >= 300) {
    $('.header-top').addClass('fixed-header');
  }
  else {
    $('.header-top').removeClass('fixed-header');
  }
});

/*$("#video1").prop('muted', true);*/

$('.bxslider').bxSlider({
    minSlides: 3,
    maxSlides: 3,
    slideWidth: 960,
    slideMargin: 10,
    controls: true,
    touchEnabled: false

});

$('[data-fancybox]').fancybox({
    selector: '[data-fancybox="gallery"]',
    loop: true
});


$("a#single_image").fancybox();