$('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 3,
  slideWidth: 960,
  slideMargin: 10,
  controls: true
});

$('[data-fancybox]').fancybox({
  selector : '[data-fancybox="gallery"]',
  loop     : true
});