<?php

namespace App\Http\Controllers\Admin;
use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use App\Location;
use App\LocationLogs;
use App\ImportLogs;
use App\TempLocation;
use App\TempVenue;
use App\TempCategory;
use App\Facilities;
use App\VenueFacilities;
use App\VenueSliders;
use App\VenueTransportInfo;
use App\TempVenueFacilities;
use App\TempVenueSliders;
use App\TempVenueTransportInfo;
use App\Transport;
use App\TempShow;
use App\Show;
use App\Show_Images;
use App\Schedule;
use App\Category;
use App\Venue;
use Session;

class DashboardOldController extends Controller
{
    protected $authorization;
    protected $usernameSetting;
    protected $passwordSetting;
    protected $liveUrlSetting;

    public function __construct(){
        view()->share('route', 'dashboard');
        view()->share('moduleName','Dashboard');
        $this->authorization = Setting::where('key', 'authorization')->first();
        $this->usernameSetting = Setting::where('key', 'username')->first();
        $this->passwordSetting = Setting::where('key', 'password')->first();
        $this->liveUrlSetting = Setting::where('key', 'live_url')->first();
        //view()->share('authorization', $this->authorization);
        ini_set('max_execution_time', 3000);

    }

    public function index()
    {
        $latestLocation = ImportLogs::where('import_type', 'location')->latest()->first();
        $latestCategory = ImportLogs::where('import_type', 'category')->latest()->first();
        $latestVenue = ImportLogs::where('import_type','venue')->latest()->first();
        $latestShow = ImportLogs::where('import_type', 'show')->latest()->first();
        return view('admin.dashboard.index',compact('latestLocation','latestCategory','latestVenue','latestShow'));
    }


    public function getLocation()
    {

        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $URL = $this->liveUrlSetting->value . 'content/location/';

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
              "Authorization:".$authorization
            ),
          ));

        $result = curl_exec($ch);

        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);

        if($result)
        {
            $location_logs = LocationLogs::all();

            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);
            TempLocation::truncate();

            $data = array();
            foreach ($array['location'] as $key => $value) {
                $data[$key]['locationTitle'] = $value['title'];
                $data[$key]['locationCode'] = $value['@attributes']['id'];
            }
            $location = TempLocation::insert($data);

            $tempLocation = TempLocation::all();

            if($tempLocation != null)
            {
                $html = view('admin.dashboard.locationList', compact('tempLocation'))->render();
                $message = 'Listed Successfully';
                return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
            }
            else {
                $message = 'No have Location';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
        }
        else {
                $message = 'Unauthorization';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }

    public function importLocation()
    {
        ImportLogs::create(['import_type' => 'location']);
        Location::truncate();
        $sql = "INSERT INTO locations (locationTitle, locationCode, status)
        SELECT locationTitle, locationCode, status FROM tmp_location;";

        DB::statement($sql);
        Session::flash('flash_success', 'Location added!');
        return redirect('admin/dashboard');

    }

    public function getCategory()
    {

        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');

        $URL = $this->liveUrlSetting->value.'content/location/';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);
        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);

            TempCategory::truncate();
            $data = array();
            foreach ($array['location'] as $key => $value) {
                $data[$key]['locationTitle'] = $value['title'];
                $data[$key]['locationCode'] = $value['@attributes']['id'];
            }
            $location = TempCategory::insert($data);

            $tempCategory = TempCategory::all();

            if($tempCategory != null)
            {
                $html = view('admin.dashboard.categoryList', compact('tempCategory'))->render();
                $message = 'Listed Successfully';
                return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
            }
            else {
                $message = 'No have Location';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
        }
        else {
                $message = 'Unauthorization';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }

    public function importCategory()
    {
        ImportLogs::create(['import_type' => 'location']);
        Location::truncate();
        $sql = "INSERT INTO locations (locationTitle, locationCode, status)
        SELECT locationTitle, locationCode, status FROM tmp_location;";
        DB::statement($sql);
        Session::flash('flash_success', 'Location added!');
        return redirect('admin/dashboard');

    }

    public function getVenue()
    {
        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $URL = $this->liveUrlSetting->value . 'content/venue/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);

        if($result)
        {
            $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($locationxml), true);

            TempVenue::truncate();
            TempVenueFacilities::truncate();
            TempVenueSliders::truncate();
            TempVenueTransportInfo::truncate();
            $data = array();

            foreach ($array['venue'] as $key => $value) {
                // if($key > 0 && $key < 100)
                // {
                    //dd($value['location']['@attributes']['id']);
                    $location = Location::where('locationCode',$value['location']['@attributes']['id'])->first();
                    $link = $value['@attributes']['href'];
                    $link_array = explode('/', $link);
                    $venueCode = end($link_array);
                    if($location != null)
                    {
                        $data[$key]['venueTitle'] = $value['title'];
                        $data[$key]['venueCode'] = $venueCode;
                        $data[$key]['locationId'] = $location->id;
                    }
               // }
            }

            $venue = TempVenue::insert($data);

            $tempVenue = TempVenue::with('location')->get();
            $flag = 0;
            if($tempVenue != null)
            {
                $html = view('admin.dashboard.venueList', compact('tempVenue','flag'))->render();
                $message = 'Listed Successfully';
                return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
            }
            else {
                $message = 'No have Venue';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
        }
        else {
                $message = 'Unauthorization';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }
    public function getVenueDetails()
    {
        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');


        TempVenueFacilities::truncate();
        TempVenueSliders::truncate();
        TempVenueTransportInfo::truncate();

        $tempVenue = TempVenue::with('location')->get();

        foreach ($tempVenue as $key => $value) {
            $URL = $this->liveUrlSetting->value . 'content/venue/';
            $URL .= $value->location->locationCode.'/'.$value->venueCode;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            $result = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
            curl_close($ch);

            if($result)
            {
                $locationxml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
                $resValue = json_decode(json_encode($locationxml), true);

                $data['venueDescription'] = isset($resValue['description']) ? $resValue['description'] : '';
                $data['addressLine1'] = isset($resValue['address']['line1']) ? $resValue['address']['line1'] : '';
                $data['addressLine2'] = isset($resValue['address']['line2']) ? $resValue['address']['line2'] : '';
                $data['postcode'] = isset($resValue['address']['postcode']) ? $resValue['address']['postcode'] : '';
                $data['latitude'] = isset($resValue['address']['latitude']) ? $resValue['address']['latitude'] : '';
                $data['longitude'] =  isset($resValue['address']['longitude']) ? $resValue['address']['longitude']: '';


                $venue = TempVenue::where('id',$value->id)->first();
                if ($venue) {
                    $venue->update($data);

                    if(isset($resValue['facilities']) && isset($resValue['facilities']['facility']) && is_array($resValue['facilities']['facility']))
                    {
                        foreach ($resValue['facilities']['facility'] as $facilityKey => $facilityValue) {
                            $facility = Facilities::where('facilitieTitle',$facilityValue)->first();
                            if($facility == null)
                            {
                                $dataFacility['facilitieTitle'] = $facilityValue;
                                $facility = Facilities::create($dataFacility);
                            }
                            TempVenueFacilities::create(['facilitieId'=>$facility->id,'venueId'=>$value->id]);
                        }

                    }
                    if (isset($resValue['resources']['resource'])) {
                        foreach ($resValue['resources']['resource'] as $resourceKey => $resourceValue) {
                            $dataResource['venueId'] = $value->id;
                            $dataResource['image'] = ((isset($resourceValue['uri']) && $resourceValue['uri'] != null ) ? $resourceValue['uri'] : '');
                            $dataResource['type'] = ((isset($resourceValue['type']) && $resourceValue['type'] != null ) ? $resourceValue['type'] : '');
                            $dataResource['title'] = ((isset($resourceValue['title']) && $resourceValue['title'] != null ) ? $resourceValue['title'] : '');
                            $dataResource['width'] = ((isset($resourceValue['width']) && $resourceValue['width'] != null ) ? $resourceValue['width'] : '');
                            $dataResource['height'] = ((isset($resourceValue['height']) && $resourceValue['height'] != null ) ? $resourceValue['height'] : '');
                            TempVenueSliders::create($dataResource);
                        }

                    }
                    if (isset($resValue['transportInfo'])) {
                       foreach ($resValue['transportInfo'] as $transportInfoKey => $transportInfoValue) {
                            $transport = Transport::where('transportTitle', $transportInfoKey)->first();
                            if (
                                $transport == null) {
                                $dataTransport['transportTitle'] = $transportInfoValue;
                                $dataTransport['transportKey'] = $transportInfoKey;
                                $transport = Transport::create($dataTransport);
                            }
                            TempVenueTransportInfo::create(['transportId' => $transport->id, 'venueId' => $value->id,'transportValue'=> $transportInfoValue]);
                        }
                    }
                }
            }
        }
        $tempVenue = TempVenue::with('location')->get();

        if($tempVenue != null)
        {
            $flag = 1;
            $html = view('admin.dashboard.venueList', compact('tempVenue','flag'))->render();
            $message = 'Listed Successfully';
            return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
        }
        else {
            $message = 'No have Venues';
            return response()->json(['message' => $message, 'code' => 400], 200);
        }
    }
    public function importVenues()
    {
        ImportLogs::create(['import_type' => 'venue']);

        Venue::truncate();
        VenueFacilities::truncate();
        VenueSliders::truncate();
        VenueTransportInfo::truncate();

        $sql = "INSERT INTO venues (locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude , venueCode, addressLine1 , addressLine2, status) SELECT locationId, categoryId, venueTitle, venueDescription, postcode, latitude, longitude , venueCode, addressLine1 , addressLine2, status FROM tmp_venue;";
        DB::statement($sql);
        $sql = "INSERT INTO venue_facilities (venueId, facilitieId)
        SELECT venueId, facilitieId FROM tmp_venue_facilities;";
        DB::statement($sql);
        $sql = "INSERT INTO venue_sliders (venueId, image, type , title ,width ,height)
        SELECT venueId, image, type , title ,width ,height FROM tmp_venue_sliders;";
        DB::statement($sql);
        $sql = "INSERT INTO venue_transport_info (venueId, transportId	, transportValue )
        SELECT venueId, transportId	, transportValue  FROM tmp_venue_transport_info;";
        DB::statement($sql);
        Session::flash('flash_success', 'Location added!');
        return redirect('admin/dashboard');

    }
    public function getShow()
    {
        $username = (isset($this->usernameSetting->value) ? $this->usernameSetting->value : '');
        $password = (isset($this->passwordSetting->value) ? $this->passwordSetting->value : '');
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $URL = $this->liveUrlSetting->value . 'content/show/';
        $ch = curl_init();

        curl_setopt_array($ch, array(
        CURLOPT_URL => $URL,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $result = curl_exec($ch);
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
        curl_close($ch);
        if($result)
        {
            $xml = simplexml_load_string($result,'SimpleXMLElement', LIBXML_NOCDATA);
            $array = json_decode(json_encode($xml), true);

            TempShow::truncate();
            $data = array();

            foreach ($array['show'] as $key => $value) {
                // if($key > 0 && $key < 50)
                // {
                        $link = $value['@attributes']['href'];
                        $link_rating=$link.'/quote';
                        $link_array = explode('/', $link, 9);
                        $showLink = end($link_array);

                        $data[$key]['showTitle'] = $value['name'];
                        $data[$key]['showLink'] = $showLink;

                        $show_details=$this->show_details($link);
                        $show_rating=$this->show_rating($link_rating);
                        $show_details_xml = simplexml_load_string($show_details,'SimpleXMLElement',LIBXML_NOCDATA);

                        $show_details_array = json_decode(json_encode($show_details_xml), true);

                        if(isset($show_rating) && $show_rating != null)
                        {
                            $data[$key]['avg_rating']=$show_rating;
                        }
                        else
                        {
                            $data[$key]['avg_rating'] = "";
                        }

                        if(isset($show_details_array['indexName']) && $show_details_array['indexName'] != null)
                        {
                            $data[$key]['indexName']=$show_details_array['indexName'];
                        }
                        else
                        {
                            $data[$key]['indexName'] = "";
                        }


                        if(isset($show_details_array['isEvent']) && $show_details_array['isEvent'] != null)
                        {
                            $data[$key]['isEvent']=$show_details_array['isEvent'];
                        }
                        else
                        {
                            $data[$key]['isEvent'] = "";
                        }

                        if(isset($show_details_array['priceFrom']) && $show_details_array['priceFrom'] != null)
                        {
                            $data[$key]['priceFrom']=$show_details_array['priceFrom'];
                        }
                        else
                        {
                            $data[$key]['priceFrom'] = "";
                        }

                        if(isset($show_details_array['priceTo']) && $show_details_array['priceTo'] != null)
                        {
                            $data[$key]['priceTo']=$show_details_array['priceTo'];
                        }
                        else
                        {
                            $data[$key]['priceTo'] = "";
                        }

                        if(isset($show_details_array['onSaleDate']) && $show_details_array['onSaleDate'] != null)
                        {
                            $data[$key]['onSaleDate']=$show_details_array['onSaleDate'];
                        }
                        else
                        {
                            $data[$key]['onSaleDate'] = "";
                        }

                        if(isset($show_details_array['runningTime']) && $show_details_array['runningTime'] != null)
                        {
                            $data[$key]['runningTime']=$show_details_array['runningTime'];
                        }
                        else
                        {
                            $data[$key]['runningTime'] = "";
                        }

                        if(isset($show_details_array['ageRestriction']) && $show_details_array['ageRestriction'] != null)
                        {
                            $data[$key]['ageRestriction']=$show_details_array['ageRestriction'];
                        }
                        else
                        {
                            $data[$key]['ageRestriction'] = "";
                        }

                        if(isset($show_details_array['limitedStock']) && $show_details_array['limitedStock'] != null)
                        {
                            $data[$key]['limitedStock']=$show_details_array['limitedStock'];
                        }
                        else
                        {
                            $data[$key]['limitedStock'] = "";
                        }

                        if(isset($show_details_array['whitelabelURI']) && $show_details_array['whitelabelURI'] != null)
                        {
                            $data[$key]['whitelabelURI']=$show_details_array['whitelabelURI'];
                        }
                        else
                        {
                            $data[$key]['whitelabelURI'] = "";
                        }

                        if(isset($show_details_array['importantInfo']) && $show_details_array['importantInfo'] != null)
                        {

                            $data[$key]['importantInfo']=$show_details_array['importantInfo'];
                        }
                        else
                        {
                            $data[$key]['importantInfo'] = "";
                        }

                        if(isset($show_details_array['summary']) && $show_details_array['summary'] != null)
                        {

                            $data[$key]['summary']=$show_details_array['summary'];
                        }
                        else
                        {
                            $data[$key]['summary'] = "";
                        }

                        if(isset($show_details_array['description']) && $show_details_array['description'] != null)
                        {

                            $data[$key]['description']=$show_details_array['description'];
                        }
                        else
                        {
                            $data[$key]['description'] = "";
                        }

                        if(isset($show_details_array['showType']) && $show_details_array['showType'] != null)
                        {

                            $data[$key]['showType']=$show_details_array['showType'];
                        }
                        else
                        {
                            $data[$key]['showType'] = "";
                        }

                        if(isset($show_details_array['bannerBackgroundColour']) && $show_details_array['bannerBackgroundColour'] != null)
                        {
                            $data[$key]['bannerBackgroundColour']=$show_details_array['bannerBackgroundColour'];
                        }
                        else
                        {
                            $data[$key]['bannerBackgroundColour'] = "";
                        }

                        if(isset($value['venues']['venue']['@attributes']['href']) && $value['venues']['venue']['@attributes']['href'] != null)
                        {
                            $link = $value['venues']['venue']['@attributes']['href'];
                            $link_array = explode('/', $link);
                            $venueCode = end($link_array);
                            $data[$key]['venueCode'] = $venueCode;
                        }
                        else
                        {
                            $data[$key]['venueCode'] = "";
                        }

                        if(isset($value['venues']['venue']['bookingStarts']) && $value['venues']['venue']['bookingStarts'] != null)
                        {
                            $data[$key]['bookingStarts'] = $value['venues']['venue']['bookingStarts'];
                        }
                        else
                        {
                            $data[$key]['bookingStarts'] = "";
                        }
                        if(isset($value['venues']['venue']['bookingEnds']) && $value['venues']['venue']['bookingEnds'] != null)
                        {
                            $data[$key]['bookingEnds'] = $value['venues']['venue']['bookingEnds'];
                        }
                        else
                        {
                            $data[$key]['bookingEnds'] = "";
                        }

                        if(isset($show_details_array['resources']['resource']))
                        {
                            $data[$key]['resource_image'] = json_encode($show_details_array['resources']['resource']);
                        }
                        else
                        {
                            $data[$key]['resource_image'] = "";
                        }

                        if(isset($show_details_array['categories']['category']))
                        {
                            $data[$key]['categories'] = json_encode($show_details_array['categories']['category']);
                        }
                        else
                        {
                            $data[$key]['categories'] = "";
                        }

                // }

            }

            $show = TempShow::insert($data);
            $tempShow = TempShow::all();

            if($tempShow != null)
            {
                $html = view('admin.dashboard.showList', compact('tempShow'))->render();
                $message = 'Listed Successfully';
                return response()->json(['message' => $message, 'code' => 200, 'html' => $html], 200);
            }
            else {
                $message = 'No have Show';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
        }
        else {
                $message = 'Unauthorization';
                return response()->json(['message' => $message, 'code' => 400], 200);
            }
    }

    public function importShow()
    {
        ImportLogs::create(['import_type' => 'show']);
        Show::truncate();
        Show_Images::truncate();
        Schedule::truncate();
        $tempShow = TempShow::all();
        $show=array();
        if($tempShow != null)
        {
            foreach($tempShow as $key=>$value)
            {
                $show['showTitle']=$value->showTitle;
                $show['venueCode']=$value->venueCode;
                $show['indexName']=$value->indexName;
                $show['showLink']=$value->showLink;
                $show['bookingStarts']=$value->bookingStarts;
                $show['bookingEnds']=$value->bookingEnds;
                $show['isEvent']=$value->isEvent;
                $show['summary']=$value->summary;
                $show['description']=$value->description;
                $show['priceFrom']=$value->priceFrom;
                $show['priceTo']=$value->priceTo;
                $show['onSaleDate']=$value->onSaleDate;
                $show['runningTime']=$value->runningTime;
                $show['ageRestriction']=$value->ageRestriction;
                $show['limitedStock']=$value->limitedStock;
                $show['whitelabelURI']=$value->whitelabelURI;
                $show['importantInfo']=$value->importantInfo;
                $show['bannerBackgroundColour']=$value->bannerBackgroundColour;
                $show['showType']=$value->showType;

                $show_data=Show::insert($show);

                if($value->resource_image != NULL)
                {


                    $image_data=json_decode($value->resource_image,true);

                    foreach($image_data as $key=>$value_data)
                    {
                        if(count($value_data) > 0){
                            $image['show_id']=$value->id;
                            $image['url']=$value_data['@attributes']['uri'];
                            $image['type']=$value_data['@attributes']['type'];
                            $image['title']=$value_data['@attributes']['title'];
                            $image['width']=$value_data['@attributes']['width'];
                            $image['height']=$value_data['@attributes']['height'];
                            $show_data=Show_Images::insert($image);
                        }
                    }
                }

                $schedule_details = $this->schedule_details($value->showLink);
                $schedule_details_xml = simplexml_load_string($schedule_details, 'SimpleXMLElement', LIBXML_NOCDATA);
                $schedule_details_array = json_decode(json_encode($schedule_details_xml), true);

                if($schedule_details_array != NULL)
                {
                    foreach($schedule_details_array['schedule']['day'] as $key=>$value_data)
                    {
                        $str = $value_data['@attributes']['time'];
                        if (($timestamp = strtotime($str)) != false) {
                            $schedule['show_id']=$value->id;
                            $schedule['day_id']=$value_data['@attributes']['id'];
                            $schedule['time']=date('H:i:s', $timestamp);
                            $show_data = Schedule::insert($schedule);
                        }
                    }
                }
                if($value->categories != NULL)
                {
                    $categories_data=json_decode($value->categories,true);
                    if($categories_data != null && is_array($categories_data))
                    {
                        for($i=0;$i<count($categories_data);$i++)
                    {
                        $category=Category::where('categoryTitle',$categories_data[$i])->first();

                        if($category == null)
                        {
                            $category_data=new Category();
                            $category_data->categoryTitle=$categories_data[$i];
                            $category_data->save();

                            $category_data->show()->attach($value->id);
                        }
                        else
                        {

                            $category->show()->attach($value->id);
                        }

                    }
                    }

                }
            }
        }

        Session::flash('flash_success', 'Show added!');
        return redirect('admin/dashboard');

    }

    public function schedule_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;
        $URL = $this->liveUrlSetting->value . 'content/show/'.$showlink."/schedule";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);
        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }

    }

    private function show_details($showlink)
    {
        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    private function show_rating($showlink)
    {

        $authorization = (isset($this->authorization->value) ? $this->authorization->value : '');

        $header['Authorization'] = $authorization;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $showlink,
        //CURLOPT_URL => 'https://api.entstix.com/api/v1/xlive/content/show/LDN/1014/quote',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "",
        CURLOPT_HTTPHEADER => array(
            "Authorization:".$authorization
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else
        {

            $show_rating_xml = simplexml_load_string($response);
            $show_rating_array = json_decode(json_encode($show_rating_xml), true);
            if(is_array($show_rating_array['quotes']))
            {
                $show_rating=$show_rating_array['quotes']['quote'];
                $rating=0;

                foreach($show_rating as $value)
                {

                    if(isset($value['starRating']) && is_integer($value['starRating']))
                    {
                        $rating=$rating+$value['starRating'];
                    }
                }

                $avg_rating = $rating / count($show_rating);

            }
            else
            {
                return 0;
            }

        }
    }

}
